import CryptoJS from 'crypto-js';
import Hashids from 'hashids';

export default angular.module('app.common.services', [])

  .service('userService', function (API, $http, $httpParamSerializerJQLike, localStorageService, $q, $analytics,
    growl, GoogleAnalyticsService, $auth, gettextCatalog, $window, IntegrationService,
    analyticsService, $rootScope, $httpParamSerializer) {

    this.get = function () {
      return $http({
        method: 'GET',
        url: API + 'users.json'
      }).then(function (res) {
        return res.data;
      });
    };

    this.submitContact = function (contactInfo) {
      return $http({
        method: 'POST',
        url: API + 'users/contact.json',
        headers: {
          'Content-Type': 'application/json'
        },
        data: contactInfo
      });
    };

    this.login = function (email, password, gaClientId, trackMixpanel) {
      var def = $q.defer();

      $http({
        method: 'POST',
        url: API + 'users/login.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $httpParamSerializerJQLike({
          "data": {
            "User": {
              "username": email,
              "password": password,
              "ga_client_id": gaClientId
            }
          }
        })
      })
        .success(function (res) {
          if (trackMixpanel) {
            IntegrationService.mixpanelTrackLogin(res.user);
          }
          localStorageService.set('user', res.user);
          localStorageService.set('token', res.token);

          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.pay = function (coupons, competitionFeed, seasonId, teamIds, redirectURL, locale) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'payments/create',
        headers: {
          'Content-Type': 'application/json'
        },
        data: { teamIds, coupons, competitionFeed, seasonId, redirectURL, locale }
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.cancelPayment = function (paymentId) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'payments/cancel',
        headers: {
          'Content-Type': 'application/json'
        },
        data: { paymentId },
        ignoreLoadingBar: false
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.paymentStatus = function (paymentId) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'payments/status',
        headers: {
          'Content-Type': 'application/json'
        },
        data: { paymentId },
        ignoreLoadingBar: false
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.payments = function () {
      return $http({
        method: 'GET',
        url: API + 'payments'
      })
        .then(function (res) {
          return (res.data && res.data.payments) || [];
        });
    };

    this.ogone = function (competitionFeed, callbackParams) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'users/ogone.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $httpParamSerializerJQLike({
          "data": {
            "competition_feed": competitionFeed,
            "callback_params": callbackParams,
          }
        })
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.hln = function (code) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'users/hln.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $httpParamSerializerJQLike({
          "data": {
            "code": code,
          }
        })
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.vrt = function (code, state, redirectURI, ua) {
      const payload = $httpParamSerializerJQLike({
        "data": {
          "code": code,
          "state": state,
          "redirectUri": redirectURI,
          "UA": ua,
        }
      });
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'users/vrt.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: payload
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.vrtState = function (userAgent) {
      const payload = $httpParamSerializerJQLike({
        "data": {
          "UA": userAgent,
        }
      });
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'users/state.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: payload
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.validateCoupon = function (coupon, competitionFeed) {
      var def = $q.defer();
      $http({
        method: 'POST',
        url: API + 'payments/coupon',
        data: {
          coupon: coupon,
          competitionFeed: competitionFeed
        }
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.changeAvatar = function (userId, image) {
      var fd = new FormData();
      fd.append('image', image);

      return $http.post(API + 'users/avatar/' + userId + '.json', fd, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
      })
        .then(function (res) {
          return res.data;
        }, function () {
          growl.addErrorMessage('Could not save the avatar.');
          return $q.reject();
        });
    };

    this.edit = function (id, user) {
      return $http({
        method: 'PUT',
        url: API + 'users/' + id + '.json',
        data: user
      }).then(function (res) {
        return res.data;
      }, function (err) {
        growl.addErrorMessage(gettextCatalog.getString('Error editing profile'));
        return $q.reject();
      });
    };

    this.getBornDateFilters = function () {
      return {
        days: new Array(31)
          .fill()
          .map((item, index) => ({ id: index + 1, label: index + 1 })),
        months: new Array(12)
          .fill()
          .map((item, index) => ({ id: index + 1, label: index + 1 })),
        years: new Array(101)
          .fill()
          .map((item, index) => ({ id: index + 1918, label: index + 1918 }))
          .sort((a, b) => a.id > b.id ? -1 : (a.id < b.id ? 1 : 0)),
      };
    };

    this.register = function (dob, first, last, email, password, referral, timezone, coolblue, street, aptNumber,
      post_code, city, language, domain, clientId, optin) {
      var data = {
        "data": {
          "User": {
            "first_name": first,
            "last_name": last,
            "username": email,
            "password": password,
            "referral": referral,
            "timezone": timezone,
            "coolblue": coolblue,
            "street": street,
            "post_code": post_code,
            "city": city,
            "ga_client_id": clientId,
            "apt_number": aptNumber,
            "dob": dob,
            "language": language,
            "domain_url": domain,
            "optin": optin
          }
        }
      };

      return $http({
        method: 'POST',
        url: API + 'users/register.json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $httpParamSerializerJQLike(data)
      })
        .then(function (res) {
          localStorageService.set('user', res.data.user);
          localStorageService.set('token', res.data.token);
          return res.data;
        }, function (err) {
          growl.addErrorMessage(err.data.message);
          return $q.reject();
        });
    };

    this.forgotPassword = function (username, domain) {
      return $http({
        method: 'POST',
        url: API + 'users/forgot.json',
        data: {
          "domain": domain,
          "User": {
            "username": username
          }
        }
      });
    };

    this.resetPassword = function (password, code) {
      return $http({
        method: 'POST',
        url: API + 'users/reset.json',
        data: {
          "User": {
            "newpassword": password,
            "code": code
          }
        }
      }).then(function (res) {
        return res.data;
      }, function (err) {
        growl.addErrorMessage(err.data.message);
        return $q.reject();
      });
    };

    this.confirmAccount = function (code) {
      return $http({
        method: 'POST',
        url: API + 'users/confirm.json',
        data: {
          code: code
        }
      })
        .then(function (res) {
          return res.data;
        }, function (err) {
          growl.addErrorMessage(err.data.message);
          return $q.reject();
        });
    };

    this.getResetCode = function (code) {
      return $http({
        method: 'POST',
        url: API + 'users/code.json',
        data: {
          "code": code
        }
      }).then(function (res) {
        return res.data;
      }, function (err) {
        growl.addErrorMessage(err.data.message);
        return $q.reject();
      });
    };

    this.bootIntercom = function (user) {
      if (user.username && user.username.indexOf('sneakersgames.com') == -1) {
        var hash = CryptoJS.HmacSHA256(user.id.toString(), "jGouFwJyKHPS7eOMsmNUlhaeS0Y_cdj37_rm6pyK");
        var hashInHex = CryptoJS.enc.Hex.stringify(hash);

        if (window.Intercom) {
          window.Intercom('boot', {
            app_id: 'g5jampdn',
            name: user.first_name + " " + user.last_name,
            email: user.username,
            user_id: user.id,
            user_hash: hashInHex,
            created_at: user.created,
            custom_launcher_selector: '#intercomLauncher',
            hide_default_launcher: true
          });
        }
      }
    };

    this.bootCrisp = function (user) {
      if (user && user.username && user.username.indexOf('sneakersgames.com') === -1) {
        var hash = CryptoJS.HmacSHA256(user.id.toString(), "jGouFwJyKHPS7eOMsmNUlhaeS0Y_cdj37_rm6pyK");
        var hashInHex = CryptoJS.enc.Hex.stringify(hash);

        if (window.$crisp) {
          window.$crisp.push(["set", "user:email", [user.username]])
          window.$crisp.push(["set", "user:nickname", [user.first_name + " " + user.last_name]])
        }
      }
    };

    this.fbLogin = function (statesPrefix, disableRedirect, trackMixPanel) {
      return $auth.authenticate('facebook', {
        referral: localStorageService.get('shareReferral'),
        timezone: jstz.determine().name()
      })
        .then(function (res) {
          localStorageService.remove('shareReferral');
          if (localStorage && localStorage.removeItem) {
            localStorage.removeItem('satellizer_token')
          }
          const user = res.data.user.User ? res.data.user.User : res.data.user;

          if (res.data.user.User) {
            analyticsService.addToDataLayer({
              event: 'account_created',
              authentication_type: 'facebook',
              user_id: user.id
            });
          }

          if (user.facebook_id !== null && trackMixPanel) {
            IntegrationService.mixpanelTrackLogin(user);
          }

          localStorageService.set('user', user);
          localStorageService.set('fbl', true);
          localStorageService.set('token', res.data.token);

          let params = '';
          if (Object.keys($rootScope.originalQueryParams).length) {
            params = `?${$httpParamSerializer($rootScope.originalQueryParams)}`
          }

          if (!disableRedirect) {
            if (localStorageService.get('shareLeague') !== null) {
              if (statesPrefix) {
                $window.location.href = `${statesPrefix}/new-game${params}`;
              } else {
                $window.location.href = `new-game${params}`;
              }
            }
            else {
              if (statesPrefix) {
                $window.location.href = `${statesPrefix}/dashboard${params}`;
              } else {
                $window.location.href = `home${params}`;
              }
            }
          }

        })
        .catch(function (res) {
          console.log(res);
          growl.addErrorMessage(gettextCatalog.getString('Could not connect with facebook'));
        });
    }
  })

  .service('gameService', function (API, $http, $q, growl) {

    this.getCompetitions = function () {
      return [
        {
          id: 1,
          name: 'Pro League',
          competition_feed: 'BE1'
        },
        {
          id: 2,
          name: 'Eredivisie',
          competition_feed: 'NL1'
        },
        {
          id: 3,
          name: 'Premier League',
          competition_feed: 'GB1'
        },
        {
          id: 4,
          name: 'Ligue 1',
          competition_feed: 'FR1'
        },
        {
          id: 5,
          name: 'Bundesliga',
          competition_feed: 'L1'
        },
        {
          id: 6,
          name: 'La Liga',
          competition_feed: 'ES1'
        },
        {
          id: 7,
          name: 'Serie A',
          competition_feed: 'IT1'
        }
        //,
        // {
        // 	id: 8,
        // 	name: 'Giro',
        // 	competition_feed: 'CL'
        // }
      ];
    };

    this.getCyclingCompetitions = function () {
      return [
        {
          id: 12,
          name: 'Cyclocross',
          competition_feed: 'CROS'
        }
      ];
    };

    this.getGameModes = function () {
      return [
        {
          id: 1,
          key: "public",
          name: "Play Sports competition",
          icon: "icon-logo-telenet",
          className: 'game-playsports',
          description: "Bouw een heel seizoen aan je carrière, promoveer naar eerste klasse en win gesponsorde prijzen."
        },
        {
          id: 2,
          key: "draft",
          name: "Draft Fantasy League",
          icon: "icon-game-draft",
          className: "game-draft",
          description: "Start of neem deel aan een league – publiek of enkel met vrienden – kies je unieke team en ga voor de eer."
        },
        {
          id: 3,
          name: "Daily Fantasy game",
          icon: "icon-game-daily",
          className: "game-daily",
          description: "Kies wekelijks een nieuw team, daag andere spelers uit, verzamel punten en win cash geldprijzen."
        },
        {
          id: 4,
          name: "Budget Fantasy League",
          icon: "icon-game-budget",
          className: "game-budget",
          description: "Stel je eigen dream team samen – de enige limiet is je budget – en strijd mee in de tabellen over een heel seizoen."
        }
      ];
    };

    this.getPublicLeagues = function (competition_feed) {
      var url = API + 'leagues.json';
      if (competition_feed)
        url += '?competition_feed=' + competition_feed;

      return $http({
        method: 'GET',
        url: url
      }).then(function (res) {
        return res.data.Leagues;
      });
    };

    this.joinLeague = function (obj) {
      return $http({
        method: 'POST',
        url: API + 'teams.json',
        data: obj
      }).then(function (res) {
        return res.data;
      }, function (err) {
        return $q.reject(err.data);
      });
    };

    this.createLeague = function (obj) {
      return $http({
        method: 'POST',
        url: API + 'teams/add.json',
        data: obj
      }).then(function (res) {
        return res.data;
      }, function (err) {
        if (err.data.message) {
          growl.addErrorMessage(err.data.message);
        }

        return $q.reject();
      });
    };

    this.getClubs = function (competition_feed, seasonId) {
      let url = `clubs.json?competition_feed=${competition_feed}`;

      if (seasonId) {
        url += `&season_id=${seasonId}`;
      }

      return $http({
        method: 'GET',
        url: API + url
      }).then(function (data) {
        return data.data.Clubs;
      });
    };

    this.getClubsBySeasonId = function (competition_feed, season_id) {
      return $http({
        method: 'GET',
        url: API + 'clubs.json?competition_feed=' + competition_feed + '&season_id=' + season_id
      }).then(function (data) {
        return data.data.Clubs;
      });
    };

    this.getSponsors = function (competition_feed, season_id) {
      return $http({
        method: 'GET',
        url: API + 'clubs.json?competition_feed=' + competition_feed + '&season_id=' + season_id
      })
        .then(function (data) {
          return data.data.Clubs.filter(function (club) {
            return club.Club.level == 1
          });
        });
    };

    this.getSubscribed = function (competition_feed) {
      return $http({
        method: 'GET',
        url: API + 'teams/subscriptions.json?competition_feed=' + competition_feed
      }).then(function (data) {
        return data.data;
      });
    };
  })

  .service('adminService', function (GENERATOR_API, $http, $q, $httpParamSerializerJQLike, $window, base64) {

    this.generateImage = function (data) {
      $window.open(GENERATOR_API + 'templates/generator?data=' + base64.encode(JSON.stringify(data)), '_blank');
    };

    this.generateWeeklyWinnerLineupImage = function (teamId) {
      $window.open(`${GENERATOR_API}/weekly-winner?teamId=${teamId}`, '_blank');
    };
  })

  .service('layoutService', function () {

    var visibleElements = {
      header: true,
      footer: true
    };

    function hideHeader() {
      visibleElements.header = false;
    }

    function hideFooter() {
      visibleElements.footer = false;
    }

    function elementIsVisible(element) {
      return visibleElements[element];
    }

    return {
      hideHeader: hideHeader,
      hideFooter: hideFooter,
      elementIsVisible: elementIsVisible
    }

  })

  .service('teamService', function (API, $http, $q, growl, PlayerService, gettextCatalog) {

    this.getAll = function () {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'teams.json',
        params: {
          _t: Date && Date.now()
        }
      })
        .success(function (res) {
          def.resolve(res.Teams);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.get = function (id, week, isWeekNotPlayed) {
      var def = $q.defer();
      var url = API + 'teams/' + id + '.json';

      if (isWeekNotPlayed && week !== undefined && week !== null) {
        url = url + '?week=' + week;
      }

      if (!isWeekNotPlayed && week !== undefined && week !== null) {
        url = API + 'teams/get.json?id=' + id + '&week=' + week;
      }
      $http({
        method: 'GET',
        url: url
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.changeFootbalCaptain = function (teamId, weekId, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/captain.json',
        data: {
          team_id: teamId,
          captain_id: captainId,
          week_id: weekId,
          competition_feed: competitionFeed,
        },
        ignoreLoadingBar: true
      });
    };

    this.getDisplayWeek = function (team) {
      return ((team.Team.Team.week_id < team.weeks.display_week) &&
        (team.weeks.display_week === team.weeks.deadline_week) &&
        (moment.utc() < moment.utc(team.weeks.deadline_date))) ?
        (team.weeks.display_week - 1) :
        team.weeks.display_week;
    };

    this.aggregateValuesForDuplicatedPlayers = function (starting, captainFactor, captainWithTeamPoints, captainExtraOnWin) {
      let aggregatedPlayerValues = [];
      const teamPlayersWithValues = Object.keys(starting).reduce(
        (acc, item) => {
          return acc.concat(starting[item])
        }, []
      );

      const playerPointsExtra = player => {
        let points = 0;

        if (player && player.selections.captain && player.pts) {

          if (captainFactor) {
            if (captainExtraOnWin) {
              if (player.pts && player.pts.value && player.pts.value.result === 'W') {
                points += player.pts.points * captainFactor;
              } else {
                points += player.pts.points;
              }
            } else {
              points += player.pts.points * captainFactor;
            }
          } else {
            points += player.pts.points;
          }


          if (captainWithTeamPoints && player.pts.value) {
            points += player.pts.value.team_points;
          }
        } else {
          points += player && player.pts ? player.pts.points : null;
        }

        return points;
      };


      teamPlayersWithValues.forEach(teamPlayerWithValues => {
        const currentIterationPlayerId = PlayerService.getPlayerProp(teamPlayerWithValues, 'id');

        const alreadyDefined = aggregatedPlayerValues.find(aggregatedPlayer =>
          currentIterationPlayerId === PlayerService.getPlayerProp(aggregatedPlayer, 'id')
        );

        const getPropOrList = (item, prop) => {
          return item[prop] ? item[prop] : [];
        }

        if (alreadyDefined) {
          aggregatedPlayerValues = aggregatedPlayerValues.map(aggregatedPlayer => {
            if (currentIterationPlayerId === PlayerService.getPlayerProp(aggregatedPlayer, 'id') && aggregatedPlayer.pts) {
              aggregatedPlayer.pts.points = aggregatedPlayer.pts.points + playerPointsExtra(teamPlayerWithValues);
              aggregatedPlayer.pts.value.goals = getPropOrList(aggregatedPlayer.pts.value, 'goals')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'goals'));
              aggregatedPlayer.pts.value.conceeded = getPropOrList(aggregatedPlayer.pts.value, 'conceeded')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'conceeded'));
              aggregatedPlayer.pts.value.assists = getPropOrList(aggregatedPlayer.pts.value, 'assists')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'assists'));
              aggregatedPlayer.pts.value.penalty_goals = getPropOrList(aggregatedPlayer.pts.value, 'penalty_goals')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'penalty_goals'));
              aggregatedPlayer.pts.value.penalty_miss = getPropOrList(aggregatedPlayer.pts.value, 'penalty_miss')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'penalty_miss'));
              aggregatedPlayer.pts.value.own_goals = getPropOrList(aggregatedPlayer.pts.value, 'own_goals')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'own_goals'));
              aggregatedPlayer.pts.value.red_card = getPropOrList(aggregatedPlayer.pts.value, 'red_card')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'red_card'));
              aggregatedPlayer.pts.value.yellow_cards = getPropOrList(aggregatedPlayer.pts.value, 'yellow_cards')
                .concat(getPropOrList(teamPlayerWithValues.pts.value, 'yellow_cards'));
              aggregatedPlayer.pts.value.time = (aggregatedPlayer.pts.value.time || 0) + (teamPlayerWithValues.pts.value.time || 0);
              aggregatedPlayer.pts.value.result = teamPlayerWithValues.pts.value.result;

            }
            return aggregatedPlayer;
          });
        } else {
          if (teamPlayerWithValues.pts) {
            teamPlayerWithValues.pts.played = teamPlayerWithValues.pts.points !== null;
            teamPlayerWithValues.pts.points = playerPointsExtra(teamPlayerWithValues);
          }
          aggregatedPlayerValues.push(teamPlayerWithValues);
        }
      })

      return aggregatedPlayerValues;
    }
      ;

    this.delete = function (id) {
      return $http({
        method: 'DELETE',
        url: API + 'teams/' + id + '.json'
      }).then(null, function (err) {
        growl.addErrorMessage(err.data.message);
        return $q.reject();
      });
    };

    // Save team selection
    this.changeCaptain = function (teamId, weekId, pickIds) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: {
          picks: pickIds,
          week: weekId
        },
        ignoreLoadingBar: true
      });
    };

    this.booster = function (type, weekId, teamId) {
      return $http({
        method: 'POST',
        url: API + 'teams/booster.json',
        data: {
          booster: type,
          week_id: weekId,
          team_id: teamId
        },
        ignoreLoadingBar: true
      });
    };

    this.saveFormation = function (teamId, weekId, pickIds, benchIds, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: {
          week_id: weekId,
          starting: pickIds,
          bench: benchIds,
          captain_id: captainId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.saveFootballFormation = function (teamId, weekId, pickIds, benchIds, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/selections/' + teamId + '.json',
        data: {
          week_id: weekId,
          starting: pickIds,
          bench: benchIds,
          captain_id: captainId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.savePSBudgetFootballFormation = function (teamId, weekId, pickIds, benchIds, captainId, viceCaptainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/selections/' + teamId + '.json',
        data: {
          week_id: weekId,
          starting: pickIds,
          bench: benchIds,
          captain_id: captainId,
          vice_captain_id: viceCaptainId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.saveCoachBudgetFootballFormation = function (teamId, weekId, pickIds, benchIds, captainId, viceCaptainId, coach, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/selections/' + teamId + '.json',
        data: {
          week_id: weekId,
          starting: pickIds,
          bench: benchIds,
          captain_id: captainId,
          vice_captain_id: viceCaptainId,
          coach: coach,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.saveSelection = function (teamId, weekId, pickIds, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: {
          week_id: weekId,
          picks: pickIds,
          captain_id: captainId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.saveFootballSelection = function (teamId, weekId, pickIds, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/selections/' + teamId + '.json',
        data: {
          week_id: weekId,
          picks: pickIds,
          captain_id: captainId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.clone = function (teamId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/copy.json',
        data: {
          team_id: teamId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: false
      });
    };

    this.blanko = function (teamId, playerId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/blanko.json',
        data: {
          team_id: teamId,
          player_id: playerId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: false
      });
    };

    this.resetTransfer = function (teamId, competitionFeed) {
      return $http({
        method: 'PUT',
        url: API + 'teams/transfers.json',
        data: {
          team_id: teamId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: false
      })
        .catch(function (err) {
          growl.addErrorMessage(gettextCatalog.getString(err.data.message));
          return $q.reject();
        });
    };

    this.editFootbalSelection = function (teamId, payload) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: payload,
        ignoreLoadingBar: false
      })
        .catch(function (err) {
          growl.addErrorMessage(gettextCatalog.getString(err.data.message));
          return $q.reject();
        });
    };

    this.changeCaptainById = function (teamId, weekId, captainId, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/captain.json',
        data: {
          captain_id: captainId,
          week_id: weekId,
          team_id: teamId,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.saveCyclingSelection = function (teamId, weekId, pickIds, benchIds, captainId, sponsor_id, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: {
          week_id: weekId,
          starting: pickIds,
          bench: benchIds,
          captain_id: captainId,
          sponsor_id: sponsor_id,
          competition_feed: competitionFeed
        },
        ignoreLoadingBar: true
      });
    };

    this.editCyclingSelection = function (teamId, weekId, pickIds, captainId, sponsor_id, competition_feed) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + teamId + '.json',
        data: {
          week_id: weekId,
          picks: pickIds,
          captain_id: captainId,
          sponsor_id: sponsor_id,
          competition_feed: competition_feed
        },
        ignoreLoadingBar: true
      });
    };

    this.editSelections = function (teamId, weekId, pickIds, captainId, sponsor_id, competition_feed) {
      return $http({
        method: 'POST',
        url: API + 'teams/selections/' + teamId + '.json',
        data: {
          week_id: weekId,
          picks: pickIds,
          captain_id: captainId,
          sponsor_id: sponsor_id,
          competition_feed: competition_feed
        },
        ignoreLoadingBar: true
      });
    };

    this.editF1Selection = function (payload) {
      return $http({
        method: 'POST',
        url: API + 'teams/' + payload.teamId + '.json',
        data: payload,
        ignoreLoadingBar: true
      });
    };

    this.edit = function (team) {
      return $http({
        method: 'PUT',
        url: API + 'teams/' + team.id + '.json',
        data: {
          Team: team
        },
        ignoreLoadingBar: false
      });
    };

    this.cyclingTransfer = function (transfer) {
      return $http({
        method: 'POST',
        url: API + 'teams/transfers.json',
        data: transfer,
        ignoreLoadingBar: true
      });
    };

    this.footballBatchTransfers = function (transfer) {
      return $http({
        method: 'POST',
        url: API + 'teams/transfers.json',
        data: transfer,
        ignoreLoadingBar: true
      });
    };

    this.footballTransfer = function (transfer) {
      return $http({
        method: 'POST',
        url: API + 'teams/transfers.json',
        data: {
          in_id: transfer.in_id,
          out_id: transfer.out_id,
          week_id: transfer.week_id,
          team_id: transfer.team_id
        },
        ignoreLoadingBar: true
      });
    };

    this.cyclingEditTransfer = function (teamId, weekId, captainId, oldIn, newIn) {
      return $http({
        method: 'POST',
        url: API + 'teams/edit_transfer.json',
        data: {
          week_id: weekId,
          captain: captainId,
          team_id: teamId,
          old: oldIn,
          new: newIn
        },
        ignoreLoadingBar: true
      });
    };

    this.getDeadlineWeeks = function (seasonId, competitionFeed) {
      return $http({
        method: 'GET',
        url: API + 'matches.json?season_id=' + seasonId + '&competition_feed=' + competitionFeed
      }).then(function (res) {
        return res.data;
      });
    };

    this.getMatches = function (week, editionId, seasonId) {
      var def = $q.defer();

      let url = `matches/data.json?week=${week}&competition_feed=${editionId}`;

      if (seasonId) {
        url += `&season_id=${seasonId}`;
      }

      $http({
        method: 'GET',
        url: API + url,
        ignoreLoadingBar: true
      })
        .success(function (res) {
          def.resolve(res.Matches);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.gameWeekHasAnyMatch = function (week, editionId) {
      return this.getMatches(week, editionId)
        .then(matches => {
          if (matches && matches.length > 0) {
            if (matches.length === 1) {
              return !!matches[0]['c_AwayTeam'] && !!matches[0]['c_HomeTeam'];
            } else {
              return true;
            }
          } else {
            return false;
          }
        })
    };

    this.getCyclingMatches = function (seasonId, editionId) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'matches.json?season_id=' + seasonId + '&competition_feed=' + editionId,
        ignoreLoadingBar: true
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getById = function (id) {
      return $http({
        method: 'GET',
        url: API + 'teams/get.json?id=' + id
      })
    };


  }
  )

  .service('leagueService', function (API, $http, $q, growl) {

    this.get = function (id, weekId, page) {
      let url = 'leagues/' + id + '.json'

      if (weekId) {
        url += '?week_id=' + weekId;

        if (page) {
          url += '&page=' + page;
        }
      } else {
        if (page) {
          url += '?page=' + page;
        }
      }

      return $http({
        method: 'GET',
        url: API + url
      }).then(function (res) {
        return res.data;
      });
    };

    this.delete = function (id) {
      return $http({
        method: 'DELETE',
        url: API + 'leagues/' + id + '.json'
      }).then(null, function (err) {
        growl.addErrorMessage(err.data.message);
        return $q.reject();
      });
      ;
    };

    this.createMiniCompetition = function (competitionName, competitionFeed, teamId) {
      return $http({
        method: 'POST',
        url: API + 'leagues.json',
        data: {
          name: competitionName,
          competition_feed: competitionFeed,
          team_id: teamId
        }
      }).then(function (res) {
        return res.data;
      });
    }

    this.updateMiniCompetition = function (leagueId, weekId) {
      return $http({
        method: 'PUT',
        url: API + `leagues/${leagueId}.json`,
        data: {
          week_id: weekId
        }
      })
        .then(function (res) {
          return res.data;
        });
    }

    this.searchLeaguesByName = function (name, competitionFeed) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + `leagues/search.json?competition_feed=${competitionFeed}&name=${name}`
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getDraft = function (teamId) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'leagues/draft.json?id=' + teamId
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getTimeLeft = function (leagueId) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'leagues/timeleft.json?league_id=' + leagueId
      })
        .success(function (res) {
          def.resolve(res.timeleft);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getInvites = function (leagueId) {
      var def = $q.defer();

      setTimeout(function () {
        return def.resolve([
          't.vansteelandt@gmail.com',
          'octaviansusnea@gmail.com'
        ]);
      }, 100);

      return def.promise;
    };

    this.sendInvites = function (leagueId, domain, emails) {
      return $http({
        method: 'POST',
        url: API + 'leagues/invites.json',
        data: {
          league_id: leagueId,
          domain: domain,
          invites: emails
        }
      }).then(function (res) {
        return res.data;
      });
    };

    this.getById = function (leagueId) {
      return $http({
        method: 'GET',
        url: API + 'leagues/' + leagueId + '.json'
      });
    };

    this.getByIdWithRankings = function (leagueId, externalTeamIds, weekId) {
      let queryParams = '';

      if (externalTeamIds && externalTeamIds.length) {
        queryParams += `&externalTeamIds=[${externalTeamIds.join(',')}]`
      }

      if (weekId) {
        queryParams += `&weekId=${weekId}`
      }

      return $http({
        method: 'GET',
        url: API + `league/${leagueId}?t${queryParams}`
      });
    };


    this.saveSettings = function (leagueId, obj) {
      return $http({
        method: 'PUT',
        url: API + 'leagues/' + leagueId + '.json',
        data: obj
      });
    };

    this.toggleMock = function (playerId, teamId) {
      var def = $q.defer();

      $http({
        method: 'POST',
        url: API + 'mocks.json',
        data: {
          player_id: playerId,
          team_id: teamId
        }
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.sortMocks = function (mocks, teamId) {
      return $http({
        method: 'POST',
        url: API + 'mocks/sorter.json',
        data: {
          mocks: mocks,
          team_id: teamId
        }
      });
    };

    this.pickPlayer = function (teamId, playerId) {
      return $http({
        method: 'POST',
        url: API + 'leagues/pick.json',
        data: {
          team_id: teamId,
          player_id: playerId
        }
      }).then(function (res) {
        return res.data;
      }, function (err) {
        return $q.reject(err.data);
      });
    };

    this.getFixture = function (fixtureId) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'fixtures/' + fixtureId + '.json'
      })
        .success(function (res) {
          def.resolve(res.Fixture);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getByInviteCode = function (code) {
      return $http({
        method: 'GET',
        url: API + 'leagues.json?invite_code=' + code
      }).then(function (res) {
        return res.data.Leagues;
      });
    };

    this.validateDeadlinePicker = function ($view, $dates, $leftDate, $upDate, $rightDate) {
      var now = moment().utc();

      if ($view === 'year') {
        var active = moment().startOf('year');

        $dates.filter(function (date) {
          return moment(date.utcDateValue).startOf('year') < active;
        }).forEach(function (date) {
          date.selectable = false;
        });
      }
      if ($view === 'month') {
        var active = moment().startOf('month');

        $dates.filter(function (date) {
          return moment(date.utcDateValue).startOf('month') < active;
        }).forEach(function (date) {
          date.selectable = false;
        });
      }
      if ($view === 'day') {
        var active = moment().subtract(1, 'day');

        $dates.filter(function (date) {
          return date.localDateValue() <= active.valueOf();
        }).forEach(function (date) {
          date.selectable = false;
        });
      }
      if ($view === 'hour') {
        var active = moment(Math.floor((+now) / (60 * 60 * 1000)) * (60 * 60 * 1000));

        $dates.filter(function (date) {
          return moment(date.utcDateValue).startOf('hour') <= active;
        }).forEach(function (date) {
          date.selectable = false;
        });
      }
      if ($view === 'minute') {
        var active = moment().startOf('minute');

        $dates.filter(function (date) {
          return date.localDateValue() <= active.valueOf();
        }).forEach(function (date) {
          date.selectable = false;
        });
      }
    };

    this.search = function () {
      return $http({
        method: 'GET',
        url: API + 'leagues/search.json?name=giro&competition_feed=GIRO'
      }).then(function (res) {
        return res.data;
      });
    };

    this.getLeagues = function (competitionFeed, seasonId) {
      return $http({
        method: 'GET',
        url: API + `leagues.json?competition_feed=${competitionFeed}&seasonId=${seasonId}`
      }).then(function (res) {
        return res.data.Leagues.map(league => {
          return {
            id: league.League.id,
            name: league.League.name,
          }
        });
      });
    }

    this.getLeaguesRankings = function (payload) {
      return $http({
        method: 'GET',
        url: API + 'leagues/rankings.json',
        params: payload
      }).then(function (res) {
        return res.data;
      });
    };

    this.getRankings = function (payload) {
      return $http({
        method: 'GET',
        url: API + 'rankings.json',
        params: payload
      }).then(function (res) {
        return res.data;
      });
    };

    this.getRankingsV2 = function (competitionFeed, rankingType, rankingTypeId, pageNumber, pageRecords, name, weekId, postCode) {
      let query = `competitionFeed=${competitionFeed}&pageNumber=${pageNumber}&pageRecords=${pageRecords}`;

      if (name) {
        query += `&name=${name}`;
      }

      if (weekId) {
        query += `&weekId=${weekId}`;
      }

      if (postCode) {
        query += `&postCode=${postCode}`;
      }

      if (rankingTypeId) {
        query += `&rankingTypeId=${rankingTypeId}`;
      }

      return $http({
        method: 'GET',
        url: API + `rankings/${rankingType}?${query}`
      })
        .then(function (res) {
          return res.data;
        });
    };

    this.joinMiniCompetition = function (teamId, leagueId) {
      return $http({
        method: 'POST',
        url: API + 'leagues/join.json',
        data: {
          team_id: teamId,
          league_id: leagueId
        }
      }).then(function (res) {
        return res.data;
      }, function (err) {
        return $q.reject(err.data);
      });
    };

    this.createLeagueWithExistingTeam = function (teamId, leagueName, competitionFeed) {
      return $http({
        method: 'POST',
        url: API + 'leagues.json',
        data: {
          team_id: teamId,
          name: leagueName,
          competition_feed: competitionFeed
        }
      }).then(function (res) {
        return res.data;
      }, function (err) {
        return $q.reject(err.data);
      });
    };

    this.leaveMiniCompetition = function (miniLeagueId) {
      return $http({
        method: 'DELETE',
        url: API + `leagues/${miniLeagueId}.json`
      }).then(null, function (err) {
        growl.addErrorMessage(err.data.message);
        return $q.reject();
      });
      ;
    };
  })

  .service('matchService', function (API, $http, $q) {
    this.get = function (matchId, competition_feed, season_id) {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'matches/' + matchId + '.json?competition_feed=' + competition_feed + '&season_id=' + season_id
      })
        .success(function (res) {
          def.resolve(res.Match);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    }

    this.withBreakDays = function (stages) {
      const newStagesList = [];

      stages.forEach((stage, index) => {
        const currentStageDate = stage.matches.date;
        const nextMatch = stages[index + 1];
        const nextStageDate = nextMatch && nextMatch.matches && nextMatch.matches.date;

        newStagesList.push(stage)

        if (nextStageDate) {
          const daysBetweenStages = moment(nextStageDate).startOf('day').diff(moment(currentStageDate).startOf('day'), 'days');
          if (daysBetweenStages > 1) {
            for (let daysFromFirstStage = 1; daysFromFirstStage < daysBetweenStages; daysFromFirstStage++) {
              newStagesList.push({
                matches: {
                  date: moment(currentStageDate).add(daysFromFirstStage, 'days').format('YYYY-MM-DD HH:mm:ss'),
                  feed_url: 'Rustdag'
                }
              })
            }
          }
        }

      });

      return newStagesList;
    }

    this.getPointsConfirmation = function (competitionFeed, seasonId) {
      return new Promise((resolve, reject) => {
        $http({
          method: 'GET',
          url: `${API}points-confirmation?competitionFeed=${competitionFeed}&seasonId=${seasonId}`,
          headers: {
            'Content-Type': 'application/json'
          },
          data: null
        })
          .success(function (res) {
            resolve(res);
          })
      })
    }
  })

  .service('playerService', function (API, $http, $q, PlayerService, $httpParamSerializer) {
    this.get = function (playerId, teamId, competitionFeed, seasonId) {
      var def = $q.defer();

      var url = API + 'players/' + playerId + '.json';
      if (teamId) {
        url += '?team_id=' + teamId;
      } else {
        if (competitionFeed) {
          url += '?competition_feed=' + competitionFeed;
        }

        if (seasonId) {
          url += (competitionFeed ? '&' : '?') + 'season_id=' + seasonId;
        }
      }

      $http({
        method: 'GET',
        url: url
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.playerStatusEmojiName = function (rawPlayer) {
      if (!rawPlayer) {
        return '';
      }

      const player = PlayerService.getPlayer(rawPlayer);

      if (!player) {
        return '';
      }

      const isBanned = !!player.ban;
      const isInjured = !!player.injury;
      const isReserve = !!player.reserve;
      const isOnFire = player.form && player.form === 1;
      const isMelting = player.form && player.form === 2;

      if (isBanned) {
        return 'ban';
      } else if (isInjured) {
        return 'injury';
      } else if (isReserve) {
        return 'reserve';
      } else if (isOnFire) {
        return 'fire'
      } else if (isMelting) {
        return 'snowflake';
      } else {
        return '';
      }
    };

    this.goalkeeperFirst = bench => {
      const goalkeeper = bench
        .find(player => PlayerService.getPlayerProp(player, 'position_id') === 1);
      return [goalkeeper].concat(
        bench
          .filter(player => PlayerService.getPlayerProp(player, 'position_id') !== 1)
      );
    };

    this.breaksTheCyclingClubLimit = function (cyclist, starting, maxPlayers, exceptions, extraStarting) {
      const exceptionCyclistIds = exceptions || [];
      const extraCyclists = extraStarting || [];

      if (!cyclist) {
        return false;
      }
      const cyclistsSameClub = starting
        .concat(extraCyclists)
        .filter(startingCyclist => {
          const startingCyclistClubId = (startingCyclist && startingCyclist.Club && startingCyclist.Club.id)
            || (startingCyclist && startingCyclist.Player && startingCyclist.Player.club_id);
          const shouldIgnoreCyclist = exceptionCyclistIds.indexOf(startingCyclist && startingCyclist.Player && startingCyclist.Player.id) !== -1;

          return startingCyclistClubId === cyclist.Club.id && !shouldIgnoreCyclist;
        });
      return cyclistsSameClub.length === maxPlayers;
    };

    this.breaksTheFootballClubLimit = function (player, players, maxPlayers, exceptions, extraStarting) {
      const exceptionPlayerIds = exceptions || [];
      const extraPlayers = extraStarting || [];

      if (!player) {
        return false;
      }

      const playerClubId = (player && player.Club && player.Club.id)
        || (player && player.Player && player.Player.club_id)
        || (player && player.clubs && player.clubs.id);

      const playersSameClub = players
        .concat(extraPlayers)
        .filter(teamPlayer => {
          const teamPlayerClubId = (teamPlayer && teamPlayer.Club && teamPlayer.Club.id)
            || (teamPlayer && teamPlayer.Player && teamPlayer.Player.club_id)
            || (teamPlayer && teamPlayer.clubs && teamPlayer.clubs.id);

          const teamPlayerId = (teamPlayer && teamPlayer.Player && teamPlayer.Player.id) ||
            (teamPlayer && teamPlayer.players && teamPlayer.players.id);
          const shouldIgnorePlayer = exceptionPlayerIds.indexOf(teamPlayerId) !== -1;

          return teamPlayerClubId === playerClubId && !shouldIgnorePlayer;
        });
      return playersSameClub.length >= maxPlayers;
    };

    this.computePoints = function (player, captainFactor, captainFactorByTeamResult, captainWithTeamPoints) {
      if (!player) {
        return null;
      }

      const teamPrestatieMinuteLimit = (player && player.config && player.config.teamPrestatieMinuteLimit) || 60;

      const playerTime = (player && player.pts && player.pts.value && player.pts.value.time) || 0;
      const calculatedCleanSheet = (
        (((player.pts.value && player.pts.value.conceeded && player.pts.value.conceeded.length) || 0) === 0) &&
        (playerTime >= 60) &&
        [1, 2].indexOf(player.players.position_id) !== -1
      ) ? 2 : 0;
      const haltTimePoints = (
        (playerTime >= 45) &&
        [1, 2].indexOf(player.players.position_id) !== -1
      ) ? 1 : 0;
      const goals = (player.pts.value && player.pts.value.goals && player.pts.value.goals.length && player.pts.value.goals
        .reduce((acc, item) => {
          if (player.players.position_id === 1) {
            return acc + 5;
          } else if (player.players.position_id === 2) {
            return acc + 4;
          } else {
            return acc + 3;
          }
        }, 0)) || 0;

      const assists = (player.pts.value && player.pts.value.assists && player.pts.value.assists.length && player.pts.value.assists
        .reduce((acc, item) => {
          return acc + 2;
        }, 0)) || 0;

      const red = (player.pts.value && player.pts.value.red_card && player.pts.value.red_card.length && player.pts.value.red_card
        .reduce((acc, item) => {
          return acc - 3;
        }, 0)) || 0;

      const yellow = (red === 0 && player.pts.value && player.pts.value.yellow_cards && player.pts.value.yellow_cards.length && player.pts.value.yellow_cards
        .reduce((acc, item) => {
          return acc - 1;
        }, 0)) || 0;


      const concedeed = (player.pts.value && player.pts.value.conceeded && player.pts.value.conceeded.length && player.pts.value.conceeded
        .reduce((acc, item) => {
          if (player.players.position_id === 1 || player.players.position_id === 2) {
            return acc - 1;
          }
        }, 0)) || 0;

      const teamResultPoints = ((playerTime >= teamPrestatieMinuteLimit) && (player.pts.value && player.pts.value.result === 'W')) ? 3 :
        ((player.pts.value && player.pts.value.result) === 'D' ? 1 : 0);

      let captainPoints = 0;
      let pointsWithCaptain = player.pts.points || 0;

      if (player.selections && player.selections.captain) {
        const factor = captainFactor || 1;

        if (!captainFactorByTeamResult) {
          pointsWithCaptain = player.pts.points * factor;
        } else {
          if (player.pts.value && player.pts.value.result === 'W') {
            pointsWithCaptain = player.pts.points * factor;
          }
        }

        if (captainWithTeamPoints) {
          pointsWithCaptain += player.pts.value.team_points;
        }

        if (pointsWithCaptain !== player.pts.points) {
          captainPoints = pointsWithCaptain - player.pts.points;
        }
      }


      return {
        name: player.players.short,
        clubId: player.clubs.id,
        trend: 'up',
        goals: goals,
        assists: assists,
        yellow: yellow,
        red: red,
        cleanSheet: calculatedCleanSheet,
        conceeded: concedeed,
        halfTime: haltTimePoints,
        teamResultPoints: teamResultPoints,
        captain: captainPoints,

        statMatchPlayed: player.pts.value && player.pts.value.time && player.pts.value.time >= 0 ? 1 : 0,
        statTeamPrestatie: teamResultPoints === 3 ? 1 : 0,
        statTimePlayed: playerTime,
        statGoals: (player.pts.value && player.pts.value.goals && player.pts.value.goals.length) || 0,
        statAssists: (player.pts.value && player.pts.value.assists && player.pts.value.assists.length) || 0,
        statCleanSheet: (
          (((player.pts.value && player.pts.value.conceeded && player.pts.value.conceeded.length) || 0) === 0) &&
          (playerTime >= 60) &&
          [1, 2].indexOf(player.players.position_id) !== -1
        ) ? 1 : 0,
        statConceeded: (player.pts.value && player.pts.value.conceeded && player.pts.value.conceeded.length) || 0,
        statYellow: (player.pts.value && player.pts.value.yellow_cards && player.pts.value.yellow_cards.length) || 0,
        statRed: (player.pts.value && player.pts.value.red_card && player.pts.value.red_card.length) || 0,

        total: pointsWithCaptain
      }
    };


    this.getCyclist = function (playerId, seasonId) {
      var def = $q.defer();

      var url = API + 'players/' + playerId + '.json?season_id=' + seasonId;

      $http({
        method: 'GET',
        url: url
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    };

    this.getDistinctCountriesByPlayers = function (players) {
      var nationalities = [];

      players.forEach(function (player) {
        var found = nationalities.find(function (nationality) {
          return nationality.id === player.National.id;
        });

        if (!found) {
          nationalities.push(player.National);
        }
      });

      return nationalities;
    };

    this.getAll = function (competition_feed, season_id, league_id) {
      if (typeof league_id === 'undefined') { 
        return $http({
          method: 'GET',
          url: API + 'players.json?competition_feed=' + competition_feed + (season_id ? '&season_id=' + season_id : '')
        }).then(function (data) {
          return data.data.Players;
        });
      } else {
        return $http({
          method: 'GET',
          url: API + 'players.json?league_id=' + league_id + '&competition_feed=' + competition_feed + (season_id ? '&season_id=' + season_id : '')
        }).then(function (data) {
          return data.data.Players;
        });
      }      
    };

    this.getAllWithStats = function (competition_feed, season_id) {
      return $http({
        method: 'GET',
        url: API + 'players.json?competition_feed=' + competition_feed + '&season_id=' + season_id + '&week_id=all'
      }).then(function (data) {
        return data.data.Players;
      });
    };

    this.stats = function (payload) {
      return $http({
        method: 'GET',
        url: API + 'players-stats?' + $httpParamSerializer(payload)
      }).then(function (res) {
        return res.data;
      });
    };

    this.getAllStats = function (competition_feed, year) {
      return $http({
        method: 'GET',
        url: API + 'players/stats.json?season_id=' + year + '&competition_feed=' + competition_feed
      }).then(function (result) {
        return result.data;
      });
    };

    this.getAllStatsByWeekId = function (competition_feed, year, week_id) {
      return $http({
        method: 'GET',
        url: API + 'players.json?season_id=' + year + '&competition_feed=' + competition_feed + '&week_id=' + week_id
      })
        .then(function (result) {
          return result.data;
        });
    };

    this.transfer = function (pickId, teamId) {
      return $http({
        method: 'POST',
        url: API + 'teams/waiver.json',
        data: {
          team_id: teamId,
          pick_id: pickId
        }
      });
    };

    this.recall = function (pickId, teamId) {
      return $http({
        method: 'POST',
        url: API + 'teams/recall.json',
        data: {
          team_id: teamId,
          pick_id: pickId
        }
      });
    };
    this.getRankings = function (competitionFeed, seasonId) {
      return $http({
        method: 'GET',
        url: API + 'teams/rankings.json',
        params: {
          competition_feed: competitionFeed,
          season_id: seasonId
        }
      })
        .then(function (res) {
          return res.data;
        });
    };
    this.getRankingsWithFilters = function (payload) {
      return $http({
        method: 'GET',
        url: API + 'rankings.json',
        params: payload
      }).then(function (res) {
        return res.data;
      });
    };

    this.getCyclingRankingsWithFilters = function (payload) {
      return $http({
        method: 'GET',
        url: API + 'rankings.json',
        params: payload
      }).then(function (res) {
        return res.data;
      });
    };

    this.getRankingsByPostCode = function (postCode, competitionFeed, pageNumber) {
      return $http({
        method: 'GET',
        url: API + 'rankings/postcode.json',
        params: {
          post_code: postCode,
          competition_feed: competitionFeed,
          page: pageNumber
        }
      }).then(function (res) {
        return res.data;
      });
    };

    this.getRankingsByLeague = function (competitionFeed, ranking) {
      return $http({
        method: 'GET',
        url: API + 'teams/rankings.json',
        params: {
          competition_feed: competitionFeed,
          ranking: "league"
        }
      }).then(function (res) {
        return res.data;
      });
    };
  })

  .service('newsService', function (API, $http, $q, growl) {
    this.getPS = function () {
      var def = $q.defer();

      $http({
        method: 'GET',
        headers: {
          'Content-Type': 'text/plain'
        },
        data: {
          ignorePreflight: true
        },
        url: 'https://www.playsports.be/nieuws/jupiler-pro-league/feed',
        transformResponse: function (cnv) {
          var x2js = new X2JS();
          var aftCnv = x2js.xml_str2json(cnv);
          return aftCnv;
        }
      })
        .success(function (res) {
          def.resolve(res.Elements.Element);
        })
        .error(function (res) {
          def.reject(res);
        });

      return def.promise;
    };

    this.getFA = function () {
      return $http({
        method: 'GET',
        headers: {
          'Content-Type': 'text/plain'
        },
        data: {
          ignorePreflight: true
        },
        url: 'https://fanarena.com/wp-json/wp/v2/posts'
      }).then(function (data) {
        return data.data;
      });
    };
  })

  .service('timerService', function () {
    this.start = null;

    this.startTimer = function () {
      this.start = new Date().getTime();
    };

    this.stopTimer = function () {
      var stop = new Date().getTime();

      return stop - this.start;
    };
  })

  .service('subnavService', function ($rootScope) {
    this.items = [];

    this.subscribeSet = function (scope, callback) {
      var handler = $rootScope.$on('subnav:set', callback);
      scope.$on('$destroy', handler);
    };

    this.set = function (items) {
      this.items = items;
      $rootScope.$emit('subnav:set');
    };

    this.subscribeActive = function (scope, callback) {
      var handler = $rootScope.$on('subnav:active', callback);
      scope.$on('$destroy', handler);
    };

    this.active = function () {
      $rootScope.$emit('subnav:active');
    };
  })

  .service('faPopupService', function ($uibModal, faHelpService) {

    this.show = function (module) {
      var generalHelpContent = faHelpService.getGeneralHelpContent();

      if (!generalHelpContent[module.id].disabled) {
        $uibModal.open({
          templateUrl: 'directives/fa-popup-help.html',
          controller: 'faPopupController',
          backdrop: false,
          windowClass: 'fa-popoup',
          animation: true,
          resolve: {
            module: function () {
              return module;
            }
          }
        });
      }
    }
  })

  .service('faHelpService', function (HelpContent, HelpTypes, ngRamda) {
    var currentModuleId = null;
    var placeholderData = {};
    var isHelpOpen = false;

    this.setCurrentHelpModule = function (moduleId) {
      currentModuleId = moduleId;
    };

    this.wasSeen = function () {
      var helpSettings = this.getGeneralHelpContent();
      if (currentModuleId && helpSettings) {
        return helpSettings[currentModuleId].seen;
      } else {
        return false;
      }
    };

    this.setPlaceholderData = function (prop, value) {
      placeholderData[prop] = value;
    };

    this.getPlaceholderData = function () {
      return placeholderData;
    };

    this.deactivateHelp = function () {
      var helpSettings = this.getGeneralHelpContent();
      if (helpSettings && helpSettings[currentModuleId]) {
        helpSettings[currentModuleId].seen = true;
        this.updateGeneralHelpContentSettings(helpSettings);
      }
      isHelpOpen = false;
    };

    this.activateHelp = function () {
      isHelpOpen = true;
    };

    this.isHelpOpen = function () {
      return isHelpOpen;
    };

    this.getCurrentModule = function () {
      return _.find(HelpContent, function (module) {
        return module.id === currentModuleId;
      });
    };

    this.getHelpContent = function (moduleId) {
      var id = moduleId || currentModuleId;
      var helpModule = _.find(HelpContent, function (module) {
        return module.id === id;
      });
      if (helpModule) {
        return helpModule.html(placeholderData);
      } else {
        return null;
      }
    };

    this.getModuleById = function (moduleId) {
      var id = moduleId || currentModuleId;
      var helpModule = _.find(HelpContent, function (module) {
        return module.id === id;
      });
      return helpModule ? helpModule : null;
    };

    this.getGeneralHelpContent = function () {
      var defaultSettings = this.getDefaultHelpSettings();
      var localStorageSettings = this.getGeneralHelpContentSettings();

      if (!localStorage.helpSettings) {
        this.updateGeneralHelpContentSettings(defaultSettings);
        return defaultSettings;
      } else if (!_.isEqual(Object.keys(defaultSettings), Object.keys(localStorageSettings))) {
        var mergedSettings = ngRamda.merge(defaultSettings, localStorageSettings);
        this.updateGeneralHelpContentSettings(mergedSettings);
        return mergedSettings;
      } else {
        return this.getGeneralHelpContentSettings();
      }
    };

    this.updateGeneralHelpContentSettings = function (generalHelpContent) {
      localStorage.helpSettings = JSON.stringify(generalHelpContent);
    };

    this.getGeneralHelpContentSettings = function () {
      return JSON.parse(localStorage.getItem('helpSettings'));
    };

    this.hasHelpContent = function () {
      return !!currentModuleId;
    };

    this.getDefaultHelpSettings = function () {
      var helpSettings = {};
      _.forEach(HelpContent, function (module) {
        helpSettings[module.id] = { disabled: false, seen: false };
      });
      return helpSettings;
    };
  })

  .service('sidebarService', function () {
    var isVisible = false;

    this.setSidebarStatus = function (status) {
      isVisible = status;
    };

    this.getSidebarStatus = function () {
      return isVisible;
    }
  })
  .service('GTMService', function () {
    var dataLayer = window.dataLayer = window.dataLayer || [];

    function push(obj) {
      dataLayer.push(obj);
    }

    return {
      push: push
    }
  })
  .service('GoogleAnalyticsService', function ($q) {
    var ga = window.ga || {};

    function getClientId() {
      var def = $q.defer();
      ga(function (tracker) {
        def.resolve(tracker.get('clientId'));
        ;
      });
      return def.promise;
    }

    return {
      getClientId: getClientId
    }
  })
  .service('Hashids', function (HASHIDS_SALT) {
    return new Hashids(HASHIDS_SALT);
  })
  .service('NewGameCacheService', function (localStorageService) {
    var defaultProgress = 1;
    var defaultGameMode = 0;
    var defaultSportType = 0;

    function getProgress() {
      var progress = localStorageService.get('progress');
      if (!progress) {
        localStorageService.set('progress', defaultProgress);
        return defaultProgress;
      }
      return progress;
    }

    function resetProgress() {
      localStorageService.set('progress', defaultProgress);
    }

    function setGameProgress(progress) {
      localStorageService.set('progress', progress);
    }

    function incrementProgress() {
      var currentProgress = localStorageService.get('progress');
      localStorageService.set('progress', currentProgress + 1);
    }

    function decrementProgress() {
      var currentProgress = localStorageService.get('progress');
      if (currentProgress > 1) {
        localStorageService.set('progress', currentProgress - 1);
      }
    }

    function setGameMode(mode) {
      localStorageService.set('game-mode', mode);
    }

    function getGameMode() {
      var gameMode = localStorageService.get('game-mode');
      if (!gameMode) {
        localStorageService.set('game-mode', defaultGameMode);
        return defaultGameMode;
      }
      return gameMode;
    }

    function resetGameMode() {
      localStorageService.set('game-mode', defaultGameMode);
    }

    function setSportType(sportType) {
      localStorageService.set('sport-type', sportType);
      return sportType;
    }

    function getSportType() {
      var sportType = localStorageService.get('sport-type');
      if (!sportType) {
        localStorageService.set('sport-type', defaultSportType);
        return defaultSportType;
      }
      return sportType;
    }

    function resetSportType() {
      localStorageService.set('sport-type', defaultSportType);
    }

    return {
      getProgress: getProgress,
      resetProgress: resetProgress,
      incrementProgress: incrementProgress,
      decrementProgress: decrementProgress,
      setGameMode: setGameMode,
      getGameMode: getGameMode,
      resetGameMode: resetGameMode,
      setGameProgress: setGameProgress,
      setSportType: setSportType,
      getSportType: getSportType,
      resetSportType: resetSportType
    }
  })

  .service('HistoryService', function ($q, API, $http) {
    this.getAll = function () {
      var def = $q.defer();

      $http({
        method: 'GET',
        url: API + 'teams/history.json'
      })
        .success(function (res) {
          def.resolve(res);
        })
        .error(function (err) {
          def.reject(err);
        });

      return def.promise;
    }
  })

  .service('PlayerService', function () {

    function getPlayer(player) {
      if (player && player.Player) {
        return player.Player;
      } else if (player && player.players) {
        return player.players;
      } else {
        return null;
      }
    }

    function getPlayerProp(player, prop) {
      if (player && player.Player) {
        return player.Player[prop];
      } else if (player && player.players) {
        return player.players[prop];
      } else {
        return null;
      }
    }

    return {
      getPlayer: getPlayer,
      getPlayerProp: getPlayerProp
    }
  })

  .service('FootballPicker', function (PlayerService) {

    var result = { Goalkeeper: [], Defender: [], Midfielder: [], Forward: [], Bench: [] };

    var maxPositionsPicks = {
      Goalkeeper: { min: 1, max: 1 },
      Defender: { min: 3, max: 5 },
      Midfielder: { min: 3, max: 5 },
      Forward: { min: 1, max: 3 },
      Bench: { min: 4, max: 4 }
    };

    var positionsIds = {
      Goalkeeper: 1,
      Defender: 2,
      Midfielder: 3,
      Forward: 4
    };

    function pickAtPosition(positionName, playerId) {
      result[positionName].push(playerId);
    }

    function setPositionPickLimit(position, limits) {
      maxPositionsPicks[position] = limits;
    }

    function set(starting, bench) {
      result = { Goalkeeper: [], Defender: [], Midfielder: [], Forward: [], Bench: [] };

      starting.forEach(function (player) {
        result[getPositionNameById(PlayerService.getPlayerProp(player, 'position_id'))].push(PlayerService.getPlayerProp(player, 'id'))
      });

      bench.forEach(function (player) {
        result.Bench.push(PlayerService.getPlayerProp(player, 'id'));
      });

      return {
        possibleFormations: getPossibleFormations(),
        result: result
      };
    }

    function canPick(player) {
      return checkAndPick(player, true);
    }

    function checkAndPick(player, justCheck) {
      var checkedPositions = ['Defender', 'Midfielder', 'Forward'];
      var maxAllowedPlayersForPositions = getMaxAllowedPlayersForPositions(checkedPositions);
      var pickedPositionName = getPositionNameById(PlayerService.getPlayerProp(player, 'position_id'));

      if (result[pickedPositionName] && result[pickedPositionName].indexOf(PlayerService.getPlayerProp(player, 'id')) !== -1 || result.Bench.indexOf(PlayerService.getPlayerProp(player, 'id')) !== -1) {
        return false;
      }

      if (result[pickedPositionName] && (result[pickedPositionName].length < maxAllowedPlayersForPositions[pickedPositionName])) {
        if (!justCheck) {
          pickAtPosition(pickedPositionName, PlayerService.getPlayerProp(player, 'id'));
        }
        return true;
      } else {
        var benchCount = result['Bench'].length;
        if (benchCount < maxPositionsPicks['Bench'].max) {
          if (!justCheck) {
            pickAtPosition('Bench', PlayerService.getPlayerProp(player, 'id'));
          }
          return true;
        } else {
          return false;
        }
      }
    }

    function getMaxAllowedPlayersForPositions(checkedPositions) {
      // we ignore the goalkeeper since it can be only one (min==max==1) in the field
      // but the other positions may fluctuate from min to max
      var maxPlayersForEachPosition = {
        Goalkeeper: 1,
        Defender: 0,
        Midfielder: 0,
        Forward: 0
      };

      var totalFieldPlayers = 10;

      checkedPositions.forEach(function (checkingPosition) {
        var pickedAlreadyForPosition = result[checkingPosition].length;

        var checkingAgainstPositions = checkedPositions.filter(function (position) {
          return position !== checkingPosition;
        });

        var spotsLeft = totalFieldPlayers - pickedAlreadyForPosition;

        checkingAgainstPositions.forEach(function (againstPosition) {
          if (result[againstPosition].length < maxPositionsPicks[againstPosition].min) {
            spotsLeft = spotsLeft - maxPositionsPicks[againstPosition].min;
          } else {
            spotsLeft = spotsLeft - result[againstPosition].length;
          }
        });

        if (spotsLeft > 0) {
          var maxForPosition = pickedAlreadyForPosition;
          while (spotsLeft > 0 && maxForPosition < maxPositionsPicks[checkingPosition].max) {
            maxForPosition += 1;
            spotsLeft -= 1;
          }
          maxPlayersForEachPosition[checkingPosition] = maxForPosition;
        } else {
          maxPlayersForEachPosition[checkingPosition] = pickedAlreadyForPosition;
        }
      });

      return maxPlayersForEachPosition;
    }

    function getCombinations(maxAllowedPlayersForPositions) {
      var combinations = [];
      var defaults = {
        defenderNumber: result.Defender.length > maxPositionsPicks.Defender.min ? result.Defender.length : maxPositionsPicks.Defender.min,
        midfielderNumber: result.Midfielder.length > maxPositionsPicks.Midfielder.min ? result.Midfielder.length : maxPositionsPicks.Midfielder.min,
        forwardNumber: result.Forward.length > maxPositionsPicks.Forward.min ? result.Forward.length : maxPositionsPicks.Forward.min
      };

      for (var defenderNumber = defaults.defenderNumber; defenderNumber <= maxAllowedPlayersForPositions.Defender; defenderNumber++) {
        for (var midfielderNumber = defaults.midfielderNumber; midfielderNumber <= maxAllowedPlayersForPositions.Midfielder; midfielderNumber++) {
          for (var forwardNumber = defaults.forwardNumber; forwardNumber <= maxAllowedPlayersForPositions.Forward; forwardNumber++) {
            var combination = defenderNumber + '-' + midfielderNumber + '-' + forwardNumber;
            if ((defenderNumber + midfielderNumber + forwardNumber) === 10) {
              if (combinations.indexOf(combination) === -1) {
                combinations.push(combination);
              }
            }
          }
        }
      }
      return combinations;
    }

    function getPossibleFormations() {
      var checkedPositions = ['Defender', 'Midfielder', 'Forward'];
      var maxAllowedPlayersForPositions = getMaxAllowedPlayersForPositions(checkedPositions);
      return getCombinations(maxAllowedPlayersForPositions);
    }

    function pick(player) {
      var picked = checkAndPick(player);

      return {
        possibleFormations: getPossibleFormations(),
        result: result,
        picked: picked
      };
    }

    function getPositionNameById(positionId) {
      return Object.keys(positionsIds)
        .find(function (positionName) {
          return positionsIds[positionName] === positionId
        })
    }

    function remove(player) {
      var removeId = PlayerService.getPlayerProp(player, 'id');
      var positionId = PlayerService.getPlayerProp(player, 'position_id');
      var removed = false;

      result[getPositionNameById(positionId)] = result[getPositionNameById(positionId)]
        .filter(removeHandler);
      result.Bench = result.Bench
        .filter(removeHandler);

      function removeHandler(playerId) {
        if (playerId === removeId) {
          removed = true;
          return false;
        } else {
          return true;
        }
      }

      return {
        possibleFormations: getPossibleFormations(),
        result: result,
        removed: removed
      };
    }

    return {
      pick: pick,
      remove: remove,
      set: set,
      canPick: canPick,
      getPossibleFormations: getPossibleFormations,
      setPositionPickLimit: setPositionPickLimit
    };
  })

  .service('UtilsService', function (gettextCatalog) {
    function getLocalDate(utcDate, format) {
      return moment.utc(utcDate).local().format(format || 'YYYY-MM-DD HH:mm:ss');
    }

    return {
      getLocalDate: getLocalDate
    };
  })

  .service('FACms', function ($q, $http, gettextCatalog, API) {
    const rankingMapping = {
      TOUR: {
        '11': 'general',
        '12': 'week',
        '9': 'peloton',
        '10': 'peloton-week',
        '7': 'sprint',
        '8': 'mountain',
        'value7': 'periode',
        'value8': 'de-dubbel'
      },
      GKLAS: {
        '25': 'general',
        '26': 'week',
        '28': 'peloton',
        '29': 'peloton-week',
        '27': 'de-dubbel'
      },
      VUEL: {
        '17': 'general',
        '18': 'week',
        '15': 'peloton',
        '16': 'peloton-week',
        '13': 'sprint',
        '14': 'mountain'
      },
      GCX: {
        '19': 'general',
        '20': 'week',
        '23': 'peloton',
        '22': 'sprint',
        '21': 'mountain',
        '24': 'peloton-week'
      },
      G11: {
        '1': 'general',
        '2': 'week',
        '3': 'periode-1',
        '4': 'periode-2',
        '5': 'peloton',
        '6': 'peloton-week',
      },
      HOCKM: {
        '13': 'general',
        '14': 'week'
      },
      HOCKF: {
        '15': 'general',
        '16': 'week'
      }
    };

    function orderResult(res) {
      return res.data ? res.data.sort((left, right) => {
        if (left.sort > right.sort) {
          return 1;
        } else if (right.sort > left.sort) {
          return -1;
        } else {
          return 0;
        }
      }) : [];
    }

    function getImageUrl(imageDate) {
      return `https://fanarena-admin.s3-eu-west-1.amazonaws.com/${imageDate.fileName}`
    }

    function getPromotions(competitionFeed, seasonId) {
      return $http({ method: 'GET', url: `${API}promotions?competitionFeed=${competitionFeed}&seasonId=${seasonId}` });
    }

    function getPrizes(rankingsEnabled, competitionFeed) {
      return $http({ method: 'GET', url: `${API}prizes?competitionFeed=${competitionFeed}` })
        .then(orderResult)
        .then(result => {
          const prizesMap = {};

          const prizesList = result.map(prizeData => {
            const translatedPrize = prizeData.translations.find(
              translation => translation.languageCode === gettextCatalog.getCurrentLanguage()
            );
            let imageURL = null;

            if (prizeData.image && prizeData.image.fileName) {
              imageURL = getImageUrl(prizeData.image);
            }
            return Object.assign(
              {
                firstRank: prizeData.firstRank,
                lastRank: prizeData.lastRank,
              },
              translatedPrize || {},
              {
                sort: prizeData.sort,
                image: imageURL,
                ranking: prizeData.ranking
              }
            );
          });

          const rankingsValues = Object.keys(rankingMapping[competitionFeed])
            .filter(rankingValue => rankingsEnabled.indexOf(rankingMapping[competitionFeed][rankingValue]) !== -1);
          const prizesGrouped = rankingsValues
            .map(rankingValue => ({ [rankingValue]: prizesList.filter(prizeItem => prizeItem.ranking === rankingValue) }));

          prizesGrouped.forEach(item => {
            const rankingValue = Object.keys(item)[0];
            prizesMap[rankingMapping[competitionFeed][rankingValue]] = item[rankingValue];
          });

          return prizesMap;
        });
    }

    return {
      getPrizes: getPrizes,
      getPromotions: getPromotions
    };
  })

  .service('IntegrationService', function ($analytics) {

    this.mixpanelTrackLogin = function (user) {
      var moment = require('moment');
      var currentDateTime = moment();

      user.lastRouteChangeDatetime = currentDateTime.toISOString();

      mixpanel.identify(user.id);
      mixpanel.people.set({ 'lastLogin': user.lastRouteChangeDatetime });
      mixpanel.register({ 'lastLogin': user.lastRouteChangeDatetime });
      mixpanel.people.increment("numberOfLogins");

      $analytics.eventTrack('logIn', { dateTimeOfLogin: user.lastRouteChangeDatetime });
    }
  })
  .service('analyticsService', function () {

    function addToDataLayer(data) {
      if (window.dataLayer) {
        window.dataLayer.push(data);
      }
    }

    return {
      addToDataLayer: addToDataLayer
    }
  });
