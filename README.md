# FanArena Common Libraries

## Prerequisites

* Node version >= 5.0

## Installation 

* `npm install mocha`
* `npm install`

## Testing

* `npm run test`

cd fanarena-common
git tag 1.7.2
git push origin master --tags