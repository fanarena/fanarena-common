const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const PickPlayers = require('./pick-football-players').default;

chai.use(chaiHttp);

const playersList = [
  {Player: {position_id: 1, id: 1}},
  {Player: {position_id: 1, id: 2}},
  {Player: {position_id: 1, id: 3}},
  {Player: {position_id: 1, id: 4}},
  {Player: {position_id: 1, id: 5}},
  {Player: {position_id: 1, id: 6}},
  {Player: {position_id: 2, id: 7}},
  {Player: {position_id: 2, id: 8}},
  {Player: {position_id: 2, id: 9}},
  {Player: {position_id: 2, id: 10}},
  {Player: {position_id: 2, id: 11}},
  {Player: {position_id: 2, id: 12}},
  {Player: {position_id: 2, id: 13}},
  {Player: {position_id: 2, id: 14}},
  {Player: {position_id: 3, id: 15}},
  {Player: {position_id: 3, id: 16}},
  {Player: {position_id: 3, id: 17}},
  {Player: {position_id: 3, id: 18}},
  {Player: {position_id: 3, id: 19}},
  {Player: {position_id: 3, id: 20}},
  {Player: {position_id: 3, id: 21}},
  {Player: {position_id: 3, id: 22}},
  {Player: {position_id: 3, id: 23}},
  {Player: {position_id: 3, id: 24}},
  {Player: {position_id: 4, id: 25}},
  {Player: {position_id: 4, id: 26}},
  {Player: {position_id: 4, id: 27}},
  {Player: {position_id: 4, id: 28}},
  {Player: {position_id: 4, id: 29}},
  {Player: {position_id: 4, id: 30}},
  {Player: {position_id: 4, id: 31}},
  {Player: {position_id: 4, id: 32}},
  {Player: {position_id: 4, id: 33}},
  {Player: {position_id: 4, id: 34}},
];

const maxPositionsPicks = {
  Goalkeeper: {min: 1, max: 1},
  Defender: {min: 3, max: 5},
  Midfielder: {min: 3, max: 5},
  Forward: {min: 1, max: 3},
  Bench: {min: 4, max: 4}
};

const positionsIds = {
  Goalkeeper: 1,
  Defender: 2,
  Midfielder: 3,
  Forward: 4
};

function getPlayerById(players, id) {
  return players.find(player => player.Player.id === id);
}

describe('Pick soccer players algorithm', function () {
  describe('picking 4-3-3', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    it('picking one gk first', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 1))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [],
          Bench: []
        },
        picked: true
      });
    });

    it('picking second gk', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 2))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking first attacker', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 34))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking second attacker', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 33))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '5-3-2'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking third attacker', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 32))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3',
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 32],
          Bench: [2]
        },
        picked: true
      });
    });

    it('removing one attacker', () => {
      expect(pickPlayersService.remove(getPlayerById(playersList, 32))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '5-3-2'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33],
          Bench: [2]
        },
        removed: true
      });
    });

    it('picking third attacker again', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 25))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking second attacker for bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 26))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26]
        },
        picked: true
      });
    });


    it('picking third attacker for bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 27))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27]
        },
        picked: true
      });
    });

    it('picking another attacker for bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 28))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });


    it('picking fourth attacker for bench should return picked as false', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 30))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: false
      });
    });

    it('pick one defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 7))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the second defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 8))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the third defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 9))).to.deep.equal({
        possibleFormations: [
          '3-4-3', '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the fourth defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 10))).to.deep.equal({
        possibleFormations: [
          '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the first midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 18))).to.deep.equal({
        possibleFormations: [
          '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [18],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the second midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 19))).to.deep.equal({
        possibleFormations: [
          '4-3-3'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [18, 19],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick the third midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 20))).to.deep.equal({
        possibleFormations: ['4-3-3'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [18, 19, 20],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: true
      });
    });

    it('pick another midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 21))).to.deep.equal({
        possibleFormations: ['4-3-3'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [18, 19, 20],
          Forward: [34, 33, 25],
          Bench: [2, 26, 27, 28]
        },
        picked: false
      });
    });

  });

  describe('picking 5-4-1', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    it('picking one gk first', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 1))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [],
          Bench: []
        },
        picked: true
      });
    });

    it('picking the bench gk', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 2))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking first defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 7))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });


    it('picking the second defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 8))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking the third defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 9))).to.deep.equal({
        possibleFormations: ['3-4-3', '3-5-2', '4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking the fourth defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 10))).to.deep.equal({
        possibleFormations: ['4-3-3', '4-4-2', '4-5-1', '5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking the fifth defender', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 11))).to.deep.equal({
        possibleFormations: ['5-3-2', '5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [],
          Forward: [],
          Bench: [2]
        },
        picked: true
      });
    });

    it('picking the sixth defender and add it to bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 12))).to.deep.equal({
        possibleFormations: [
          '5-3-2', '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [],
          Forward: [],
          Bench: [2, 12]
        },
        picked: true
      });
    });

    it('picking the first midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 15))).to.deep.equal({
        possibleFormations: [
          '5-3-2', '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15],
          Forward: [],
          Bench: [2, 12]
        },
        picked: true
      });
    });

    it('picking the second midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 16))).to.deep.equal({
        possibleFormations: [
          '5-3-2', '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16],
          Forward: [],
          Bench: [2, 12]
        },
        picked: true
      });
    });

    it('picking the third midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 17))).to.deep.equal({
        possibleFormations: [
          '5-3-2', '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17],
          Forward: [],
          Bench: [2, 12]
        },
        picked: true
      });
    });

    it('picking the fourth midfielder', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 18))).to.deep.equal({
        possibleFormations: [
          '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [],
          Bench: [2, 12]
        },
        picked: true
      });
    });

    it('picking first midfielder for the bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 19))).to.deep.equal({
        possibleFormations: [
          '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [],
          Bench: [2, 12, 19]
        },
        picked: true
      });
    });

    it('picking second midfielder for the bench', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 20))).to.deep.equal({
        possibleFormations: [
          '5-4-1'
        ],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [],
          Bench: [2, 12, 19, 20]
        },
        picked: true
      });
    });

    it('picking another midfielder should not work', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 24))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [],
          Bench: [2, 12, 19, 20]
        },
        picked: false
      });
    });

    it('picking an attacker for starting', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 26))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 19, 20]
        },
        picked: true
      });
    });

    it('picking another attacker should not work', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 27))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 19, 20]
        },
        picked: false
      });
    });

    it('removing one bench player', () => {
      expect(pickPlayersService.remove(getPlayerById(playersList, 19))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20]
        },
        removed: true
      });
    });

    it('picking another attacker should work', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 27))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20, 27]
        },
        picked: true
      });
    });
  });

  describe('setting starting & bench. Removing and picking again', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 7),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 10),
      getPlayerById(playersList, 11),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 17),
      getPlayerById(playersList, 18),
      getPlayerById(playersList, 26)
    ], [
      getPlayerById(playersList, 2),
      getPlayerById(playersList, 12),
      getPlayerById(playersList, 20),
      getPlayerById(playersList, 27)
    ]);

    it('picking existing player', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 1))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20, 27]
        },
        picked: false
      });
    });

    it('should remove last player on bench', () => {
      expect(pickPlayersService.remove(getPlayerById(playersList, 27))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20]
        },
        removed: true
      });
    });

    it('picking existing player again when bench empty', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 1))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20]
        },
        picked: false
      });
    });

    it('since bench has one empty spot, canPick should return true', () => {
      expect(pickPlayersService.canPick(getPlayerById(playersList, 3))).to.be.true;
    });

    it('picking existing player again when bench has one empty spot', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 3))).to.deep.equal({
        possibleFormations: ['5-4-1'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10, 11],
          Midfielder: [15, 16, 17, 18],
          Forward: [26],
          Bench: [2, 12, 20, 3]
        },
        picked: true
      });
    });

    it('since bench is full and starting as well, canPick should return false', () => {
      expect(pickPlayersService.canPick(getPlayerById(playersList, 4))).to.be.false;
    });
  });

  describe('maximum picks for positions when spots left does not fit', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 7),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 10),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 26),
      getPlayerById(playersList, 28),
      getPlayerById(playersList, 29)
    ], [
      getPlayerById(playersList, 2),
      getPlayerById(playersList, 12),
      getPlayerById(playersList, 20),
      getPlayerById(playersList, 27)
    ]);

    it('picking the fifth defender should not be allowed', () => {
      const result = pickPlayersService.pick(getPlayerById(playersList, 11));
      expect(result).to.deep.equal({
        possibleFormations: ['4-3-3'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [15, 16],
          Forward: [26, 28, 29],
          Bench: [2, 12, 20, 27]
        },
        picked: false
      });
    });
  });

  describe('picking players when bench max is 0', () => {
    const maxPositionsPicksBenchZero = {
      Goalkeeper: {min: 1, max: 1},
      Defender: {min: 3, max: 5},
      Midfielder: {min: 3, max: 5},
      Forward: {min: 1, max: 3},
      Bench: {min: 0, max: 0}
    };
    const pickPlayersService = PickPlayers(maxPositionsPicksBenchZero, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 7),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 10),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 17),
      getPlayerById(playersList, 26),
      getPlayerById(playersList, 28),
      getPlayerById(playersList, 29)
    ], [
    ]);

    it('picked should return false', () => {
      expect(pickPlayersService.pick(getPlayerById(playersList, 3))).to.deep.equal({
        possibleFormations: ['4-3-3'],
        result: {
          Goalkeeper: [1],
          Defender: [7, 8, 9, 10],
          Midfielder: [15, 16, 17],
          Forward: [26,28,29],
          Bench: []
        },
        picked: false
      });
    });
  })

  describe('isValid #1', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 7),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 10),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 17),
      getPlayerById(playersList, 26),
      getPlayerById(playersList, 28),
      getPlayerById(playersList, 29)
    ], [
      getPlayerById(playersList, 2),
      getPlayerById(playersList, 12),
      getPlayerById(playersList, 20),
      getPlayerById(playersList, 27)
    ]);

    it('should return true when calling isValid for a valid lineup', () => {
      const result = pickPlayersService.isValid();
      expect(result).to.deep.equal(true);
    });
  });

  describe('isValid #2', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 4),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 10),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 17),
      getPlayerById(playersList, 26),
      getPlayerById(playersList, 28),
      getPlayerById(playersList, 29)
    ], [
      getPlayerById(playersList, 2),
      getPlayerById(playersList, 12),
      getPlayerById(playersList, 20),
      getPlayerById(playersList, 27)
    ]);

    it('should return false when calling isValid for a lineup that has two goalkeepers (2-3-3-3)', () => {
      const result = pickPlayersService.isValid();
      expect(result).to.deep.equal(false);
    });
  });

  describe('isValid #3', () => {
    const pickPlayersService = PickPlayers(maxPositionsPicks, positionsIds);

    pickPlayersService.set([
      getPlayerById(playersList, 1),
      getPlayerById(playersList, 7),
      getPlayerById(playersList, 8),
      getPlayerById(playersList, 9),
      getPlayerById(playersList, 15),
      getPlayerById(playersList, 16),
      getPlayerById(playersList, 25),
      getPlayerById(playersList, 26),
      getPlayerById(playersList, 33),
      getPlayerById(playersList, 28),
      getPlayerById(playersList, 29)
    ], [
      getPlayerById(playersList, 2),
      getPlayerById(playersList, 12),
      getPlayerById(playersList, 20),
      getPlayerById(playersList, 27)
    ]);

    it('should return false when calling isValid for a lineup that has two 5 attackers (1-3-2-5)', () => {
      const result = pickPlayersService.isValid();
      expect(result).to.deep.equal(false);
    });
  });
});