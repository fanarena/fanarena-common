export default angular.module('app.common.constants', [])
  .constant('HelpContent',
    [
      {"id": 1, "type": 2, "subject": "Select Help Subject", "html": noHelpForCurrentStateHTML},
      {"id": 2, "type": 2, "subject": "What you can do when choosing to start a new game?", "html": newGameStartUpHTML},
      {"id": 3, "type": 2,"subject": "Play Sports Competition","html": [playSportsCompetitionHTML, trophyCompetitionHTML]},
      {"id": 4,"type": 2,"subject": "New Game With Friends","html": newGameWithFriends},
      {"id": 5,"type": 1,"subject": "Invite Players to League","html": inviteFriends},
      {"id": 6,"type": 1,"subject": "About Wish list","html": wishlist}
    ]
  )
  .constant('HelpModulesIDs',
    {
      "NoHelpForCurrentState": 1,
      "NewGameStartup": 2,
      "CompetitionTypes": 3,
      "NewGameWithFriends": 4,
      "InviteFriends": 5,
      "Wishlist": 6
    }
  )
  .constant('POSITIONSIDS', {
    'Goalkeeper': 1,
    'Defender': 2,
    'Midfielder': 3,
    'Forward': 4
  })
  .constant('POSITIONS', {
    1: 'Doelman',
    2: 'Verdediger',
    3: 'Middenvelder',
    4: 'Aanvaller'
  })
  .constant('SOCCER_FORMATIONS', [{
    id: 1,
    name: '4-4-2',
    Goalkeeper: 1,
    Defender: 4,
    Midfielder: 4,
    Forward: 2
  }, {
    id: 2,
    name: '5-2-3',
    Goalkeeper: 1,
    Defender: 5,
    Midfielder: 2,
    Forward: 3
  }, {
    id: 3,
    name: '5-4-1',
    Goalkeeper: 1,
    Defender: 5,
    Midfielder: 4,
    Forward: 1
  }, {
    id: 4,
    name: '5-3-2',
    Goalkeeper: 1,
    Defender: 5,
    Midfielder: 3,
    Forward: 2
  }, {
    id: 5,
    name: '3-5-2',
    Goalkeeper: 1,
    Defender: 3,
    Midfielder: 5,
    Forward: 2
  }, {
    id: 6,
    name: '3-4-3',
    Goalkeeper: 1,
    Defender: 3,
    Midfielder: 4,
    Forward: 3
  }, {
    id: 7,
    name: '4-5-1',
    Goalkeeper: 1,
    Defender: 4,
    Midfielder: 5,
    Forward: 1
  }, {
    id: 8,
    name: '4-3-3',
    Goalkeeper: 1,
    Defender: 4,
    Midfielder: 3,
    Forward: 3
  }])
  .constant('HelpTypes',
    {
      "About": 1,
      "General": 2
    }
  )
  .constant('DraftBestOptions',
    {
      'BE1': {
        'goalkeeper': ['Butelle', 'Penneteau', 'Gillet'],
        'goal-getter': ['Teodorczyk', 'Vossen', 'Sá'],
        'defender': ['Derijck', 'Van Rhijn', 'Cools'],
        'midfielder': ['Hanni', 'Kage', 'Vormer'],
        'assist-king': ['Kaya', 'Schrijvers', 'Peeters'],
        'penalty-taker': ['Vossen', 'Tielemans', 'Berrier']
      },
      'NL1': {
        'goalkeeper': ['Jones', 'Onana', 'Zoet'],
        'goal-getter': ['Jorgensen', 'Unal', 'Klaassen'],
        'defender': ['Moreno', 'Karsdorp', 'Sanchez'],
        'midfielder': ['Klaassen', 'Ziyech', 'Toornstra'],
        'assist-king': ['Toornstra', 'Jorgensen', 'Hasselbaink'],
        'penalty-taker': ['Van Wolfswinkel', 'Hasselbaink', 'Armenteros']
      },
      'GB1': {
        'goalkeeper': ['Courtois', 'Lloris', 'De Gea'],
        'goal-getter': ['Costa', 'Sanchez', 'Ibrahimovic'],
        'defender': ['Cahill', 'Alonso', 'Azpilicueta'],
        'midfielder': ['Alli', 'Hazard', 'Lallana'],
        'assist-king': ['De Bruyne', 'Phillips', 'Eriksen'],
        'penalty-taker': ['Milner', 'Defoe', 'Mahrez']
      },
      'FR1': {
        'goalkeeper': ['Cardinale', 'Anthony Lopes', 'Subašic'],
        'goal-getter': ['Cavani', 'Lacazette', 'Falcao'],
        'defender': ['Fabinho', 'Sarr', 'Glik'],
        'midfielder': ['Boudebouz', 'Thauvin', 'Lemar'],
        'assist-king': ['Seri', 'Sanson', 'Fernando Marcal De Oliveira'],
        'penalty-taker': ['Lacazette', 'Cavani', 'Tavares']
      },
      'L1': {
        'goalkeeper': ['Neuer', 'Hrádecký', 'Jarstein'],
        'goal-getter': ['Aubameyang', 'Modeste', 'Lewandowski'],
        'defender': ['Abraham', 'Oczipka', 'Alaba'],
        'midfielder': ['Malli', 'Forsberg', 'Gnabry'],
        'assist-king': ['Forsberg', 'Dembele', 'Brandt'],
        'penalty-taker': ['Malli', 'Ibisevic', 'Petersen']
      },
      'ES1': {
        'goalkeeper': ['Asenjo', 'Oblak', 'Ter Steegen'],
        'goal-getter': ['Messi', 'Suarez', 'Juncal'],
        'defender': ['Jaume Costa', 'Godin', 'Marcelo'],
        'midfielder': ['Carrasco', 'Soriano', 'Parejo'],
        'assist-king': ['Kroos', 'Piatti', 'Neymar'],
        'penalty-taker': ['Soriano', 'Viera Ramos', 'Parejo']
      },
      'IT1': {
        'goalkeeper': ['Szczesny', 'Donnarumma', 'Skorupski'],
        'goal-getter': ['Icardi', 'Belotti', 'Dzeko'],
        'defender': ['Kessie', 'Conti', 'Caldara'],
        'midfielder': ['Salah', 'Hamšík', 'Perotti'],
        'assist-king': ['Callejon', 'Pereira Gomes', 'Hamsik'],
        'penalty-taker': ['Perotti', 'Biglia', 'Kessie']
      }
    }
  )
  .constant('SportTypesValues', {
    'FOOTBALL': 1,
    'CYCLING': 2,
    'FORMULA_ONE': 3
  })
  .constant('CHANNELS', {
    'Play Sports HD1': 1,
    'Play Sports HD2': 2,
    'Play Sports HD3': 3,
    'Play Sports HD4': 4,
    'Play Sports HD5': 5,
    'Play Time': 10
  })
  .constant('CYCLINGCOMPETITIONS', {
    'los': 1,
    'worldcup': 2,
    'superprestige': 3,
    'worldchampionship': 4,
    'europetour': 5,
    'worldtour' : 6
  })
  .constant('LeaguesIds',
    {
      paper: 2413
    }
  )
  .constant('RESOURCES', '/images/')
  .constant('FLAGS',
    [
      {
        "competition_feed": "BE1",
        "flag": "🇧🇪"
      },
      {
        "competition_feed": "NL1",
        "flag": "🇳🇱"
      },
      {
        "competition_feed": "GB1",
        "flag": "🇬🇧"
      },
      {
        "competition_feed": "L1",
        "flag": "🇩🇪"
      },
      {
        "competition_feed": "FR1",
        "flag": "🇫🇷"
      },
      {
        "competition_feed": "IT1",
        "flag": "🇮🇹"
      },
      {
        "competition_feed": "ES1",
        "flag": "🇪🇸"
      },
      {
        "competition_feed": "CL",
        "flag": "🚴"
      }
    ]
  );

function noHelpForCurrentStateHTML() {
  return "<p> Choose a subject from the list above and get information regarding different parts of the game </p>";
}

function newGameStartUpHTML(data) {
  return "<p> Let's kick off, <b>" + data.firstName + "</b>!</p>" +
    "<p> Join an existing public league or Create a league with your friends.</p>";
}

function playSportsCompetitionHTML() {
  return "<p>In the Play Sports Competition you play in a league with 6 other fans. After 10 gameweeks, " +
    "the winner promotes to the next level. Climb all the way up to Eerste Klasse!.</p>";
}

function trophyCompetitionHTML() {
  return "<p>A Trophy is a standalone public league named after a football legend. Choose any competition you like " +
    "and compete against 7 others. The trophy will be added to the winner’s trophy case! </p>";
}

function newGameWithFriends() {
  return "<p>Create a league and invite your friends and find out who really knows all about football. " +
    "Pick a draft date and put your money where your mouth is!</p>";
}

function inviteFriends() {
  return "<p>A friendly league should have between 4 to 12 teams. " +
    "We recommend 8.You can still change the draft date and time.</p>";
}

function wishlist() {
  return "<p>To be fully prepared for the draft you’ll have to make a wish list. During the draft each participant " +
    "picks in turn a player for their team. Each time you only have 30 seconds to do so. Put your most wanted players " +
    "at the top to quickly find them. </p>";
}