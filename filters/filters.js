export default angular.module('app.common.filters', [])

.filter('jsDate', function(){
	return function(input){
		if(input === undefined)
			return input;

		return new Date(parseInt(input.substr(6)));
	};
})

.filter('momentDate', function(){
	return function(input, format){
		var date = moment.utc(input);
		date.local();

		return date.format(format);
	}
})

.filter('flag', function(FLAGS){
	return function(input){
		var flag = _.findWhere(FLAGS.default, {competition_feed: input});

		if(flag)
			return flag.flag;
		else
			return input;
	}
})

.filter('slice', function() {
	return function(arr, start, end) {
		if(arr)
			return arr.slice(start, end);
		else {
			return arr;
		}
	};
})
