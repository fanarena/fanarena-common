import subNavTemplate from './templates/subnav.html';
import psNewsTemplate from './templates/ps-news.html';
import faNewsTemplate from './templates/fa-news.html';
import faConfirmationDialogTemplate from './templates/fa-confirmation-dialog.template.html';
import playerStatsTemplate from './templates/player-stats.html';

export default angular.module('app.common.directives', [])

  .directive('hasMenu', function ($rootScope, sidebarService) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        $rootScope.$on('sidemenu:open', function () {
          angular.element(elem).addClass('has-menu');
          sidebarService.setSidebarStatus(true);
        });

        $rootScope.$on('sidemenu:close', function () {
          angular.element(elem).removeClass('has-menu');
          sidebarService.setSidebarStatus(false);
        });
      }
    };
  })
  .directive('faConfirmationDialog', function ($timeout, $uibModal) {
    return {
      restrict: 'A',
      scope: {
        title: '@',
        body: '@',
        onSuccess: '&',
        onCancel: '&'
      },
      link: function (scope, element) {
        element.bind('click', function () {
          var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: faConfirmationDialogTemplate,
            controller: 'FaConfirmationDialogCtrl',
            controllerAs: '$ctrl',
            resolve: {
              title: function () {
                return scope.title;
              },
              body: function () {
                return scope.body;
              }
            }
          });

          modalInstance.result
            .then(function () {
              scope.onSuccess();
            });
        });

      },
      controller: 'faPopoverController'
    };
  })
  .directive('collapseTeam', function ($rootScope) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        console.log(attrs);
      }
    }
  })

  .directive('fallbackSrc', function () {
    var fallbackSrc = {
      link: function postLink(scope, iElement, iAttrs) {
        iElement.bind('error', function () {
          angular.element(this).attr("src", iAttrs.fallbackSrc);
        });
      }
    }
    return fallbackSrc;
  })

  .directive('multipleEmails', function () {
    return {
      require: 'ngModel',
      link: function (scope, el, attrs, ctrl) {
        ctrl.$parsers.unshift(function (viewValue) {
          var emails = viewValue.split('\n');

          var re = /\S+@\S+\.\S+/;

          var validityArr = emails.map(function (str) {
            return re.test(str.trim());
          });

          var atLeastOneInvalid = false;

          angular.forEach(validityArr, function (val) {
            if (val === false)
              atLeastOneInvalid = true;
          });

          if (!atLeastOneInvalid) {
            ctrl.$setValidity('multipleEmails', true);
            return viewValue;
          } else {
            ctrl.$setValidity('multipleEmails', false);
            return undefined;
          }
        });
      }
    }
  })

  .directive('matchPw', [function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        match: "=matchPw"
      },
      link: function (scope, elem, attrs, ngModel) {
        ngModel.$validators.matchPw = function (modelValue) {
          return modelValue == scope.match ? true : false;
        };
      }
    };
  }])

  .directive('playSportsWrapper', function ($window) {
    return {
      restrict: 'A',
      link: function (scope, el, attrs, ctrl) {
        /*if (PLAYSPORTS) {
         scope.resize = function () {
         var psElements = document.getElementsByClassName('ps');
         var height = 0;
         angular.forEach(psElements, function (val, key) {
         height += val.offsetHeight;
         });
         var main_wrapper = document.querySelector('.main-wrapper');
         if (main_wrapper !== null && height < 500) {
         var diff = $window.innerHeight - height;
         main_wrapper.style.minHeight = diff + "px";
         }
         };

         scope.$watch(function () {
         scope.__height = el.height();
         });

         scope.$watch('__height', function (newVal, oldVal) {
         scope.resize();
         });

         angular.element($window).bind('resize', function () {
         scope.resize();
         });
         }*/
      }
    }
  })

  .directive('sidebarHeight', function ($window) {
    return {
      restrict: 'A',
      link: function (scope, el, attrs) {
        scope.resize = function () {
          var height = 0;

          $('#header > div, #footer > footer').each(function (index, el) {
            height += $(el).outerHeight();
          });

          $('.main-wrapper').css({'min-height': $window.innerHeight - height + 'px'});
        };

        scope.$watch(function () {
          scope.__height = el.height();
        });

        scope.$watch('__height', function (newVal, oldVal) {
          scope.resize();
        });

        angular.element($window).bind('resize', function () {
          scope.resize();
        });
      }
    };
  })

  .directive('hasSubnav', function ($rootScope, subnavService) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        subnavService.subscribeSet(scope, function () {
          if (subnavService.items.length > 0)
            angular.element(elem).addClass('has-subnav');
          else
            angular.element(elem).removeClass('has-subnav');
        });

      }
    };
  })

  .directive('subnav', function ($rootScope, subnavService) {
    return {
      restrict: 'E',
      replace: true,
      template: subNavTemplate,
      link: function (scope) {
        scope.tabs = subnavService.items;

        subnavService.subscribeSet(scope, function () {
          scope.tabs = subnavService.items;
        });

        scope.change = function (tab) {
          _.findWhere(scope.tabs, {active: true}).active = false;
          tab.active = true;

          // Notify
          subnavService.active();
        };
      }
    };
  })

  .directive('bindFileToModel', [function () {
    return {
      scope: {
        bindFileToModel: '='
      },
      link: function (scope, element) {
        element.bind('change', handleFileSelect);

        function handleFileSelect(evt) {
          var file = evt.currentTarget.files[0];
          var reader = new FileReader();
          reader.onload = function (eventResult) {
            scope.$apply(function () {
              scope.bindFileToModel = eventResult.target.result;
            });
          };
          reader.readAsDataURL(file);
        }

      }
    }
  }])

  .directive('subnavTab', function (subnavService) {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        id: '='
      },
      template: '<div class="subnav-tab" ng-class="{active:active}" ng-transclude></div>',
      link: function (scope, elem) {
        scope.active = false;

        subnavService.subscribeActive(scope, function () {
          scope.setActive();
        });

        scope.setActive = function () {
          scope.active = false;
          if (subnavService.items.length && _.findWhere(subnavService.items, {active: true}).id === parseInt(scope.id))
            scope.active = true;
        };

        scope.setActive();
      }
    }
  })

  .directive('playerStats', function ($rootScope, $templateRequest, $compile, playerService) {
    return {
      restrict: 'A',
      link: function (scope, el, attrs) {

        el.on('click', 'a.player-avatar, a.player-info', function (ev) {
          if (el.hasClass('stats')) {
            scope.hideStats();
          } else {
            scope.showStats();
          }
        });

        scope.showStats = function () {
          scope.loading = true;
          el.addClass('stats');

          var tpl = angular.element(playerStatsTemplate);
          el.append(tpl);
          el[0].querySelector('.player-stats').style.top = el[0].offsetHeight + "px";

          $compile(tpl)(scope);

          // Get player data
          playerService.get(scope.player.Player.id).then(function (data) {
            scope.stats = {
              time: 0,
              goals: 0,
              conceeded: 0,
              assists: 0
            };

            _.each(data.Player.Stat, function (el) {
              scope.stats.time += el.value.time;
              scope.stats.goals += el.value.goals.length;
              scope.stats.conceeded += el.value.conceeded.length;
              scope.stats.assists += el.value.assists.length;
            });

            scope.loading = false;
          });
        };

        scope.hideStats = function () {
          el.removeClass('stats');
          el[0].querySelector('.player-stats').remove();
        };
      }
    };
  })

  .directive('psNews', function (newsService) {
    return {
      restrict: 'E',
      scope: {
        limit: '='
      },
      template: psNewsTemplate,
      link: function (scope, elem, attrs) {
        scope.news = [];
        newsService.getPS().then(function (res) {
          scope.news = res;
        });
      }
    };
  })

  .directive('faNews', function (newsService) {
    return {
      restrict: 'E',
      scope: {
        limit: '='
      },
      template: faNewsTemplate,
      link: function (scope, elem, attrs) {
        scope.news = [];
        newsService.getFA().then(function (res) {
          scope.news = res;
        });
      }
    };
  })

  .directive('faPopover', function ($timeout, faHelpService, HelpContent, HelpTypes, HelpModulesIDs) {
    return {
      restrict: 'A',
      transclude: true,
      templateUrl: 'directives/fa-popover.html',
      scope: {
        triggerEvent: '@',
        content: '&',
        placement: '@',
        className: '@',
        appendToBody: '&?'
      },
      link: function (scope, element) {
        scope.appendToBody = scope.appendToBody ? scope.appendToBody() : false;
        scope.placement = scope.placement ? scope.placement : 'bottom';
        scope.className = scope.className ? scope.className : '';
        scope.triggerEvent = scope.triggerEvent ? scope.triggerEvent : 'click';

        element.bind('click', function () {
          $timeout(function () {
            scope.help = {
              selected: faHelpService.getCurrentModule() || faHelpService.getModuleById(HelpModulesIDs.NoHelpForCurrentState),
              modules: _.filter(HelpContent, function (module) {
                return module.type === HelpTypes.About;
              })
            };
            scope.helpModulesIDs = HelpModulesIDs;

            scope.openPopup();
          });
        });

      },
      controller: 'faPopoverController'
    };
  })

  .directive('notifyGtm', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attributes, ngModel) {
        element.bind('change', function () {

          var el = element && element[0] ? element[0] : '';

          if (el) {

            dataLayer.push({
              event: 'ngNotifyGtm',
              attributes: {
                element: el,
                modelValue: ngModel.$modelValue,
                viewValue: ngModel.$viewValue
              }
            });

          }

        });
      }
    };
  })

  .directive('email', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attributes, ctrl) {
        ctrl.$validators.email = function(value) {
          var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

          if (ctrl.$isEmpty(value)) {
            return false;
          } else if (value.match(emailRegex)) {
            return true;
          } else {
            return false;
          }
        };
      }
    };
  })

  .directive('password', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attributes, ctrl) {
        ctrl.$validators.password = function(value) {
          
          if (ctrl.$isEmpty(value)) {
            return false;
          } else if (value.length <8) {
            return false;
          } else {
            return true;
          }
        };
      }
    };
  })

  .directive('number', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attributes, ctrl) {
        ctrl.$validators.number = function(value) {
          
          if (ctrl.$isEmpty(value)) {
            return false;
          } else if (isNaN(value)) {
            return false;
          } else {
            return true;
          }
        };
      }
    };
  })
