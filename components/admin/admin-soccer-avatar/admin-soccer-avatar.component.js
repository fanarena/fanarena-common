import template from './admin-soccer-avatar.component.tpl.html';

export default {
  template,
  bindings: {
    player: '=',
    teamId: '<',
    points: '=',
    preventClick: '=',
    popoverClick: '=',
    showTransfer: '<',
    remove: '='
  },
  controller: function (RESOURCES, $rootScope, $state, $stateParams, playerService, growl) {
    var vm = this;
    vm.resources = RESOURCES;
    vm.popoverTpl = "playerPopover.html";

    $rootScope.$on('popover:close', function (ev, args) {
      vm.player.popoverOpen = false;
    });

    vm.click = function () {
      // Show popover?
      if (!vm.popoverClick) {
        vm.goToProfile();
      }
      else {
        // Close all others
        $rootScope.$broadcast('popover:close');
        vm.player.popoverOpen = true;
      }
    };

    vm.goToProfile = function () {
      if (vm.teamId !== undefined && !vm.preventClick)
        $state.go('teamPlayer', { teamId: vm.teamId, playerId: vm.player.players.id });
      else if (!vm.preventClick) {
        if (vm.player.hasOwnProperty('player_id'))
          $state.go('publicPlayer', { playerId: vm.player.player_id });
        else
          $state.go('publicPlayer', { playerId: vm.player.players.id });
      }
    };

    vm.transfer = function () {
      playerService.transfer(vm.player.picks.id, vm.player.picks.team_id).then(function (res) {
        growl.addSuccessMessage(vm.player.players.name + ' was transferred out!');
        $rootScope.$broadcast('bench:remove', { obj: vm.player });
      });
    };
  }
};