import template from './admin-templates.component.tpl.html';

export default {
  template,
  controller: function ($location, base64, $rootScope, layoutService) {
    var vm = this;

    layoutService.hideFooter();
    layoutService.hideHeader();

    $rootScope.hideElements = true;

    vm.generatorInput = base64.decode($location.search().templateInfo);

    vm.params = base64.decode($location.search().templateInfo);
    vm.starting = JSON.parse(vm.params).starting;
  }
};