import AdminContentGenrator from './admin-content-generator/admin-content-generator.component';
import AdminMain from './admin-main/admin-main.component';
import AdminSoccerAvatar from './admin-soccer-avatar/admin-soccer-avatar.component';
import AdminTemplates from './admin-templates/admin-templates.component';

export {
  AdminContentGenrator,
  AdminMain,
  AdminSoccerAvatar,
  AdminTemplates
};