import template from './admin-content-generator.component.tpl.html';

export default {
  template,
  bindings: {
    adminCompetitions: '<',
    seasonId: '<'
  },
  controller: function (playerService, gameService, adminService, teamService, SPORTS_ASSETS, $location,
                        gettextCatalog, PlayerService) {
    var vm = this;
    vm.starting = {};
    vm.playersPoints = {};
    vm.players = [];
    vm.clubs = [];
    vm.deadlineWeeks = [];
    vm.competitions = vm.adminCompetitions || gameService.getCompetitions();
    vm.budget = 84;
    vm.removeEnabled = true;

    vm.currentCompetition = vm.competitions[0];

    vm.competitionChanged = function () {
      activate();
    };

    vm.assetsURL = SPORTS_ASSETS.football.remote;

    vm.pagination = {current: 1};
    vm.searchPlayer = {value: ''};
    vm.filterOptions = {};
    vm.filterItem = {
      club: {Club: {id: 0, name: 'All clubs'}},
      position: {id: 0, min: 0, inTeam: 0, name: 'All positions', type: ''}
    };
    vm.displayWeek = null;

    vm.customFilter = customFilter;
    vm.dataFetched = dataFetched;
    vm.pickedAllready = pickedAllready;
    vm.pick = pick;
    vm.isPickable = isPickable;
    vm.generateImage = generateImage;
    vm.onPlayerRemove = onPlayerRemove;

    vm.positions = [
      {id: 0, min: 0, inTeam: 0, name: 'All positions', type: ''},
      {id: 1, min: 1, inTeam: 0, name: 'Goalkeepers', type: 'gk'},
      {id: 2, min: 4, inTeam: 0, name: 'Defenders', type: 'def'},
      {id: 3, min: 4, inTeam: 0, name: 'Midfielders', type: 'mf'},
      {id: 4, min: 2, inTeam: 0, name: 'Forwards', type: 'fwd'},
    ];

    vm.starting = {
      'Goalkeeper': [],
      'Defender': [],
      'Midfielder': [],
      'Forward': []
    };

    activate();

    function getPlayersValues() {
      if (vm.displayWeek.id === -1) {
        return playerService.getAllWithStats(vm.currentCompetition.competition_feed, vm.seasonId)
          .then(Players => ({Players}));
      } else {
        return playerService.getAllStatsByWeekId(vm.currentCompetition.competition_feed, vm.seasonId, vm.displayWeek.id);
      }
    }

    function fetchPlayersAndClubs() {
      let tempPlayers = null;
      getPlayersValues()
        .then(function (result) {
          tempPlayers = result.Players;
          return gameService.getClubs(vm.currentCompetition.competition_feed)
        })
        .then(function (result) {
          vm.clubs = [vm.filterItem.club].concat(result);
          vm.filterOptions = {
            clubs: vm.clubs,
            positions: vm.positions
          };
          vm.players = tempPlayers
            .map(player => {
              const playerClub = vm.clubs.find(club => club.Club.id === player.Club.id);
              const clubObject = playerClub && playerClub.Club;
              if (clubObject) {
                return Object.assign({}, player, {Club: clubObject});
              } else {
                return player;
              }
            })
            .sort(function (playerA, playerB) {
              var playerAPoints = playerA && playerA.Stat && playerA.Stat
                .reduce(function (acc, playerAStat) {
                  return acc + playerAStat.points;
                }, 0);

              var playerBPoints = playerB && playerB.Stat && playerB.Stat
                .reduce(function (acc, playerBStat) {
                  return acc + playerBStat.points;
                }, 0);

              vm.playersPoints[playerA.Player.id] = playerAPoints;
              vm.playersPoints[playerB.Player.id] = playerBPoints;

              if (playerAPoints < playerBPoints) {
                return 1;
              }

              if (playerAPoints > playerBPoints) {
                return -1;
              }
              return 0;
            });
        });
    }

    function activate() {
      teamService.getDeadlineWeeks(vm.seasonId, vm.currentCompetition.competition_feed)
        .then(function (result) {
          vm.deadlineWeeks = []
            .concat(result.weeks)
            .map(item => ({
              id: item.week_id,
              label: `Match day ${item.week_id}`
            }))

          vm.deadlineWeeks.unshift({
            label: 'all gameweeks',
            id: -1
          });


          vm.displayWeek = vm.deadlineWeeks.find(function (week) {
            return result.week.display_week === week.id;
          });
          return fetchPlayersAndClubs();
        })

    }

    function onPlayerRemove(player, e) {
      console.log('player', player);
      e.preventDefault();
      e.stopPropagation();

      let removeIndex = null;
      let positionName = '';

      if (player.player_position_id === 1) {
        positionName = 'Goalkeeper';
      } else if (player.player_position_id === 2) {
        positionName = 'Defender';
      } else if (player.player_position_id === 3) {
        positionName = 'Midfielder';
      } else if (player.player_position_id === 4) {
        positionName = 'Forward';
      }

      vm.budget += player.value;

      const removedPlayer = vm.starting[positionName]
        .find((startingPlayer, index) => {
          const foundPlayer =
            startingPlayer.player_position_id === player.player_position_id;
          if (foundPlayer) {
            removeIndex = index;
          }
          return !!removedPlayer;
        });

      const newPositionObject = {};
      newPositionObject[positionName] = vm.starting[positionName]
        .map((item, index) => {
          if (index === removeIndex) {
            return null;
          } else {
            return item;
          }
        });

      newPositionObject[positionName] = newPositionObject[positionName]
        .filter(item => !!item);

      vm.starting = Object.assign({}, vm.starting, newPositionObject);
    }

    vm.weekChanged = function () {
      fetchPlayersAndClubs();
    };

    function customFilter(player) {
      if (vm.filterItem.club.Club && vm.filterItem.club.Club.id !== 0 && player.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      } else if (vm.filterItem.position.id !== 0 && player.Player.position_id !== vm.filterItem.position.id) {
        return false;
      } else if (vm.searchPlayer.value !== '' && player.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) === -1) {
        return false;
      } else {
        return true;
      }
    }

    function pickedAllready(player) {
      return isPickedInPosition('Goalkeeper', player) || isPickedInPosition('Defender', player) || isPickedInPosition('Midfielder', player) || isPickedInPosition('Forward', player);
    }

    function isPickable(player) {
      return !isPickedInPosition('Goalkeeper', player) &&
        !isPickedInPosition('Defender', player) &&
        !isPickedInPosition('Midfielder', player) &&
        !isPickedInPosition('Forward', player);
    }

    function isPickedInPosition(position, player) {
      return vm.starting[position].find(function (pickedPlayer) {
        return pickedPlayer.player_id === player.Player.id;
      });
    }

    function pick(player) {
      switch (player.Player.position_id) {
        case 1:
          vm.starting.Goalkeeper.push(playerBasicInfo(player));
          break;
        case 2:
          vm.starting.Defender.push(playerBasicInfo(player));
          break;

        case 3:
          vm.starting.Midfielder.push(playerBasicInfo(player));
          break;

        case 4:
          vm.starting.Forward.push(playerBasicInfo(player));
          break;
      }
    }

    vm.getPositionNameById = function (positionId) {
      switch (positionId) {
        case 0:
          return gettextCatalog.getString('Coach');
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
        default:
          return gettextCatalog.getString('Goalkeeper');
      }
    };

    function dataFetched() {
      return vm.players.length && vm.clubs.length && vm.deadlineWeeks.length;
    }

    function playerBasicInfo(player) {
      vm.budget -= player.Player.value;
      return {
        player_id: player.Player.id,
        player_position_id: player.Player.position_id,
        value: player.Player.value,
        player_points: vm.playersPoints[player.Player.id] || 0,
        player_short: player.Player.short || 0,
        club_infostrada_id: player.Club.infostrada_id,
        club_id: player.Club.id,
      }
    }

    function generateImage() {
      var data = {
        template: 'team-of-the-week',
        starting: vm.starting
      };

      adminService.generateImage(data);
    }
  }
};