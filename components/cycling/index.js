import CyclingCalendar from './cycling-calendar/cycling-calendar.component';
import CyclingContact from './cycling-contact/cycling-contact.component';
import CyclingCyclist from './cycling-cyclist/cycling-cyclist.component';
import CyclingCyclistProfile from './cycling-cyclist-profile/cycling-cyclist-profile.component';
import CyclingFaq from './cycling-faq/cycling-faq.component';
import CyclingHome from './cycling-home/cycling-home.component';
import CyclingJoinLeague from './cycling-join-league/cycling-join-league.component';
import CyclingLeague from './cycling-league/cycling-league.component';
import CyclingLogin from './cycling-login/cycling-login.component';
import CyclingMatchInfo from './cycling-match-info/cycling-match-info.component';
import CyclingNewGame from './cycling-new-game/cycling-new-game.component';
import CyclingPlatoon from './cycling-platoon/cycling-platoon.component';
import CyclingPlayersList from './cycling-players-list/cycling-players-list.component';
import CyclingPlayersStats from './cycling-players-stats/cycling-players-stats.component';
import CyclingPrizes from './cycling-prizes/cycling-prizes.component';
import CyclingPrizesMd from './cycling-prizes/cycling-prizes-md-component';
import CyclingPrizesFr from './cycling-prizes/cycling-prizes-fr-component';
import CyclingRace from './cycling-race/cycling-race.component';
import CyclingRaces from './cycling-races/cycling-races.component';
import CyclingRegister from './cycling-register/cycling-register.component';
import CyclingResetPassword from './cycling-reset-password/cycling-reset-password.component';
import CyclingRules from './cycling-rules/cycling-rules.component';
import CyclingSquad from './cycling-squad/cycling-squad.component';
import CyclingSquadTeam from './cycling-squad-team/cycling-squad-team.component';
import CyclingSquadTeamEdit from './cycling-squad-team-edit/cycling-squad-team-edit.component';
import CyclingSquadTransfer from './cycling-squad-transfer/cycling-squad-transfer.component';
import CyclingSquadTransfers from './cycling-squad-transfers/cycling-squad-transfers.component';
import CyclingUserProfile from './cycling-user-profile/cycling-user-profile.component';
import CyclingRankingPrize from './cycling-ranking-prize/cycling-ranking-prize.component';
import CyclingSquadSimpleTransfer from './cycling-squad-simple-transfer/cycling-squad-simple-transfer.component';
import CyclingSimpleMatchInfo from './cycling-simple-match-info/cycling-simple-match-info.component';

import CyclingEndwinnerWidget from './cycling-endwinner-widget/cycling-endwinner-widget.component';
import CyclingWinnerWidget from './cycling-winner-widget/cycling-winner-widget.component';
import CyclingMostPickedWidget from './cycling-most-picked-widget/cycling-most-picked-widget.component';
import CyclingPrizesWidget from './cycling-prizes-widget/cycling-prizes-widget.component';
import CyclingTopTransfersWidget from './cycling-top-transfers-widget/cycling-top-transfers-widget.component';
import CyclingTourWidget from './cycling-tour-widget/cycling-tour-widget.component';
import CyclingPostCode from './cycling-postcode/cycling-postcode.component';


export {
  CyclingCalendar,
  CyclingContact,
  CyclingCyclist,
  CyclingCyclistProfile,
  CyclingFaq,
  CyclingHome,
  CyclingJoinLeague,
  CyclingLeague,
  CyclingLogin,
  CyclingMatchInfo,
  CyclingNewGame,
  CyclingPlatoon,
  CyclingPlayersList,
  CyclingPlayersStats,
  CyclingPrizes,
  CyclingPrizesMd,
  CyclingPrizesFr,
  CyclingRace,
  CyclingRaces,
  CyclingRegister,
  CyclingResetPassword,
  CyclingRules,
  CyclingSquad,
  CyclingSquadTeam,
  CyclingSquadTeamEdit,
  CyclingSquadTransfer,
  CyclingSquadTransfers,
  CyclingUserProfile,
  CyclingRankingPrize,
  CyclingEndwinnerWidget,
  CyclingWinnerWidget,
  CyclingMostPickedWidget,
  CyclingPrizesWidget,
  CyclingTopTransfersWidget,
  CyclingTourWidget,
  CyclingSquadSimpleTransfer,
  CyclingSimpleMatchInfo,
  CyclingPostCode
};