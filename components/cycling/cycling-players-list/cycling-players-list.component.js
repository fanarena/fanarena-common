import template from './cycling-players-list.component.tpl.html';

export default {
  bindings: {
    competitionInfo: '<',
    localAssets: '<',
    gameName: '<',
    lastGameWeek: '<',
    hasContainerClass: '<',
  },
  template,
  controller: function (playerService, gameService, $rootScope, gettextCatalog) {
    var vm = this;
    vm.selectedCyclist = null;
    vm.selectedCyclistInfo = null;
    vm.selectedCyclistFinalGameBonus = null;
    vm.currentLanguage = gettextCatalog.getCurrentLanguage();

    $rootScope.title = gettextCatalog.getString("Players List");

    vm.selectCyclist = selectCyclist;
    vm.clubs = [];

    vm.allTeamsLabel = gettextCatalog.getString('Alle ploegen');

    vm.downloadPdfLabel = gettextCatalog.getString('PLAYER LIST (PDF)');
    vm.downloadXlsLabel = gettextCatalog.getString('PLAYER LIST (EXCEL)');

    if (['GCX'].indexOf(vm.competitionInfo.competitionFeed) !== -1) {
      vm.allTeamsLabel = gettextCatalog.getString('All countries')
    }

    if (['GCX', 'GKLAS'].indexOf(vm.competitionInfo.competitionFeed) !== -1) {
      vm.downloadPdfLabel = gettextCatalog.getString('CYCLING PLAYER LIST (PDF)');
      vm.downloadXlsLabel = gettextCatalog.getString('CYCLING PLAYER LIST (EXCEL)');
    }

    vm.competitionPositions = {
      GCX: [
        { id: -1, name: gettextCatalog.getString('All categories') },
        { id: 10, name: gettextCatalog.getString('Elite') },
        { id: 9, name: gettextCatalog.getString('Dames') },
        { id: 8, name: gettextCatalog.getString('Beloften') }
      ]
    };

    vm.positions = vm.competitionPositions[vm.competitionInfo.competitionFeed] || [];

    vm.filterOptions = {
      clubs: [],
      budgets: [
        { budget: 100, text: gettextCatalog.getString('All prizes') },
        { budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}` },
        { budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}` },
        { budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}` },
        { budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}` },
        { budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}` }
      ],
      selectedPosition: vm.positions[0] || null
    };

    vm.filterItem = {
      budget: 999999999,
      text: gettextCatalog.getString('All prizes')
    };


    vm.getPositionNameById = function (positionId) {
      const position = vm.positions.find(item => item.id === positionId);
      return (position && position.name) || ''
    };

    vm.getClubName = clubId => {
      if (vm.clubs && vm.clubs.length) {
        const foundClub = vm.clubs.find(club => club.Club.id === clubId);
        if (foundClub && foundClub.Club) {
          return foundClub.Club.name
        }
      }
      return '';
    };

    vm.customFilter = function (cyclist) {
      if (vm.filterItem.budget && cyclist.Player.value >= vm.filterItem.budget.budget) {
        return false;
      }

      else if (vm.filterOptions.selectedPosition && vm.filterOptions.selectedPosition.id !== -1 && vm.positions && vm.positions.length && cyclist.Player.position_id !== vm.filterOptions.selectedPosition.id) {
        return false;
      }

      if (vm.filterItem.club && vm.filterItem.club.Club.id !== 0 && cyclist.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      } else if (vm.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    vm.filterChanged = function () {
      vm.pagination.current = 1;
    };
    vm.searchPlayer = { value: "" };
    vm.pagination = { current: 1 };


    activate();

    function formatSelectedPlayer(playerInfo) {
      vm.selectedCyclistInfo = playerInfo;
      const hasSelectedCyclistFinalGameBonus = vm.selectedCyclistInfo && vm.selectedCyclistInfo.Player && vm.selectedCyclistInfo.Player.Stat &&
        vm.selectedCyclistInfo.Player.Stat.find(item => item.week_id === vm.lastGameWeek);

      if(hasSelectedCyclistFinalGameBonus) {
        const value = JSON.parse(hasSelectedCyclistFinalGameBonus.value);
        vm.selectedCyclistFinalGameBonus = Object.keys(value)
          .map(type => {
            return {
              type: gettextCatalog.getString(`jersey-${type}`),
              points: value[type].points, 
              rank: value[type].rank, 
            }
          });
      } else {
        vm.selectedCyclistFinalGameBonus = null;
      }

      if (vm.selectedCyclistInfo && vm.selectedCyclistInfo.Player && vm.selectedCyclistInfo.Player.Stat) {
        vm.selectedCyclistInfo.Player.Stat = vm.selectedCyclistInfo.Player.Stat.filter(item => item.week_id !== vm.lastGameWeek);
      }
    }

    function activate() {
      playerService.getAll(vm.competitionInfo.competitionFeed, vm.competitionInfo.seasonId)
        .then(function (cyclists) {
          vm.cyclists = cyclists.sort((firstCyclist, secondCyclist) => {
            if (firstCyclist.Player.value > secondCyclist.Player.value) {
              return -1;
            } else if (firstCyclist.Player.value < secondCyclist.Player.value) {
              return 1;
            } else {
              return 0;
            }
          });
          vm.selectedCyclist = vm.cyclists[0];
          return gameService.getClubs(vm.competitionInfo.competitionFeed, vm.competitionInfo.seasonId);
        })
        .then(function (clubs) {
          vm.clubs = clubs;
          vm.clubs = vm.clubs.map(club => {
            club.Club.name = gettextCatalog.getString(club.Club.name);
            return club;
          });
          vm.clubs.unshift({ Club: { id: 0, name: vm.allTeamsLabel } });
          vm.filterOptions.clubs = vm.clubs;
          vm.filterItem = {
            club: vm.clubs[0],
            budget: vm.filterOptions.budgets[0]
          };
        })
        .then(function () {
          return playerService.get(vm.selectedCyclist.Player.id, null, vm.competitionInfo.competitionFeed, vm.competitionInfo.seasonId)
        })
        .then(formatSelectedPlayer);
    }

    vm.getValueOject = function (value) {
      return angular.fromJson(value);
    };

    function selectCyclist(cyclist) {
      vm.selectedCyclist = cyclist;

      playerService.get(vm.selectedCyclist.Player.id, null, vm.competitionInfo.competitionFeed, vm.competitionInfo.seasonId)
        .then(formatSelectedPlayer);
    }
  }
};