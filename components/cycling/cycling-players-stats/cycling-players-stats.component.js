import template from './cycling-players-stats.component.tpl.html';

export default {
  template,
  controller: function ($rootScope, gettextCatalog) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Players Stats");

    activate();

    function activate() {
      vm.stats = [
        {
          day: 1,
          match: 'John S',
          time: 84
        },
        {
          day: 2,
          match: 'Filip D',
          time: 77
        },
        {
          day: 3,
          match: 'David T',
          time: 31
        },
        {
          day: 4,
          match: 'Jim T',
          time: 99
        },
        {
          day: 5,
          match: 'Jim F',
          time: 32
        },
        {
          day: 6,
          match: 'Jarvis A',
          time: 99
        }
      ];
    }
  }
};