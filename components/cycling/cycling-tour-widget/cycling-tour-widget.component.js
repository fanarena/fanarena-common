import template from './cycling-tour-widget.component.tpl.html';

export default {
  template,
  controller: function (appService) {
    var vm = this;
    vm.setActiveActiveTab = setActiveActiveTab;
    vm.getTabContent = getTabContent;
    vm.nextTab = nextTab;
    var isFrench = appService.translateToFrance();


    activate();

    function activate() {
      vm.activeTab = 1;
    }

    function setActiveActiveTab(tab) {
      vm.activeTab = tab;
    }

    function nextTab() {
      var nextTab = vm.activeTab + 1;
      if(nextTab > 5) {
        vm.activeTab = 1;
      } else {
        vm.activeTab = nextTab;
      }
    }

    function getTabContent() {
      if(isFrench) {
        var content = {
          1: {
            text: "Vous rêvez de devenir <strong>manager de votre équipe cycliste</strong>? <strong>Le Giro d’Or</strong> fera de vous un vrai coach. Composez votre équipe pour Le Giro et gagnez <strong>10.000€ cash</strong> ou d’autres superbes prix."
          },
          2: {
            text: '<ol>' +
            '<li>Connectez ou enrégistrez & cliquez "Nouvelle Équipe".</li>' +
            '<li>Choisissez un nom original et 15 coureurs (max. 3 de la même équipe - max. 275 million) dont 1 superchef d\'équipe.</li>' +
            '<li>Indiquez si vous voulez participer dans un peloton avec vos amis ou collègues et confirmez votre équipe.</li>' +
            '</ol>'
          },
          3: {
            text: 'Nous avons le grand prix de €10.000. En plus vous pouvez gagner chaque semaine de prix multimedia, vélos,...'
          },
          4: {
            text: 'Fière des résultats de votre équipe? Partagez-les via Facebook, Twitter ou Google+ avec vos amis et tout le monde! Et que pensez-vous d\'un classement privé entre vous et vos collègues et amis? Invitez-les dans un peloton.'
          },
          5: {
            text: 'Ajoutez une nouvelle dimension au cyclisme et inscrivez vos équipes maintenant! Vivez La Giro encore en plus intense grâce au \'Giro d\'Or\'!'
          }
        };
      } else {
        var content = {
          1: {
            text: 'De <strong>Gouden Giro</strong> is het nieuwste wielerspel voor de <strong>echte kenners</strong>. Kies 15 renners en <strong>word ploegleider van je eigen team</strong>. Sprokkel zoveel mogelijk ritoverwinningen en <strong>ga voor de hoofdprijs van €10.000</strong> en nog véél meer geweldige prijzen.'
          },
          2: {
            text: '<ol>' +
            '<li>Registreer en klik op "Nieuwe ploeg".</li>' +
            '<li>Kies originele ploegnaam en 15 renners (max. 3 van dezelfde ploeg - samen max. 275 miljoen) waarvan 1 superkopman.</li>' +
            '<li>Geef aan of je samen met vrienden, familie of collega\'s in een peloton wil spelen en bevestig je ploeg(en).</li>' +
            '</ol>'
          },
          3: {
            text: 'Speel mee en maak kans op de hoofdprijs van €10.000. Daarnaast geven we élke speeldag ook nog tal van prijzen weg zoals Jaguar racefietsen of mountainbikes, laptops, friteuses, keukentoestellen...'
          },
          4: {
            text: 'Trots op de prestatie van jouw team? Deel ze via Facebook, Twitter of Google+ met vrienden, familie ' +
            'of de héle wereld! <br> Of wat dacht je van een privé-competitie met vrienden en/of collega\'s? Dankzij een' +
            ' peloton richt je een competitie op waaraan enkel diegene kunnen deelnemen die hiervoor werden uitgenodigd.'
          },
          5: {
            text: 'Geef de Ronde van Italië een extra dimensie en schrijf nu reeds je ploeg(en) in! Je kan namelijk je betaalde ploeg(en) nog aanpassen tot de eerstvolgende deadline!'
          }
        };
      }


      return content[vm.activeTab].text || '';
    }

    vm.content = "";
  }
};