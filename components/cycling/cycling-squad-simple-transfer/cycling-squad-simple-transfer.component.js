import template from './cycling-squad-simple-transfer.component.tpl.html';

export default {
  bindings: {
    squad: '<',
    cyclists: '<',
    clubs: '<',
    gameBudget: '<',
    localAssets: '<',
    gameName: '<',
    maxPlayersSameClub: '<',
    statesPrefix: '<',
    totalTransfers: '<',
    saveSingleTransfer: '<'
  },
  template,
  controller: function ($scope, $rootScope, teamService, growl, gettextCatalog, playerService, localStorageService,
                        analyticsService) {
    var vm = this;

    $rootScope.$broadcast('sidemenu:team:collapse', {team: this.squad.Team.Team.id});

    vm.squad = this.squad;
    vm.budget = getBudgetDefault();
    vm.displayWeek = vm.squad.weeks.display_week;
    vm.deadlineWeek = vm.squad.weeks.deadline_week;
    vm.transfersAllowed = vm.totalTransfers || 2;
    vm.transfersAtATime = vm.transfersAtOnce || 6;
    vm.hasTransferHistory = false;
    vm.inTransferWait = false;
    vm.outTransferWait = false;
    vm.user = localStorageService.get('user');
    $rootScope.title = gettextCatalog.getString("Squad Transfer");

    vm.resetTransfer = resetTransfer;
    vm.outPlayersLimitExceeded = outPlayersLimitExceeded;
    vm.inPlayersLimitExceeded = inPlayersLimitExceeded;
    vm.totalSavedTransfers = totalSavedTransfers;
    vm.onCyclistOut = onCyclistOut;
    vm.onCyclistIn = onCyclistIn;
    vm.isOutOfBudget = isOutOfBudget;
    vm.hasPlaceholdersFilled = hasPlaceholdersFilled;
    vm.confirmTransfer = confirmTransfer;
    vm.filterChanged = filterChanged;
    vm.isInMyTeam = isInMyTeam;
    vm.customFilter = customFilter;
    vm.hasAlreadyThreeCyclistFromTheSameTeam = hasAlreadyThreeCyclistFromTheSameTeam;
    vm.hasTransfer = hasTransfer;
    vm.getFrontMan = getFrontMan;
    vm.arrangeStarting = arrangeStarting;
    vm.alreadyPicked = alreadyPicked;
    vm.removePickedPlayer = removePickedPlayer;
    vm.canTransfer = canTransfer;
    vm.transfersToSave = transfersToSave;
    vm.firstTransferIdToSave = firstTransferIdToSave;
    vm.getAvailableTransfersSlots = getAvailableTransfersSlots;
    vm.getLineUpLayout = getLineUpLayout;


    // cycling parent
    vm.starting = vm.squad && vm.squad.starting;
    vm.frontman = getFrontMan();
    vm.starting = arrangeStarting();

    this.clubs = this.clubs.map(club => {
      club.Club.name = gettextCatalog.getString(club.Club.name);
      return club;
    });

    // clubs
    this.clubs.unshift({
      Club: {
        id: 0,
        name: gettextCatalog.getString('All clubs')
      }
    });

    // All players
    vm.cyclists = this.cyclists;
    vm.teamName = {
      value: ''
    };

    var competitionsBudges = {
      'CROS': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GCX': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'KLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`},
        {budget: 2, text: `${gettextCatalog.getString('Onder')} 2 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GIRO': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'TOUR': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'VUEL': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GKLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    vm.competitionPositions = {
      GCX: [
        {id: -1, name: gettextCatalog.getString('All categories')},
        {id: 10, name: gettextCatalog.getString('Elite')},
        {id: 9, name: gettextCatalog.getString('Dames')},
        {id: 8, name: gettextCatalog.getString('Beloften')}
      ]
    };

    vm.maxPicksForPositionByCompetition = {
      GCX: {
        10: 9,
        9: 3,
        8: 1
      }
    };

    const maxPicksForCurrentCompetition = vm.maxPicksForPositionByCompetition[vm.squad.Team.League.competition_feed];

    vm.positions = vm.competitionPositions[vm.squad.Team.League.competition_feed] || [];

    vm.filterOptions = {
      clubs: vm.clubs,
      budgets: competitionsBudges[vm.squad.Team.League.competition_feed],
      selectedPosition: vm.positions[0] || null
    };


    vm.filterItem = {
      club: this.clubs[0],
      budget: vm.filterOptions.budgets[0]
    };

    vm.searchPlayer = {value: ""};

    // Players pagination
    vm.pagination = {
      current: 1
    };

    vm.fieldDrag = false;
    vm.dragElem = null;

    vm.getPositionNameById = function (positionId) {
      const position = vm.positions.find(item => item.id === positionId);
      return (position && position.name) || ''
    };

    function resetTransfer() {
      teamService.resetTransfer(vm.squad.Team.Team.id, vm.squad.Team.League.competition_feed)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Transfer has been reset!'));
          return updateSquad();
        });
    }

    function outPlayersLimitExceeded() {
      var transferWithOutPlayers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.out_id;
      }) || [];

      return ((vm.pastTransfers && vm.pastTransfers.length) + transferWithOutPlayers.length) >= vm.transfersAllowed || vm.outTransferWait;
    }

    function alreadyPicked(cyclist, type) {
      var alreadyOut = vm.currentTransfers && vm.currentTransfers.find(function (transfer) {
        if (type === 'in') {
          return transfer.in_id === cyclist.Player.id;
        } else {
          return transfer.out_id === cyclist.Player.id || transfer.in_id === cyclist.Player.id;
        }
      });
      return !!alreadyOut;
    }

    function inPlayersLimitExceeded() {
      var transferWithInPlayers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.in_id;
      }) || [];
      return ((vm.pastTransfers && vm.pastTransfers.length) + transferWithInPlayers.length) >= vm.transfersAllowed || vm.inTransferWait;
    }

    function getAvailableTransfersSlots() {
      var currentTransfersFiltered = (vm.currentTransfers && vm.currentTransfers.filter(function (currentTransfer) {
        return currentTransfer.out_id !== vm.blancoId;
      })) || [];
      var availableTransfers = vm.transfersAllowed - currentTransfersFiltered.length - vm.pastTransfers.length;

      return availableTransfers < 0 ? 0 : availableTransfers;
    }

    function getBudgetDefault() {
      return parseInt(vm.gameBudget);
    }

    function totalSavedTransfers() {
      return vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.id;
      }).length;
    }

    function onCyclistOut(cyclist) {
      var isAllreadyOut = vm.currentTransfers.find(function (transfer) {
        return transfer.out_id === cyclist.Player.id;
      });

      if (isAllreadyOut) {
        growl.addErrorMessage(gettextCatalog.getString('Player is already out!'));
        return;
      }

      var outSlot = vm.currentTransfers.find(function (transfer) {
        return !transfer.out_id;
      });

      if (outSlot) {
        var linked = false;
        vm.currentTransfers.forEach(function (transfer) {
          if (!transfer.out_id && !linked) {
            transfer.out_id = cyclist.Player.id;
            transfer.Out = cyclist.Player;
            linked = true;
          }
        });
      } else {
        vm.currentTransfers.push({
          id: null,
          in_id: null,
          In: null,
          out_id: cyclist.Player.id,
          Out: cyclist.Player,
          team_id: vm.squad.Team.Team.id,
          week_id: vm.squad.weeks.deadline_week
        })
      }
      vm.budget += (cyclist.Player.value);

      const plannedTransfers = vm.currentTransfers.filter(transfer => !transfer.id && transfer.out_id);

      if (plannedTransfers.length === vm.transfersAtATime) {
        vm.outTransferWait = true;
      } else {
        vm.outTransferWait = false;
      }
    }

    function onCyclistIn(cyclist) {
      const maxPlayers = vm.maxPlayersSameClub || 2;
      const playersSelectedToLeave = vm.currentTransfers
        .filter(item => !item.id)
        .map(item => item.out_id);

      const startingFutureTransfers = vm.currentTransfers
        .filter(item => !item.id && item.in_id && item.out_id)
        .map(item => vm.cyclists.find(cyclist => cyclist.Player.id === item.in_id))

      if (playerService.breaksTheCyclingClubLimit(cyclist, vm.starting, maxPlayers, playersSelectedToLeave, startingFutureTransfers)) {
        growl.addErrorMessage(`${gettextCatalog.getString('You are allowed to pick maximum')} ${maxPlayers} ${gettextCatalog.getString('players from the same team!')}`);
        return;
      }

      var inSlot = vm.currentTransfers && vm.currentTransfers.find(function (transfer) {
        return !transfer.in_id;
      });

      if (inSlot) {
        var linked = false;
        vm.currentTransfers = vm.currentTransfers.map(function (transfer) {
          if (!transfer.in_id && !linked) {
            transfer.in_id = cyclist.Player.id;
            transfer.In = cyclist.Player;
            linked = true;
          }
          return transfer;
        });
      } else {
        vm.currentTransfers.push({
          id: null,
          in_id: cyclist.Player.id,
          In: cyclist.Player,
          out_id: null,
          Out: null,
          team_id: vm.squad.Team.Team.id,
          week_id: vm.squad.weeks.deadline_week
        })
      }
      vm.budget -= (cyclist.Player.value);

      const plannedTransfers = vm.currentTransfers.filter(transfer => !transfer.id && transfer.in_id);

      if (plannedTransfers.length === vm.transfersAtATime) {
        vm.inTransferWait = true;
      } else {
        vm.inTransferWait = false;
      }
    }

    function isOutOfBudget() {
      return vm.budget < 0;
    }

    function transfersToSave() {
      return vm.currentTransfers.filter(transfer => !transfer.id).length;
    }

    function firstTransferIdToSave() {
      const found = vm.currentTransfers.find(transfer => !transfer.id);
      return found.out_id;
    }

    function hasPlaceholdersFilled(transfer) {
      return !!transfer.in_id && !!transfer.out_id;
    }

    function confirmTransfer(transfer) {
      const firstTransfer = vm.currentTransfers.find(currentTransfer => !currentTransfer.id);
      const input = vm.saveSingleTransfer ?
        {
          in_id: transfer.in_id,
          out_id: transfer.out_id,
          week_id: transfer.week_id,
          team_id: transfer.team_id
        } :
        firstTransfer && {
          week_id: firstTransfer.week_id,
          team_id: firstTransfer.team_id,
          transfers: vm.currentTransfers
            .filter(currentTransfer => !currentTransfer.id)
            .map(currentTransfer => ({
              in_id: currentTransfer.in_id,
              out_id: currentTransfer.out_id
            }))
        };

      teamService.cyclingTransfer(input)
        .then(function (result) {
          growl.addSuccessMessage(result.data.message);

          analyticsService.addToDataLayer({
            event: 'transfer_done',
            game: vm.statesPrefix,
            user_id: vm.user.id
          });

          if (vm.saveSingleTransfer) {
            vm.currentTransfers = vm.currentTransfers.filter(function (transferRecord) {
              return transferRecord.out_id !== transfer.out_id && transferRecord.in_id !== transfer.in_id;
            });
          } else {
            vm.currentTransfers = [];
          }

          vm.inTransferWait = false;
          vm.outTransferWait = false;
          return updateSquad();
        });
    }

    function updateSquad() {
      return teamService.getById(vm.squad.Team.Team.id)
        .then(function (result) {
          vm.squad = result.data;
          prepareData();
        });
    }

    function filterChanged() {
      vm.pagination.current = 1;
    }

    function isInMyTeam(searchedCyclist) {
      return vm.starting && !!vm.starting.find(function (cyclist) {
        return cyclist.Player.id === searchedCyclist.Player.id;
      });
    }

    function customFilter(cyclist) {
      // Check budget
      if ((cyclist.Player.value) > vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if (vm.filterItem.club.Club.id !== 0 && cyclist.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }

      else if (vm.filterOptions.selectedPosition && vm.filterOptions.selectedPosition.id !== -1 && vm.positions && vm.positions.length && cyclist.Player.position_id !== vm.filterOptions.selectedPosition.id) {
        return false;
      }

      // Check name
      else if (vm.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      }
      else if (vm.isInMyTeam(cyclist)) {
        return false;
      }
      else {
        return true;
      }
    }

    activate();

    function activate() {
      return updateSquad()
        .then(function () {
          vm.displayWeek = vm.squad.weeks.display_week;
          vm.deadlineWeek = vm.squad.weeks.deadline_week;
        });
    }

    function prepareData() {
      vm.budget = getBudgetDefault();
      vm.starting = vm.squad && vm.squad.starting;
      vm.starting = arrangeStarting();
      vm.starting.forEach(function (cyclist) {
        vm.budget -= (cyclist.Player.value);
      });

      vm.pastTransfers = vm.squad.Team.Transfer.filter(function (transfer) {
        return transfer.week_id !== vm.squad.weeks.deadline_week;
      });

      var notConfirmedTransfers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !transfer.id;
      }) || [];

      if (notConfirmedTransfers.length) {
        notConfirmedTransfers.forEach(function (notConfirmedTransfer) {
          if (notConfirmedTransfer.in_id) {
            vm.budget -= notConfirmedTransfer.In.value;
          }

          if (notConfirmedTransfer.out_id) {
            vm.budget += notConfirmedTransfer.Out.value;
          }
        });
      }

      vm.currentTransfers = vm.squad.Team.Transfer.filter(function (transfer) {
        return transfer.week_id === vm.squad.weeks.deadline_week;
      }).concat(notConfirmedTransfers);
    }

    function hasAlreadyThreeCyclistFromTheSameTeam(cyclist) {
      var colleguesCount = vm.starting.filter(function (startingCyclist) {
        return startingCyclist.Player.club_id === cyclist.Player.club_id;
      }).length;

      vm.currentTransfers.forEach(function (transfer) {
        if (!transfer.id) {

          if (transfer.Out && transfer.Out.club_id === cyclist.Player.club_id) {
            colleguesCount -= 1;
          }

          if (transfer.In && transfer.In.club_id === cyclist.Player.club_id) {
            colleguesCount += 1;
          }
        }
      });

      return colleguesCount >= 3;
    }

    function hasTransfer() {
      return vm.squad && vm.squad.Team.Transfer.length > 0;
    }

    function getFrontMan() {
      return vm.starting && vm.starting.find(function (cyclist) {
        return !!parseInt(cyclist.Selection.captain);
      });
    }

    function arrangeStarting() {
      var withoutCaptain = vm.starting.filter(function (cyclist) {
        return !parseInt(cyclist.Selection.captain);
      });
      var captain = getFrontMan();
      return [captain].concat(withoutCaptain);
    }

    function removePickedPlayer(playerId, type) {
      var transfers = vm.currentTransfers.map(function (transfer) {
        if (type === 'out' && transfer.out_id === playerId) {
          vm.budget -= (transfer.Out.value);
          transfer.out_id = null;
          transfer.Out = null;
          vm.outTransferWait = false;
        }

        if (type === 'in' && transfer.in_id === playerId) {
          vm.budget += (transfer.In.value);
          transfer.in_id = null;
          transfer.In = null;
          vm.inTransferWait = false;
        }

        return (transfer.in_id || transfer.out_id) ? transfer : null;
      });

      vm.currentTransfers = transfers.filter(function (transfer) {
        return !!transfer;
      });
    }


    function getLineUpLayout() {
      const defaultLayout = 'default-background';

      if (vm.squad.Team.League.competition_feed === 'CROS') {
        if (vm.sponsor && vm.sponsor.Club) {
          return 'background-' + vm.sponsor.Club.id;
        } else {
          return defaultLayout;
        }
      } else if (vm.squad.Team.League.competition_feed === 'GCX') {
        return 'lineup-cx';
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        if (vm.sponsor && vm.sponsor.Club) {
          return 'background-' + vm.sponsor.Club.id + ' cycling-classic-lineup';
        } else {
          return 'klassiekers-layout cycling-classic-lineup';
        }

      } else if (['GIRO', 'TOUR', 'VUEL', 'GKLAS'].indexOf(vm.squad.Team.League.competition_feed) !== -1) {
        return 'lineup-cycling';
      } else {
        return defaultLayout;
      }
    };

    vm.getLocalDate = function (utcDate) {
      return moment.utc(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
    }

    vm.getClubName = function (clubId) {
      const foundClub = vm.clubs.find(club => club.Club.id === clubId);
      return (foundClub && foundClub.Club && foundClub.Club.name) || '';
    }

    function canTransfer() {
      return vm.pastTransfers && vm.pastTransfers.length < vm.totalTransfers;
    }
  }
};