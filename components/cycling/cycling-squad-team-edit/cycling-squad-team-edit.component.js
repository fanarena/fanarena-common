import template from './cycling-squad-team-edit.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    cyclists: '<',
    clubs: '<',
    sponsors: '<',
    gameBudget: '<',
    sponsorsEnabled: '<',
    localAssets: '<',
    gameName: '<',
    hasContainerClass: '<',
    maxPlayersSameClub: '<',
    statesPrefix: '<',
    playerValueDecimals: '<'
  },
  controller: function ($scope, $rootScope, teamService, growl, $state, $stateParams, gettextCatalog, playerService) {
    var vm = this;

    vm.teamId = $stateParams.id;

    vm.isMyTeam = $rootScope && $rootScope.teams && $rootScope.teams.find(function (team) {
      return team.Team.id == vm.teamId;
    });

    if (!vm.isMyTeam) {
      $state.go('404');
    }

    vm.squad = this.squad;
    vm.cyclists = this.cyclists;
    vm.clubs = this.clubs;
    vm.sponsor = getSponsor();
    vm.teamChanges = 0;
    vm.displayWeek = vm.squad.weeks.display_week;
    vm.deadlineWeek = vm.squad.weeks.deadline_week;
    vm.defaultWeek = vm.squad.week;
    vm.getClubName = getClubName;
    vm.competitionFeed = vm.squad.Team.League.competition_feed;
    vm.valueDecimals = vm.playerValueDecimals || 0;

    if (vm.competitionFeed === 'CROS') {
      vm.nationalClubs = _.sortBy(playerService.getDistinctCountriesByPlayers(vm.cyclists), 'name');
      vm.nationalClubs.unshift({id: 0, name: 'Land'});
      vm.selectedNationalClub = vm.nationalClubs[0];
    }

    if (vm.competitionFeed === 'GCX') {
      vm.totalFieldPlayers = 13;
    } else {
      vm.totalFieldPlayers = 15;
    }

    $rootScope.title = gettextCatalog.getString("Edit Team");
    initBudgetValue();
    vm.searchPlayer = {value: ''};
    vm.clubs = vm.clubs.map(club => {
      club.Club.name = gettextCatalog.getString(club.Club.name);
      return club;
    });

    var competitionsBudges = {
      'CROS': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GCX': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'KLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`},
        {budget: 2, text: `${gettextCatalog.getString('Onder')} 2 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GIRO': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'TOUR': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'VUEL': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GKLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ]
    };


    vm.competitionPositions = {
      GCX: [
        {id: -1, name: gettextCatalog.getString('All categories')},
        {id: 10, name: gettextCatalog.getString('Elite')},
        {id: 9, name: gettextCatalog.getString('Dames')},
        {id: 8, name: gettextCatalog.getString('Beloften')}
      ]
    };

    vm.maxPicksForPositionByCompetition = {
      GCX: {
        10: 9,
        9: 3,
        8: 1
      }
    };

    const maxPicksForCurrentCompetition = vm.maxPicksForPositionByCompetition[vm.competitionFeed];

    vm.positions = vm.competitionPositions[vm.competitionFeed] || [];
    vm.filterOptions = {
      clubs: vm.clubs,
      budgets: competitionsBudges[vm.competitionFeed]
    };

    vm.filterItem = {
      club: vm.clubs[0],
      budget: vm.filterOptions.budgets[0],
      selectedPosition: vm.positions[0] || null
    };

    vm.pagination = {
      current: 1
    };

    vm.isNotEmpty = function (cyclist) {
      return (cyclist && cyclist.Player) || (cyclist && cyclist.players);
    }

    vm.customFilter = function (cyclist) {
      // Check budget
      if (cyclist && cyclist.Player.value >= vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if (vm.filterItem.club.Club.id !== 0 && cyclist.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }

      else if (vm.filterItem.selectedPosition && vm.filterItem.selectedPosition.id !== -1 && vm.positions && vm.positions.length && cyclist.Player.position_id !== vm.filterItem.selectedPosition.id) {
        return false;
      }

      // Check national club
      else if (vm.competitionFeed === 'CROS' && vm.selectedNationalClub.id !== 0 && cyclist.National.id !== vm.selectedNationalClub.id) {
        return false;
      }

      // Check name
      else if (cyclist && vm.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    vm.getPositionNameById = function (positionId) {
      const position = vm.positions.find(item => item.id === positionId);
      return (position && position.name) || ''
    };

    vm.cyclistRemove = function (cyclist) {
      vm.budget += cyclist.Player.value;
      _.each(vm.starting, function (c, i) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === cyclist.Player.id) {
          vm.starting[i] = {};

          if (i == 0) {
            var firstNext = null;
            _.each(vm.starting, function (c2, i2) {
              if (c2 && c2.hasOwnProperty('Player') && c2.Player.id !== cyclist.Player.id && firstNext === null) {
                firstNext = i2;
              }
            });

            if (firstNext !== null) {
              vm.starting[0] = vm.starting[firstNext];
              vm.starting[firstNext] = {};
              vm.frontman = vm.starting[0];
            }
          }
        }
      });
    };

    vm.cyclistsSelected = function () {
      var count = 0;

      _.each(vm.starting, function (cyclist) {
        if (cyclist && cyclist.hasOwnProperty('Player'))
          count++;
      });

      return count;
    };

    vm.sponsorChanged = function () {
      vm.teamChanges += 1;
    };

    vm.frontmanChanged = function () {
      var tmp = vm.starting[0];

      if (Object.keys(tmp).length === 0) return 0;

      if (tmp.Selection) {
        tmp.Selection.captain = 0;
      }

      if (vm.frontman.Selection) {
        vm.frontman.Selection.captain = 1;
      }

      vm.teamChanges += 1;

      var index = null;
      _.each(vm.starting, function (c, i) {
        if (c && c.hasOwnProperty('Player') && c.Player.id == vm.frontman.Player.id)
          index = i;
      });

      vm.starting[0] = vm.frontman;
      vm.starting[index] = tmp;
    };

    vm.isPickable = function (cyclist) {
      var pickable = true;

      if (vm.competitionFeed === 'GCX') {
        const playersPickedSoFar = vm.starting
          .filter(item => item.Player && item.Player.id);

        if (playersPickedSoFar.length > 13) {
          pickable = false;
        }

        if (cyclist && cyclist.Player) {
          const pickedForPlayerPosition = playersPickedSoFar
            .filter(item => item && item.Player && item.Player.position_id === cyclist.Player.position_id);

          if (pickedForPlayerPosition.length >= maxPicksForCurrentCompetition[cyclist.Player.position_id]) {
            pickable = false;
          }
        }
      }

      if (cyclist.Player.value > vm.budget)
        return false;

      _.each(vm.starting, function (c) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === cyclist.Player.id) {
          pickable = false;
        }
      });

      if (!hasEmptySpots(vm.starting)) {
        pickable = false;
      }

      return pickable;
    };

    vm.teamLimitExceeded = function (cyclist) {
      const maxPlayers = vm.maxPlayersSameClub || 5;
      if (playerService.breaksTheCyclingClubLimit(cyclist, vm.starting, maxPlayers)) {
        return true;
      } else {
        return false;
      }
    };

    vm.pick = function (cyclist) {
      const maxPlayers = vm.maxPlayersSameClub || 5;
      if (playerService.breaksTheCyclingClubLimit(cyclist, vm.starting, maxPlayers)) {
        growl.addErrorMessage(`${gettextCatalog.getString('You are allowed to pick maximum')} ${maxPlayers} ${gettextCatalog.getString('players from the same team!')}`);
        return;
      }

      addToFirstAvailableSpot(cyclist, vm.starting);

      vm.budget -= cyclist.Player.value;
      vm.teamChanges += 1;
    };

    function hasEmptySpots(arr) {
      var hasEmpty = false;
      _.each(arr, function (player) {
        if (player && !player.hasOwnProperty('Player')) {
          hasEmpty = true;
        }
      });

      return hasEmpty;
    }

    function pickedFourthCyclistFromAClub(cyclist) {
      var pickedFromTheSameClub = vm.starting.filter(function (cyclistInStarting) {
        return cyclistInStarting.Player && cyclistInStarting.Player.club_id === cyclist.Player.club_id;
      });
      return pickedFromTheSameClub.length > 2;
    }

    function addToFirstAvailableSpot(cyclist, arr) {
      var index = null;
      _.each(arr, function (player, i) {
        if (player && !player.hasOwnProperty('Player') && index === null) {
          index = i;
        }
      });

      arr[index] = cyclist;
    }

    vm.getLineUpLayout = function () {
      const competitionFeed = vm.squad.Team.League.competition_feed;
      const defaultLayout = 'lineup-cycling';

      if (competitionFeed === 'CROS') {
        if (vm.sponsor && vm.sponsor.Club && vm.currentDisplayWeek.type !== 'worldchampionship') {
          return 'background-' + vm.sponsor.Club.id;
        } else {
          return defaultLayout;
        }
      } else if (competitionFeed === 'GCX') {
        return 'lineup-cx';
      } else if (competitionFeed === 'KLAS') {
        if (vm.sponsor && vm.sponsor.Club) {
          return 'background-' + vm.sponsor.Club.id + ' cycling-classic-lineup';
        } else {
          return 'klassiekers-layout cycling-classic-lineup';
        }

      } else {
        return defaultLayout;
      }
    };

    vm.saveSelection = function () {
      if (vm.teamChanges === 0) {
        return;
      }

      if (!vm.sponsor && vm.sponsorsEnabled) {
        growl.addErrorMessage(gettextCatalog.getString('No sponsor selected!'));
        return;
      }

      var picks = vm.starting
        .map(function (cyclist) {
          return cyclist && cyclist.Player && cyclist.Player.id;
        })
        .filter(item => !!item);

      if (picks.length < vm.totalFieldPlayers) {
        growl.addErrorMessage(gettextCatalog.getString(`You need ${vm.totalFieldPlayers} cyclists in your team!`));

        if (vm.competitionFeed === 'GCX') {
          growl.addErrorMessage(`${gettextCatalog.getString('you must have 9 elite runners, 3 dames runners and 1 beloften runner')}`);
        }
        return;
      }

      teamService.editCyclingSelection(
        vm.squad.Team.Team.id,
        vm.squad.weeks.deadline_week,
        picks,
        picks[0],
        vm.sponsor && vm.sponsor.Club.id,
        vm.competitionFeed
      )
        .then(function () {
          vm.teamChanges = 0;
          growl.addSuccessMessage(gettextCatalog.getString('Team has been successfully edited!'));
          return teamService.getById(vm.teamId)
        })
        .then(function (result) {
          vm.starting = arrangeStarting(result.data.starting);
          vm.frontman = getFrontMan();
        });
    };

    function getFrontMan() {
      return vm.starting.find(function (cyclist) {
        return cyclist.Selection.captain;
      });
    }

    function getSponsor() {
      var clubId = vm.squad.Team.Team.sponsor_id;
      return vm.sponsors && vm.sponsors.find(function (sponsor) {
        return sponsor.Club.id === clubId;
      });
    }

    function arrangeStarting(starting) {
      const structure = starting || vm.starting;
      var withClubs = structure && structure.map(function (cyclist) {
        var club = vm.clubs.find(function (clubItem) {
          return clubItem.Club.id == cyclist.Player.club_id
        });

        if (club) {
          return Object.assign({}, cyclist, {Club: club.Club})
        } else {
          return cyclist;
        }

      });

      var withoutCaptain = withClubs && withClubs.filter(function (cyclist) {
        return !cyclist.Selection.captain;
      });

      vm.frontman = withClubs.find(function (cyclist) {
        return cyclist.Selection.captain;
      });

      if (vm.frontman) {
        return [vm.frontman].concat(withoutCaptain) || [];
      } else {
        return withoutCaptain;
      }

    }

    vm.activated = false;

    activate();

    function activate() {
      return teamService.get(vm.squad.Team.Team.id, vm.defaultWeek)
        .then(function (data) {
          vm.squad = data;
          vm.starting = arrangeStarting(vm.squad && vm.squad.starting);
          initBudgetValue();
          vm.starting.forEach(function (cyclist) {
            vm.budget -= (cyclist.Player.value);
          });
          vm.displayWeek = vm.squad.weeks.display_week;
          vm.deadlineWeek = vm.squad.weeks.deadline_week;
          vm.frontman = getFrontMan();
          vm.activated = true;
          vm.lineupLayout = vm.getLineUpLayout();
        });
    }

    function initBudgetValue() {
      vm.budget = vm.gameBudget;
    }

    function getClubName(clubId) {
      const foundClub = vm.clubs.find(club => club.Club.id === clubId);
      return (foundClub && foundClub.Club && foundClub.Club.name) || '';
    }

    vm.goToCyclist = function (cyclist) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-cyclistProfile', {cyclistId: cyclist.Player.id});
      } else {
        $state.go('cyclistProfile', {cyclistId: cyclist.Player.id});
      }
    };
  }
};