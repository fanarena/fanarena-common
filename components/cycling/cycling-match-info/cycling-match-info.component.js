import template from './cycling-match-info.component.tpl.html';

export default {
  template,
  bindings: {
    matchInfo: '<',
    points: '<',
    competitionFeed: '<'
  },
  controller: function (CHANNELS, CYCLINGCOMPETITIONS, UtilsService, $state) {
    var vm = this;
    vm.matchChannels = [];
    vm.parcours = [];

    setMatchChannels(vm.matchInfo);
    setParcours(vm.matchInfo);

    function setMatchChannels(matchInfo) {
      if(matchInfo && matchInfo.channels) {
        vm.matchChannels = JSON.parse(matchInfo.channels);
      }
    }
    function setParcours(matchInfo) {
      if(matchInfo && matchInfo.parcours) {
        vm.parcours = JSON.parse(matchInfo.parcours);
      }
    }


    this.$onChanges = function (bindings) {
      if(bindings.matchInfo && bindings.matchInfo.currentValue && bindings.matchInfo.currentValue.channels) {
        setMatchChannels(bindings.matchInfo.currentValue);
        setParcours(bindings.matchInfo.currentValue);
      }
    };

    vm.getChannelIdByName = function (name) {
      return CHANNELS[name];
    };

    vm.goToMatch = function () {
      $state.go('cyclingRace', {race: vm.matchInfo.week_id});
    };

    vm.getCompetitionIdByName = function (name) {
      return CYCLINGCOMPETITIONS[name];
    };

    vm.localDate = UtilsService.getLocalDate;
  }
};