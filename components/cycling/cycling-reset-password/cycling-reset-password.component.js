import template from './cycling-reset-password.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<'
  },
  controller: function (userService, $window, growl, $location, localStorageService,
                        $stateParams, $rootScope, gettextCatalog) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Reset Password");

    vm.resetCode = $stateParams.code;
    vm.resetPassword = vm.verifyPassword = "";
    vm.sendReset = function (frm) {
      if (frm.$valid) {
        userService.resetPassword(vm.resetPassword, vm.resetCode)
          .then(function (res) {
            localStorageService.set('token', res.token);
            localStorageService.set('user', res.user.User);
            $window.location.href = '/home';
          });
      }
    };

  }
};