import template from './cycling-contact.component.tpl.html';

export default {
  template,
  controller: function ($rootScope, userService, growl, $state, gettextCatalog) {
    $rootScope.title = gettextCatalog.getString("Contact");
    $rootScope.$broadcast('sidemenu:team:collapse');

    var vm = this;

    vm.contactInfo = {
      first_name: '',
      last_name: '',
      phone: '',
      email: '',
      message: ''
    };

    vm.contact = function (frm) {
      if (frm.$valid) {
        userService.submitContact(vm.contactInfo).then(function (res) {
          growl.addSuccessMessage(res.data.message);
          $state.go('dashboard');
        });
      }
    };
  }
};