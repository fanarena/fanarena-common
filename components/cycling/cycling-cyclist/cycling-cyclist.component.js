import template from './cycling-cyclist.component.tpl.html';

export default {
  template,
  bindings: {
    player: '<',
    showShort: '<',
    onRemove: '=',
    showPoints: '<',
    useLocalFolder: '<',
    gameName: '<',
    statesPrefix: '<',
    showForename: '<',
    showShortPosition: '<',
    showPositionBackground: '<'
  },
  controller: function ($state, gettextCatalog) {
    var vm = this;

    vm.cyclist = this.player;
    vm.hasRemove = function () {
      return !!vm.onRemove;
    };
    vm.cyclistName = '';
    vm.remove = this.onRemove;

    initCyclistNameAndClub();

    this.$onChanges = function (bindings) {
      if (bindings.player && bindings.player.currentValue) {
        vm.player = bindings.player.currentValue;
        vm.cyclist = vm.player;
        initCyclistNameAndClub();
      }
    };

    function initCyclistNameAndClub() {
      vm.cyclistName = vm.cyclist && vm.cyclist.Player && vm.cyclist.Player.name;

      if (vm.showForename) {
        vm.cyclistName = vm.cyclist && vm.cyclist.Player && vm.cyclist.Player.forename;
      }

      if (vm.showShort && vm.cyclist && vm.cyclist.Player && vm.cyclist.Player.short) {
        vm.cyclistName = vm.cyclist.Player.short;
      }

      vm.clubId = (vm.cyclist && vm.cyclist.Club && vm.cyclist.Club.id) ||
        (vm.cyclist && vm.cyclist.Player && vm.cyclist.Player.club_id) ||
        (vm.cyclist && vm.cyclist.Club && vm.cyclist.Club.id) || null;
    }

    vm.getValueOject = function (value) {
      return angular.fromJson(value);
    };

    vm.remoteFolder = function () {
      return vm.assetRemoteFolder || 'fanarena';
    }

    vm.goToCyclist = function () {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-cyclistProfile', {cyclistId: vm.cyclist.Player.id});
      } else {
        $state.go('cyclistProfile', {cyclistId: vm.cyclist.Player.id});
      }
    };

    vm.getPositionShort = function (positionId) {
      switch (positionId) {
        case 10:
          return gettextCatalog.getString('elite-short-label');

        case 9:
          return gettextCatalog.getString('dames-short-label');

        case 8:
          return gettextCatalog.getString('beloften-short-label');
      }
    }

  }
};