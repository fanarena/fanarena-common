import template from './cycling-register.component.tpl.html';

export default {
  template,
  controller: function (userService, Piwik, localStorageService, $window, $state, GoogleAnalyticsService) {
    var vm = this;

    activate();

    // REGISTER
    vm.user = {
      first: null,
      last: null,
      email: null,
      password: null,
      referral: localStorageService.get('shareReferral'),
      timezone: jstz.determine().name(),
      coolblue: 0,
      street: null,
      post_code: null,
      city: null
    };

    vm.register = function (frm) {
      if (frm.$valid) {
        return GoogleAnalyticsService.getClientId()
          .then(function (clientId) {
            userService.register(
              vm.user.first,
              vm.user.last,
              vm.user.email,
              vm.user.password,
              vm.user.referral,
              vm.user.timezone,
              vm.user.coolblue,
              vm.user.street,
              vm.user.post_code,
              vm.user.city,
              clientId)
              .then(function (res) {
                localStorageService.remove('shareReferral');
                vm.show = false;

                // Piwik track registration
                Piwik.trackGoal(1);

                // FB Pixel
                fbq('track', 'CompleteRegistration', {
                  status: 1
                });

                // Google conversion
                var google_conversion_id = 881534302;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "rZf3CMvMi2oQ3sqspAM";
                var google_remarketing_only = false;

                ga('send', 'event', {
                  eventCategory: 'registration',
                  eventAction: 'submit',
                  eventLabel: 'newuser',
                  eventValue: 100
                });

                // Boot intercom
                userService.bootIntercom(res.user);

                if (localStorageService.get('shareLeague') !== null) {
                  $window.location.href = '/new-game';
                }
                else {
                  $window.location.href = '/home';
                }
              });
          });
      }
    };

    function activate() {
      if (localStorageService.get('token')) {
        $state.go('home');
      }
    }
  }
};