import template from './cycling-most-picked-widget.component.tpl.html';

export default {
  template,
  bindings: {
    cyclists: '<',
    gameName: '<'
  },
  controller: function (gettextCatalog) {
    var vm = this;
    vm.title = gettextCatalog.getString('Most picked cyclists');
    vm.span1 = "keer gekozen";
  }
};