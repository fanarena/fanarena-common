import template from './cycling-league.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    league: '<',
    shareRoute: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    competitionFeed: '<',
    enableLeagueSelection: '<',
    seasonId: '<',
    globalLeagueIds: '<',
    deadlineInfo: '<'
  },
  controller: function ($location, $scope, $rootScope, gettextCatalog, Hashids, leagueService,
                        teamService, growl, $state, APP_CONFIG, analyticsService, localStorageService, PROMO_BANNER_URL) {
    var vm = this;
    const currentUserTeamIds = $rootScope.teams.map(team => team.Team.id);

    vm.squad = this.squad;
    vm.league = this.league;
    vm.seasonId = this.seasonId;
    vm.shownLeague = {};
    vm.inviteURL = "";
    vm.pagination = {current: 1};
    vm.displayItems = 25;
    vm.totalRecords = vm.league.total_records;
    vm.processInviteURL = processInviteURL;
    vm.leagueStandings = vm.league.table.map(markOwnTeam);
    vm.joinLeague = joinLeague;
    vm.deleteTeam = deleteTeam;
    vm.gameWeekChanged = gameWeekChanged;
    vm.newLeagueName = '';
    vm.createLeague = createLeague;
    vm.leaveCompetition = leaveCompetition;
    vm.user = localStorageService.get('user');
    vm.onPageChange = onPageChange;
    vm.goToTeam = goToTeam;

    vm.promoBannerURL = PROMO_BANNER_URL[vm.competitionFeed];
    vm.promoBannerImage = `/images/promo-wide-banner-${vm.competitionFeed}.jpg`;

    vm.deadlineWeeksOptions = [{id: 0, label: gettextCatalog.getString('General')}]
      .concat(
        vm.deadlineInfo.weeks.map(
          week => ({id: week.week_id, label: `${gettextCatalog.getString('Matchday')} ` + week.week_id}))
          .filter(item => item)
      )

    vm.currentDisplayWeek = vm.deadlineWeeksOptions[0];

    vm.firstWeek = (vm.squad.weeks.deadline_week === vm.squad.Team.Team.week_id) ||
      (vm.editableTeamWeeks && vm.editableTeamWeeks.indexOf(vm.squad.weeks.deadline_week) !== -1);

    if (vm.enableLeagueSelection) {
      leagueService.getLeagues(vm.competitionFeed, vm.seasonId).then(function (result) {
        vm.listOfLeagues = result;
        vm.listOfLeagues.push({id: vm.league.League.League.id, name: vm.league.League.League.name});

        vm.selected = {value: vm.listOfLeagues[vm.listOfLeagues.length - 1]};
        vm.shownLeague = vm.league;
      });
    }

    function leaveCompetition() {
      vm.squad.Team.Team.league_id = vm.globalLeagueIds.find(item => item);

      return teamService.edit(vm.squad.Team.Team)
        .then(result => {
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');
          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {
              id: vm.squad.Team.Team.id,
              leagueId: vm.squad.Team.Team.league_id,
              '#': null
            });
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id, '#': null});
          }
        })
    }

    vm.isGlobalLeague = vm.league && vm.league.League ?
      vm.globalLeagueIds.indexOf(vm.league.League.League.id) !== -1 :
      true;

    $rootScope.title = gettextCatalog.getString("League");


    if (vm.league && vm.league.League) {
      const sharingRoute = vm.shareRoute || 'join-cx';
      if (vm.statesPrefix) {
        vm.shareLink = `https://${$location.host()}/${vm.statesPrefix}/${sharingRoute}/${Hashids.encode(vm.league.League.League.id)}`;
      } else {
        vm.shareLink = `https://${$location.host()}/${sharingRoute}/${Hashids.encode(vm.league.League.League.id)}`;
      }
    }

    // Delete
    function deleteTeam() {
      teamService
        .delete(vm.squad.Team.Team.id).then(function (res) {
        growl.addSuccessMessage('Successfully deleted!');

        // Refresh sidebar
        $rootScope.$broadcast('sidemenu:refresh');

        $state.go('home');
      });
    };

    function markOwnTeam(team) {
      team.Team.belongsToCurrentUser = currentUserTeamIds.indexOf(team.Team.id) !== -1;
      return team;
    }

    function processInviteURL() {
      var inviteCode = getInviteCode(vm.inviteURL);
      if (inviteCode) {
        vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.inviteLeague = data.League.League;
            vm.squad.Team.Team.league_id = data.League.League.id;
          }, function (err) {
            vm.inviteLeague = null;
          });
      }
    }

    function getLeagueTeamsStats() {
      if (vm.currentDisplayWeek.id !== 0) {
        return leagueService.get(vm.squad.Team.Team.league_id, vm.currentDisplayWeek.id, vm.pagination.current);
      } else {
        return leagueService.get(vm.squad.Team.Team.league_id, null, vm.pagination.current);
      }
    }

    function gameWeekChanged() {
      vm.pagination.current = 1;
      return getLeagueTeamsStats()
        .then(result => {
          vm.leagueStandings = result.table.map(markOwnTeam);
          vm.totalRecords = result.total_records;
        })
    }

    function joinLeague() {
      teamService.edit(vm.squad.Team.Team)
        .then(function () {
          return leagueService.get(vm.squad.Team.Team.league_id)
        })
        .then(function (result) {
          vm.league = result;
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');

          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id});
          } else {
            $state.go('cyclingLeague', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id});
          }
        });
    }

    function getInviteCode(invite) {
      if (invite && invite.length && invite.indexOf('/') !== -1) {
        var inviteURLParts = invite.split('/');
        return inviteURLParts[inviteURLParts.length - 1];
      } else {
        return null;
      }
    }

    function createLeague() {
      return leagueService.createLeagueWithExistingTeam(vm.squad.Team.Team.id, vm.newLeagueName, vm.competitionFeed)
        .then(newLeagueResult => {
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');
          analyticsService.addToDataLayer({
            event: 'superclass_creation',
            user_id: vm.user.id,
            game: vm.statesPrefix
          })
          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {
              id: vm.squad.Team.Team.id,
              leagueId: newLeagueResult.League.id,
              '#': null
            });
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: newLeagueResult.League.id, '#': null});
          }
        })
    }

    function onPageChange(newPageNumber) {
      vm.pagination.current = newPageNumber;
      return getLeagueTeamsStats()
        .then(result => {
          vm.leagueStandings = result.table.map(markOwnTeam);
          vm.totalRecords = result.total_records;
        })
    }

    function goToTeam(teamId) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-publicTeam', {id: teamId});
      } else {
        $state.go('publicTeam', {id: teamId});
      }
    }
  }
};