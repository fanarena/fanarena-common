import template from './cycling-races.component.tpl.html';

export default {
  bindings: {
    competitionInfo: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    showRestDays: '<',
    lastGameWeek: '<'
  },
  template,
  controller: function (teamService, $rootScope, gettextCatalog, $state, matchService, PROMO_BANNER_URL) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString('Races');

    vm.races = null;
    vm.getLocalDate = getLocalDate;
    vm.hideTypeColumn = ['GCX', 'GKLAS'].indexOf(vm.competitionInfo.competitionFeed) !== -1;

    vm.promoBannerURL = PROMO_BANNER_URL[vm.competitionInfo.competitionFeed];
    vm.promoBannerImage = `/images/promo-wide-banner-${vm.competitionInfo.competitionFeed}.jpg`;

    activate();

    function activate() {

      teamService.getCyclingMatches(vm.competitionInfo.seasonId, vm.competitionInfo.competitionFeed)
        .then(function (result) {
          const stages = result.stages
            .filter(stage => vm.lastGameWeek ? stage.matches.week_id < vm.lastGameWeek : true);

          const withShowDate = race => {
            const notFirstRaceInGameWeek = stages
              .find(item =>
                item.matches.week_id === race.matches.week_id
                && moment.utc(item.matches.date).local().isBefore(moment.utc(race.matches.date), 'hours')
              );
            return Object.assign({}, race, { showDate: !notFirstRaceInGameWeek });
          };

          if (vm.showRestDays) {
            vm.races = matchService
              .withBreakDays(stages)
              .map(withShowDate);
          } else {
            vm.races = stages
              .map(withShowDate);
          }
        });
    }

    vm.goToMatch = function (raceId) {
      if (raceId) {
        if (vm.statesPrefix) {
          $state.go(`${vm.statesPrefix}-cyclingRace`, { race: raceId });
        } else {
          $state.go('cyclingRace', { race: raceId });
        }
      }
    };

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('ddd DD MMMM HH:mm:ss');
    }
  }
};