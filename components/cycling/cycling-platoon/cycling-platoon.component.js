import template from './cycling-platoon.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<',
    inviteURL: '<',
    statesPrefix: '<'
  },
  controller: function ($stateParams, gameService, localStorageService, leagueService, $rootScope, growl, $state,
                        gettextCatalog, Hashids, $location, NewGameCacheService, analyticsService) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Platoon");

    vm.activeTab = 1;
    vm.inviteLeagueStorageKey = vm.inviteURL || 'cxInviteLeagueId';
    vm.inviteLeagueId = Hashids.decode(localStorageService.get(vm.inviteLeagueStorageKey))[0];
    vm.game = $stateParams.payload;
    vm.user = localStorageService.get('user');

    vm.setActiveTab = setActiveTab;
    vm.saveTeam = saveTeam;
    vm.inviteCodeChanged = inviteCodeChanged;
    vm.joinInviteLeague = joinInviteLeague;
    vm.createLeague = createLeague;
    vm.joinLeague = joinLeague;

    activate();

    function setActiveTab(tab) {
      vm.activeTab = tab;
    }

    function goToPayments() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-payments');
      } else {
        $state.go('dashboard');
      }
    }

    function goToPaymentsWithTeamInfo(res) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-payments', {subjectTeamInfo: res.team.Team});
      } else {
        $state.go('dashboard');
      }
    }

    function goToLeague(res) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-squadTeam', {id: res.team.Team.id, leagueId: res.team.Team.league_id});
      } else {
        $state.go('cyclingLeague', {id: res.team.Team.id, leagueId: res.team.Team.league_id});
      }
    }

    function saveTeam() {
      vm.game.captain = vm.game.picks[0];
      gameService.joinLeague(vm.game)
        .then(function (res) {
          if (res.error) {
            growl.addErrorMessage(res.error);
            goToPayments();
          } else {
            analyticsService.addToDataLayer({
              event: 'team_creation',
              game: vm.statesPrefix,
              total_team_numbers: ($rootScope.teams && $rootScope.teams.length && ($rootScope.teams.length + 1)) || 1,
              user_id: vm.user.id
            });

            growl.addSuccessMessage(gettextCatalog.getString('Team created!'));
            $rootScope.$broadcast('sidemenu:refresh');
            goToPaymentsWithTeamInfo(res);
          }
          resetCyclingCache();
        });
    }

    function resetCyclingCache() {
      localStorageService.set('cyclingPicks', null);
      NewGameCacheService.resetSportType();
      NewGameCacheService.resetGameMode();
      NewGameCacheService.resetProgress();
    }

    // Actual join league
    function joinLeague() {
      vm.game.captain = vm.game.picks[0];
      gameService.joinLeague(vm.game)
        .then(function (data) {
          if (data.error) {
            growl.addErrorMessage(data.error);
            NewGameCacheService.resetSportType();
            NewGameCacheService.resetGameMode();
            NewGameCacheService.resetProgress();
            goToPayments();
          } else {
            analyticsService.addToDataLayer({
              event: 'team_creation',
              game: vm.statesPrefix,
              total_team_numbers: ($rootScope.teams && $rootScope.teams.length && ($rootScope.teams.length + 1)) || 1,
              user_id: vm.user.id
            });

            growl.addSuccessMessage(gettextCatalog.getString('Team created!'));
            $rootScope.$broadcast('sidemenu:refresh');
            goToPaymentsWithTeamInfo(data);
          }
          resetCyclingCache();
          localStorageService.remove(vm.inviteLeagueStorageKey);
        }, function (err) {
          if (err.message)
            growl.addErrorMessage(err.message);
          NewGameCacheService.resetSportType();
          NewGameCacheService.resetGameMode();
          NewGameCacheService.resetProgress();
          $state.go('dashboard');
        });
    };


    function inviteCodeChanged() {
      vm.inviteLeague = null;
      vm.inviteLeagueId = null;

      if (vm.invite && vm.invite.length && vm.invite.length > 16) {
        var inviteCode = getInviteCode(vm.invite);
        if (inviteCode) {
          vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
          if (vm.inviteLeagueId) {
            leagueService.get(vm.inviteLeagueId)
              .then(function (data) {
                vm.inviteLeague = data.League.League;
              }, function (err) {
                vm.inviteLeague = null;
                localStorageService.remove(vm.inviteLeagueStorageKey);
              });
          }
        }
      }
    }

    function getInviteCode(invite) {
      if (invite && invite.length && invite.indexOf('/') !== -1) {
        var inviteURLParts = invite.split('/');
        return inviteURLParts[inviteURLParts.length - 1];
      } else {
        return null;
      }
    }

    if (vm.inviteLeagueId) {
      vm.activeTab = 2;
    }

    function joinInviteLeague() {
      //vm.frmTeamName.$setSubmitted();

      if (vm.game.teamName !== '') {
        vm.game.league = null;
        vm.game.league_id = vm.inviteLeague.id;
        vm.game.invite = vm.invite;
        vm.game.gameMode = {key: 'budget'};
        vm.game.captain = vm.game.picks[0];
        vm.joinLeague();
      }
      else {
        growl.addSuccessMessage(gettextCatalog.getString('Please choose a team name'));
      }
    };


    // Create league
    vm.createLeagueName = '';
    vm.createLeagueDeadline = null;
    vm.dateTimeBeforeRender = leagueService.validateDeadlinePicker;

    function createLeague() {
      if (vm.frmTeamName) {
        vm.frmTeamName.$setSubmitted();
      }
      vm.frmCreateLeague.$setSubmitted();

      var valid = true;

      if (vm.createLeagueName === '') {
        growl.addSuccessMessage(gettextCatalog.getString('Please choose a league name'));
        valid = false;
      }

      if (valid) {
        var deadline = moment(vm.createLeagueDeadline);
        deadline.utc();

        vm.game.league = {
          name: vm.createLeagueName,
          deadline: deadline.format('YYYY-MM-DD HH:mm:ss')
        };
        vm.game.gameMode = {
          key: 'budget'
        };
        vm.game.captain = vm.game.picks[0];

        gameService.createLeague(vm.game)
          .then(function (data) {
            if (data.error) {
              growl.addErrorMessage(data.error);
              goToPayments();
            } else {
              analyticsService.addToDataLayer({
                event: 'team_creation',
                game: vm.statesPrefix,
                total_team_numbers: ($rootScope.teams && $rootScope.teams.length && ($rootScope.teams.length + 1)) || 1,
                user_id: vm.user.id
              });

              analyticsService.addToDataLayer({
                event: 'superclass_creation',
                user_id: vm.user.id,
                game: vm.statesPrefix
              })

              growl.addSuccessMessage(gettextCatalog.getString('New game created!'));
              $rootScope.$broadcast('sidemenu:refresh');
              goToPaymentsWithTeamInfo(data);
            }
            resetCyclingCache();
          });
      }
    }

    function activate() {

      if (!vm.game) {
        if (vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-newGame');
        } else {
          $state.go('faNewGame');
        }
      }


      if (vm.inviteLeagueId) {
        vm.invite = `https://${$location.host()}/join/${localStorageService.get(vm.inviteLeagueStorageKey)}`;

        if (vm.statesPrefix) {
          vm.invite = `https://${$location.host()}/${vm.statesPrefix}/join/${localStorageService.get(vm.inviteLeagueStorageKey)}`;
        }

        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.activeTab = 2;
            vm.inviteLeague = data.League.League;
          }, function (err) {
            vm.inviteLeague = null;
            localStorageService.remove(vm.inviteLeagueStorageKey);
          });
      }
    }
  }
};