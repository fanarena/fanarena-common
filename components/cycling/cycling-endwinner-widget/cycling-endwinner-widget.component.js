import template from './cycling-endwinner-widget.component.tpl.html';

export default {
  template,
  bindings: {
    previousYear: '<',
    data: '<',
    gameName: '<',
    teamId: '<',
    week: '<',
    competitionFeed: '<',
    seasonId: '<'
  },
  controller: function (playerService, teamService, $scope, gettextCatalog) {
    var vm = this;
    vm.title = gettextCatalog.getString("Eindwinnaar");

    function chunk(arr, size) {
      var newArr = [];
      for (var i = 0; i < arr.length; i += size) {
        newArr.push(arr.slice(i, i + size));
      }
      return newArr;
    };

    function activate() {
      var rankingsPayload = {
        competition_feed: vm.competitionFeed,
        season_id: vm.seasonId,
        type: 'general',
        search_query: '',
        limit: 10,
        page: 1
      };


      if (vm.data) {
        vm.leaderTeam = vm.data;
        vm.chunkedData = chunk(vm.data.players.splice(0, 15), 3);
      } else {
        playerService.getRankingsWithFilters(rankingsPayload)
          .then(function (result) {
            const winner = result.Teams.find(team => team.Team.rank === 1);
            if (winner) {
              return teamService.get(winner.Team.id, vm.week)
            } else {
              return null;
            }
          })
          .then(function (team) {
            if (team) {
              vm.leaderTeam = team;
              vm.chunkedData = chunk(vm.leaderTeam.starting.splice(0, 15), 3);
            }
          })
          .catch(function () {
            vm.teams = [];
          });
      }
    };
    activate();
  }
};