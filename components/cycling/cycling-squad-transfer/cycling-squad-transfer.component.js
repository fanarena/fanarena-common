import template from './cycling-squad-transfer.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    cyclists: '<',
    clubs: '<',
    sponsors: '<',
    transferWeek: '<',
    gameBudget: '<'
  },
  controller: function ($scope, $rootScope, teamService, growl, gettextCatalog, $state, playerService) {
    var vm = this;

    $rootScope.$broadcast('sidemenu:team:collapse', {team: this.squad.Team.Team.id});

    vm.squad = this.squad;
    vm.cyclists = this.cyclists;
    vm.displayWeek = vm.squad.weeks.display_week;
    vm.deadlineWeek = vm.squad.weeks.deadline_week;
    $rootScope.title = gettextCatalog.getString("Squad Transfer");

    vm.onCyclistIn = onCyclistIn;
    vm.isOutOfBudget = isOutOfBudget;
    vm.confirmTransfer = confirmTransfer;
    vm.filterChanged = filterChanged;
    vm.isInMyTeam = isInMyTeam;
    vm.customFilter = customFilter;
    vm.hasAlreadyThreeCyclistFromTheSameTeam = hasAlreadyThreeCyclistFromTheSameTeam;
    vm.removePickedPlayer = removePickedPlayer;
    vm.toggleCyclist = toggleCyclist;
    vm.infoToggled = infoToggled;
    vm.activateTransferSlot = activateTransferSlot;
    vm.hasOneUnconfirmedTransfer = hasOneUnconfirmedTransfer;
    vm.removeInCyclist = removeInCyclist;
    vm.isTransferActive = isTransferActive;
    vm.confirmTransfer = confirmTransfer;
    vm.cancelTransfer = cancelTransfer;
    vm.dataFetched = dataFetched;
    vm.isPickable = isPickable;
    vm.hasTransfers = hasTransfers;
    vm.resetTransfers = resetTransfers;
    vm.playerProfile = playerProfile;
    vm.canTransfer = canTransfer;

    vm.starting = vm.squad && vm.squad.starting;

    activate();

    function dataFetched() {
      return vm.starting && vm.bench && vm.teamPlayersValues;
    }

    function initBench() {
      if (vm.squad.Team.League.competition_feed === 'CROS') {
        vm.bench = vm.squad && vm.squad.starting.slice(8);
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        vm.bench = vm.squad && vm.squad.starting.slice(10);
      } else {
        vm.bench = [];
      }
    }

    function resetTransfers() {
      teamService.resetTransfer(vm.squad.Team.Team.id)
        .then(function () {
          updateTeamInfo();
        })
    }

    function getBudgetDefault() {
      if (vm.squad.Team.League.competition_feed === 'CROS') {
        return vm.squad.Team.Team.budget;
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        var spent = vm.squad.starting.reduce((acc, player) => {
          var tranfersList = vm.transfers || [];
          var foundTransfer = vm.transfers.find(transfer => transfer.out.Player.id === player.Player.id && transfer.in);
          var playerValue = 0;
          if (foundTransfer && foundTransfer.in) {
            playerValue = foundTransfer.in.Player.value;
          } else {
            playerValue = player.Player.value;
          }
          return acc + playerValue;
        }, 0);
        return parseInt(vm.gameBudget) - spent;
      } else {
        return 0;
      }

    }

    function onCyclistIn(cyclist, $event) {
      $event.stopPropagation();

      if (vm.activeTransferSlot) {
        vm.transfers[vm.activeTransferSlot.transferIndex].in = cyclist;
      }

      vm.budget -= cyclist.info.current.value;
    }

    function isOutOfBudget() {
      return vm.budget < 0;
    }

    function filterChanged() {
      vm.pagination.current = 1;
    }

    function isInMyTeam(searchedCyclist) {
      return vm.teamPlayersValues && !!vm.teamPlayersValues.find(function (cyclist) {
        return cyclist.out.Player.id === searchedCyclist.Player.id;
      });
    }

    function isPickable(cyclist) {
      return cyclist.Player.value <= vm.budget;
    }

    function customFilter(cyclist) {
      var alreadyTransfer = vm.squad.Team.Transfer.find(function (transfer) {
        return transfer.In.id === cyclist.Player.id && transfer.week_id === vm.deadlineWeek;
      });

      // Check budget
      if ((cyclist.Player.value) > vm.filterItem.budget.budget) {
        return false;
      }
      // Check if transfer already
      else if (alreadyTransfer) {
        return false;
      }
      // Check national club
      else if (vm.squad.Team.League.competition_feed === 'CROS' && vm.selectedNationalClub.id !== 0 && cyclist.National.id !== vm.selectedNationalClub.id) {
        return false;
      }
      // Check club
      else if (vm.filterItem.club.Club.id !== 0 && cyclist.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }
      // Check name
      else if (vm.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      }
      else if (vm.isInMyTeam(cyclist)) {
        return false;
      }
      else {
        return true;
      }
    }

    function hasTransfers() {
      var transferCurrentWeek = vm.squad.Team.Transfer.find(function (transfer) {
        return transfer.week_id === vm.deadlineWeek;
      });
      return vm.squad.Team.Transfer.length > 0 && transferCurrentWeek;
    }

    function getTrendInfo(currentValue, previousValue) {
      var trend = {};
      var percentageFactor = 100;
      var increase = Math.round(Math.abs(currentValue * percentageFactor / previousValue));
      var trendPercentage = increase - percentageFactor;
      trend.percentage = trendPercentage;

      if (trendPercentage === 0) {
        trend.direction = 'constant';
      } else if (trendPercentage < 0) {
        trend.direction = 'down';
      } else {
        trend.direction = 'up';
      }

      trend.difference = currentValue - previousValue;

      return trend;
    }

    function getCyclistInfo(cyclist) {
      var info = {
        initial: {week_id: 1, value: null},
        current: {week_id: null, value: null},
        previous: {week_id: null, value: null},
        previously: {week_id: null, value: null}
      };
      var initialWeekId = 0;
      var searchWeekId = vm.squad.weeks.deadline_week - 1;

      var initialPlayerValue = cyclist && cyclist.History && cyclist.History
        .find(function (playerHistory) {
          return playerHistory.week_id === initialWeekId;
        });


      info.initial.week_id = initialWeekId + 1;
      if (initialPlayerValue) {
        info.initial.value = initialPlayerValue.price;
      } else {
        info.initial.value = cyclist.Player.value;
      }

      var transferPlayerValueInfo = cyclist && cyclist.History && cyclist.History
        .find(function (playerHistory) {
          return playerHistory.week_id === searchWeekId;
        });

      var previousWeekHistory = cyclist && cyclist.History && cyclist.History.find(function (playerHistory) {
        return playerHistory.week_id === (searchWeekId - 1)
      });

      if (previousWeekHistory) {
        info.previous.week_id = previousWeekHistory.week_id + 1;
        info.previous.value = previousWeekHistory.price;
      } else {
        var checkWeek = searchWeekId - 2;
        var secondSearch = null;
        while (checkWeek >= 0) {
          var prevHistory = cyclist && cyclist.History && cyclist.History.find(function (playerHistory) {
            return playerHistory.week_id === checkWeek;
          });

          if (prevHistory) {
            secondSearch = prevHistory;
            break;
          }
          checkWeek -= 1;
        }

        info.previous.week_id = searchWeekId;

        if (secondSearch) {
          info.previous.value = secondSearch.price;
        } else {
          info.previous.value = info.initial.value;
        }
      }

      if (transferPlayerValueInfo) {
        info.current.week_id = transferPlayerValueInfo.week_id + 1;
        info.current.value = transferPlayerValueInfo.price;
      } else {
        info.current.week_id = searchWeekId + 1;
        info.current.value = cyclist.Player.value;
      }

      if (info.previous && info.previous.week_id > 0) {
        var previously = cyclist && cyclist.History && cyclist.History.find(function (playerHistory) {
          return playerHistory.week_id === (searchWeekId - 2)
        });
        if (previously) {
          info.previously.week_id = previously.week_id + 1;
          info.previously.value = previously.price;
        }
      }

      info.current.trend = info.current.value && (info.previous && info.previous.value) &&
        getTrendInfo(info.current.value, info.previous.value);

      info.previous.trend = info.previous.value && (info.previously && info.previously.value) &&
        getTrendInfo(info.previous.value, info.previously.value);

      return info;
    }

    function toggleCyclist(cyclist) {
      vm.toggledPlayers[cyclist.Player.id] = !vm.toggledPlayers[cyclist.Player.id];
    }

    function infoToggled(cyclist) {
      return !!vm.toggledPlayers[cyclist.Player.id];
    }

    function updateTeamInfo() {
      var weekIdNo = vm.transferWeek.isFirstWeek ? vm.transferWeek.week_id : vm.squad.weeks.deadline_week - 1;
      return teamService.get(vm.squad.Team.Team.id, weekIdNo)
        .then(function (result) {
          vm.squad = result;
          vm.starting = vm.squad && vm.squad.starting;
          initBench();
          initTransfers();
        });
    }

    function activate() {
      initBench();

      // clubs
      vm.clubs.unshift({Club: {id: 0, name: gettextCatalog.getString('Ploeg')}});

      // All players
      vm.teamName = {value: ''};

      if (vm.squad.Team.League.competition_feed === 'CROS') {
        // National Clubs
        vm.nationalClubs = _.sortBy(playerService.getDistinctCountriesByPlayers(vm.cyclists), 'name');
        vm.nationalClubs.unshift({id: 0, name: gettextCatalog.getString('Land')});
        vm.selectedNationalClub = vm.nationalClubs[0];
      }

      // filters

      var competitionsBudges = {
        'CROS': [
          {budget: 100, text: gettextCatalog.getString('Budgetten')},
          {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
        ],
        'KLAS': [
          {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
          {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
          {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`},
          {budget: 2, text: `${gettextCatalog.getString('Onder')} 2 ${gettextCatalog.getString('miljoen')}`}
        ],
        'GIRO': [
          {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
          {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
          {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
        ],
        'TOUR': [
          {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
          {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
          {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
        ]
      };

      vm.filterOptions = {
        clubs: vm.clubs,
        budgets: competitionsBudges[vm.squad.Team.League.competition_feed]
      };
      vm.filterItem = {
        club: vm.clubs[0],
        budget: vm.filterOptions.budgets[0]
      };

      vm.toggledPlayers = {};

      vm.isTransfersTabActive = true;

      vm.searchPlayer = {value: ""};

      vm.transfersWeekId = vm.squad.weeks.deadline_week;

      vm.infoMessages = {
        intro: gettextCatalog.getString('Hier kan je de bankzitters van vorige speeldag transfereren. De nieuwe renners komen meteen in je team van deze week. Als je een starter wil transfereren, moet je hem eerst een weekje op de bank zetten.')
      };

      vm.activeInfo = 'intro';

      // Players pagination
      vm.pagination = {
        current: 1
      };

      vm.cyclists = vm.cyclists.map(function (cyclist) {
        return Object.assign({}, cyclist, {info: getCyclistInfo(cyclist)})
      });

      return updateTeamInfo();
    }

    function initTransfers() {
      vm.activeTransferSlot = null;

      // team players with prices
      vm.teamPlayersValues = vm.starting && vm.starting.map(function (cyclist, index) {

        var club = vm.clubs.find(function (clubItem) {
          return clubItem.Club.id === cyclist.Player.club_id
        });

        var cyclistPriceInfo = {
          currentTransferValue: cyclist.Player.value
        };

        var cyclistTransfer = vm.squad.Team.Transfer.find(function (transfer) {
          return transfer.out_id === cyclist.Player.id && transfer.week_id === vm.deadlineWeek;
        });

        var inClub = cyclistTransfer && vm.clubs.find(function (clubItem) {
          return clubItem.Club.id === cyclistTransfer.In.club_id;
        });

        var inCyclist = cyclistTransfer && {Player: cyclistTransfer.In, Club: inClub.Club, confirmed: true};

        if (club) {
          return {
            out: Object.assign({}, cyclist, {Club: club.Club}, {Price: cyclistPriceInfo}),
            in: inCyclist
          };
        } else {
          return {
            out: Object.assign({},
              cyclist,
              {Price: cyclistPriceInfo}),
            in: inCyclist
          };
        }
      });

      if (vm.squad.Team.League.competition_feed === 'CROS') {
        vm.transfers = vm.teamPlayersValues && vm.teamPlayersValues.slice(8);
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        vm.transfers = vm.teamPlayersValues && vm.teamPlayersValues.slice(10);
      } else {
        vm.transfers = [];
      }

      vm.budget = getBudgetDefault();

    }

    function hasAlreadyThreeCyclistFromTheSameTeam(cyclist) {
      var colleaguesCount = vm.starting.filter(function (startingCyclist) {
        return startingCyclist.Player.club_id === cyclist.Player.club_id;
      }).length;

      vm.transfers.forEach(function (transfer) {
        if (transfer.in && transfer.in.Player.club_id === cyclist.Player.club_id) {
          colleaguesCount += 1;
        }
      });

      return colleaguesCount >= 3;
    }

    function canTransfer() {
      let transfersAllowed = 0;

      if (vm.squad.Team.League.competition_feed === 'CROS') {
        transfersAllowed = 7;
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        transfersAllowed = 1;
      } else {
        vm.bench = [];
      }

      const transfersPerformed = (vm.transfers && vm.transfers.filter(transfer => !!transfer.in).length) || 0

      return transfersAllowed > transfersPerformed;
    }

    function removePickedPlayer(playerId, type) {
      var transfers = vm.currentTransfers.map(function (transfer) {
        if (type === 'out' && transfer.out_id === playerId) {
          vm.budget -= (transfer.Out.value);
          transfer.out_id = null;
          transfer.Out = null;
          vm.outTransferWait = false;
        }

        if (type === 'in' && transfer.in_id === playerId) {
          vm.budget += (transfer.In.value);
          transfer.in_id = null;
          transfer.In = null;
          vm.inTransferWait = false;
        }

        return (transfer.in_id || transfer.out_id) ? transfer : null;
      });

      vm.currentTransfers = transfers.filter(function (transfer) {
        return !!transfer;
      });
    }

    vm.getLocalDate = function (utcDate) {
      return moment.utc(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
    };

    function activateTransferSlot(slot) {
      vm.activeTransferSlot = {transferIndex: slot};
      vm.budget += vm.transfers[vm.activeTransferSlot.transferIndex].out.Price.currentTransferValue;
    }

    function hasOneUnconfirmedTransfer() {
      return vm.transfers && vm.transfers.filter(function (transfer) {
        return !!transfer.in && transfer.in.confirmed === undefined;
      }).length === 1
    }

    function removeInCyclist(transferIndex) {
      vm.budget -= vm.transfers[transferIndex].out.Price.currentTransferValue;
      vm.budget += vm.transfers[transferIndex].in.info.current.value;
      vm.activeTransferSlot = null;
      vm.transfers[transferIndex].in = null;
    }

    function isTransferActive(transferIndex) {
      return vm.activeTransferSlot && vm.activeTransferSlot.transferIndex === transferIndex;
    }

    function cancelTransfer() {
      vm.budget -= vm.transfers[vm.activeTransferSlot.transferIndex].out.Price.currentTransferValue;
      vm.activeTransferSlot = null;
    }

    function confirmTransfer() {

      teamService.cyclingTransfer({
        week_id: vm.transfersWeekId,
        team_id: vm.squad.Team.Team.id,
        in_id: vm.transfers[vm.activeTransferSlot.transferIndex].in.Player.id,
        out_id: vm.transfers[vm.activeTransferSlot.transferIndex].out.Player.id
      })
        .then(function (result) {
          if (result.data.message === 'Je hebt niet genoeg budget') {
            growl.addErrorMessage(result.data.message);
          } else {
            growl.addSuccessMessage(result.data.message);
          }
          updateTeamInfo();
        });
    }

    function playerProfile(cyclist, $event) {
      $event.stopPropagation();
      $state.go('cyclistProfile', {cyclistId: cyclist.Player.id, '#': null});
    }
  }
};