import template from './cycling-winner-widget.component.tpl.html';

export default {
  template,
  bindings: {
    competitionFeed: '<',
    seasonId: '<',
    gameName: '<',
    lastGameWeek: '<'
  },
  controller: function (playerService, teamService, $scope, gettextCatalog, $q) {

    var vm = this;
    vm.winnerWeek = null;
    vm.winner = null;
    vm.leaderTeam = null;
    vm.chunkedData = null;

    vm.title = gettextCatalog.getString('Weekly winner');

    function chunk(arr, size) {
      var newArr = [];
      for (var i = 0; i < arr.length; i += size) {
        newArr.push(arr.slice(i, i + size));
      }
      return newArr;
    };

    function getRankingsForWeekId(weekId) {
      return $q(function (resolve, reject) {
        var payload = {
          competition_feed: vm.competitionFeed,
          season_id: vm.seasonId,
          type: 'week',
          week: weekId,
          search_query: '',
          limit: 1,
          page: 1
        };

        playerService.getRankingsWithFilters(payload)
          .then(function (result) {
            resolve(result.Teams);
          })
          .catch(function (error) {
            console.log(error);
          });
      })
        .then(function (teams) {
          if (teams[0] || vm.winnerWeek < 1) {
            return teams[0] || null;
          } else {
            vm.winnerWeek -= 1;
            return getRankingsForWeekId(vm.winnerWeek);
          }
        });
    }

    function activate() {
      return teamService.getDeadlineWeeks(vm.seasonId, vm.competitionFeed)
        .then(function (data) {
          vm.winnerWeek = data.week.deadline_week ? data.week.deadline_week - 1 :
            (data.week.display_week === vm.lastGameWeek ? data.week.display_week - 1 : data.week.display_week);
          return getRankingsForWeekId(vm.winnerWeek);
        })
        .then(function (result) {
          if (result) {
            vm.winner = result;
            return teamService.get(result.Team.id, vm.winnerWeek)
              .then(function (team) {
                vm.leaderTeam = team;
                vm.chunkedData = chunk(vm.leaderTeam.starting.splice(0, 15), 3);
              })
          }
        });
    }

    activate();
  }
};