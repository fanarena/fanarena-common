import template from './cycling-calendar.component.tpl.html';

export default {
  template,
  bindings: {
    displayWeek: '<',
    team: '<',
    deadlineWeeks: '<',
    showSelect: '<',
    hideTitle: '<',
    competitionFeed: '<'
  },
  controller: function ($scope, RESOURCES, teamService, gameService, $state, UtilsService, CHANNELS) {
    var self = this;
    self.resources = RESOURCES;
    self.selectedCompetition = {competition_feed: getCompetitionFeed()};
    self.stages = [];

    self.months = [
      {id: 0, value: 0, name: 'Alle maanden'},
    ];

    self.types = [
      {id: 0, name: 'all-types', label: 'Alle types'}
    ];

    self.isVisible = function (stage) {
      if (self.selectedMonth && self.selectedMonth.value !== 0 && parseInt(moment(stage.matches.date).format("M")) !== self.selectedMonth.value) {
        return false;
      } else if (self.selectedType.id !== 0 && stage.matches.type !== self.selectedType.name) {
        return false;
      } else {
        return true;
      }
    };

    function getCompetitionFeed() {
      return (self.team && self.team.Team.League.competition_feed) || self.competitionFeed;
    }

    // Show calendar outside team page
    self.getDeadlineWeeks = function () {
      // Get deadlineweeks and competitions
      const seasonId = (self.team && self.team.Team.League.season_id) || '2018';

      return teamService.getDeadlineWeeks(seasonId, self.selectedCompetition.competition_feed).then(function (data) {
        self.deadlineWeeks = data.weeks;
        self.displayWeek = _.findWhere(self.deadlineWeeks, {week_id: data.week.display_week});
        self.stages = prepareStages(data.stages);
        buildMonthFilter(self.stages);
        buildTypesFilter(self.stages);

        if (self.displayWeek) {
          self.selectedMonth = self.months.find(function (month) {
            return month.value === parseInt(moment(self.displayWeek.date).format("M"));
          });
        } else {
          self.selectedMonth = self.months.find(function (month) {
            return month.value === parseInt(moment().format("M"));
          });
        }

        self.selectedType = self.types[0];

      });
    };

    function buildTypesFilter(stages) {
      stages.forEach(stage => {
        const alreadyAdded = self.types.find(type => type.name === stage.matches.type);

        if(!alreadyAdded) {
          self.types.push({
            id: self.types.length,
            name: stage.matches.type,
            label: stage.matches.type
          })
        }
      })
    }

    function buildMonthFilter(stages) {
      stages.forEach(stage => {
        const monthNumber = parseInt(moment(stage.matches.date).format('M'));
        const monthLabel = moment(stage.matches.date).format('MMMM');
        const alreadyAdded = self.months.find(month => month.value === monthNumber);

        if(!alreadyAdded) {
          self.months.push({
            id: self.months.length,
            value: monthNumber,
            name: monthLabel
          })
        }
      })
    }

    function prepareStages(stages) {
      return stages.map(function (stage) {
        var matches = _.assign({}, stage.matches, {channels: getMatchChannels(stage)})
        return {matches: matches};
      })
    }

    self.getLocalDate = UtilsService.getLocalDate;

    function getMatchChannels(match) {
      if (match.matches.channels && match.matches.channels.length) {
        return JSON.parse(match.matches.channels);
      } else {
        return [];
      }
    };

    self.getDeadlineWeeks();

    self.competitionChanged = function () {
      self.getDeadlineWeeks();
    };

    $scope.selectedWeek = _.findWhere(self.deadlineWeeks, {week_id: self.displayWeek});

    // On updated week (parent comp)
    var previousDisplayWeekVal = self.displayWeek;
    this.$doCheck = function () {
      if (!self.showSelect && previousDisplayWeekVal !== self.displayWeek) {
        $scope.selectedWeek = _.findWhere(self.deadlineWeeks, {week_id: self.displayWeek});

        // Get new matches
        self.getMatches(self.displayWeek);
        previousDisplayWeekVal = self.displayWeek;
      }
    };

    self.goToMatch = function (match) {
      if(self.competitionFeed === 'CROS') {
        $state.go('cyclingRace', {race: match.matches.week_id});
      }
    };

    self.getChannelIdByName = function (name) {
      return CHANNELS[name];
    };

    self.matchHasStarted = function (match) {
      var start = moment.utc(match.d_MatchStartTime);
      start.local();

      var now = moment();

      return now > start;
    };
  }
};