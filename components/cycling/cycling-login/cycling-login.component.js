import template from './cycling-login.component.tpl.html';

export default {
  template,
  controller: function (teamService, userService, $window, growl, $location, localStorageService, $state,
                        $rootScope, gettextCatalog, GoogleAnalyticsService) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Login");

    // login
    vm.user = {
      email: null,
      password: null
    };

    vm.login = function (frm) {
      if (frm.$valid) {
        return GoogleAnalyticsService.getClientId()
          .then(function (clientId) {
            userService.login(vm.user.email, vm.user.password, clientId)
              .then(function () {
                return teamService.getAll()
                  .then(function (teams) {
                    if (teams.length) {
                      $window.location.href = '/dashboard';
                    } else {
                      $window.location.href = '/new-game';
                    }
                  });
              }, function () {
                growl.addErrorMessage('Wrong credentials!');
              });
          });
      }
    };

    // forgot password
    vm.showForgotPassword = false;
    vm.username = "";
    vm.sent = false;
    vm.sendForgot = function (frm) {
      if (frm.$valid) {
        userService.forgotPassword(vm.username, $location.host()).then(function (res) {
          vm.username = "";
          vm.sent = true;
          vm.frmForgotPassword.$setPristine();
        }, function (err) {
          growl.addErrorMessage(err.data.message);
        });
      }
    };

  }
};