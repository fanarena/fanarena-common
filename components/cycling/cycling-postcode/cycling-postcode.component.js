import template from './cycling-postcode.component.tpl.html';

export default {
  template,
  bindings: {
    postCode: '<',
    competitionFeed: '<'
  },
  controller: function (leagueService) {
    var vm = this;

    vm.postcodeTeams = [];

    vm.pagination = { current: 1 };
    vm.displayItems = 10;
    vm.onPageChange = onPageChange;

    function onPageChange(newPageNumber) {
      vm.pagination.current = newPageNumber;
      activate();
    }

    activate();

    function activate() {
      return leagueService.getRankingsV2(vm.competitionFeed, 'postcode', null, vm.pagination.current, vm.displayItems, null, null, vm.postCode)
        .then(result => {
          vm.postcodeTeams = result.data;
          vm.totalRecords = result.totalRecords;
        });
    }
  }
};