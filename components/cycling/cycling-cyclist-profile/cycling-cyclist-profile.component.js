import template from './cycling-cyclist-profile.component.tpl.html';

export default {
  template,
  bindings: {
    cyclist: '<',
    cyclistHistory: '<',
    hasContainerClass: '<',
    gameName: '<',
    playerValueDecimals: '<',
    lastGameWeek: '<'
  },
  controller: function ($scope, gettextCatalog) {
    $scope.cyclist = this.cyclist;
    var vm = this;
    vm.finalGameBonus = null;
    vm.valueDecimals = vm.playerValueDecimals || 0;
    vm.showFinalGameBonus = false;

    activate();

    function activate() {
      const hasFinalGameBonus = $scope.cyclist && $scope.cyclist.Player && $scope.cyclist.Player.Stat &&
        $scope.cyclist.Player.Stat.find(item => item.week_id === vm.lastGameWeek);

      if (hasFinalGameBonus) {
        const value = JSON.parse(hasFinalGameBonus.value);
        vm.finalGameBonus = Object.keys(value)
          .map(type => {
            return {
              type: gettextCatalog.getString(`jersey-${type}`),
              points: value[type].points,
              rank: value[type].rank,
            }
          });
      }
      if ($scope.cyclist && $scope.cyclist.Player && $scope.cyclist.Player.Stat) {
        $scope.cyclist.Player.Stat = $scope.cyclist.Player.Stat.filter(item => item.week_id !== vm.lastGameWeek);
      }

      vm.cyclistHistoryWithTrend = vm.cyclistHistory && vm.cyclistHistory.map(function (cyclistWeekHistory, index) {
        var trend = null;
        var prevValue = vm.cyclistHistory[index - 1];
        if (prevValue) {
          if (prevValue.price > cyclistWeekHistory.price) {
            trend = 'down';
          } else if (prevValue.price < cyclistWeekHistory.price) {
            trend = 'up';
          } else {
            trend = 'constant';
          }
        } else {
          trend = 'constant';
        }
        return Object.assign({}, cyclistWeekHistory, { trend: trend })
      })
    }
  }
};