import template from './cycling-rules.component.tpl.html';

export default {
  bindings: {
    hasContainerClass: '<'
  },
  template,
  controller: function ($rootScope, gettextCatalog, appService, FACms) {
    var vm = this;
    vm.rules = [];
    $rootScope.title = gettextCatalog.getString("Rules");
    activate();

    function activate() {
      return FACms.getTableEntries('rules')
        .then(result => {

          vm.rules = result.data.map(ruleData => {
            const translatedRule = ruleData.rule.data.find(
              rule => rule.language_code === gettextCatalog.getCurrentLanguage()
            );
            return translatedRule && translatedRule.body;
          })
        });
    }
  }
};