import template from './cycling-race.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<',
    gameName: '<',
    competitionInfo: '<',
    showProfile: '<',
    showHistory: '<'
  },
  controller: function ($rootScope, gettextCatalog, $stateParams, teamService) {
    var vm = this;
    vm.race = parseInt($stateParams.race);
    vm.match = {};

    vm.showMapFullScreen = function () {
      vm.fullScreenImageMap = true;
    };

    vm.exitMapFullScreen = function () {
      vm.fullScreenImageMap = false;
    };

    vm.showProfileFullScreen = function () {
      vm.fullScreenImageProfile = true;
    };

    vm.exitProfileFullScreen = function () {
      vm.fullScreenImageProfile = false;
    };

    function activate() {
      teamService.getDeadlineWeeks(vm.competitionInfo.seasonId, vm.competitionInfo.competitionFeed)
        .then(function (data) {
          var found = data.stages.find(function(stage){
            return stage.matches.id === vm.race;
          });
          vm.match = found.matches;

          if(found) {
            vm.history = JSON.parse(found.matches.history);
          } else {
            vm.history = [];
          }
        });
    }

    activate();

    vm.isMobile = $rootScope.isMobile;

    $rootScope.title = gettextCatalog.getString("Race");
  }
};