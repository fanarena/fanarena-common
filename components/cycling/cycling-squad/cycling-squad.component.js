import template from './cycling-squad.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    hasContainerClass: '<'
  },
  controller: function ($scope, $rootScope, gettextCatalog) {

    // cycling parent
    $scope.starting = this.squad.starting;
    $scope.frontman = $scope.starting[0];

    $rootScope.title = gettextCatalog.getString("Squad");

    $scope.frontmanChanged = function () {
      // Substitute runners
      var tmp = $scope.starting[0];

      var index = null;
      _.each($scope.starting, function (c, i) {
        if (c.hasOwnProperty('Player') && c.Player.id == $scope.frontman.Player.id)
          index = i;
      });

      $scope.starting[0] = $scope.frontman;
      $scope.starting[index] = tmp;
    };
  }
};