import template from './cycling-join-league.component.tpl.html';

export default {
  template,
  bindings: {
    statesPrefix: '<',
    inviteURL: '<'
  },
  controller: function (localStorageService, $state, $stateParams, NewGameCacheService, $rootScope, growl) {
    const vm = this;

    if ($stateParams.inviteLeagueId) {
      if(vm.inviteURL) {
        localStorageService.set(vm.inviteURL, $stateParams.inviteLeagueId);
      } else {
        localStorageService.set('cxInviteLeagueId', $stateParams.inviteLeagueId);
      }
    }

    if (localStorageService.get('user') === null) {
      $state.go('publicRegister');
    } else {
      if ($stateParams.inviteLeagueId && !$rootScope.hasACyclingTeam) {
        NewGameCacheService.setGameProgress(2);
        NewGameCacheService.setSportType(2);

        if(vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-newGame');
        } else {
          $state.go('cyclingNewGame');
        }

      } else {
        growl.addErrorMessage('U heeft reeds een veldritploeg!');
        if(vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-dashboard');
        } else {
          $state.go('dashboard');
        }
      }
    }
  }
};