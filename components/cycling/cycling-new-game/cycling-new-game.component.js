import template from './cycling-new-game.component.tpl.html';

export default {
  template,
  bindings: {
    cyclists: '<',
    clubs: '<',
    sponsors: '<',
    competitionFeed: '=',
    gameBudget: '<',
    sponsorsEnabled: '<',
    statesPrefix: '<',
    localAssets: '<',
    gameName: '<',
    hasContainerClass: '<',
    maxPlayersSameClub: '<',
    playerValueDecimals: '<'
  },
  controller: function ($scope, $state, RESOURCES, playerService, growl, gameService,
                        localStorageService, $rootScope, gettextCatalog, NewGameCacheService) {

    activate();

    $scope.resources = RESOURCES;
    $rootScope.title = gettextCatalog.getString("Cycling new game");

    $scope.sponsorsEnabled = this.sponsorsEnabled;
    // Dummy data
    $scope.starting = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
    $scope.sponsor = null;
    $scope.statesPrefix = this.statesPrefix;
    $scope.maxPlayersSameClub = this.maxPlayersSameClub;
    $scope.valueDecimals = this.playerValueDecimals || 0;

    var cachedPicks = localStorageService.get('cyclingPicks');

    if (cachedPicks) {
      $scope.starting = cachedPicks.map(function (cyclist) {
        return cyclist ? cyclist : {};
      });
      $scope.frontman = $scope.starting[0];
    }

    let teamsFilterLabel = gettextCatalog.getString('Teams');

    if(['GCX'].indexOf(this.competitionFeed) !== -1) {
      teamsFilterLabel = gettextCatalog.getString('All countries');
    }

    this.clubs = this.clubs.map(club => {
      club.Club.name = gettextCatalog.getString(club.Club.name);
      return club;
    });

    // clubs
    this.clubs.unshift({
      Club: {
        id: 0,
        name: teamsFilterLabel
      }
    });

    // All players
    $scope.cyclists = this.cyclists;
    $scope.competitionFeed = this.competitionFeed;
    $scope.sponsors = this.sponsors;
    $scope.clubs = this.clubs;
    $scope.localAssets = this.localAssets;
    $scope.budget = this.gameBudget;

    // National Clubs
    if ($scope.competitionFeed === 'CROS') {
      $scope.nationalClubs = _.sortBy(playerService.getDistinctCountriesByPlayers($scope.cyclists), 'name');
      $scope.nationalClubs.unshift({id: 0, name: 'Land'});
      $scope.selectedNationalClub = $scope.nationalClubs[0];
    }

    if ($scope.starting.length) {
      $scope.starting.forEach(function (cyclist) {
        if (cyclist && cyclist.Player && cyclist.Player.value) {
          $scope.budget -= cyclist.Player.value;
        }
      });
    }

    if ($scope.competitionFeed === 'GCX') {
      $scope.totalFieldPlayers = 13;
    } else {
      $scope.totalFieldPlayers = 15;
    }

    $scope.teamName = {
      value: ''
    };

    var competitionsBudges = {
      'CROS': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GCX': [
        {budget: 100, text: gettextCatalog.getString('Budgetten')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'KLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`},
        {budget: 2, text: `${gettextCatalog.getString('Onder')} 2 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GIRO': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'TOUR': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'VUEL': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ],
      'GKLAS': [
        {budget: 100, text: gettextCatalog.getString('Alle budgetten')},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 8, text: `${gettextCatalog.getString('Onder')} 8 ${gettextCatalog.getString('miljoen')}`},
        {budget: 6, text: `${gettextCatalog.getString('Onder')} 6 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`},
        {budget: 4, text: `${gettextCatalog.getString('Onder')} 4 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    $scope.competitionPositions = {
      GCX: [
        {id: -1, name: gettextCatalog.getString('All categories')},
        {id: 10, name: gettextCatalog.getString('Elite')},
        {id: 9, name: gettextCatalog.getString('Dames')},
        {id: 8, name: gettextCatalog.getString('Beloften')}
      ]
    };

    $scope.maxPicksForPositionByCompetition = {
      GCX: {
        10: 9,
        9: 3,
        8: 1
      }
    };

    const maxPicksForCurrentCompetition = $scope.maxPicksForPositionByCompetition[$scope.competitionFeed];

    $scope.positions = $scope.competitionPositions[$scope.competitionFeed] || [];

    $scope.filterOptions = {
      clubs: this.clubs,
      budgets: competitionsBudges[$scope.competitionFeed],
      selectedPosition: $scope.positions[0] || null
    };

    $scope.filterItem = {
      club: this.clubs[0],
      budget: $scope.filterOptions.budgets[0]
    };

    $scope.getPositionNameById = function (positionId) {
      const position = $scope.positions.find(item => item.id === positionId);
      return (position && position.name) || ''
    };

    $scope.frontmanChanged = function () {
      // Substitute runners
      var tmp = $scope.starting[0];

      var index = null;
      _.each($scope.starting, function (c, i) {
        if (c && c.hasOwnProperty('Player') && c.Player.id == $scope.frontman.Player.id)
          index = i;
      });

      if ($scope.frontman) {
        $scope.starting[0] = $scope.frontman;
      }
      $scope.starting[index] = tmp;

      updateCyclingCache();
    };

    $scope.filterChanged = function () {
      $scope.pagination.current = 1;
    };

    $scope.searchPlayer = {value: ""};
    $scope.customFilter = function (cyclist) {
      // Check budget
      if (cyclist.Player.value >= $scope.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if ($scope.filterItem.club.Club.id !== 0 && cyclist.Club.id !== $scope.filterItem.club.Club.id) {
        return false;
      }

      else if ($scope.filterOptions.selectedPosition && $scope.filterOptions.selectedPosition.id !== -1 && $scope.positions && $scope.positions.length && cyclist.Player.position_id !== $scope.filterOptions.selectedPosition.id) {
        return false;
      }

      // Check national club
      else if ($scope.competitionFeed === 'CROS' && $scope.selectedNationalClub.id !== 0 && cyclist.National.id !== $scope.selectedNationalClub.id) {
        return false;
      }
      // Check name
      else if ($scope.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf($scope.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    // Players pagination
    $scope.pagination = {
      current: 1
    };

    $scope.checkTeamName = function () {
      var teamName = $scope.teamName.value;

      if (teamName && teamName.length && teamName[teamName.length - 1] === ',') {
        $scope.teamName.value = teamName.substr(0, teamName.length - 1);
      }
    }

    $scope.pick = function (cyclist) {
      addToFirstAvailableSpot(cyclist, $scope.starting);

      updateCyclingCache();

      $scope.budget -= cyclist.Player.value;
    };

    function pickedFourthCyclistFromAClub(cyclist) {
      var pickedFromTheSameClub = $scope.starting.filter(function (cyclistInStarting) {
        return cyclistInStarting && cyclistInStarting.Club && cyclistInStarting.Club.id === cyclist.Club.id;
      });
      return pickedFromTheSameClub.length > 2;
    }

    var hasEmptySpots = function (arr) {
      var hasEmpty = false;
      _.each(arr, function (player) {
        if (player && !player.hasOwnProperty('Player')) {
          hasEmpty = true;
        }
      });

      return hasEmpty;
    };

    var addToFirstAvailableSpot = function (cyclist, arr) {
      var index = null;
      _.each(arr, function (player, i) {
        if (player && !player.hasOwnProperty('Player') && index === null) {
          index = i;
        }
      });

      arr[index] = cyclist;
    };

    $scope.isPickable = function (cyclist) {
      var pickable = true;

      if ($scope.competitionFeed === 'GCX') {
        const playersPickedSoFar = $scope.starting
          .filter(item => item.Player && item.Player.id);

        if (playersPickedSoFar.length > 13) {
          pickable = false;
        }

        if (cyclist && cyclist.Player) {
          const pickedForPlayerPosition = playersPickedSoFar
            .filter(item => item && item.Player && item.Player.position_id === cyclist.Player.position_id);

          if (pickedForPlayerPosition.length >= maxPicksForCurrentCompetition[cyclist.Player.position_id]) {
            pickable = false;
          }
        }
      }

      if (cyclist.Player.value > $scope.budget)
        return false;

      _.each($scope.starting, function (c) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === cyclist.Player.id) {
          pickable = false;
        }
      });

      if (!hasEmptySpots($scope.starting)) {
        pickable = false;
      }

      return pickable;
    };

    $scope.teamLimitExceeded = function (cyclist) {
      const maxPlayers = $scope.maxPlayersSameClub || 5;
      if (playerService.breaksTheCyclingClubLimit(cyclist, $scope.starting, maxPlayers)) {
        return true;
      } else return false;
    };

    $scope.cyclistsSelected = function () {
      var count = 0;

      _.each($scope.starting, function (cyclist) {
        if (cyclist && cyclist.hasOwnProperty('Player'))
          count++;
      });

      return count;
    };

    $scope.cyclistRemove = function (cyclist) {
      $scope.budget += cyclist.Player.value;
      _.each($scope.starting, function (c, i) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === cyclist.Player.id) {
          $scope.starting[i] = {};

          // Remove front runner?
          if (i == 0) {
            var firstNext = null;
            _.each($scope.starting, function (c2, i2) {
              if (c2 && c2.hasOwnProperty('Player') && c2.Player.id !== cyclist.Player.id && firstNext === null) {
                firstNext = i2;
              }
            });

            if (firstNext !== null) {
              $scope.starting[0] = $scope.starting[firstNext];
              $scope.starting[firstNext] = {};
              $scope.frontman = $scope.starting[0];
            }
          }
        }
      });
      updateCyclingCache();
    };

    $scope.getLineUpLayout = function () {
      const defaultLayout = 'default-background';

      if ($scope.competitionFeed === 'CROS') {
        if ($scope.sponsor && $scope.sponsor.Club) {
          return 'background-' + $scope.sponsor.Club.id;
        } else {
          return defaultLayout;
        }
      } else if ($scope.competitionFeed === 'GCX') {
        return 'lineup-cx';
      } else if ($scope.competitionFeed === 'KLAS') {
        if ($scope.sponsor && $scope.sponsor.Club) {
          return 'background-' + $scope.sponsor.Club.id + ' cycling-classic-lineup';
        } else {
          return 'klassiekers-layout cycling-classic-lineup';
        }

      } else if (['GIRO', 'TOUR', 'VUEL', 'GKLAS'].indexOf($scope.competitionFeed) !== -1) {
        return 'lineup-cycling';
      } else {
        return defaultLayout;
      }
    };

    $scope.notEmpty = function (cyclist) {
      if (cyclist && cyclist.hasOwnProperty('Player'))
        return true;
      else {
        return false;
      }
    };

    $scope.create = function () {
      var valid = true;

      if (!$scope.teamName.value) {
        growl.addErrorMessage(gettextCatalog.getString('You need to choose a team name!'));
        $scope.frmTeamName.$setSubmitted();
        $scope.frmTeamName.$setValidity('required', false);
        valid = false;
      }

      if (!$scope.frontman) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('You need to select the frontman!'));
      }

      if (!$scope.sponsor && $scope.sponsorsEnabled) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('You need to select the sponsor!'));
      }

      if ($scope.cyclistsSelected() < $scope.totalFieldPlayers) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString(`You need ${$scope.totalFieldPlayers} cyclists in your team!`));

        if ($scope.competitionFeed === 'GCX') {
          growl.addErrorMessage(`${gettextCatalog.getString('you must have 9 elite runners, 3 dames runners and 1 beloften runner')}`);
        }
      }

      if (valid) {
        var picks = [];
        var bench = [];

        if (['KLAS', 'CROS'].indexOf($scope.competitionFeed) !== -1) {
          _.each($scope.starting, function (cyclist, index) {
            if (index < 8) {
              picks.push(cyclist.Player.id);
            } else {
              bench.push(cyclist.Player.id);
            }
          });
        }

        if (['GIRO', 'TOUR', 'VUEL', 'GKLAS'].indexOf($scope.competitionFeed) !== -1) {
          _.each($scope.starting, function (cyclist, index) {
            picks.push(cyclist.Player.id);
          });
        }

        if (['GCX'].indexOf($scope.competitionFeed) !== -1) {
          picks = $scope.starting
            .map((item, index) => {
              if (index < 13) {
                return item.Player.id;
              } else {
                return null;
              }
            })
            .filter(item => !!item);
        }


        // Post new team
        var obj = {
          teamName: $scope.teamName.value,
          competition: {
            competition_feed: $scope.competitionFeed
          },
          gameMode: {
            key: 'budget'
          },
          captain: picks[0],
          picks: picks,
          bench: bench
        };

        if ($scope.sponsorsEnabled) {
          obj.sponsor_id = $scope.sponsor.Club.id;
        }

        if ($scope.statesPrefix) {
          $state.go(`${$scope.statesPrefix}-platoon`, {payload: obj});
        } else {
          $state.go('platoon', {payload: obj});
        }

        /*gameService.joinLeague(obj).then(function (res) {
         growl.addSuccessMessage('Team created!');
         $state.go('squadTeam', { id: res.team.Team.id });
         });*/
      }
    };

    function updateCyclingCache() {
      localStorageService.set('cyclingPicks', $scope.starting);
    }

    function activate() {
      if (NewGameCacheService.getProgress() === 1) {
        NewGameCacheService.resetSportType();
        NewGameCacheService.resetGameMode();
        NewGameCacheService.resetProgress();
        $state.go('faNewGame');
      }
    }

    $scope.decreaseNewGameProgressStep = function () {
      NewGameCacheService.resetSportType();
      NewGameCacheService.resetGameMode();
      NewGameCacheService.resetProgress();
      $state.go('faNewGame');
    }

    $scope.getClubName = function (clubId) {
      const foundClub = $scope.clubs.find(club => club.Club.id === clubId);
      return (foundClub && foundClub.Club && foundClub.Club.name) || '';
    }

    $scope.getPossibleCaptains = function () {
      if (['GIRO', 'TOUR', 'VUEL'].indexOf($scope.competitionFeed)) {
        return $scope.starting;
      } else {
        return $scope.starting.slice(0, 8);
      }
    }

    $scope.goToCyclist = function (cyclist) {
      if ($scope.statesPrefix) {
        $state.go($scope.statesPrefix + '-cyclistProfile', {cyclistId: cyclist.Player.id});
      } else {
        $state.go('cyclistProfile', {cyclistId: cyclist.Player.id});
      }
    };
  }
};