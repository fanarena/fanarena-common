import template from './cycling-prizes-widget.component.tpl.html';

export default {
  template,
  controller: function (FACms) {
    var vm = this;
    vm.numberOnePrize = null;

    activate();

    function activate() {
      return FACms.getPrizes(['general'])
        .then(result => {
          vm.numberOnePrize = result.general.find(prize => prize.sort === 1);
        })
    }
  }
};