import template from './cycling-squad-transfers.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    cyclists: '<',
    clubs: '<',
    sponsors: '<',
    deadlineWeeks: '<',
    gameBudget: '<',
    sponsorsEnabled: '<',
    localAssets: '<',
    withEditMode: '<',
    maxPlayersSameClub: '<',
    totalTransfers: '<',
    statesPrefix: '<',
    gameName: '<'
  },
  controller: function () {
    var vm = this;
    vm.squad = this.squad;
    vm.cyclists = this.cyclists;
    vm.clubs = this.clubs;
    vm.deadlineWeeks = this.deadlineWeeks;
    vm.transferWeek = {
      week_id: vm.squad.Team.Team.week_id,
      isFirstWeek: vm.squad.weeks.deadline_week === vm.squad.Team.Team.week_id
    };
    vm.isFirstDeadlineWeek = function() {
      return vm.transferWeek.isFirstWeek;
    }
  }
};