import template from './cycling-user-profile.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<'
  },
  controller: function ($rootScope, userService, localStorageService, growl, gettextCatalog) {
    $rootScope.title = gettextCatalog.getString("Profile");
    $rootScope.$broadcast('sidemenu:team:collapse');

    var vm = this;
    vm.profile = JSON.stringify(this.user, null, "   ");
    vm.user = localStorageService.get('user');

    vm.editProfile = function (frm) {
      if (frm.$valid) {
        var obj = {
          'User': {
            'first_name': vm.user.first_name,
            'last_name': vm.user.last_name,
            'username': vm.user.username,
            'phone': vm.user.phone,
            'street': vm.user.street,
            'street_nr': vm.user.street_nr,
            'post_code': vm.user.post_code,
            'city': vm.user.city,
            'gender': vm.user.gender
          }
        };

        userService.edit(vm.user.id, obj).then(function (res) {
          localStorageService.set('user', res.user.User);
          localStorageService.set('token', res.token);
          growl.addSuccessMessage('Profile updated!');

          $rootScope.$broadcast('profile:updated');
        });
      }
    };

    vm.editPassword = function (frm) {
      if (frm.$valid) {
        var obj = {
          "newpassword": vm.newPassword
        };

        userService.edit(vm.user.id, obj).then(function (res) {
          growl.addSuccessMessage('Password updated!');
          localStorageService.set('token', res.token);
          vm.newPassword = vm.verifyNewPassword = "";
          frm.$setPristine();
        });
      }
    };

    vm.editReferral = function (frm) {
      if (frm.$valid) {
        var obj = {
          "referrer": vm.referral
        };

        userService.edit(vm.user.id, obj).then(function (res) {
          localStorageService.set('token', res.token);
          growl.addSuccessMessage('Referral added!');
        });
      }
    };

    vm.logout = function () {
      localStorageService.clearAll();
      $state.go('publicHome');
    };
  }
};