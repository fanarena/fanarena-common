import template from './cycling-home.component.tpl.html';

export default {
  template,
  bindings: {
    competitionInfo: '<',
    enabledWidgets: '<',
    showRegisterAndLogin: '<',
    user: '<',
    deadlineWeeks: '<',
    gameName: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    enableHLNLogin: '<',
    hlnConfig: '<',
    facebookPage: '<',
    lastGameWeek: '<'
  },
  controller: function ($state, $rootScope, $scope, playerService, teamService, GoogleAnalyticsService,
    localStorageService, growl, $auth, $window, gettextCatalog, RankingsService, $location,
    userService, appGamesService, COMPETITION_FEED_TO_SEASON_ID, PROMO_BANNER_URL) {
    const vm = this;
    $scope.cyclistsByPicks = [];
    $scope.cyclistsByPoints = [];


    vm.lastYearWinner = RankingsService.getPreviousYearWinner();
    vm.deadlineWeek = vm.deadlineWeeks.deadline_week;
    vm.currentLocale = gettextCatalog.getCurrentLanguage();
    vm.seasonId = COMPETITION_FEED_TO_SEASON_ID[vm.competitionInfo.competitionFeed];
    vm.gameHeaderClass = `${vm.statesPrefix}-home-banner ${vm.currentLocale}`;
    vm.promoBannerURL = PROMO_BANNER_URL[vm.competitionInfo.competitionFeed];
    vm.promoBannerImage = `/images/promo-banner-${vm.competitionInfo.competitionFeed}.jpg`;

    vm.showNewGameButton = vm.deadlineWeeks.deadline_week && vm.deadlineWeeks.deadline_week < vm.lastGameWeek;
    activate();

    vm.competitionFeedToFBPages = {
      'GCX': 'https://www.facebook.com/goudencross',
      'GKLAS': 'https://www.facebook.com/goudenklassiekers'
    };

    vm.hasSponsor = ['GCX'].indexOf(vm.competitionInfo.competitionFeed) !== -1;

    vm.facebookPage = vm.competitionFeedToFBPages[vm.competitionInfo.competitionFeed];

    $rootScope.$on('active-game:changed', function () {
      vm.competitionInfo.competitionFeed = appGamesService.getActiveCompetitionFeed();
      vm.statesPrefix = appGamesService.getActiveGameSlug();
      vm.promoBannerURL = PROMO_BANNER_URL[vm.competitionInfo.competitionFeed];
      vm.promoBannerImage = `/images/promo-banner-${vm.competitionInfo.competitionFeed}.jpg`;    
      vm.gameHeaderClass = `${vm.statesPrefix}-home-banner ${vm.currentLocale}`;
    });

    $rootScope.$on('language:changed', function () {
      vm.statesPrefix = appGamesService.getActiveGameSlug();
      vm.currentLocale = gettextCatalog.getCurrentLanguage();
      vm.gameHeaderClass = `${vm.statesPrefix}-home-banner ${vm.currentLocale}`;
    });

    // Check if login/register route
    if ($state.current.name === 'publicLogin') {
      $rootScope.$broadcast('login:show');
    }
    else if ($state.current.name === 'publicRegister') {
      $rootScope.$broadcast('register:show');
    }
    else if ($state.current.name === 'publicResetPasswordCode') {
      $rootScope.$broadcast('reset:show');
    }

    function activate() {
      const queryParams = $location.search();

      playerService.getAllStats(vm.competitionInfo.competitionFeed, vm.seasonId)
        .then(function (result) {
          $scope.cyclistsByPicks = result.picks;
          $scope.cyclistsByPoints = result.points;
        })
    }

    vm.login = function () {
      $state.go('publicLogin');
    };

    vm.fbLogin = function () {
      return userService.fbLogin(vm.statesPrefix);
    };

    vm.register = function () {
      $state.go('publicRegister');
    };

    vm.goToSponsor = function () {
      $window.open(`https://www.bioracer.be/${vm.currentLocale}`, '_blank');
    };

    vm.onNewGameClick = function () {
      $state.go(appGamesService.getActiveGameSlug() + '-newGame');
    }

    vm.onHLNLogin = function () {
      const authURL = (vm.hlnConfig && vm.hlnConfig.auth_url) || '';
      const clientId = (vm.hlnConfig && vm.hlnConfig.client_id) || '';
      const redirectURI = (vm.hlnConfig && vm.hlnConfig.redirect_uri) || '';
      const href = `${authURL}?response_type=code&scope=openid&client_id=${clientId}&redirect_uri=${redirectURI}`;
      $window.location.href = href;
    }
  }
};