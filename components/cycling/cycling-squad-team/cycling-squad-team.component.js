import template from './cycling-squad-team.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    clubs: '<',
    cyclists: '<',
    sponsors: '<',
    gameBudget: '<',
    sponsorsEnabled: '<',
    statesPrefix: '<',
    gameName: '<',
    lastGameWeek: '<',
    hasContainerClass: '<',
    maxPlayersSameClub: '<'
  },
  controller: function ($scope, $rootScope, teamService, $stateParams, gettextCatalog, growl, PROMO_BANNER_URL, ISSUE_WEEK_IDS) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Team");

    $rootScope.$broadcast('sidemenu:team:collapse', { team: vm.squad.Team.Team.id });

    let teamsFilterLabel = gettextCatalog.getString('Alle ploegen');

    if (['GCX'].indexOf(vm.squad.Team.League.competition_feed) !== -1) {
      teamsFilterLabel = gettextCatalog.getString('All countries');
    }


    // clubs
    vm.clubs.unshift({
      Club: {
        id: 0,
        name: teamsFilterLabel
      }
    });

    vm.teamId = $stateParams.id;
    vm.isMyTeam = $rootScope && $rootScope.teams && $rootScope.teams.find(function (team) {
      return team.Team.id == vm.teamId;
    });

    vm.displayWeekNotification = null;

    // cycling parent
    vm.starting = [];
    vm.frontman = getFrontMan();
    vm.matches = [];
    vm.starting = arrangeStarting();
    vm.sponsor = getSponsor();
    vm.currentWeekRaces = [];
    vm.isFetching = false;
    vm.lastMatchInTheLeague = null;
    vm.currentWeekMatches = [];
    vm.currentWeekPoints = null;
    vm.currentWeekRank = null;
    vm.endGameBonusPlayersPoints = [];

    var squadCompetitionFeed = vm.squad && vm.squad.Team && vm.squad.Team.League && vm.squad.Team.League.competition_feed;
    vm.promoBannerURL = PROMO_BANNER_URL[squadCompetitionFeed];
    vm.promoBannerImage = `/images/promo-banner-${squadCompetitionFeed}.jpg`;

    vm.getWeekRaces = function () {
      return vm.matches && vm.matches.stages
        .filter(filterByCurrentWeek)
        .map(getRaceProps);
    };

    function filterByCurrentWeek(match) {
      if (vm.currentDisplayWeek && vm.currentDisplayWeek.week_id) {
        return match.matches.week_id == vm.currentDisplayWeek.week_id;
      }
      return false;
    }

    function getRaceProps(race) {
      return {
        date: getLocalDate(race.matches.date),
        feed_url: race.matches.feed_url,
        type: race.matches.type
      }
    }

    vm.frontmanSave = function () {
      teamService.changeCaptainById(vm.squad.Team.Team.id, vm.currentDisplayWeek.week_id, vm.frontman.Player.id, vm.squad.Team.League.competition_feed)
        .then(function (result) {
          growl.addSuccessMessage(gettextCatalog.getString('Cycling captain changed!'));
        });
    };

    vm.frontmanChanged = function () {
      // Substitute runners
      var tmp = vm.starting[0];
      tmp.Selection.captain = 0;
      vm.frontman.Selection.captain = 1;

      var index = null;
      _.each(vm.starting, function (c, i) {
        if (c.hasOwnProperty('Player') && c.Player.id == vm.frontman.Player.id)
          index = i;
      });

      vm.starting[0] = vm.frontman;
      vm.starting[index] = tmp;
    };

    // All players
    vm.budget = vm.gameBudget;
    vm.teamName = {
      value: ''
    };
    vm.filterOptions = {
      clubs: vm.clubs,
      budgets: [
        {
          budget: 100,
          text: 'Alle budgetten'
        },
        {
          budget: 25,
          text: 'Onder 25 miljoen'
        },
        {
          budget: 20,
          text: 'Onder 20 miljoen'
        },
        {
          budget: 15,
          text: 'Onder 15 miljoen'
        },
        {
          budget: 10,
          text: 'Onder 10 miljoen'
        },
        {
          budget: 5,
          text: 'Onder 5 miljoen'
        }
      ]
    };
    vm.filterItem = {
      club: vm.clubs[0],
      budget: vm.filterOptions.budgets[0]
    };

    vm.filterChanged = function () {
      vm.pagination.current = 1;
    };

    vm.searchPlayer = { value: "" };
    vm.customFilter = function (cyclist) {
      // Check budget
      if (cyclist && (cyclist.Player.value / 10) > vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if (vm.filterItem.club.Club.id !== 0 && cyclist.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }
      // Check name
      else if (vm.searchPlayer.value !== "" && cyclist.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    // Players pagination
    vm.pagination = {
      current: 1
    };
    vm.activated = false;
    activate();

    function setCurrentWeekMatches(weekId) {
      const searchWeekId = weekId || vm.week_id;
      vm.currentWeekMatches = vm.matches.stages && vm.matches.stages.filter(function (stage) {
        return stage.matches.week_id === searchWeekId;
      });
    }

    function activate() {
      teamService.getCyclingMatches(vm.squad.Team.League.season_id, vm.squad.Team.League.competition_feed)
        .then(function (result) {
          vm.matches = result;

          if (vm.matches && vm.matches.week && vm.matches.week.deadline_week && vm.lastGameWeek) {
            vm.matches.weeks = vm.matches.weeks.filter(deadlineWeek => deadlineWeek.week_id < vm.lastGameWeek);
          }

          if ((vm.matches && vm.matches.week && vm.matches.week.deadline_week) && !vm.isMyTeam) {
            vm.matches.weeks = vm.matches.weeks.filter(deadlineWeek =>
              deadlineWeek.week_id < vm.matches.week.deadline_week);
          }

          vm.lastMatchInTheLeague = vm.matches.stages[vm.matches.stages.length - 1];
          vm.deadline_week_id = result.week.deadline_week;
          vm.week_id = result.week.display_week;

          if (!vm.isMyTeam) {
            const pointsWeek = (vm.matches && vm.matches.week && vm.matches.week.deadline_week) && vm.matches.week.deadline_week > 1 ?
              vm.matches.week.deadline_week - 1 : vm.week_id;
            vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
              return stage.matches.week_id === pointsWeek;
            }).matches;

          } else {
            vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
              return stage.matches.week_id === vm.week_id;
            }).matches;
          }

          vm.isFirstWeek = vm.squad.weeks.deadline_week === vm.squad.Team.Team.week_id;
          // vm.isFirstWeek = true;

          setCurrentWeekMatches();
          updateLineUp();

          vm.currentWeekPoints = vm.teamPoints();
          vm.currentWeekRank = vm.teamRank();
        });
    }

    vm.leagueIsOver = leagueIsOver;

    function leagueIsOver() {
      return vm.lastMatchInTheLeague && vm.lastMatchInTheLeague.matches && (vm.lastMatchInTheLeague.matches.week_id + 1) === vm.week_id;
    }

    vm.changePlayDate = function (type) {
      var valid = true;

      if (type == 'prev' && vm.currentDisplayWeek.week_id <= 1) {
        valid = false;
      }
      else if (type == 'next' && vm.currentDisplayWeek.week_id == vm.matches.weeks[vm.matches.weeks.length - 1]) {
        valid = false;
      }

      if (valid) {
        if (type == 'prev') {
          const prevMatch = vm.matches.stages && vm.matches.stages.find(function (stage) {
            return stage.matches.week_id === vm.currentDisplayWeek.week_id - 1;
          });

          if (prevMatch) {
            vm.currentDisplayWeek = prevMatch.matches;
          }

        }
        else if (type == 'next') {
          const nextMatch = vm.matches.stages && vm.matches.stages.find(function (stage) {
            return stage.matches.week_id === vm.currentDisplayWeek.week_id + 1;
          });

          if (nextMatch) {
            vm.currentDisplayWeek = nextMatch.matches;
          }
        }

        setCurrentWeekMatches(vm.currentDisplayWeek.week_id);
        vm.currentWeekPoints = vm.teamPoints();
        vm.currentWeekRank = vm.teamRank();
        vm.week_id = vm.currentDisplayWeek;

        updateLineUp();
      }
    };

    function getFrontMan() {
      return vm.starting && vm.starting.find(function (cyclist) {
        return cyclist.Selection.captain;
      });
    }

    function getSponsor() {
      var clubId = vm.starting && vm.starting[0] && vm.starting[0].Selection.sponsor_id;
      return vm.sponsors && vm.sponsors.find(function (sponsor) {
        return sponsor.Club.id === clubId;
      });
    }

    function updateLineUp() {
      vm.isFetching = true;
      vm.endGameBonusPlayersPoints = [];
      teamService.get(vm.squad.Team.Team.id, vm.currentDisplayWeek.week_id)
        .then(function (res) {
          vm.starting = res.starting;
          vm.frontman = getFrontMan();
          vm.starting = arrangeStarting();
          vm.sponsor = getSponsor();
          vm.currentWeekRaces = vm.getWeekRaces();
          vm.week_id = res.week;
          vm.isFetching = false;

          if (leagueIsOver()) {
            vm.starting[0].Selection.captain = 0;
          }

          if (res && res.Team && res.Team.Team && res.Team.Team.paid && (vm.currentDisplayWeek.week_id === vm.lastGameWeek - 1)) {
            const matchesIds =  vm.matches.stages.map(stage => stage.matches.id);

            vm.starting
              .forEach(startingPlayer => {
                const endGameBonusPoints = startingPlayer && startingPlayer.Player && startingPlayer.Player.Stat
                  && startingPlayer.Player.Stat.find(item => matchesIds.includes(item.match_id)  && item.week_id === vm.lastGameWeek && item.stat_id === 1);

                if (endGameBonusPoints) {
                  vm.endGameBonusPlayersPoints.push({
                    Player: {
                      id: startingPlayer.Player.id,
                      name: startingPlayer.Player.name
                    },
                    Stat: Object.assign({}, endGameBonusPoints, { value: JSON.parse(endGameBonusPoints.value) })
                  })
                }
              });
          }
          vm.displayWeekNotification = ISSUE_WEEK_IDS[vm.squad.Team.League.competition_feed]
            .find(notification => notification.weekId === vm.currentDisplayWeek.week_id);

          vm.activated = true;
        })
        .catch(function (error) {
          console.log('error', error);
        });
    }

    function arrangeStarting() {
      if (!vm.starting) {
        return [];
      }

      var withClubs = vm.starting && vm.starting.map(function (cyclist) {
        var club = vm.clubs.find(function (clubItem) {
          return clubItem.Club.id == cyclist.Player.club_id
        });

        if (club) {
          return Object.assign({}, cyclist, { Club: club.Club })
        } else {
          return cyclist;
        }

      });

      var withoutCaptain = withClubs && withClubs.filter(function (cyclist) {
        return !cyclist.Selection.captain;
      });

      vm.frontman = withClubs && withClubs.find(function (cyclist) {
        return cyclist.Selection.captain;
      });

      if (vm.frontman) {
        return [vm.frontman].concat(withoutCaptain) || [];
      } else {
        return withoutCaptain;
      }
    }

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
    }

    vm.hasTeamPoints = function () {
      return !angular.isUndefined(vm.teamPoints);
    };

    vm.currentWeekIsLessThanDeadlineWeek = currentWeekIsLessThanDeadlineWeek;

    function currentWeekIsLessThanDeadlineWeek() {
      const isLeagueIsOver = vm.currentDisplayWeek && (vm.currentDisplayWeek.week_id > vm.deadline_week_id) && (vm.deadline_week_id === 1);
      return vm.currentDisplayWeek && (vm.currentDisplayWeek.week_id < vm.deadline_week_id) || isLeagueIsOver || !vm.deadline_week_id;
    }

    vm.getTeamPoints = getTeamPoints;

    function getTeamPoints() {
      var totalPoints = 0;
      vm.starting.forEach(function (cyclist) {
        if (cyclist && cyclist.Selection) {
          totalPoints += cyclist.Selection.points;
        }
      });
      return totalPoints;
    }

    vm.isFirstDeadlineWeek = function () {
      return vm.squad.weeks.deadline_week === vm.squad.Team.Team.week_id;
    };

    vm.isDraggable2 = function (player) {
      const rightCompetition = ['CROS', 'KLAS'].indexOf(vm.squad.Team.League.competition_feed) !== -1;
      return vm.week_id && (vm.week_id >= vm.matches.week.deadline_week) && rightCompetition;
    };

    vm.onDragStart2 = function ($event) {
      $event.stopPropagation();

      vm.dragElem = $event.target;
      var player = angular.element($event.target).scope();

      if (!$event.target.classList.contains('player-bench'))
        vm.fieldDrag = true;

      $scope.$apply();
    };

    vm.onDragStop2 = function ($event) {
      $scope.$apply(function () {
        vm.fieldDrag = false;

        setTimeout(function () {
          $scope.$apply(function () {
            vm.dragElem = null;
          });
        }, 50);
      });
    };

    vm.isBenchDroppable = function (player) {
      if (vm.dragElem !== null && vm.fieldDrag)
        return true;

      return false;
    };

    vm.isFieldDroppable = function (type, player) {
      if ($scope.dragElem !== null && !$scope.fieldDrag)
        return true;

      return false;
    };

    function lineupPicks() {
      var startingPicks = [];
      var benchPicks = [];

      if (vm.squad.Team.League.competition_feed === 'CROS') {
        vm.starting.forEach(function (player, index) {
          if (index < 8) {
            startingPicks.push(player.Player.id);
          } else {
            benchPicks.push(player.Player.id);
          }
        });
      } else if (vm.squad.Team.League.competition_feed === 'KLAS') {
        vm.starting.forEach(function (player, index) {
          if (index < 10) {
            startingPicks.push(player.Player.id);
          } else {
            benchPicks.push(player.Player.id);
          }
        });
      }

      return {
        startingPicks,
        benchPicks
      }
    }

    vm.sponsorSave = function () {
      var lineUpPicksResult = lineupPicks();
      var startingPicks = lineUpPicksResult.startingPicks;
      var benchPicks = lineUpPicksResult.benchPicks;

      teamService.saveCyclingSelection(
        vm.squad.Team.Team.id,
        vm.currentDisplayWeek.week_id,
        startingPicks,
        benchPicks,
        startingPicks[0],
        (vm.sponsor && vm.sponsor.Club && vm.sponsor.Club.id) || null,
        vm.squad.Team.League.competition_feed
      )
        .then(function (result) {
          $rootScope.$broadcast('team:refresh');
          growl.addSuccessMessage(result.data.message);
        })
    };

    vm.getLineUpLayout = function () {
      const competitionFeed = vm.squad.Team.League.competition_feed;
      const defaultLayout = 'lineup-cycling';

      if (competitionFeed === 'CROS') {
        if (vm.sponsor && vm.sponsor.Club && vm.isMyTeam && vm.currentDisplayWeek.type !== 'worldchampionship' && !leagueIsOver()) {
          return 'background-' + vm.sponsor.Club.id;
        } else {
          return defaultLayout;
        }
      } else if (competitionFeed === 'GCX') {
        return 'lineup-cx';
      } else if (competitionFeed === 'KLAS') {
        if (vm.sponsor && vm.sponsor.Club && vm.isMyTeam) {
          return 'background-' + vm.sponsor.Club.id + ' cycling-classic-lineup';
        } else {
          return 'klassiekers-layout cycling-classic-lineup';
        }

      } else {
        return defaultLayout;
      }
    };

    var getObjIndexInArray = function (arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (obj == arr[i])
          return i;
      }
    };

    function getStartingIndex(player, index) {
      var index = null;

      for (var i = 0; i < vm.starting.length; i++) {
        if (vm.starting[i].Player.id === player.Player.id) {
          index = i;
        }
      }

      return index;
    }

    vm.onDropStop2 = function ($event, $index) {
      // Check if not dropped onto same type
      var dragged = angular.element(vm.dragElem).scope();
      var dropped = angular.element($event.target).scope();

      var draggedIndex = getStartingIndex(dragged.cyclist);
      var droppedIndex = getStartingIndex(dropped.cyclist);

      if (dragged.cyclist.Selection.captain) {
        dragged.cyclist.Selection.captain = false;
        dropped.cyclist.Selection.captain = true;
      }

      if (dropped.cyclist.Selection.captain) {
        dropped.cyclist.Selection.captain = false;
        dragged.cyclist.Selection.captain = true;
      }

      vm.starting[draggedIndex] = angular.copy(dropped.cyclist);
      vm.starting[droppedIndex] = angular.copy(dragged.cyclist);

      vm.frontman = getFrontMan();

      //swap the indexes
      $scope.dragElem = null;
      // Save selection to backend

      var lineUpPicksResult = lineupPicks();
      var startingPicks = lineUpPicksResult.startingPicks;
      var benchPicks = lineUpPicksResult.benchPicks;

      teamService.saveCyclingSelection(
        vm.squad.Team.Team.id,
        vm.currentDisplayWeek.week_id,
        startingPicks,
        benchPicks,
        startingPicks[0],
        (vm.sponsor && vm.sponsor.Club && vm.sponsor.Club.id) || null,
        vm.squad.Team.League.competition_feed
      )
        .then(function (result) {
          $rootScope.$broadcast('team:refresh');
          growl.addSuccessMessage(result.data.message);
        })
    };

    vm.editSelection = function () {
      if (vm.currentDisplayWeek.week_id < vm.matches.week.deadline_week) {
        vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
          return stage.matches.week_id === vm.matches.week.deadline_week;
        }).matches;
        setCurrentWeekMatches(vm.matches.week.deadline_week);
        updateLineUp();

        vm.currentWeekPoints = vm.teamPoints();
        vm.currentWeekRank = vm.teamRank();
      }
    };

    vm.teamPoints = function () {
      const currentStat = vm.squad.Team.Weekstat.find(weekStat => weekStat.week_id === vm.currentDisplayWeek.week_id);
      return currentStat && currentStat.points;
    }

    vm.teamRank = function () {
      const currentStat = vm.squad.Team.Weekstat.find(weekStat => weekStat.week_id === vm.currentDisplayWeek.week_id);
      return currentStat && currentStat.rank;
    }
  }
};