import template from './cycling-simple-match-info.component.tpl.html';

export default {
  template,
  bindings: {
    matches: '<',
    points: '<',
    rank: '<',
    competitionFeed: '<',
    statesPrefix: '<',
    totalPoints: '<',
    totalRank: '<',
    endGameBonusPlayersPoints: '<'
  },
  controller: function (UtilsService, $state) {
    var vm = this;
    vm.stages = [];

    vm.getLocalDate = getLocalDate;

    setStages(vm.matches);

    function setStages(matches) {
      vm.stages = matches.map(match => match.matches);
    }

    this.$onChanges = function (bindings) {
      if (bindings.matches && bindings.matches.currentValue && bindings.matches) {
        setStages(bindings.matches.currentValue);
      }
    };

    vm.goToCyclist = function (playerId) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-cyclistProfile', {cyclistId: playerId});
      } else {
        $state.go('cyclistProfile', {cyclistId: playerId});
      }
    };

    vm.goToMatch = function (raceId) {
      if (vm.statesPrefix) {
        $state.go(`${vm.statesPrefix}-cyclingRace`, {race: raceId});
      } else {
        $state.go('cyclingRace', {race: raceId});
      }
    };

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('ddd DD MMMM');
    }
  }
};