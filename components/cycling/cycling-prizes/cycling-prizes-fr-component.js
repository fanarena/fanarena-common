import template from './cycling-prizes-fr.component.tpl.html';

export default {
  template,
  controller: function (appService) {
    var vm = this;
    vm.isFrench = appService.translateToFrance();
  }
};