import template from './cycling-prizes.component.tpl.html';

export default {
  bindings: {
    enabledRankings: '<',
    hasContainerClass: '<'
  },
  template,
  controller: function ($rootScope, gettextCatalog, FACms) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString('Prizes');

    vm.activeTab = 1;
    vm.setActiveTab = setActiveTab;
    vm.rankingsEnabled = vm.enabledRankings || ['general', 'week', 'peloton', 'peloton-week', 'sprint', 'mountain'];
    vm.prizesMap = {};

    activate();

    function activate() {
      return FACms.getPrizes(vm.rankingsEnabled)
        .then(result => {
          vm.prizesMap = result;
        })
    }

    function setActiveTab(tabNumber) {
      vm.activeTab = tabNumber;
    }
  }
};