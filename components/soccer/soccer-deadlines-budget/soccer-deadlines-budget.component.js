import template from './soccer-deadlines-budget.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    globalLeagueId: '<',
    competitionInfo: '<',
    counterEnabled: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    useLocalFolder: '<',
    gameName: '<',
    matchesPhases: '<',
    deadlinesTableConfig: '<',
    doNotCheckIfDeadlineWeekHasMatch: '<'
  },
  controller: function (teamService, $rootScope, gettextCatalog, $stateParams) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Races");
    
    vm.weeks = null;
    vm.setCalendarWeek = setCalendarWeek;
    vm.calendarWeekId = $stateParams.activeWeekId || 1;
    vm.currentCompetition = {
      name: vm.competitionInfo.competitionFeed,
      competition_feed: vm.competitionInfo.competitionFeed,
      id: vm.globalLeagueId
    };

    vm.deadlineWeekHasMatches = false;

    activate();

    function activate() {
      teamService.getCyclingMatches(vm.competitionInfo.seasonId, vm.competitionInfo.competitionFeed)
        .then(function(result){
          return teamService.gameWeekHasAnyMatch(result.week.deadline_week, vm.competitionInfo.competitionFeed)
            .then(function (hasAnyMatch) {
              vm.deadlineWeekHasMatches = hasAnyMatch;
              if(!vm.deadlineWeekHasMatches && !vm.doNotCheckIfDeadlineWeekHasMatch) {
                vm.weeks = result.weeks.filter(week => week.week_id !== result.week.deadline_week);
              } else {
                vm.weeks = result.weeks;
              }
            });
        });
    }

    function setCalendarWeek(week) {
      vm.calendarWeekId = week.week_id;
    }
  }
};