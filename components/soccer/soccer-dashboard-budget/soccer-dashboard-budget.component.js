import template from './soccer-dashboard-budget.component.tpl.html';

export default {
  template,
  controller: function (teamService, userService, $window, $rootScope, $state, growl, gettextCatalog) {
    var vm = this;
    vm.getUnpaidTeams = getUnpaidTeams;
    vm.payTeams = payTeams;
    vm.removeTeam = removeTeam;
    vm.addCoupon = addCoupon;
    vm.removeCoupon = removeCoupon;
    vm.getTeamPoints = getTeamPoints;
    vm.getTeamRank = getTeamRank;
    vm.editTeam = editTeam;
    vm.updateTeam = updateTeam;
    vm.cancelEditTeam = cancelEditTeam;
    vm.validateCoupon = validateCoupon;
    vm.couponValid = couponValid;
    vm.cloneTeam = cloneTeam;
    vm.onKeyPress = onKeyPress;
    vm.coupons = [];
    vm.getTeamsPaidWithCoupons = getTeamsPaidWithCoupons;
    vm.couponValidationClass = couponValidationClass;
    $rootScope.title = gettextCatalog.getString("Dashboard");
    vm.editTeams = {};
    vm.validCoupons = [];
    vm.centsForATeam = 700;
    vm.getPreviousWeekTeamPoints = getPreviousWeekTeamPoints;
    vm.getPreviousWeekTeamRank = getPreviousWeekTeamRank;


    activate();

    function activate() {
      teamService.getAll()
        .then(function (teams) {
          vm.teams = teams.filter(filterCycling);
          return teamService.getDeadlineWeeks('2018', 'WC')
        })
        .then(function (data) {
          vm.deadlineWeeks = data.weeks;
          vm.displayWeek = data.week.display_week;
          vm.deadlineWeek = data.week.deadline_week;
        });
    }

    function filterCycling(team) {
      return team['League'].competition_feed === 'WC';
    }

    function getUnpaidTeams() {
      return vm.teams.filter(function (team) {
        return !parseInt(team.Team.paid);
      });
    }

    function getTeamsPaidWithCoupons() {
      var totalCents = 0;
      vm.validCoupons.forEach(function (coupon) {
        totalCents += parseInt(coupon.Coupon.value);
      });
      return totalCents ? (totalCents / vm.centsForATeam) : 0;
    }

    function payTeams() {
      var amount = getUnpaidTeams().length * 700;
      var coupons = [];

      vm.validCoupons.forEach(function (coupon) {
        coupons.push(coupon.Coupon.code);
      });

      return userService.pay(amount, coupons.join(','))
        .then(function (result) {
          if (result.payment) {
            if (result.payment.links) {
              $window.location.href = result.payment.links.paymentUrl;
            } else {
              $rootScope.$broadcast('sidemenu:refresh');
              growl.addSuccessMessage(result.payment);
              vm.validCoupons = [];
              vm.coupons = [];
              activate();
            }
          }
          else if (result.message) {
            growl.addErrorMessage(result.message);
          }
        });
    }

    function removeTeam(team) {
      teamService.delete(team.Team.id).then(function (res) {
        growl.addSuccessMessage('Successfully deleted!');
        $rootScope.$broadcast('sidemenu:refresh');
        activate();
      });
    }

    function onKeyPress(event, teamId) {
      if (event.keyCode === 13) {
        updateTeam(vm.editTeams[teamId]);
      }
    }

    function addCoupon() {
      var couponsLength = vm.coupons.length;
      var toBePaid = vm.getUnpaidTeams().length - vm.getTeamsPaidWithCoupons();
      var previousProvidedCoupon = vm.coupons[couponsLength - 1];
      var isPreviousValid = vm.validCoupons.find(function (coupon) {
        return previousProvidedCoupon === coupon.Coupon.code;
      });

      if (couponsLength === 5) {
        return;
      }

      if (couponsLength > 0 && !isPreviousValid) {
        growl.addErrorMessage('You must provide a valid coupon at a time!');
        return;
      }

      if (toBePaid === 0) {
        growl.addErrorMessage('No need to provide a coupon!');
        return;
      }

      vm.coupons.push('');
    }

    function removeCoupon(couponIndex) {
      vm.validCoupons = vm.validCoupons.filter(function (coupon) {
        return coupon.Coupon.code !== vm.coupons[couponIndex];
      });
      vm.coupons.splice(couponIndex, 1);
    }

    function getTeamPoints(team) {
      var points = team.Team.points; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].points;
      if (!points) {
        return 0;
      }
      return points;
    }

    function getTeamRank(team) {
      var rank = team.Team.rank; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].rank;
      if (!rank) {
        return '-';
      }
      return rank;
    }

    function hasPreviousStats(team) {
      if(!vm.displayWeek || vm.displayWeek < 1) {
        return false;
      }
      return true;
    }

    function getPreviousTeamStats(team) {
      return team.Weekstat.find(function(weekstat){
        return weekstat.week_id === vm.displayWeek;
      });
    }

    function getPreviousWeekTeamPoints(team) {
      if (hasPreviousStats(team)) {
        var previousWeekStat = getPreviousTeamStats(team);
        return previousWeekStat && previousWeekStat.points || 0;
      } else {
        return 0;
      }
    }

    function getPreviousWeekTeamRank(team) {
      if (hasPreviousStats(team)) {
        var previousWeekStat = getPreviousTeamStats(team);
        return previousWeekStat && previousWeekStat.rank;
      } else {
        return 0;
      }
    }

    function editTeam(team) {
      vm.editTeams[team.Team.id] = Object.assign({}, team.Team, {inviteURL: ''});
    }

    function cancelEditTeam(team) {
      vm.editTeams[team.id] = null;
    }

    function updateTeam(team) {
      team.name = team.name.replace(new RegExp(',', 'g'), '');
      teamService.edit(team)
        .then(function () {
          growl.addSuccessMessage('Team successfully modified!');
          $rootScope.$broadcast('sidemenu:refresh');
          cancelEditTeam(team);
          activate();
        });
    }

    function validateCoupon(couponIndex) {
      if (vm.coupons[couponIndex] && (vm.coupons[couponIndex].length > 6) && (vm.coupons[couponIndex].length < 9)) {
        userService.validateCoupon(vm.coupons[couponIndex])
          .then(function (result) {
            var alreadyUsed = vm.validCoupons.find(function (coupon) {
              return coupon.Coupon.code === result.coupon.Coupon.code;
            });

            if (result.coupon && !alreadyUsed) {
              vm.validCoupons.push(result.coupon);
            }
          });
      }
    }

    function couponValid(couponIndex) {
      var couponValue = vm.coupons[couponIndex];
      if (vm.coupons[couponIndex] && vm.coupons[couponIndex].length < 7) {
        return true;
      }
      return !!vm.validCoupons.find(function (coupon) {
        return coupon.Coupon.code === couponValue;
      });
    }

    function couponValidationClass(couponIndex) {
      var couponValue = vm.coupons[couponIndex];
      var couponValid = !!vm.validCoupons.find(function (coupon) {
        return coupon.Coupon.code === couponValue;
      });

      if (couponValue && couponValue.length < 7) {
        return '';
      } else {
        return couponValid ? 'fa fa-check-circle-o text-success' : 'fa fa-exclamation-triangle text-danger';
      }
    }

    function cloneTeam(team) {
      teamService.clone(team.Team.id)
        .then(function () {
          growl.addSuccessMessage('Successfully copied!');
          $rootScope.$broadcast('sidemenu:refresh');
          activate();
        });
    }
  }
};