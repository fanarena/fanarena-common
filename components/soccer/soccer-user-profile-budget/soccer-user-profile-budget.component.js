import template from './soccer-user-profile-budget.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<',
  },
  controller: function ($rootScope, userService, localStorageService, growl, gettextCatalog) {
    $rootScope.title = gettextCatalog.getString("Profile");
    $rootScope.$broadcast('sidemenu:team:collapse');

    var vm = this;
    vm.profile = JSON.stringify(this.user, null, "   ");
    vm.user = localStorageService.get('user');
    vm.bornDateFilter = userService.getBornDateFilters();
    vm.dobArr = (vm.user && vm.user.dob && vm.user.dob.split('-')) || [];

    vm.born = {
      day: vm.bornDateFilter.days.find(day => day.id === parseInt(vm.dobArr[2])),
      month: vm.bornDateFilter.months.find(month => month.id === parseInt(vm.dobArr[1])),
      year: vm.bornDateFilter.years.find(year => year.id === parseInt(vm.dobArr[0]))
    };

    vm.editProfile = function (frm) {
      let valid = true;

      if(!vm.born.year || !vm.born.month || !vm.born.day) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('Please select your birth date.'));
        return;
      }

      const dobMoment = moment(`${vm.born.year.label}-${vm.born.month.label}-${vm.born.day.label}`, 'YYYY-MM-DD');
      const dob = dobMoment.format('YYYY-MM-DD');

      if (vm.user.post_code && vm.user.post_code.length !== 4 && !angular.isNumber(vm.user.post_code)) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('Postal code must be 4 digits!'));
      }

      if (moment.utc().diff(dobMoment, 'years') < 12) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('You must be at least 12 years old!'));
      }

      if (frm.$valid) {
        if (valid) {

          var obj = {
            'User': {
              'first_name': vm.user.first_name,
              'dob': dob,
              'last_name': vm.user.last_name,
              'username': vm.user.username,
              'phone': vm.user.phone,
              'street': vm.user.street,
              'street_nr': vm.user.street_nr,
              'post_code': vm.user.post_code,
              'city': vm.user.city,
              'gender': vm.user.gender,
              'optin': vm.user.optin
            }
          };

          userService.edit(vm.user.id, obj).then(function (res) {
            localStorageService.set('user', res.user.User);
            localStorageService.set('token', res.token);
            growl.addSuccessMessage('Profile updated!');

            $rootScope.$broadcast('profile:updated');
          });
        } else {
          return;
        }
      } else {
        growl.addErrorMessage(gettextCatalog.getString('Please complete all the fields') /*'Vul alles in'*/);
      }
    };

    vm.editPassword = function (frm) {
      if (frm.$valid) {
        var obj = {
          "newpassword": vm.newPassword
        };

        userService.edit(vm.user.id, obj).then(function (res) {
          growl.addSuccessMessage('Password updated!');
          localStorageService.set('token', res.token);
          vm.newPassword = vm.verifyNewPassword = "";
          frm.$setPristine();
        });
      }
    };

    vm.editReferral = function (frm) {
      if (frm.$valid) {
        var obj = {
          "referrer": vm.referral
        };

        userService.edit(vm.user.id, obj).then(function (res) {
          localStorageService.set('token', res.token);
          growl.addSuccessMessage('Referral added!');
        });
      }
    };

    vm.logout = function () {
      localStorageService.clearAll();
      $state.go('publicHome');
    };
  }
};