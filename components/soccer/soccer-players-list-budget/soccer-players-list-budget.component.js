import template from './soccer-players-list-budget.component.tpl.html';

export default {
  template,
  bindings: {
    competitionInfo: '<',
    localAssets: '<',
    gameName: '<',
    hasContainerClass: '<',
    clubAssetsURL: '<',
    jerseyOnly: '<',
    showCoach: '<',
    hidePositionBackground: '<',
    simpleShirts: '<'
  },
  controller: function (playerService, gameService, $rootScope, gettextCatalog, SPORTS_ASSETS, $ngRedux, $scope,
                        storeActions, $stateParams, $location, $anchorScroll) {

    this.$onInit = function () {
      let disconnect = $ngRedux.connect(mapStateToParams, Object.assign({},
        storeActions.playersActions, storeActions.clubsActions))(this);
      $scope.$on('$destroy', disconnect);

      vm.seasons = [
        {id: 2016, label: `${gettextCatalog.getString('Season')} 2015/2016`},
        {id: 2017, label: `${gettextCatalog.getString('Season')} 2016/2017`},
        {id: 2018, label: `${gettextCatalog.getString('Season')} 2017/2018`},
        {id: 2019, label: `${gettextCatalog.getString('Season')} 2018/2019`},
      ];

      vm.selectedSeason = vm.seasons.find(season => season.id === vm.competitionInfo.seasonId);
      
      const clubs = this.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const players = this.clubs.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);

      if (!players || !clubs) {
        fetchPlayers()
          .then(() => {
            if (!clubs) {
              return fetchClubs();
            } else {
              return players
            }
          })
          .then(() => initialize());
      }

      if (players && clubs) {
        initialize()
      }
    };

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    var vm = this;
    vm.selectedPlayer = null;
    vm.selectedPlayerInfo = null;
    vm.assetsURL = SPORTS_ASSETS.football.remote;
    vm.currentLanguage = gettextCatalog.getCurrentLanguage();

    if (vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    $rootScope.title = gettextCatalog.getString("Spelerslijst");

    vm.selectPlayer = selectPlayer;

    vm.filterOptions = {
      clubs: [],
      budgets: [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    vm.positions = [
      {id: -1, name: gettextCatalog.getString('All positions')},
      {id: 1, name: gettextCatalog.getString('Goalkeeper')},
      {id: 2, name: gettextCatalog.getString('Defender')},
      {id: 3, name: gettextCatalog.getString('Midfielder')},
      {id: 4, name: gettextCatalog.getString('Attacker')}
    ];

    if (vm.showCoach) {
      vm.positions.push({id: 0, name: gettextCatalog.getString('Coach')});
    }

    vm.filterItem = {
      budget: 999999999,
      text: 'Alle prijzen',
      selectedPosition: vm.positions[0]
    };

    vm.customFilter = function (player) {
      if (vm.filterItem.budget && player && player.Player && player.Player.value > vm.filterItem.budget.budget) {
        return false;
      }

      if (vm.filterItem.club && vm.filterItem.club.Club.id !== 0 && player.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }

      // Check position
      else if (vm.filterItem.selectedPosition.id !== -1 && player.Player.position_id !== vm.filterItem.selectedPosition.id) {
        return false;
      }

      else if (vm.searchPlayer.value !== "" && player.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    vm.filterChanged = function () {
      vm.pagination.current = 1;
    };
    vm.searchPlayer = {value: ""};
    vm.pagination = {current: 1};

    function fetchPlayers() {
      return playerService.getAll(vm.competitionInfo.competitionFeed)
        .then(function (players) {
          $ngRedux.dispatch(storeActions.playersActions.setFootballPlayers({
            competitionFeed: vm.competitionInfo.competitionFeed,
            players
          }));
        })
    }

    function fetchClubs() {
      return gameService.getClubs(vm.competitionInfo.competitionFeed)
        .then(function (clubs) {
          $ngRedux.dispatch(storeActions.clubsActions.setFootballClubs({
            competitionFeed: vm.competitionInfo.competitionFeed,
            clubs: [{
              Club: {
                id: 0,
                name: gettextCatalog.getString('All clubs') /*'Alle clubs'*/
              }
            }].concat(clubs)
          }));
        })
    }

    function initialize() {
      const clubsInfo = vm.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const playersInfo = vm.players.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      vm.clubs = (clubsInfo && clubsInfo.clubs) || [];
      vm.players = (playersInfo && playersInfo.players) || [];

      if (vm.clubs.length) {
        vm.filterOptions.clubs = vm.clubs;
        vm.filterItem = {
          club: vm.clubs[0],
          budget: vm.filterOptions.budgets[0],
          selectedPosition: vm.positions[0]
        };
      }

      const positionsToShow = [1, 2, 3, 4];
      if (vm.showCoach) {
        positionsToShow.push(0);
      }

      vm.players = vm.players
        .map(player => {
          const playerClub = vm.clubs.find(club => player.Club.id === club.Club.id);
          return Object.assign({}, player, {Club: playerClub && playerClub.Club});
        })
        .filter(player => {
          return positionsToShow.indexOf(player.Player.position_id) !== -1;
        });

      if (vm.players.length) {

        vm.selectedPlayer = $stateParams.subjectPlayerId ?
          vm.players.find(player => player.Player.id === $stateParams.subjectPlayerId) :
          vm.players[0];

        playerService.get(vm.selectedPlayer.Player.id, null, vm.competitionInfo.competitionFeed, vm.selectedSeason.id)
          .then(function (playerInfo) {
            vm.selectedPlayerInfo = playerInfo;

            if ($stateParams.subjectPlayerId && $rootScope.isMobile) {
              $location.hash('playerStat');
              $anchorScroll();
            }
          });
      }
    }

    vm.getValueOject = function (value) {
      return angular.fromJson(value);
    };

    function selectPlayer(player) {
      vm.selectedPlayer = player;

      playerService.get(vm.selectedPlayer.Player.id, null, vm.competitionInfo.competitionFeed, vm.selectedSeason.id)
        .then(function (playerInfo) {
          vm.selectedPlayerInfo = playerInfo;

          if ($rootScope.isMobile) {
            $location.hash('playerStat');
            $anchorScroll();
          }
        });
    }

    vm.getPositionNameById = function (positionId) {
      switch (positionId) {
        case 0:
          return gettextCatalog.getString('Coach');
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
        default:
          return gettextCatalog.getString('Goalkeeper');
      }
    };
  }
};