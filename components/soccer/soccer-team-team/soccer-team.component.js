import template from './soccer-team.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    timeleft: '<'
  },
  controller: function ($scope, $rootScope, $state, $interval, timerService, teamService, $pusher, growl) {
    var self = this;

    $scope.current = 0;
    self.$state = $state;

    var stop;
    $scope.start = function () {
      if (angular.isDefined(stop)) return;

      stop = $interval(function () {
        if ($scope.current > 0)
          $scope.current--;
        else
          $scope.stop();

      }, 1000);
    };

    $scope.stop = function () {
      if (angular.isDefined(stop)) {
        $interval.cancel(stop);
        stop = undefined;
      }
    };

    $scope.reset = function (time) {
      $scope.current = time;
      $scope.start();
    };

    $scope.$on('$destroy', function () {
      $scope.stop();
    });

    $rootScope.$on('timer:reset', function (event, args) {
      $scope.reset(args.value);
    });

    $rootScope.$on('team:refresh', function () {
      teamService.get(self.team.Team.Team.id).then(function (res) {
        self.team = res;
      });
    });

    if (self.team.Team.League.status == 2 && self.timeleft !== null) {
      var latency = timerService.stopTimer();
      $scope.reset(self.timeleft - (latency / 1000));
    }

    // Initialize pusher
    var pusher = $pusher(pusherClient);
    var channel = pusher.subscribe('league-' + self.team.Team.League.id);
    channel.bind('start', function (data) {
      $scope.$apply(function () {
        growl.addSuccessMessage("Draft has started!");
        self.team.Team.League.status = 2;
      });
    });
  }
};
