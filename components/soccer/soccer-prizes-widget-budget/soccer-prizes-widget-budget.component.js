import template from './soccer-prizes-widget-budget.component.tpl.html';

export default {
  template,
  controller: function (gameService, $rootScope) {
    var vm = this;
    vm.showSubscribed = showSubscribed;

    activate();

    function activate() {
      gameService.getSubscribed('WC')
        .then(function (data) {
          vm.subscriptions = data.count;
        });
    }

    function showSubscribed() {
      return !!$rootScope.user;
    }
  }
};