import template from './soccer-home-budget.component.tpl.html';

export default {
  template,
  bindings: {
    teams: '<',
    deadlineWeeks: '<',
    statesPrefix: '<',
    user: '<',
    deadlineInfo: '<',
    competitionInfo: '<',
    weeklyWinnerEnabled: '<',
    jerseyOnly: '<',
    captainFactor: '<',
    deadlinesTableConfig: '<',
    bannerEnabled: '<',
    captainWithTeamPoints: '<',
    gameName: '<',
    captainPointsForWinOnly: '<'
  },
  controller: function ($state, $rootScope, $scope, playerService, teamService, GoogleAnalyticsService, $auth,
                        localStorageService, userService, COMPETITION_FEED_TO_FACEBOOK_PAGE, gettextCatalog,
                        COMPETITION_FEED_TO_SEASON_ID, CAPTAIN_FACTOR_BY_COMPETITION_FEED, matchService,
                        appGamesService) {
    $scope.playersByPicks = [];
    $scope.playersByPoints = [];

    var vm = this;

    vm.captainWinOnlyPoints = ['G11', 'CL'].indexOf(vm.competitionInfo.competitionFeed) !== -1;
    vm.lastPlayedWeek = null;
    vm.confirmMessage = null;
    vm.seasonId = COMPETITION_FEED_TO_SEASON_ID[vm.competitionInfo.competitionFeed];
    vm.captainFactor = vm.captainFactor ?
      vm.captainFactor : CAPTAIN_FACTOR_BY_COMPETITION_FEED[vm.competitionInfo.competitionFeed];
    vm.hasContainerClass = true;

    vm.currentLocale = gettextCatalog.getCurrentLanguage();
    vm.fbPageURL = COMPETITION_FEED_TO_FACEBOOK_PAGE[vm.competitionInfo.competitionFeed];

    vm.currentCompetition = {
      name: vm.competitionInfo.competitionFeed,
      competition_feed: vm.competitionInfo.competitionFeed,
      id: -1
    };
    vm.infoFetched = false;

    vm.deadlineWeekId = vm.deadlineInfo.week.deadline_week;

    activate();

    // Check if login/register route
    if ($state.current.name === 'publicLogin') {
      $rootScope.$broadcast('login:show');
    }
    else if ($state.current.name === 'publicRegister') {
      $rootScope.$broadcast('register:show');
    }
    else if ($state.current.name === 'publicResetPasswordCode') {
      $rootScope.$broadcast('reset:show');
    }

    function activate() {
      playerService.getAllStats(vm.competitionInfo.competitionFeed, vm.seasonId)
        .then(function (result) {
          $scope.playersByPicks = result.picks;
          $scope.playersByPoints = result.points;

          return matchService.getPointsConfirmation(vm.competitionInfo.competitionFeed, vm.seasonId)
            .then((res) => {
              vm.checkWeekId = vm.deadlineInfo.week.deadline_week ? vm.deadlineInfo.week.deadline_week - 1 : vm.deadlineInfo.week.display_week;
              vm.lastPlayedWeek = res && res.data && res.data.find(week => week.week_id === vm.checkWeekId);
              vm.confirmMessage = (vm.lastPlayedWeek && vm.lastPlayedWeek.confirmed) ?
                gettextCatalog.getString('Confirmed!') :
                gettextCatalog.getString('Unconfirmed');
              vm.infoFetched = true;
            });
        })
        .catch(function (error) {
          vm.infoFetched = true;
        });
    }

    vm.fbLogin = function () {
      return userService.fbLogin(vm.statesPrefix);
    };

    vm.login = function () {
      $state.go('publicLogin');
    };

    vm.register = function () {
      $state.go('publicRegister');
    };

    vm.onNewGameClick = function () {
      if (!vm.user) {
        $state.go('publicRegister', {newGameRedirect: true});
      } else {
        $state.go(appGamesService.getActiveGameSlug() + '-newGame');
      }
    }

    vm.goToMatch = function (week) {
      if (vm.statesPrefix) {
        $state.go(`${vm.statesPrefix}-publicRaces`, {activeWeekId: week.week_id});
      } else {
        $state.go('publicRaces', {activeWeekId: week.week_id});
      }
    }
  }
};