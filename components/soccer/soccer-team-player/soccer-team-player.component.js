import template from './soccer-team-player.component.tpl.html';

export default {
  template,
  bindings: {
    player: '<',
    team: '<',
    deadlineWeeks: '<',
    showJerseys: '<',
    localAssets: '<'
  },
  controller: function ($scope, RESOURCES, $rootScope, leagueService, SPORTS_ASSETS) {
    $rootScope.$broadcast('sidemenu:team:collapse', { team: this.team.Team.Team.id });
    $rootScope.title = this.player.Player.Player.name;

    var self = this;

    self.resources = RESOURCES;

    self.assetsURL = SPORTS_ASSETS.football.remote;

    if (self.localAssets) {
      self.assetsURL = SPORTS_ASSETS.football.local;
    }

    $scope.isActivePlayer = function (player) {
      if (player.active)
        return true;

      return false;
    };

    self.toggleMock = function () {
      leagueService.toggleMock(self.player.Player.Player.id, self.team.Team.Team.id).then(function (res) {
        if (self.player.Player.Mock.length == 0)
          self.player.Player.Mock.push(res.Mocks.Mock);
        else
          self.player.Player.Mock = [];
      });
    };

    self.sortStats = function (stat1) {
      return stat1.Match.week_id;
    };
  }
};