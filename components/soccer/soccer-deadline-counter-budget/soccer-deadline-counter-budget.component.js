import template from './soccer-deadline-counter-budget.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    blockClass: '=',
    competitionInfo: '<'
  },
  controller: function (teamService) {
    var vm = this;

    activate();

    function activate() {
      teamService.getDeadlineWeeks(vm.competitionInfo.seasonId, vm.competitionInfo.competitionFeed)
        .then(function (result) {
          var stageFeedURL = 'Match day: ' + result.week.deadline_week;
          var date = moment
            .utc(result.week.deadline_date, 'YYYY-MM-DD HH:mm:ss')
            .local()
            .format('YYYY-MM-DD HH:mm:ss');
          vm.deadlineWeek = result.week.deadline_week;
          vm.deadlineDate = date;
          vm.stageFeedURL = stageFeedURL;
        });
    }
  }
};