import template from './soccer-squad-budget.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    ignoreContainerClass: '<'
  },
  controller: function ($scope, $rootScope, teamService, gettextCatalog) {
    $rootScope.title = gettextCatalog.getString("Squad");
    var vm = this;

    $rootScope.$on('team:refresh', function () {
      teamService.get(vm.team.Team.Team.id).then(function (res) {
        vm.team = res;
      });
    });
  }
};