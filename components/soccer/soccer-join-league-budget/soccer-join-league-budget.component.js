import template from './soccer-join-league-budget.component.tpl.html';

export default {
  template,
  controller: function (localStorageService, $state, $stateParams) {
    if ($stateParams.inviteLeagueId)
      localStorageService.set('inviteLeagueId', $stateParams.inviteLeagueId);

    if (localStorageService.get('user') === null) {
      $state.go('publicRegister');
    }
    else {
      if ($stateParams.inviteLeagueId) {
        $state.go('newGame');
      } else {
        $state.go('dashboard');
      }
    }
  }
};