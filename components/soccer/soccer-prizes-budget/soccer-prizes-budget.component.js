import template from './soccer-prizes-budget.component.tpl.html';

export default {
  template,
  controller: function ($rootScope, gettextCatalog) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("Prizes");

    vm.activeTab = 1;

    vm.setActiveTab = setActiveTab;

    function setActiveTab(tabNumber) {
      vm.activeTab = tabNumber;
    }
  }
};