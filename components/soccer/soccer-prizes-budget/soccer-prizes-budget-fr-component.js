import template from './soccer-prizes-budget-fr.component.tpl.html';

export default {
  template,
  controller: function (appService) {
    var vm = this;
    vm.isFrench = appService.translateToFrance();
  }
};