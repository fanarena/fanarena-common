import template from './soccer-new-game-custom-league-info.component.tpl.html';

export default {
  template,
  bindings: {
    increaseNewGameProgressStep: '='
  },
  controller: function ($location, $anchorScroll) {
    var vm = this;

    vm.onIncreaseNewGameProgressStep = function() {
      vm.increaseNewGameProgressStep();
      $location.hash('newGame');
      $anchorScroll();
    }
  }
};