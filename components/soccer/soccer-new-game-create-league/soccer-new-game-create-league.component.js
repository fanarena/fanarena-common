import template from './soccer-new-game-create-league.component.tpl.html';

export default {
  template,
  bindings: {
    game: '<',
    competitions: '<',
    createLeagueCompetition: '=',
    increaseNewGameProgressStep: '=',
    createLeagueName: '=',
    createLeagueDeadline: '=',
    dateTimeBeforeRender: '=',
    createLeague: '=',
    isHeadToHead: '='
  },
  controller: function (growl) {
    var vm = this;

    vm.next = next;

    function next() {
      if (!vm.createLeagueName) {
        growl.addErrorMessage('Please choose a league name');
        return;
      }

      if(!vm.createLeagueCompetition) {
        growl.addErrorMessage('Please choose a competition');
        return;
      }

      this.increaseNewGameProgressStep();
    }

  }
};