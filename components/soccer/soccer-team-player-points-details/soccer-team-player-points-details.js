import template from './soccer-team-player-points-details.html';

export default {
  template,
  bindings: {
    starting: '<',
    bench: '<',
    gameName: '<',
    captainFactor: '<',
    captainWithTeamPoints: '<',
    captainFactorByTeamResult: '<'
  },
  controller: function (playerService) {
    var vm = this;

    vm.startingWithInfo = [];
    vm.benchWithInfo = [];
    vm.categoriesResult = {};

    this.$onChanges = function (bindings) {
      const newStarting = bindings.starting && bindings.starting.currentValue && bindings.starting.currentValue;
      const newBench = bindings.bench && bindings.bench.currentValue && bindings.bench.currentValue
      if (newStarting && newBench) {
        vm.starting = bindings.starting.currentValue;
        vm.bench = bindings.bench.currentValue;
        activate();
      }
    };

    activate();

    function activate() {
      vm.categoriesResult = {
        goals: 0,
        assists: 0,
        yellow: 0,
        red: 0,
        cleanSheet: 0,
        conceeded: 0,
        halfTime: 0,
        teamResultPoints: 0,
        captain: 0,
        total: 0
      };

      vm.startingWithInfo = vm.starting
        .map(player =>
          playerService.computePoints(player, vm.captainFactor, vm.captainFactorByTeamResult, vm.captainWithTeamPoints))
        .filter(item => !!item);

      vm.benchWithInfo = vm.bench
        .map(player => playerService.computePoints(player))
        .filter(item => !!item);

      vm.categoriesResult = vm.startingWithInfo.reduce((acc, item) => {
        return {
          goals: item.goals + acc.goals,
          assists: item.assists + acc.assists,
          yellow: item.yellow + acc.yellow,
          red: item.red + acc.red,
          cleanSheet: item.cleanSheet + acc.cleanSheet,
          conceeded: item.conceeded + acc.conceeded,
          halfTime: item.halfTime + acc.halfTime,
          teamResultPoints: item.teamResultPoints + acc.teamResultPoints,
          captain: item.captain + acc.captain,
          total: item.total + acc.total
        };
      }, Object.assign({}, vm.categoriesResult));
    }
  }
};