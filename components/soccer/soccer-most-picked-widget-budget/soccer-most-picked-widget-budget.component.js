import template from './soccer-most-picked-widget-budget.component.tpl.html';

export default {
  template,
  bindings: {
    players: '<',
    localAssets: '<',
    gameName: '<',
    jerseyOnly: '<',
  },
  controller: function (SPORTS_ASSETS, gettextCatalog, playerService) {
    var vm = this;
    vm.title = gettextCatalog.getString('Most picked footballers');
    vm.span1 = "keer gekozen";
    vm.assetsURL = SPORTS_ASSETS.football.remote;

    if(vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    vm.getPlayerStatus = function(player) {
      return playerService.playerStatusEmojiName(player);
    }
  }
};