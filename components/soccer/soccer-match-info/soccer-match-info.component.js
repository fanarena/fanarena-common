import template from './soccer-match-info.component.tpl.html';

export default {
  template,
  bindings: {
    points: '<',
    totalPoints: '<',
    rank: '<',
    totalRank: '<',
    competitionFeed: '<',
    statesPrefix: '<'
  },
  controller: function (UtilsService, $state) {
    var vm = this;
    vm.stages = [];
  }
};