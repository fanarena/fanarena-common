import template from './soccer-player-budget.component.tpl.html';

export default {
  template,
  bindings: {
    player: '<',
    teamId: '<',
    preventClick: '=',
    popoverClick: '=',
    showTransfer: '<',
    statesPrefix: '<',
    gameName: '<',
    captain: '<',
    captainWithTeamPoints: '<',
    showShortPosition: '<',
    onRemove: '=',
    mouseDownOnly: '<',
    localAssets: '<',
    captainFactor: '<',
    dragAndDropEnabled: '<',
    pointsDisabled: '<',
    captainFactorByTeamResult: '<',
    hidePositionBackground: '<',
    showPrice: '<'
  },
  controller: function (RESOURCES, $rootScope, $state, $stateParams, playerService, growl, SPORTS_ASSETS,
                        gettextCatalog) {
    var vm = this;
    vm.resources = RESOURCES;
    vm.popoverTpl = "playerPopover.html";
    vm.alreadyDown = false;
    vm.playerStatus = playerService.playerStatusEmojiName(vm.player);

    $rootScope.$on('popover:close', function (ev, args) {
      vm.player.popoverOpen = false;
    });

    vm.assetsURL = SPORTS_ASSETS.football.remote;

    if (vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    vm.playerInfo = (vm.player && vm.player.Player) || (vm.player && vm.player.players);
    vm.clubInfo = (vm.player && vm.player.Club) || (vm.player && vm.player.clubs);

    vm.remove = function (player, $event) {
      if (!vm.mouseDownOnly && !vm.alreadyDown) {
        this.onRemove(player, $event);
      }
    };

    vm.pointsAsCaptain = 'C';

    if (vm.player.pts && vm.player.pts.points !== null && vm.player.pts.played) {
      const factor = vm.captainFactor || 1;

      if(!vm.captainFactorByTeamResult) {
        vm.pointsAsCaptain = vm.player.pts.points * factor;
      } else {
        if(vm.player.pts.value && vm.player.pts.value.result === 'W') {
          vm.pointsAsCaptain = vm.player.pts.points * factor;
        } else {
          vm.pointsAsCaptain = vm.player.pts.points;
        }
      }

      if (vm.captainWithTeamPoints && vm.player.pts.value) {
        vm.pointsAsCaptain += vm.player.pts.value.team_points;
      }
    }

    vm.click = function () {

      if(vm.player && vm.player.selections && vm.player.selections.blanko) {
        const playerId = (vm.player && vm.player.Player && vm.player.Player.id) ||
          (vm.player && vm.player.players && vm.player.players.id);
        $rootScope.$broadcast('blanko:show', {id: playerId});
      }

      // Show popover?
      if (!vm.popoverClick) {
        vm.goToProfile();
      }
      else {
        // Close all others
        $rootScope.$broadcast('popover:close');
        vm.player.popoverOpen = true;
      }
    };

    vm.hasRemove = function () {
      return !!vm.onRemove;
    };

    vm.goToProfile = function () {
      if (vm.player && vm.player.Player) {
        if (vm.teamId !== undefined && !vm.preventClick) {
          $state.go('teamPlayer', {teamId: vm.teamId, playerId: vm.player.Player.id});
        } else if (!vm.preventClick) {
          if (vm.player.hasOwnProperty('player_id')) {
            $state.go('publicPlayer', {playerId: vm.player.Player.id});
          }
          else {
            $state.go('publicPlayer', {playerId: vm.player.Player.id});
          }
        }
      }
    };

    vm.onRemoveMouseDown = function (player, $event) {
      if (!vm.alreadyDown) {
        $event.stopPropagation();
        $event.preventDefault();
        vm.alreadyDown = true;
        this.onRemove(player, $event);
      }
    }

    vm.transfer = function () {
      playerService.transfer(vm.player.picks.id, vm.player.picks.team_id).then(function (res) {
        growl.addSuccessMessage(vm.player.players.name + ' was transferred out!');
        $rootScope.$broadcast('bench:remove', {obj: vm.player});
      });
    };

    vm.dblClick = function ($event) {
      if (!vm.alreadyDown) {
        this.onRemove(player, $event);
      }
    }

    vm.getPositionShort = function (positionId) {
      switch (positionId) {
        case 1:
          return gettextCatalog.getString('goalkeeper-short-label');

        case 2:
          return gettextCatalog.getString('defender-short-label');

        case 3:
          return gettextCatalog.getString('midfielder-short-label');

        case 4:
          return gettextCatalog.getString('attacker-short-label');

        default:
          return gettextCatalog.getString('goalkeeper-short-label');
      }
    }
  }
};