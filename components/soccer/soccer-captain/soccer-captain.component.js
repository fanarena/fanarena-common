import template from './soccer-captain.component.tpl.html';

export default {
  template,
  bindings: {
    starting: '<',
    captain: '<',
    changeCaptainHandler: '=',
    showJerseys: '<',
    localAssets: '<'
  },
  templateUrl: 'soccer-captain/soccer-captain.component.tpl.html',
  controller: function (RESOURCES, $rootScope, SPORTS_ASSETS, POSITIONS) {
    var vm = this;
    vm.resources = RESOURCES;
    vm.popoverTpl = "captain-selections-list.html";
    vm.popoverOpen = false;
    vm.popoverStartingListOpen = false;
    vm.startingListPageIndex = 1;
    vm.assetsURL = SPORTS_ASSETS.football.remote;

    if(vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    vm.onPickCaptainClick = function(captain) {
      vm.closeStartingList();
      vm.changeCaptainHandler(captain);
    };

    $rootScope.$on('popover:close', function (ev, args) {
      vm.popoverOpen = false;
    });

    vm.closeStartingList = function() {
      $rootScope.$broadcast('popover:close');
      vm.popoverStartingListOpen = false;
    };

    vm.showStartingList = function () {
      $rootScope.$broadcast('popover:close');
      vm.popoverStartingListOpen = true;
    };

    vm.getPositionName = function(positionId) {
      return POSITIONS[positionId];
    }

  }
};