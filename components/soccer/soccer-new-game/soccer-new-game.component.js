import template from './soccer-new-game.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    inviteLeague: '<'
  },
  templateUrl: 'soccer-new-game/soccer-new-game.component.tpl.html',
  controller: function ($scope, $rootScope, $state, localStorageService, gameService, leagueService, growl,
                        NewGameCacheService, Hashids, $location) {
    $rootScope.title = "New game";

    var vm = this;

    vm.params = $location.search();

    vm.newGameProgressStep = NewGameCacheService.getProgress();
    vm.game = {
      teamName: '',
      league: null
    };
    vm.isHeadToHead = 1;
    vm.leagues = null;
    vm.competitions = gameService.getCompetitions();
    vm.competition = null;
    vm.createCompetition = vm.competitions[0];
    vm.leagueType = 'all';
    vm.currentPage = 1;
    vm.createLeagueName = '';
    vm.createLeagueDeadline = null;
    vm.createLeagueCompetition = vm.competitions[0];
    vm.dateTimeBeforeRender = leagueService.validateDeadlinePicker;

    vm.getLeagueRange = getLeagueRange;
    vm.getLeagues = getLeagues;
    vm.filterType = filterType;
    vm.joinExisting = joinExisting;
    vm.joinPlaySports = joinPlaySports;
    vm.playNewGameWithFans = playNewGameWithFans;
    vm.createNewGameWithFriends = createNewGameWithFriends;
    vm.joinInviteLeague = joinInviteLeague;
    vm.joinLeague = joinLeague;
    vm.createLeague = createLeague;
    vm.openModal = openModal;
    vm.decreaseNewGameProgressStep = decreaseNewGameProgressStep;
    vm.increaseNewGameProgressStep = increaseNewGameProgressStep;
    vm.checkIf = checkIf;
    vm.getActiveTab = getActiveTab;

    activate();

    function getLeagueRange() {
      if (vm.leagues !== null)
        return vm.leagues.slice(((vm.currentPage - 1) * 2), ((vm.currentPage) * 2));
      else
        return [];
    }

    function getLeagues() {
      var compFeed = null;
      if (vm.competition)
        compFeed = vm.competition.competition_feed;

      gameService.getPublicLeagues(compFeed).then(function (res) {
        if (compFeed === 'BE1' || compFeed === null) {
          res.unshift({
            League: {
              competition_feed: 'BE1',
              name: 'Play Sports'
            },
            playsports: true
          });
        }

        vm.leagues = res;
      });
    }

    function filterType() {

      var ps = {
        League: {
          competition_feed: 'BE1',
          name: 'Play Sports'
        },
        playsports: true
      };

      if (vm.leagueType === 'draft' || vm.leagueType === 'all') {
        var compFeed = null;
        if (vm.competition)
          compFeed = vm.competition.competition_feed;

        gameService.getPublicLeagues(compFeed).then(function (res) {
          if (vm.leagueType === 'all' && compFeed && vm.competition.competition_feed === 'BE1') {
            res.unshift(ps);
            vm.leagues = res;
          }
          else {
            vm.leagues = res;
          }
        });
      }
      else if (vm.competition === null || vm.competition.competition_feed === 'BE1') {
        vm.leagues = [ps];
      }
      else {
        vm.leagues = [];
      }
    };

    function joinExisting(league) {
      if (vm.game.teamName !== '') {
        vm.game.invite = null;
        vm.game.league = league;
        vm.game.competition = _.findWhere(vm.competitions, {competition_feed: league.League.competition_feed});
        vm.game.gameMode = {key: 'managed'};
        vm.joinLeague();
      }
      else {
        growl.addErrorMessage('Please choose a team name');
      }
    }

    function joinPlaySports() {

      if (vm.game.teamName) {

        var league = vm.leagues.find(function (league) {
          return league.playsports;
        });
        vm.game.invite = null;
        delete vm.game.league;
        vm.game.competition = _.findWhere(vm.competitions, {competition_feed: league.League.competition_feed});
        vm.game.gameMode = {key: 'public'};
        vm.joinLeague();
      }
      else {
        growl.addErrorMessage('Please choose a team name');
      }
    }

    function playNewGameWithFans() {
      vm.activeTab = NewGameCacheService.setGameMode(1);
    }

    function createNewGameWithFriends() {
      vm.activeTab = NewGameCacheService.setGameMode(2);
    }

    function getActiveTab() {
      return NewGameCacheService.getGameMode();
    }

    function joinInviteLeague() {
      if (vm.game.teamName !== '') {
        vm.game.league = null;
        vm.game.league_id = vm.inviteLeague.id;
        vm.game.invite = vm.invite;
        vm.game.competition = _.findWhere(vm.competitions, {competition_feed: vm.inviteLeague.competition_feed});
        vm.game.gameMode = null;
        vm.joinLeague();
      }
      else {
        growl.addErrorMessage('Please choose a team name');
      }
    }

    function joinLeague() {
      gameService.joinLeague(vm.game)
        .then(function (data) {

          localStorageService.remove('inviteLeagueId');
          NewGameCacheService.resetGameMode();
          NewGameCacheService.resetProgress();

          if (data.error) {
            growl.addErrorMessage(data.error);
            return;
          } else {
            $state.go('teamLeague', {id: data.team.Team.id, leagueId: data.team.Team.league_id, '#': null});
          }

          // Refresh sidebar
          $rootScope.$broadcast('sidemenu:refresh');
        }, function (err) {
          if (err.message)
            growl.addErrorMessage(err.message);
        });
    }

    function createLeague() {
      var valid = true;
      if (!vm.game.teamName) {
        growl.addErrorMessage('Please choose a team name');
        valid = false;
      }
      if (!vm.createLeagueName) {
        growl.addErrorMessage('Please choose a league name');
        valid = false;
      }

      if (valid) {
        var deadline = moment(vm.createLeagueDeadline);
        deadline.utc();

        vm.game.league = {
          name: vm.createLeagueName,
          deadline: deadline.format('YYYY-MM-DD HH:mm:ss')
        };
        vm.game.competition = vm.createLeagueCompetition;
        vm.game.gameMode = {
          key: 'draft'
        };

        gameService.createLeague(vm.game)
          .then(function (data) {
            NewGameCacheService.resetGameMode();
            NewGameCacheService.resetProgress();

            growl.addSuccessMessage('New game created');

            // Refresh sidebar
            $rootScope.$broadcast('sidemenu:refresh');

            $state.go('teamLeague', {id: data.team.Team.id, leagueId: data.team.Team.league_id, '#': null});
          });
      }
    }

    function openModal(type) {
      if (type == 'draft')
        vm.modalPS = false;
      else
        vm.modalPS = true;

      vm.showModal = true;
    }

    function decreaseNewGameProgressStep() {
      if (NewGameCacheService.getProgress() > 2) {
        NewGameCacheService.decrementProgress();
      }
    }

    function increaseNewGameProgressStep() {
      NewGameCacheService.incrementProgress();
    }

    function checkIf(label) {
      switch (label) {
        case 'league-type-selection':
          return NewGameCacheService.getProgress() === 2;

        case 'existing-league-is-selected':
          return NewGameCacheService.getGameMode() === 1;

        case 'play-with-friends':
          return NewGameCacheService.getGameMode() === 2;

        case 'is-joining-league':
          return NewGameCacheService.getProgress() === 3 && NewGameCacheService.getGameMode() === 1;

        case 'is-creating-league':
          return NewGameCacheService.getProgress() === 3 && NewGameCacheService.getGameMode() === 2;

        case 'is-joining-a-league':
          return NewGameCacheService.getProgress() === 4 && NewGameCacheService.getGameMode() === 1;

        case 'is-creating-league-last-step':
          return NewGameCacheService.getProgress() === 4 && NewGameCacheService.getGameMode() === 2;


        default:
          return false;
      }
    }

    function activate() {
      vm.getLeagues();

      if (vm.inviteLeague) {
        vm.activeTab = NewGameCacheService.setGameProgress(4);
        vm.activeTab = NewGameCacheService.setGameMode(1);
      }
    }
  }
};