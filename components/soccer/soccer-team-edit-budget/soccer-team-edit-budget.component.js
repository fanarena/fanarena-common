import template from './soccer-team-edit-budget.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    localAssets: '<',
    showJerseys: '<',
    hidePositionBackground: '<',
    clubAssetsURL: '<',
    benchEnabled: '<',
    coachEnabled: '<',
    competitionInfo: '<',
    budgetValue: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    matches: '<',
    unlimitedTransfersWeeks: '<',
    gameName: '<',
    draggableFieldPlayers: '<',
    maxPlayersSameClub: '<',
    labels: '<'
  },
  controller: function ($scope, $rootScope, teamService, growl, $stateParams, gettextCatalog, SPORTS_ASSETS,
                        PlayerService, POSITIONSIDS, FootballPicker, storeHelpers, storeActions,
                        $ngRedux, playerService, $state) {
    const vm = this;
    vm.resolved = false;

    this.$onInit = function () {
      const shouldEdit = (vm.team.Team.Team.week_id === vm.team.weeks.deadline_week) || isInTeamEditsDeadlineWeek();
      if(!shouldEdit) {
        if (vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-squadTeam', {id: vm.team.Team.Team.id, '#': null});
        } else {
          $state.go('squadTeam', {id: vm.team.Team.Team.id, '#': null});
        }
        return;
      } else {
        vm.resolved = true;
      }

      if (!vm.benchEnabled) {
        FootballPicker.setPositionPickLimit('Bench', {min: 0, max: 0});
      }

      let disconnect = $ngRedux.connect(mapStateToParams, Object.assign({},
        storeActions.playersActions, storeActions.clubsActions))(this);
      $scope.$on('$destroy', disconnect);
      const clubs = this.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const players = this.clubs.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);

      if (!players && !clubs) {
        storeHelpers.initPlayers()
          .then(() => {
            if (!clubs) {
              return storeHelpers.initClubs()
                .then(() => activate());
            } else {
              activate();
            }
          });
      } else {
        activate();
      }
    };

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    vm.team = this.team;
    vm.players = this.players;
    vm.starting = [];
    vm.bench = [];
    vm.clubs = this.clubs;
    vm.teamChanges = 0;
    vm.displayWeek = vm.team.weeks.display_week;
    vm.deadlineWeek = vm.team.weeks.deadline_week;
    vm.defaultWeek = vm.team.week;
    vm.localAssets = this.localAssets;
    vm.assetsURL = SPORTS_ASSETS.football.remote;
    vm.teamPlayersNumber = vm.benchEnabled ? 15 : 11;

    vm.capitalize = function (string) {
      return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
    }

    if (this.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    vm.maxPositionPicks = {
      Goalkeeper: {
        min: 1,
        max: 1
      },
      Defender: {
        min: 3,
        max: 5
      },
      Midfielder: {
        min: 3,
        max: 5
      },
      Forward: {
        min: 1,
        max: 3
      }
    };

    $rootScope.title = gettextCatalog.getString("Edit Team");

    vm.teamId = $stateParams.id;

    vm.searchPlayer = {value: ''};
    vm.budget = resetBudget(vm.starting);

    vm.positions = [
      {id: -1, name: gettextCatalog.getString('All positions')},
      {id: 1, name: gettextCatalog.getString('Goalkeeper')},
      {id: 2, name: gettextCatalog.getString('Defender')},
      {id: 3, name: gettextCatalog.getString('Midfielder')},
      {id: 4, name: gettextCatalog.getString('Attacker')}
    ];

    if (vm.coachEnabled) {
      vm.positions.push({id: 0, name: 'Coach'})
    }

    vm.removeCoach = function (player, event) {
      event.preventDefault();
      vm.budget += PlayerService.getPlayerProp(player, 'value');
      vm.selectedCoach = null;
    };

    vm.activateCoachFilter = function () {
      vm.filterItem.selectedPosition = vm.positions[vm.positions.length - 1];
    };

    const competitionsBudges = {
      'GWC': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'G11': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'CL': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    vm.filterOptions = {
      budgets: competitionsBudges[vm.competitionInfo.competitionFeed]
    };

    vm.filterItem = {
      budget: vm.filterOptions.budgets[0],
      selectedPosition: vm.positions[0],
      teamPlayers: getStartingListForCaptain()
    };

    vm.captain = getCaptainFromStartingList();

    if (vm.coachEnabled) {
      vm.selectedCoach = vm.team.starting[0][0];
    }

    function getCaptainFromStartingList() {
      return vm.filterItem.teamPlayers
        .find(function (player) {
          return player.captain;
        });
    }

    function getCoachFromStartingList() {
      return
    }

    function isInTeamEditsDeadlineWeek() {
      return vm.team && (vm.unlimitedTransfersWeeks.indexOf(vm.team.weeks.deadline_week) !== -1);
    }

    vm.pagination = {
      current: 1
    };

    vm.customFilter = function (player) {
      // Check budget
      if (player && player.Player && player.Player.value > vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if (player && player.Player && player.Club && vm.filterItem && vm.filterItem.club && vm.filterItem.club.Club.id !== 0 && player.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }
      // Check position
      else if (vm.filterItem && vm.filterItem.selectedPosition && vm.filterItem.selectedPosition.id !== -1 && player.Player.position_id !== vm.filterItem.selectedPosition.id) {
        return false;
      }
      // Check name
      else if (vm.searchPlayer.value !== "" && player.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    // New drag drop
    vm.isDraggable2 = function (player) {
      return vm.draggableFieldPlayers;
    };

    vm.onDragStart2 = function ($event) {
      $event.stopPropagation();

      vm.dragElem = $event.target;
      var player = angular.element($event.target).scope();

      if (!$event.target.classList.contains('player-bench'))
        vm.fieldDrag = true;

      $scope.$apply();
    };

    vm.onDragStop2 = function ($event) {
      $scope.$apply(function () {
        vm.fieldDrag = false;

        setTimeout(function () {
          $scope.$apply(function () {
            vm.dragElem = null;
          });
        }, 50);
      });
    };

    vm.isBenchDroppable = function (player) {
      if (vm.dragElem !== null && vm.fieldDrag)
        return true;

      return false;
    };

    vm.isFieldDroppable = function (type, player) {
      if (vm.dragElem !== null && !vm.fieldDrag)
        return true;

      return false;
    };

    var getObjIndexInArray = function (arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (obj == arr[i])
          return i;
      }
    };


    vm.onDropStop2 = function ($event, $index) {
      // Check if not dropped onto same type
      var dragged = angular.element(vm.dragElem).scope();
      var dropped = angular.element($event.target).scope();
      var validFormation = true;

      // Drop onto bench
      if ($event.target.classList.contains('player-bench')) {
        var fieldPlayer = null;
        var benchPlayer = null;
        var index = 0;

        var type = 'goalkeeper';
        if (dragged.hasOwnProperty('defender'))
          type = 'defender';
        if (dragged.hasOwnProperty('midfielder'))
          type = 'midfielder';
        if (dragged.hasOwnProperty('forward'))
          type = 'forward';


        fieldPlayer = dragged[type];
        benchPlayer = dropped.player;

        var startingWithoutDragged = getStartingList().filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(fieldPlayer, 'id')
        });

        var benchWithoutDropped = vm.bench.filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(benchPlayer, 'id')
        });

        FootballPicker.set(startingWithoutDragged, benchWithoutDropped.concat([fieldPlayer]));
        validFormation = FootballPicker.pick(benchPlayer).picked;

        if (!validFormation) {
          // Get indexes in arrays
          benchIndex = getObjIndexInArray(vm.bench, benchPlayer);
          fieldIndex = getObjIndexInArray(vm.starting[vm.capitalize(type)], fieldPlayer);

          // Remove from arrays
          vm.bench = _.without(vm.bench, _.findWhere(vm.bench, {$$hashKey: benchPlayer.$$hashKey}));
          vm.starting[vm.capitalize(type)] = _.without(vm.starting[vm.capitalize(type)], _.findWhere(vm.starting[vm.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          // Make copies
          benchPlayer = angular.copy(benchPlayer);
          fieldPlayer = angular.copy(fieldPlayer);

          // Add them again
          vm.bench.splice(benchIndex, 0, benchPlayer);
          vm.starting[vm.capitalize(type)].splice(fieldIndex, 0, fieldPlayer);
        }
        else {
          index = getObjIndexInArray(vm.bench, benchPlayer);
          vm.starting[vm.capitalize(type)] = _.without(vm.starting[vm.capitalize(type)], _.findWhere(vm.starting[vm.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 1)
            vm.starting.Goalkeeper.unshift(benchPlayer);
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 2)
            vm.starting.Defender.unshift(benchPlayer);
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 3)
            vm.starting.Midfielder.unshift(benchPlayer);
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 4)
            vm.starting.Forward.unshift(benchPlayer);

          vm.bench = _.without(vm.bench, _.findWhere(vm.bench, {$$hashKey: benchPlayer.$$hashKey}));
          vm.bench.splice(index, 0, fieldPlayer);
        }
      }
      // Drop onto field
      else {
        var fieldPlayer = null;
        var benchPlayer = null;
        var index = 0;

        var type = 'goalkeeper';
        if (dropped.hasOwnProperty('defender'))
          type = 'defender';
        if (dropped.hasOwnProperty('midfielder'))
          type = 'midfielder';
        if (dropped.hasOwnProperty('forward'))
          type = 'forward';

        benchPlayer = _.findWhere(vm.bench, {$$hashKey: dragged.player.$$hashKey});
        fieldPlayer = _.findWhere(vm.starting[vm.capitalize(type)], {$$hashKey: dropped[type].$$hashKey});

        var startingWithoutDropped = getStartingList().filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(fieldPlayer, 'id')
        });

        var benchWithoutDragged = vm.bench.filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(benchPlayer, 'id')
        });

        FootballPicker.set(startingWithoutDropped, benchWithoutDragged.concat([fieldPlayer]));
        var validFormation = FootballPicker.pick(benchPlayer).picked;

        if (!validFormation) {
          // Get indexes in arrays
          var benchIndex = getObjIndexInArray(vm.bench, benchPlayer);
          var fieldIndex = getObjIndexInArray(vm.starting[vm.capitalize(type)], fieldPlayer);

          // Remove from arrays
          vm.bench = _.without(vm.bench, _.findWhere(vm.bench, {$$hashKey: benchPlayer.$$hashKey}));
          vm.starting[vm.capitalize(type)] = _.without(vm.starting[vm.capitalize(type)], _.findWhere(vm.starting[vm.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          // Make copies
          benchPlayer = angular.copy(benchPlayer);
          fieldPlayer = angular.copy(fieldPlayer);

          // Add them again
          vm.bench.splice(benchIndex, 0, benchPlayer);
          vm.starting[vm.capitalize(type)].splice(fieldIndex, 0, fieldPlayer);

        }
        else {
          // Remove from bench
          vm.bench = _.without(vm.bench, benchPlayer);
          var replaceIndex = getObjIndexInArray(vm.starting[vm.capitalize(type)], fieldPlayer);
          vm.starting[vm.capitalize(type)] = _.without(vm.starting[vm.capitalize(type)], fieldPlayer);


          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 1) {
            vm.starting.Goalkeeper.splice(replaceIndex, 0, benchPlayer);
          }
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 2) {
            vm.starting.Defender.splice(replaceIndex, 0, benchPlayer);
          }
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 3) {
            vm.starting.Midfielder.splice(replaceIndex, 0, benchPlayer);
          }
          if (PlayerService.getPlayerProp(benchPlayer, 'position_id') == 4) {
            vm.starting.Forward.splice(replaceIndex, 0, benchPlayer);
          }

          vm.bench.unshift(fieldPlayer);
        }
      }

      vm.filterItem.teamPlayers = getStartingListForCaptain();

      vm.bench = vm.bench.map(function (benchPlayer) {
        if (benchPlayer.selections && benchPlayer.selections.captain) {
          benchPlayer.selections.captain = false;
        }
        return benchPlayer
      });

      var startignClean = getStartingList()
        .filter(function (player) {
          return PlayerService.getPlayer(player);
        });

      var benchClean = vm.bench
        .filter(function (player) {
          return PlayerService.getPlayer(player);
        });

      FootballPicker.set(startignClean, benchClean);
      vm.possibleFormations = FootballPicker.getPossibleFormations();

      var droppedIsCaptain = vm.captain && PlayerService.getPlayerProp(dropped[type], 'id') === vm.captain.id;
      var draggedIsCaptain = vm.captain && PlayerService.getPlayerProp(dragged[type], 'id') === vm.captain.id;

      if (validFormation && droppedIsCaptain) {
        vm.captain = vm.filterItem.teamPlayers.find(function (teamPlayer) {
          return teamPlayer.id === PlayerService.getPlayerProp(dragged.player, 'id');
        });
      }

      if (validFormation && draggedIsCaptain) {
        vm.captain = vm.filterItem.teamPlayers.find(function (teamPlayer) {
          return teamPlayer.id === PlayerService.getPlayerProp(dropped.player, 'id');
        });
      }

      vm.dragElem = null;
      vm.teamChanges += 1;
    };

    vm.startingSelected = function () {
      var count = 0;
      var positions = Object.keys(vm.starting);

      positions.forEach(function (position) {
        vm.starting[position] && vm.starting[position].forEach(function (player) {
          if (PlayerService.getPlayer(player)) {
            count++;
          }
        });
      });

      return count;
    };

    vm.benchSelected = function () {
      var count = 0;

      vm.bench.forEach(function (player) {
        if (PlayerService.getPlayer(player)) {
          count++;
        }
      });

      return count;
    };

    function removeByPosition(positionName, player) {
      var removeIndex = null;
      for (var index = 0; index <= vm.starting[positionName].length; index++) {
        if (
          vm.starting[positionName][index] &&
          PlayerService.getPlayer(vm.starting[positionName][index]) &&
          PlayerService.getPlayerProp(vm.starting[positionName][index], 'id') === PlayerService.getPlayerProp(player, 'id')
        ) {
          removeIndex = index;
          break;
        }
      }

      setTimeout(function () {
        if (removeIndex !== -1) {
          vm.budget += PlayerService.getPlayerProp(player, 'value');
        }
        vm.starting[positionName].splice(removeIndex, 1);

        if (player.selections && player.selections.captain) {
          vm.nextCaptain = true;
          vm.captain = null;
        }

        var removeResult = FootballPicker.remove(player);
        if (removeResult) {
          vm.possibleFormations = removeResult.possibleFormations;
        }

        $scope.$digest();
      }, 0);
    }

    vm.benchRemove = function (player) {
      vm.bench.forEach(function (benchPlayer, index) {
        if (PlayerService.getPlayer(benchPlayer) && PlayerService.getPlayerProp(benchPlayer, 'id') === PlayerService.getPlayerProp(player, 'id')) {
          setTimeout(function () {
            vm.bench[index] = {};
            vm.budget += PlayerService.getPlayerProp(player, 'value');
            var removeResult = FootballPicker.remove(player);
            if (removeResult) {
              vm.possibleFormations = removeResult.possibleFormations;
            }
            $scope.$digest();
          }, 0);
        }
      });
    };

    vm.playerRemove = function (player) {
      var positionId = PlayerService.getPlayerProp(player, 'position_id');

      if (vm.captain && (player.Player.id === vm.captain.id)) {
        vm.captain = null;
      }

      switch (positionId) {
        case 1:
          removeByPosition('Goalkeeper', player);
          break;

        case 2:
          removeByPosition('Defender', player);
          break;

        case 3:
          removeByPosition('Midfielder', player);
          break;

        case 4:
          removeByPosition('Forward', player);
          break;
      }
    };

    vm.playersSelected = function () {
      var count = 0;
      var positions = Object.keys(vm.starting);

      positions.forEach(function (position) {
        vm.starting[position] && vm.starting[position].forEach(function (player) {
          if (player && (player.hasOwnProperty('Player') || player.hasOwnProperty('players'))) {
            count++;
          }
        });
      });

      vm.bench.forEach(function (benchPlayer) {
        if (benchPlayer && (benchPlayer.hasOwnProperty('Player') || benchPlayer.hasOwnProperty('players'))) {
          count++;
        }
      });

      return count;
    };

    function getPlayersByIds(playersIds) {
      var startingList = getStartingList();

      return playersIds.map(function (playerId) {

        var searchFunction = function (player) {
          return PlayerService.getPlayerProp(player, 'id') === playerId
        };

        var foundInPlayers = vm.players.find(searchFunction);
        var foundInstarting = startingList.find(searchFunction);
        var foundInBench = vm.bench.find(searchFunction);

        return foundInPlayers || foundInstarting || foundInBench;
      });
    }

    vm.pick = function (player) {
      if (player.Player.position_id === 0) {
        vm.budget -= player.Player.value;
        vm.selectedCoach = player;
      } else {

        if (playerService.breaksTheFootballClubLimit(player, getStartingList().concat(vm.bench), vm.maxPlayersSameClub)) {
          growl.addErrorMessage(`${gettextCatalog.getString('You are allowed to pick maximum')} ${vm.maxPlayersSameClub} ${gettextCatalog.getString('players from the same team!')}`);
          return;
        }

        const pickResult = FootballPicker.pick(player);

        if (pickResult.picked) {
          vm.teamChanges += 1;
          vm.possibleFormations = pickResult.possibleFormations;
          vm.starting = {
            Goalkeeper: getPlayersByIds(pickResult.result.Goalkeeper),
            Defender: getPlayersByIds(pickResult.result.Defender),
            Midfielder: getPlayersByIds(pickResult.result.Midfielder),
            Forward: getPlayersByIds(pickResult.result.Forward)
          };

          vm.bench = getPlayersByIds(pickResult.result.Bench);

          var benchEmptySpots = 4 - vm.bench.length;

          if (benchEmptySpots > 0) {
            for (var emptySpot = 0; emptySpot < benchEmptySpots; emptySpot++) {
              vm.bench.push({});
            }
          }

          vm.filterItem.teamPlayers = getStartingListForCaptain();
          if (!vm.captain) {
            vm.captain = getCaptainFromStartingList();
          }
          vm.budget -= player.Player.value;
        }
      }
    };

    vm.saveSelection = function () {
      if (vm.teamChanges === 0) {
        return;
      }

      if (!vm.captain) {
        growl.addErrorMessage('Kies uw kapitein!');
        return;
      }

      if (vm.coachEnabled && !vm.selectedCoach) {
        growl.addErrorMessage('Please select a coach!');
        return;
      }

      var picks = [];
      _.each(vm.getStartingList(), function (player) {
        picks.push(PlayerService.getPlayerProp(player, 'id'));
      });

      var benchIds = [];
      vm.bench.forEach(function (benchPlayer) {
        benchIds.push(PlayerService.getPlayerProp(benchPlayer, 'id'));
      });

      // Post new team
      var payload = {
        week_id: vm.deadlineWeek,
        captain_id: vm.captain.id,
        picks: vm.benchEnabled ? picks.concat(benchIds) : picks,
        competition_feed: vm.competitionInfo.competitionFeed
      };

      if (vm.coachEnabled) {
        payload['coach_id'] = PlayerService.getPlayerProp(vm.selectedCoach, 'id');
      }

      teamService.editFootbalSelection(vm.teamId, payload)
        .then(function (result) {
          growl.addSuccessMessage(gettextCatalog.getString(result.data.message));
        });
    };

    function activate() {
      const clubsInfo = vm.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const playersInfo = vm.players.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);

      vm.clubs = clubsInfo.clubs;
      vm.players = playersInfo.players;

      vm.starting = transformStarting(vm.team.starting);

      var startignClean = getStartingList().filter(function (player) {
        return PlayerService.getPlayer(player);
      });

      var benchClean = vm.team.bench.filter(function (player) {
        return PlayerService.getPlayer(player);
      });

      FootballPicker.set(startignClean, benchClean);
      vm.possibleFormations = FootballPicker.getPossibleFormations();

      vm.starting = vm.team.starting;
      vm.bench = vm.team.bench
        .map(player => Object.assign({}, player));

      vm.filterOptions.clubs = vm.clubs;
      vm.filterItem.club = vm.clubs[0];

      vm.players = vm.players
        .map(player => {
          const playerClub = vm.clubs.find(club => player.Club.id === club.Club.id);
          return Object.assign({}, player, {Club: playerClub && playerClub.Club});
        })
        .map(player => Object.assign({}, player));

      if (vm.team && vm.team.starting) {
        vm.starting = transformStarting(vm.team.starting);
        vm.budget = resetBudget(vm.starting);
        vm.filterItem.teamPlayers = getStartingListForCaptain();
        vm.captain = getCaptainFromStartingList();
      }
      vm.displayWeek = vm.team.weeks.display_week;
      vm.deadlineWeek = vm.team.weeks.deadline_week;
    }

    function resetBudget(starting) {
      var budget = parseInt(vm.budgetValue);

      getStartingList()
        .forEach(function (player) {
          budget -= PlayerService.getPlayerProp(player, 'value')
        });

      vm.bench
        .forEach(function (player) {
          budget -= PlayerService.getPlayerProp(player, 'value')
        });

      return budget;
    }

    function transformStarting(starting) {
      const addOpponent = player => Object.assign({}, player);

      return {
        Goalkeeper: starting[1].map(addOpponent),
        Defender: starting[2].map(addOpponent),
        Midfielder: starting[3].map(addOpponent),
        Forward: starting[4].map(addOpponent)
      };
    }

    vm.isCaptain = function (player) {
      return vm.captain &&
        (vm.captain.id === PlayerService.getPlayerProp(player, 'id'));
    };

    vm.getStartingList = getStartingList;

    function getStartingList() {
      var goalkeepers = vm.starting['Goalkeeper'] || [];
      var defenders = vm.starting['Defender'] || [];
      var midfielders = vm.starting['Midfielder'] || [];
      var forwarders = vm.starting['Forward'] || [];

      return goalkeepers.concat(defenders, midfielders, forwarders)
        .map(player => Object.assign({}, player));
      ;
    }

    vm.isPickable = function (player) {
      if (PlayerService.getPlayerProp(player, 'value') > vm.budget) {
        return false;
      }

      if (playerService.breaksTheFootballClubLimit(player, getStartingList().concat(vm.bench), vm.maxPlayersSameClub)) {
        return false;
      }

      if (player && player.Player && player.Player.position_id === 0) {
        return !vm.selectedCoach;
      } else {
        return FootballPicker.canPick(player);
      }
    };

    vm.filterChanged = function () {
      vm.pagination.current = 1;
    };

    function getStartingListForCaptain() {
      var goalkeepers = vm.starting['Goalkeeper'] || [];
      var defenders = vm.starting['Defender'] || [];
      var midfielders = vm.starting['Midfielder'] || [];
      var forwarders = vm.starting['Forward'] || [];

      return goalkeepers.concat(defenders, midfielders, forwarders)
        .map(function (player) {
          return {
            id: PlayerService.getPlayerProp(player, 'id'),
            short: PlayerService.getPlayerProp(player, 'short'),
            captain: player.selections && player.selections.captain
          }
        });
    };

    vm.getPlayerProp = PlayerService.getPlayerProp;

    vm.captainChanged = function (player) {
      vm.teamChanges += 1;
    };

    vm.canSave = function () {
      if (vm.benchEnabled) {
        return !!vm.teamChanges && (vm.startingSelected() >= 11 && vm.benchSelected() >= 4);
      } else {
        return vm.teamChanges && vm.startingSelected() === 11;
      }
    };

    function getTeamBench() {
      var benchAllowed = 4;
      var diffBench = benchAllowed - vm.team.bench.length;
      var bench = [];
      for (var i = 0; i < diffBench; i++) {
        bench.push({});
      }
      return bench.concat(vm.team.bench);
    }

    function getOpponentInfo(player) {
      let playingHome = true;
      const playerClubInfostradaId = (player && player.Club && player.Club.infostrada_id) ||
        (player && player.clubs && player.clubs.infostrada_id);
      const playerMatch = vm.matches.find(match => {
        const isHome = playerClubInfostradaId === match.n_HomeTeamID;
        const isAway = playerClubInfostradaId === match.n_AwayTeamID;

        if (isAway) {
          playingHome = false;
        }

        return isHome || isAway;
      });

      if (playerMatch) {
        const opponentClubInfostradaId = playingHome ? playerMatch.n_AwayTeamID : playerMatch.n_HomeTeamID;
        const opponentClub = vm.clubs.find(club => club.Club.infostrada_id === opponentClubInfostradaId);
        return {
          club: opponentClub.Club.name,
          short: opponentClub.Club.short,
          homeAwayLabel: playingHome ? gettextCatalog.getString('home-label') : gettextCatalog.getString('away-label')
        }
      } else {
        return null;
      }
    }
  }
};

_.mixin({
  capitalize: function (string) {
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  }
});