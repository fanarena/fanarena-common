import template from './soccer-new-game-top-bar.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    timers: '<'
  },
  controller: function ($scope, $rootScope, $state, localStorageService) {
    var self = this;

    self.$state = $state;
    self.user = localStorageService.get('user');

    self.popoverTpl = 'helpTemplate.html';
    self.isPopoverOpen = false;

    self.$doCheck = function () {
      if (self.team && self.leagueStatus !== self.team.Team.League.status)
        self.leagueStatus = self.team.Team.League.status;

      if (self.timers < 0)
        self.timers = 0;
    };

    self.leagueStatus = self.team ? self.team.Team.League.status : null;
    self.isOnboarding = $state.includes('newGame') || self.leagueStatus <= 2;

    self.mocksEmpty = self.team && self.team.Team.Team.mocks_count > 0 ? false : true;
    $rootScope.$on('mocks:notempty', function (event, args) {
      self.mocksEmpty = false;
    });
    $rootScope.$on('mocks:empty', function (event, args) {
      self.mocksEmpty = true;
    });

    self.invitesEmpty = self.team && self.team.Team.League.Invite !== undefined && self.team.Team.League.Invite.length > 0 ? false : true;
    $rootScope.$on('invites:notempty', function (ev, args) {
      self.invitesEmpty = false;
    });

    self.showDraftInformation = function () {
      //question mark triggered
      $state.go('teamDraft', { teamId: self.team.Team.Team.id, showHelp: true });
    };

    self.showInviteOtherInformation = function () {
      self.isPopoverOpen = !self.isPopoverOpen;
      //question mark triggered
    };

    self.testPopup = function () {

    }
  }
};