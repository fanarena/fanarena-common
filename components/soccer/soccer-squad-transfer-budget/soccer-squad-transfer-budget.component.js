import template from './soccer-squad-transfer-budget.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    team: '<',
    blankoPlayerId: '<',
    hidePositionBackground: '<',
    budgetValue: '<',
    localAssets: '<',
    showJerseys: '<',
    deadlineWeekInfo: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    gameName: '<',
    labels: '<',
    unlimitedTransfersWeeks: '<',
    maxPlayersSameClub: '<',
    maximumTransfers: '<',
    showBlanko: '<'
  },
  controller: function ($scope, $rootScope, teamService, growl, gettextCatalog, $state, PlayerService,
                        POSITIONS, SPORTS_ASSETS, FootballPicker, storeActions, storeHelpers, $ngRedux,
                        playerService, localStorageService, analyticsService) {
    const vm = this;
    vm.blancoId = vm.blankoPlayerId;
    vm.resolved = false;

    this.$onInit = function () {

      if (isFirstDeadlineWeek()) {
        if (vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-squadTeam', {id: vm.team.Team.Team.id, '#': null});
        } else {
          $state.go('squadTeam', {id: vm.team.Team.Team.id, '#': null});
        }
        return;
      } else if (isInTeamEditsDeadlineWeek()) {
        if (vm.statesPrefix) {
          $state.go(vm.statesPrefix + '-soccerTeamEditBudget', {id: vm.team.Team.Team.id, '#': null});
        } else {
          $state.go('soccerTeamEditBudget', {id: vm.team.Team.Team.id, '#': null});
        }
        return;
      } else {
        vm.resolved = true;
      }

      // football parent
      vm.starting = transformStarting(vm.team.starting);

      let disconnect = $ngRedux.connect(mapStateToParams, Object.assign({},
        storeActions.playersActions, storeActions.clubsActions))(this);
      $scope.$on('$destroy', disconnect);

      const clubs = this.clubs.find(clubInfo => clubInfo.competitionFeed === vm.team.Team.League.competition_feed);
      const players = this.clubs.find(playerInfo => playerInfo.competitionFeed === vm.team.Team.League.competition_feed);

      if (!clubs) {
        storeHelpers.initClubs()
          .then(() => {
            if (!players) {
              return storeHelpers.initPlayers();
            } else {
              return this.players;
            }
          })
          .then(() => activate(true));
      } else {
        activate(true);
      }
    };

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    $rootScope.$broadcast('sidemenu:team:collapse', {team: this.team.Team.Team.id});

    vm.team = this.team;
    vm.budget = getBudgetDefault();
    vm.displayWeek = vm.team.weeks.display_week;
    vm.deadlineWeek = vm.team.weeks.deadline_week;
    vm.transfersAllowed = vm.maximumTransfers ?
      vm.maximumTransfers :
      vm.team && vm.team.Team && vm.team.Team.Team && vm.team.Team.Team.transfers;
    vm.hasTransferHistory = false;
    vm.inTransferWait = false;
    vm.outTransferWait = false;
    vm.bench = vm.team.bench;
    vm.user = localStorageService.get('user');
    vm.assetsURL = SPORTS_ASSETS.football.local;

    $rootScope.title = gettextCatalog.getString("Team Transfer");

    vm.resetTransfer = resetTransfer;
    vm.outPlayersLimitExceeded = outPlayersLimitExceeded;
    vm.inPlayersLimitExceeded = inPlayersLimitExceeded;
    vm.totalSavedTransfers = totalSavedTransfers;
    vm.onPlayerOut = onPlayerOut;
    vm.onPlayerIn = onPlayerIn;
    vm.isOutOfBudget = isOutOfBudget;
    vm.hasPlaceholdersFilled = hasPlaceholdersFilled;
    vm.confirmTransfer = confirmTransfer;
    vm.filterChanged = filterChanged;
    vm.isInMyTeam = isInMyTeam;
    vm.customFilter = customFilter;
    vm.hasTransfer = hasTransfer;
    vm.alreadyPicked = alreadyPicked;
    vm.removePickedPlayer = removePickedPlayer;
    vm.canTransfer = canTransfer;
    vm.isPickable = isPickable;
    vm.getAvailableTransfersSlots = getAvailableTransfersSlots;
    vm.tranfersDisabled = vm.deadlineWeekInfo && !vm.deadlineWeekInfo.hasMatches;
    vm.transfersToSave = transfersToSave;
    vm.firstTransferIdToSave = firstTransferIdToSave;
    vm.playerAlreadyOut = playerAlreadyOut;
    vm.clearCurrentTransfers = clearCurrentTransfers;
    vm.hasTransfersInProgress = hasTransfersInProgress;
    vm.breaksClubLimit = breaksClubLimit;
    vm.keptBlancoTransfers = [];
    vm.pastKeptBlancoTransfers = [];
    vm.transferRequested = false;

    function hasTransfersInProgress() {
      return vm.currentTransfers && vm.currentTransfers.length &&
        vm.currentTransfers.find(transfer => !transfer.id)
    }

    function resetTransfer() {
      teamService.resetTransfer(vm.team.Team.Team.id)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Transfers were restored!'));
          updateSquad();
          initFootballPicker();
        });
    }

    function clearCurrentTransfers() {
      vm.pendingTransfers = vm.currentTransfers.filter(transfer => !transfer.id);
      vm.pendingTransfers.forEach(pendingTransfer => {
        if (pendingTransfer.Out) {
          vm.budget -= pendingTransfer.Out.value;
        }

        if (pendingTransfer.In) {
          vm.budget += pendingTransfer.In.value;
        }
      })
      vm.currentTransfers = vm.currentTransfers.filter(transfer => transfer.id);
      if (vm.blancoId) {
        vm.blancoId = null;
      }
      initFootballPicker();
    }

    function firstTransferIdToSave() {
      const found = vm.currentTransfers.find(transfer => !transfer.id);
      return found.out_id;
    }

    function playerAlreadyOut(player) {
      return vm.currentTransfers && vm.currentTransfers
        .find(transfer => !transfer.id && transfer.out_id === PlayerService.getPlayerProp(player, 'id'));
    }

    function isInTeamEditsDeadlineWeek() {
      return vm.team && (vm.unlimitedTransfersWeeks.indexOf(vm.team.weeks.deadline_week) !== -1);
    }

    function transfersToSave() {
      return vm.currentTransfers
        .filter(transfer => !transfer.id && transfer.in_id && transfer.out_id).length;
    }


    function isFirstDeadlineWeek() {
      return vm.team.weeks.deadline_week === vm.team.Team.Team.week_id;
    }

    function outPlayersLimitExceeded() {
      var transferWithOutPlayers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.out_id;
      }) || [];

      return ((vm.pastTransfers && vm.pastTransfers.length) + transferWithOutPlayers.length) >= vm.transfersAllowed || vm.outTransferWait;
    }

    function alreadyPicked(player, type) {
      var alreadyOut = vm.currentTransfers && vm.currentTransfers.find(function (transfer) {
        if (type === 'in') {
          return transfer.in_id === PlayerService.getPlayerProp(player, 'id');
        } else {
          return transfer.out_id === player.Player.id || transfer.in_id === PlayerService.getPlayerProp(player, 'id');
        }
      });
      return !!alreadyOut;
    }

    function getAvailableTransfersSlots() {
      var currentWeekTransfersPlayersIds = vm.currentTransfers.map(transfer => transfer.Out.id);
      var blankoFreeSlots = vm.bench
        .concat(getStartingList())
        .filter(player => {
          return !!player.selections.blanko;
        });

      var blankoToUse = blankoFreeSlots
        .filter(blankoPlayer => {
          return currentWeekTransfersPlayersIds.indexOf(blankoPlayer.players.id) === -1;
        });

      var blankoUsedSoFar = vm.currentTransfers.filter(item => item.id && item.blanko_id);

      var availableTransfers = blankoFreeSlots.length + vm.transfersAllowed - vm.currentTransfers.length - vm.pastTransfers.length;

      if (!vm.showBlanko) {
        const left = availableTransfers < 0 ? 0 : availableTransfers;
        return `${gettextCatalog.getString('Transfers left')}: ${left}`;
      } else {
        const left = vm.transfersAllowed - vm.currentTransfers.length - totaltransfersWithoutBlankoId(vm.pastTransfers);
        const leftWithConsumedBlanko = (blankoUsedSoFar.length + (blankoFreeSlots.length - blankoToUse.length)) + left;
        const leftWithoutBlanko = leftWithConsumedBlanko < 0 ? 0 : leftWithConsumedBlanko;
        return `${gettextCatalog.getString('Transfers left')}: ${leftWithoutBlanko} ${(blankoToUse.length ? ' + ' + blankoToUse.length + ' blanco' : '')}`;
      }
    }

    function totaltransfersWithoutBlankoId(transfers) {
      const list = transfers && transfers.length ? transfers : [];
      return list
        .filter(item => !item.blanko_id)
        .length;
    }

    function inPlayersLimitExceeded() {
      var transferWithInPlayers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.in_id;
      }) || [];

      if (vm.blancoId || vm.showBlanko) {
        return false;
      } else {
        return ((vm.pastTransfers && totaltransfersWithoutBlankoId(vm.pastTransfers)) + transferWithInPlayers.length) >= vm.transfersAllowed || vm.inTransferWait;
      }
    }

    function getBudgetDefault() {
      return vm.budgetValue;
    }

    function totalSavedTransfers() {
      return vm.keptBlancoTransfers.length || vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !!transfer.id;
      }).length;
    }

    function sameClub(playerA, playerB) {
      return PlayerService.getPlayerProp(playerA, 'club_id') === PlayerService.getPlayerProp(playerB, 'club_id');
    }

    function onPlayerOut(player) {
      var isAllreadyOut = vm.currentTransfers.find(function (transfer) {
        return transfer.out_id === PlayerService.getPlayerProp(player, 'id');
      });

      if (isAllreadyOut) {
        growl.addErrorMessage(gettextCatalog.getString('The player is already out'));
        return;
      }

      var inPlayerFiled = vm.currentTransfers.find(function (transfer) {
        return !transfer.id
      });

      var currentWeekTransfersPlayersIds = vm.currentTransfers.map(transfer => transfer.Out.id);

      var blankoFreeSlots = vm.bench
        .concat(getStartingList())
        .filter(blankoPlayer => {
          return !!blankoPlayer.selections.blanko;
        });

      var blankoToUse = blankoFreeSlots
        .filter(blankoPlayer => {
          return currentWeekTransfersPlayersIds.indexOf(blankoPlayer.players.id) === -1;
        });

      var blankoUsedSoFar = vm.currentTransfers.filter(item => item.blanko_id);

      var isBlanko = blankoToUse.find(blankoPlayer => blankoPlayer.players.id === PlayerService.getPlayerProp(player, 'id'));

      var currentTransfersWithoutBlanko = vm.currentTransfers.filter(item => !item.blanko_id)

      if ((totaltransfersWithoutBlankoId(vm.pastTransfers) + vm.currentTransfers.length) === (vm.transfersAllowed + (blankoUsedSoFar.length + blankoToUse.length))) {
        growl.addErrorMessage(gettextCatalog.getString('No transfers left'));
        return;
      }
      var outSlot = vm.currentTransfers.find(function (transfer) {
        return !transfer.out_id;
      });

      var removeResult = FootballPicker.remove(player);
      if (removeResult) {
        vm.possibleFormations = removeResult.possibleFormations;
      }

      if (outSlot) {
        var linked = false;
        vm.currentTransfers.forEach(function (transfer) {
          if (!transfer.out_id && !linked) {
            transfer.out_id = PlayerService.getPlayerProp(player, 'id');
            transfer.Out = PlayerService.getPlayer(player);
            transfer.blanko_id = !!isBlanko || null;
            linked = true;
          }
        });
      } else {
        vm.currentTransfers.push({
          id: null,
          in_id: null,
          In: null,
          out_id: PlayerService.getPlayerProp(player, 'id'),
          Out: PlayerService.getPlayer(player),
          team_id: vm.team.Team.Team.id,
          week_id: vm.team.weeks.deadline_week,
          blanko_id: !!isBlanko || null,
        })
      }
      vm.budget += PlayerService.getPlayerProp(player, 'value');
    }

    function breaksClubLimit(player) {
      const maxPlayers = vm.maxPlayersSameClub || 2;
      const playersSelectedToLeave = vm.currentTransfers
        .filter(item => !item.id)
        .map(item => item.out_id);

      const startingFutureTransfers = vm.currentTransfers
        .filter(item => !item.id && item.in_id && item.out_id)
        .map(item => vm.players.find(playerInfo => playerInfo.Player.id === item.in_id))

      return playerService.breaksTheFootballClubLimit(player, getStartingList().concat(vm.bench), maxPlayers, playersSelectedToLeave, startingFutureTransfers);
    }

    function onPlayerIn(player) {
      if (breaksClubLimit(player)) {
        growl.addErrorMessage(`${gettextCatalog.getString('You are allowed to pick maximum')} ${vm.maxPlayersSameClub} ${gettextCatalog.getString('football players from the same team!')}`);
        return;
      }

      var inSlot = vm.currentTransfers && vm.currentTransfers.find(function (transfer) {
        return !transfer.in_id;
      });

      var pickResult = FootballPicker.pick(player);
      if (pickResult.picked) {
        vm.possibleFormations = pickResult.possibleFormations;
      }

      if (inSlot) {
        var linked = false;
        vm.currentTransfers = vm.currentTransfers.map(function (transfer) {
          if (!transfer.in_id && !linked) {
            transfer.in_id = PlayerService.getPlayerProp(player, 'id');
            transfer.In = PlayerService.getPlayer(player);
            linked = true;
          }
          return transfer;
        });
      } else {
        vm.currentTransfers.push({
          id: null,
          in_id: PlayerService.getPlayerProp(player, 'id'),
          In: PlayerService.getPlayer(player),
          out_id: null,
          Out: null,
          team_id: vm.team.Team.Team.id,
          week_id: vm.team.weeks.deadline_week
        })
      }
      vm.budget -= PlayerService.getPlayerProp(player, 'value');
    }

    function isOutOfBudget() {
      return vm.budget < 0;
    }

    function hasPlaceholdersFilled(transfer) {
      return !!transfer.in_id && !!transfer.out_id;
    }

    function confirmTransfer(transfer) {
      const firstTransfer = vm.currentTransfers.find(currentTransfer => !currentTransfer.id);

      const input = vm.saveSingleTransfer ?
        {
          in_id: transfer.in_id,
          out_id: transfer.out_id,
          week_id: transfer.week_id,
          team_id: transfer.team_id
        } :
        firstTransfer && {
          week_id: firstTransfer.week_id,
          team_id: firstTransfer.team_id,
          transfers: vm.currentTransfers
            .filter(currentTransfer => !currentTransfer.id)
            .map(currentTransfer => ({
              in_id: currentTransfer.in_id,
              out_id: currentTransfer.out_id
            }))
        };

      vm.transferRequested = true;
      teamService.footballBatchTransfers(input)
        .then(function (result) {
          growl.addSuccessMessage(result.data.message);

          analyticsService.addToDataLayer({
            event: 'transfer_done',
            game: vm.statesPrefix,
            user_id: vm.user.id
          });

          if (vm.saveSingleTransfer) {
            vm.currentTransfers = vm.currentTransfers.filter(function (transferRecord) {
              return transferRecord.out_id !== transfer.out_id && transferRecord.in_id !== transfer.in_id;
            });
          } else {
            vm.currentTransfers = [];
          }

          vm.inTransferWait = false;
          vm.outTransferWait = false;

          // reset to normal transfers
          if (vm.blancoId) {
            vm.blancoId = null;
          }

          updateSquad();
          vm.transferRequested = false;
        })
        .catch(error => {
          if (error && error.message) {
            growl.addErrorMessage(error.message);
          }
          vm.transferRequested = false;
        });
    }

    function updateSquad() {
      return teamService.getById(vm.team.Team.Team.id)
        .then(function (result) {
          vm.team = result.data;
          prepareData();
        });
    }

    function filterChanged() {
      vm.pagination.current = 1;
    }

    function isInMyTeam(searchedPlayer) {
      var startingList = getStartingList();
      return startingList
        .concat(vm.bench)
        .find(function (player) {
          return PlayerService.getPlayerProp(player, 'id') === PlayerService.getPlayerProp(searchedPlayer, 'id');
        });
    }

    function activate(mapClubsToPlayers) {
      const clubsInfo = vm.clubs.find(clubInfo => clubInfo.competitionFeed === vm.team.Team.League.competition_feed);
      const playersInfo = vm.players.find(playerInfo => playerInfo.competitionFeed === vm.team.Team.League.competition_feed);

      vm.clubs = clubsInfo.clubs;
      vm.players = playersInfo.players;

      if (mapClubsToPlayers) {
        vm.players = vm.players
          .map(player => {
            const playerClub = vm.clubs.find(club =>
              (player && player.Club && player.Club.id === club.Club.id) ||
              (player && player.clubs && player.clubs.id === club.Club.id)
            );
            return Object.assign({}, player, {Club: playerClub && playerClub.Club});
          })
          .filter(player => {
            return [1, 2, 3, 4].indexOf(PlayerService.getPlayerProp(player, 'position_id')) !== -1;
          });
      }

      // init picking algorithm
      initFootballPicker();


      // All players
      vm.teamName = {
        value: ''
      };

      vm.positions = [
        {id: -1, name: gettextCatalog.getString('All positions')},
        {id: 1, name: gettextCatalog.getString('Goalkeeper')},
        {id: 2, name: gettextCatalog.getString('Defender')},
        {id: 3, name: gettextCatalog.getString('Midfielder')},
        {id: 4, name: gettextCatalog.getString('Attacker')}
      ];

      if (vm.coachEnabled) {
        vm.positions.push({id: 0, name: 'Coach'})
      }

      const competitionsBudges = {
        'GWC': [
          {budget: 100, text: gettextCatalog.getString('All budgets')},
          {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
        ],
        'G11': [
          {budget: 100, text: gettextCatalog.getString('All budgets')},
          {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
        ],
        'CL': [
          {budget: 100, text: gettextCatalog.getString('All budgets')},
          {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
          {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
          {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
          {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
          {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
        ]
      };

      // filters
      vm.filterOptions = {
        clubs: vm.clubs,
        budgets: competitionsBudges[vm.team.Team.League.competition_feed]
      };
      vm.filterItem = {
        club: vm.clubs[0],
        budget: vm.filterOptions.budgets[0],
        selectedPosition: vm.positions[0]
      };

      vm.searchPlayer = {value: ""};

      // Players pagination
      vm.pagination = {
        current: 1
      };

      vm.fieldDrag = false;
      vm.dragElem = null;

      return updateSquad()
        .then(function () {
          vm.displayWeek = vm.team.weeks.display_week;
          vm.deadlineWeek = vm.team.weeks.deadline_week;

          if (vm.blancoId) {
            var blankoPlayer = vm.bench.concat(getStartingList()).find(function (player) {
              return PlayerService.getPlayerProp(player, 'id') === vm.blancoId;
            });

            vm.currentTransfers.push({
              id: null,
              in_id: null,
              In: null,
              out_id: vm.blancoId,
              Out: PlayerService.getPlayer(blankoPlayer),
              team_id: vm.team.Team.Team.id,
              week_id: vm.team.weeks.deadline_week
            });


            var removeResult = FootballPicker.remove(blankoPlayer);
            if (removeResult) {
              vm.possibleFormations = removeResult.possibleFormations;
            }

            vm.budget += PlayerService.getPlayerProp(blankoPlayer, 'value');
          }
        });
    }

    function customFilter(player) {
      // Check budget
      if ((vm.filterItem && vm.filterItem.budget) && (player.Player.value) > vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if ((vm.filterItem && vm.filterItem.club && vm.filterItem.club.Club) && vm.filterItem.club.Club.id !== 0 && player.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }
      // Check position
      else if (vm.filterItem && vm.filterItem.selectedPosition && vm.filterItem.selectedPosition.id !== -1 && player.Player.position_id !== vm.filterItem.selectedPosition.id) {
        return false;
      }
      // Check name
      else if (vm.searchPlayer && vm.searchPlayer.value !== "" && player.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      }
      else if (vm.isInMyTeam(player)) {
        return false;
      }
      else {
        return true;
      }
    }

    function prepareData() {
      vm.budget = getBudgetDefault();
      vm.starting = vm.team && vm.team.starting;
      vm.bench = vm.team.bench;
      vm.starting = transformStarting(vm.team.starting);
      vm.bench.concat(getStartingList())
        .forEach(function (player) {
          vm.budget -= (PlayerService.getPlayerProp(player, 'value'));
        });

      initFootballPicker();

      vm.pastTransfers = vm.team.Team.Transfer
        .filter(function (transfer) {
          return transfer.week_id !== vm.team.weeks.deadline_week && (transfer.In.id !== transfer.Out.id);
        });

      vm.pastKeptBlancoTransfers = vm.team.Team.Transfer
        .filter(function (transfer) {
          return transfer.week_id !== vm.team.weeks.deadline_week && (transfer.In.id === transfer.Out.id);
        });

      var notConfirmedTransfers = vm.currentTransfers && vm.currentTransfers.filter(function (transfer) {
        return !transfer.id;
      }) || [];

      if (notConfirmedTransfers.length) {
        notConfirmedTransfers.forEach(function (notConfirmedTransfer) {
          if (notConfirmedTransfer.in_id) {
            vm.budget -= notConfirmedTransfer.In.value;
          }

          if (notConfirmedTransfer.out_id) {
            vm.budget += notConfirmedTransfer.Out.value;
          }
        });
      }

      vm.currentTransfers = vm.team.Team.Transfer.filter(function (transfer) {
        return transfer.week_id === vm.team.weeks.deadline_week && (transfer.In.id !== transfer.Out.id);
      }).concat(notConfirmedTransfers);

      vm.keptBlancoTransfers = vm.team.Team.Transfer
        .filter(function (transfer) {
          return transfer.week_id === vm.team.weeks.deadline_week && (transfer.In.id === transfer.Out.id);
        });
    }

    function hasTransfer() {
      return vm.team && vm.team.Team.Transfer.length > 0;
    }

    function arrangeStarting() {
      var withoutCaptain = vm.starting.filter(function (player) {
        return !parseInt(player.Selection.captain);
      });
      var captain = getFrontMan();
      return [captain].concat(withoutCaptain);
    }

    function removePickedPlayer(playerId, type) {
      var transfers = vm.currentTransfers.map(function (transfer) {
        if (type === 'out' && transfer.out_id === playerId) {

          if (playerId === vm.blancoId) {
            vm.blancoId = null;
          }

          vm.budget -= (transfer.Out.value);

          var pickBack = FootballPicker.pick({Player: transfer.Out});
          if (pickBack.picked) {
            vm.possibleFormations = pickBack.possibleFormations;
          }

          transfer.out_id = null;
          transfer.Out = null;
          vm.outTransferWait = false;
        }

        if (type === 'in' && transfer.in_id === playerId) {
          vm.budget += (transfer.In.value);
          var removeResult = FootballPicker.remove({Player: transfer.In});
          if (removeResult) {
            vm.possibleFormations = removeResult.possibleFormations;
          }

          transfer.in_id = null;
          transfer.In = null;
          vm.inTransferWait = false;
        }

        return (transfer.in_id || transfer.out_id) ? transfer : null;
      });

      vm.currentTransfers = transfers.filter(function (transfer) {
        return !!transfer;
      });
    }

    vm.getLocalDate = function (utcDate) {
      return moment.utc(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
    }

    function canTransfer() {
      return (vm.pastTransfers && totaltransfersWithoutBlankoId(vm.pastTransfers) < vm.transfersAllowed) || vm.blancoId;
    }

    function transformStarting(starting) {
      return {
        Goalkeeper: starting[1],
        Defender: starting[2],
        Midfielder: starting[3],
        Forward: starting[4]
      };
    }

    vm.getPositionNameById = getPositionNameById;

    function getPositionNameById(positionId) {
      switch (positionId) {
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
      }
    }

    function resetBudget(starting) {
      var budget = vm.budgetValue;

      getStartingList()
        .forEach(function (player) {
          budget -= PlayerService.getPlayerProp(player, 'value')
        });

      vm.bench
        .forEach(function (player) {
          budget -= PlayerService.getPlayerProp(player, 'value')
        });

      return budget;
    }

    vm.getStartingList = getStartingList;

    function getStartingList() {
      var goalkeepers = vm.starting['Goalkeeper'] || [];
      var defenders = vm.starting['Defender'] || [];
      var midfielders = vm.starting['Midfielder'] || [];
      var forwards = vm.starting['Forward'] || [];

      return goalkeepers.concat(defenders, midfielders, forwards);
    }

    function isPickable(player) {
      if (PlayerService.getPlayerProp(player, 'value') > vm.budget) {
        return false;
      }

      return FootballPicker.canPick(player);
    }

    function initFootballPicker() {
      var startignClean = getStartingList().filter(function (player) {
        return PlayerService.getPlayer(player);
      });

      var benchClean = vm.team.bench.filter(function (player) {
        return PlayerService.getPlayer(player);
      });
      FootballPicker.set(startignClean, benchClean);
      vm.possibleFormations = FootballPicker.getPossibleFormations();
    }
  }
};