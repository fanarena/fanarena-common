import template from './soccer-team-league.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    league: '<',
    deadlineWeeks: '<'
  },
  controller: function ($scope, $stateParams, $rootScope, localStorageService, leagueService, teamService, moment,
                        $state, growl, $location, faHelpService, HelpModulesIDs, Hashids) {
    $rootScope.$broadcast('sidemenu:team:collapse', { team: this.team.Team.Team.id });
    faHelpService.setCurrentHelpModule(HelpModulesIDs.InviteFriends);
    // $rootScope.title = this.team.Team.Team.name + " - " + "League";
    $rootScope.title = "League";

    var self = this;
    self.user = localStorageService.get('user');

    // Current user is admin
    self.isAdmin = (self.user.id == self.league.League.League.admin ? true : false);

    $scope.tableSort = function (team) {
      if (self.league.League.League.h2h == 1) {
        return team.hth_points;
      }
      else {
        return team.rpoints;
      }
    };

    // Share
    self.shareLink = "https://" + $location.host() + "/join/" + Hashids.encode(self.league.League.League.id);

    // Invites
    self.invite_emails = "";
    self.invited_emails = self.league.League.Invite;

    self.resendInvite = function (email) {
      leagueService.sendInvites(1, [email]).then(function (data) {
        self.invited_emails = data;
      });
    };

    self.sendInvites = function (frm) {
      if (frm.$valid) {
        leagueService.sendInvites(self.league.League.League.id, $location.host(), self.invite_emails).then(function (data) {
          growl.addSuccessMessage('Invites sent');
          self.invite_emails = "";
          frm.$setPristine();
          self.invited_emails = data.league.Invite;

          $rootScope.$broadcast('invites:notempty');
        });
      }
    };

    // Delete
    self.delete = function () {
      teamService.delete(self.team.Team.Team.id).then(function (res) {
        growl.addSuccessMessage('Successfully deleted!');

        // Refresh sidebar
        $rootScope.$broadcast('sidemenu:refresh');

        $state.go('home');
      });
    };

    // Fixtures
    self.fixtureWeeks = [];
    if (self.league.League.Fixture.length > 0) {
      for (fixture in self.league.League.Fixture) {
        var fixture = self.league.League.Fixture[fixture];

        if (_.findWhere(self.fixtureWeeks, { id: fixture.week_id }) == undefined) {
          self.fixtureWeeks.push({
            id: fixture.week_id,
            name: 'Week ' + fixture.week_id
          });
        }
      }
    }

    self.selectedFixtureWeek = _.findWhere(self.fixtureWeeks, { id: self.team.weeks.display_week });
    if (self.selectedFixtureWeek === undefined)
      self.selectedFixtureWeek = self.fixtureWeeks[0];

    self.getFixturesForWeek = function () {
      return _.filter(self.league.League.Fixture, function (fix) {
        return fix.week_id == self.selectedFixtureWeek.id ? true : false;
      });
    };

    // League settings
    self.today = new Date();

    var deadline = self.league.League.League.deadline !== "0000-00-00 00:00:00" ? moment.utc(self.league.League.League.deadline) : null;

    if (deadline !== null)
      deadline = new Date(deadline.local().valueOf());

    $scope.dateTimeBeforeRender = leagueService.validateDeadlinePicker;

    self.settings = {
      deadline: deadline,
      pick_interval: self.league.League.League.draftinterval,
      isPublic: self.league.League.League.public == 1 ? true : false,
      isHeadToHead: self.league.League.League.h2h == 1 ? true : false,
      encounters: self.league.League.League.encounters,
      isLocked: self.league.League.League.locked == 1 ? true : false
    };

    self.pickIntervalOptions = [
      {
        value: 0,
        text: 'Manual draft'
      },
      {
        value: 30,
        text: '30 seconds'
      },
      {
        value: 60,
        text: '1 minute'
      },
      {
        value: 120,
        text: '2 minutes'
      },
      {
        value: 300,
        text: '5 minutes'
      },
      {
        value: 600,
        text: '10 minutes'
      },
      {
        value: 900,
        text: '15 minutes'
      },
      {
        value: 1800,
        text: '30 minutes'
      },
      {
        value: 3600,
        text: '1 hour'
      },
      {
        value: 7200,
        text: '2 hours'
      },
      {
        value: 21600,
        text: '6 hours'
      },
      {
        value: 43200,
        text: '12 hours'
      },
      {
        value: 86400,
        text: '24 hours'
      }
    ];
    self.encounterOptions = [2, 4, 6, 8];

    self.calculateEndDate = function () {
      var noTeams = self.league.table.length;
      if (noTeams < 4)
        $scope.endDeadlineWeek = null;
      else {
        var endDeadlineWeek = (noTeams - 1) * self.settings.encounters + self.league.deadline_week;
        var week = _.findWhere(self.deadlineWeeks, { week_id: endDeadlineWeek });
        var string = "";
        if (week) {
          string = week.deadlineweek ? week.deadlineweek : "";
        }

        $scope.endDeadlineWeek = moment(string.toString(), "YYYYMMDD").format("DD/MM/YYYY");
      }
    };

    self.calculateEndDate();

    self.saveSettings = function (frm) {
      var valid = true;

      // Manually check deadline
      if (self.settings.deadline === null || self.settings.deadline == undefined) {
        valid = false;
        growl.addErrorMessage('You need to set a deadline');
      }

      if (valid) {
        var deadline = moment(self.settings.deadline);
        deadline.utc();

        var obj = angular.copy(self.settings);

        obj.isLocked = self.settings.isLocked == true ? 1 : 0;
        obj.isPublic = self.settings.isPublic == true ? 1 : 0;
        obj.isHeadToHead = self.settings.isHeadToHead == true ? 1 : 0;

        var refreshTeam = false;
        if (self.league.League.Team.length > 3) {
          obj.deadline = deadline.format('YYYY-MM-DD HH:mm:ss');
          refreshTeam = true;
        }

        leagueService.saveSettings(self.league.League.League.id, obj).then(function (res) {
          growl.addSuccessMessage('Settings saved');

          if (refreshTeam) {
            $rootScope.$broadcast('team:refresh');
          }

        });
      }
    };
  }
};