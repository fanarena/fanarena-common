import template from './soccer-new-game-progress.component.tpl.html';

export default {
  template,
  bindings: {
    decreaseNewGameProgressStep: '=',
    activeTab: '<'
  },
  controller: function (NewGameCacheService, $rootScope) {
    var vm = this;

    vm.getNewGameProgress = getNewGameProgress;
    vm.isMobile = isMobile;

    function isMobile() {
      return $rootScope.isMobile;
    }

    function getNewGameProgress() {
      return NewGameCacheService.getProgress();
    }
  }
};