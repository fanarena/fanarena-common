import template from './soccer-calendar.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    displayWeek: '<',
    team: '<',
    deadlineWeeks: '<',
    deadlineWeeksInfo: '<',
    showSelect: '<',
    competition: '<',
    localAssets: '<',
    useLocalFolder: '<',
    cachedMatches: '<',
    gameName: '<',
    statesPrefix: '<',
    assetsUrl: '<',
    hideCompetitionSelect: '<',
    seasonId: '<'
  },
  controller: function ($scope, RESOURCES, teamService, gameService, $state, $rootScope, SPORTS_ASSETS, gettextCatalog) {
    var self = this;
    self.resources = RESOURCES;
    self.competitions = gameService.getCompetitions();
    self.selectedCompetition = self.competitions[0];
    self.badgesURL = SPORTS_ASSETS.football.remote;
    self.matchDayLabel = gettextCatalog.getString("Match day");
    self.clubAssetsURL = `https://s3-eu-west-1.amazonaws.com/${self.gameName || 'fanarena'}/badges`;

    if (self.assetsUrl) {
      self.clubAssetsURL = self.assetsUrl;
    }

    if (self.localAssets) {
      self.badgesURL = SPORTS_ASSETS.football.local;
    }

    if (self.competition) {
      self.selectedCompetition = self.competition;
    } else {
      if (self.team) {
        self.selectedCompetition = self.competitions
          .find(competition => competition.competition_feed === self.team.Team.League.Competition.competition_feed);
      } else {
        self.selectedCompetition = self.competitions[0];
      }
    }

    // Get matches
    self.matches = self.cachedMatches || [];
    self.getMatches = function (week) {

      teamService.getMatches(week, self.selectedCompetition.competition_feed, self.seasonId).then(function (res) {
        var matches = [];

        _.each(res, function (obj, i, list) {
          if (obj.n_RoundNr == self.displayWeek || obj.n_RoundNr == self.displayWeek.week_id) {
            matches.push(obj);
          }
        });

        self.matches = matches.map(match => {
          match.c_HomeTeam = gettextCatalog.getString(match.c_HomeTeam);
          match.c_AwayTeam = gettextCatalog.getString(match.c_AwayTeam);
          return match;
        });
      });
    };

    // Show calendar outside team page
    self.getDeadlineWeeks = function () {
      if (self.deadlineWeeksInfo) {
        self.deadlineWeeks = self.deadlineWeeksInfo.weeks;
        self.displayWeek = self.deadlineWeeks && self.deadlineWeeks.find(deadlineWeek => deadlineWeek.week_id === self.deadlineWeeksInfo.week.display_week);
        self.getMatches(self.displayWeek.week_id);
      } else {
        return teamService.getDeadlineWeeks('2018', self.selectedCompetition.competition_feed).then(function (data) {
          self.deadlineWeeks = data.weeks;
          self.displayWeek = self.deadlineWeeks && self.deadlineWeeks.find(deadlineWeek => deadlineWeek.week_id === data.week.display_week);
          self.getMatches(self.displayWeek.week_id);
        });
      }
    };

    self.weekChanged = function () {
      if (self.displayWeek) {
        self.getMatches(self.displayWeek.week_id);
      }
    };

    self.singleCompetition = function () {
      return !!self.competition;
    };

    if (self.showSelect && !self.cachedMatches) {
      self.getDeadlineWeeks();
    }

    self.competitionChanged = function () {
      self.getDeadlineWeeks();
    };

    $scope.selectedWeek = self.deadlineWeeks && self.deadlineWeeks.find(deadline => deadline.week_id === self.displayWeek);

    // On updated week (parent comp)
    var previousDisplayWeekVal = self.displayWeek;
    this.$doCheck = function () {
      if (!self.showSelect && previousDisplayWeekVal !== self.displayWeek) {
        $scope.selectedWeek = self.deadlineWeeks && self.deadlineWeeks.find(deadlineWeek => deadlineWeek.week_id === self.displayWeek);

        // Get new matches
        self.getMatches(self.displayWeek);
        previousDisplayWeekVal = self.displayWeek;
      }
    };

    if (!self.showSelect && !self.cachedMatches) {
      self.getMatches(self.displayWeek);
    }


    self.goToMatch = function (match) {
      if (self.matchHasStarted(match)) {
        if (self.statesPrefix) {
          $state.go(self.statesPrefix + '-match', {matchId: match.n_FeedID});
        } else {
          $state.go('match', {matchId: match.n_FeedID});
        }
      }
    };

    self.matchHasStarted = function (match) {
      var start = moment.utc(match.d_MatchStartTime);
      start.local();

      var now = moment();

      return now > start;
    };
  }
};