import template from './soccer-team-league-fixture.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    fixture: '<'
  },
  controller: function ($scope, $rootScope, teamService, RESOURCES) {
    $rootScope.$broadcast('sidemenu:team:collapse', { team: this.team.Team.Team.id });
    $rootScope.title = "Fixture";

    var self = this;
    self.resources = RESOURCES;

    self.home_starting = {};
    self.away_starting = {};

    teamService.get(self.fixture.Fixture.home_id, self.fixture.Fixture.week_id).then(function (data) {
      self.home_starting = data.starting;
    });

    teamService.get(self.fixture.Fixture.away_id, self.fixture.Fixture.week_id).then(function (data) {
      self.away_starting = data.starting;
    });
  }
};