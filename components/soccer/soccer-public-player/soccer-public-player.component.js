import template from './soccer-public-player.component.tpl.html';

export default {
  template,
  bindings: {
    player: '<',
    showJerseys: '<',
    localAssets: '<'
  },
  controller: function (RESOURCES, $rootScope, $scope, SPORTS_ASSETS) {
    var self = this;
    $rootScope.title = self.player.Player.Player.name;
    $rootScope.$broadcast('sidemenu:team:collapse');

    self.assetsURL = SPORTS_ASSETS.football.remote;

    if (self.localAssets) {
      self.assetsURL = SPORTS_ASSETS.football.local;
    }

    $scope.isActivePlayer = function (player) {
      if (player.active)
        return true;

      return false;
    };

    self.sortStats = function (stat1) {
      return stat1.Match.week_id;
    };

    self.resources = RESOURCES;
  }
};