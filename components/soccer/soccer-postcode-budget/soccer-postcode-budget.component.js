import template from './soccer-postcode-budget.component.tpl.html';

export default {
  template,
  bindings: {
    teams: '<',
    postcode: '<'
  },
  controller: function () {
    var vm = this;

    vm.postcodeTeams = this.teams.Teams;
    vm.totalRecords = this.teams.total_records;
    vm.pagination = {current: 1};
    vm.displayItems = 10;
    vm.onPageChange = onPageChange;

    function onPageChange(newPageNumber) {
      vm.pagination.current = newPageNumber;
    }
  }
};