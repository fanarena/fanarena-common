import template from './soccer-team-draft.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    draft: '<',
    showHelp: '<'
  },
  controller: function ($scope, $stateParams, $rootScope, localStorageService, RESOURCES, $pusher, leagueService,
                        playerService, growl, faHelpService, HelpModulesIDs, DraftBestOptions) {
    $rootScope.$broadcast('sidemenu:team:collapse', { team: this.team.Team.Team.id });
    //faHelpService.setCurrentHelpModule(HelpModulesIDs.Wishlist);

    // $rootScope.title = this.team.Team.Team.name + " - " + "Draft";
    $rootScope.title = "Draft";
    var self = this;
    self.resources = RESOURCES;
    self.user = localStorageService.get('user');

    // Initialize pusher
    var pusher = $pusher(pusherClient);

    // Keep track of teams
    $scope.selectedTeam = null;
    $scope.teams = [];
    _.each(self.draft.league.Team, function (team, index) {
      $scope.teams[team.id] = team;

      if (team.id === self.team.Team.Team.id)
        $scope.selectedTeam = team;
    });

    // Switch between shortlist / all players
    $scope.showAddPlayers = localStorageService.get('playersFilter') !== null ? localStorageService.get('playersFilter').open : true;

    self.togglePlayersList = function () {
      $scope.showAddPlayers = !$scope.showAddPlayers;
    };

    // Mocks
    $scope.mocks = self.draft.mocks;

    $scope.mocksOnlyAvailable = {
      value: !localStorageService.get('mocksOnlyAvailable') ? false : true
    };

    $scope.sortableOptions = {
      axis: 'y',
      handle: '> .drag-region',
      stop: function (e, ui) {
        self.updateMocksOrder();
      }
    };

    self.saveOnlyAvailablePref = function () {
      localStorageService.set('mocksOnlyAvailable', $scope.mocksOnlyAvailable.value);
    };

    self.updateMocksOrder = function () {
      var players = [];
      _.each($scope.mocks, function (mock) {
        players.push(mock.Player.id);
      });

      leagueService.sortMocks(players, self.team.Team.Team.id);
    };
    self.moveToTop = function (player) {
      var index = 0;
      _.each($scope.mocks, function (el, i) {
        if (el == player)
          index = i;
      });

      $scope.mocks.splice(0, 0, $scope.mocks.splice(index, 1)[0]);
      self.updateMocksOrder();
    };
    self.moveToBottom = function (player) {
      var index = 0;
      _.each($scope.mocks, function (el, i) {
        if (el == player)
          index = i;
      });

      $scope.mocks.splice($scope.mocks.length - 1, 0, $scope.mocks.splice(index, 1)[0]);
      self.updateMocksOrder();
    };

    self.getBestOptionsByPosition = function (position) {
      var leagueTopPlayers = DraftBestOptions[self.draft.league.League.competition_feed] || [];
      var bestOptions = leagueTopPlayers[position] || [];
      var result = '';

      if (bestOptions[0] && bestOptions[1]) {
        result = bestOptions[0] + ' & ' + bestOptions[1];
      }
      return result;
    };
    //Wishlist progress incicator - depends on mocks

    var updateProgressBar = function (amount) {
      if (amount < 5) {
        $scope.mocksType = "red";
        $scope.mocksLabel = "Very weak";
        $scope.mocksValue = 20;
      } else if (amount < 20) {
        $scope.mocksType = "orange";
        $scope.mocksLabel = "Weak";
        $scope.mocksValue = 40;
      } else if (amount < 35) {
        $scope.mocksType = "yellow";
        $scope.mocksLabel = "Average";
        $scope.mocksValue = 60;
      } else if (amount < 50) {
        $scope.mocksType = "lgreen";
        $scope.mocksLabel = "Strong";
        $scope.mocksValue = 80;
      } else {
        $scope.mocksType = "dgreen";
        $scope.mocksLabel = "Very strong";
        $scope.mocksValue = 100;
      }
      $scope.mocksAmount = amount;
      if ($scope.mocksAmount == 1)
        $scope.mocksPlayers = "player";

      else  $scope.mocksPlayers = "players";
    }
    updateProgressBar($scope.mocks.length);
    // Players pagination
    $scope.playersPerPage = 10;
    $scope.pagination = {
      current: localStorageService.get('playersFilter') !== null ? localStorageService.get('playersFilter').page : 1
    };

    // Players
    $scope.players = self.draft.players;

    $scope.positions = [
      { id: 0, min: 0, inTeam: 0, name: 'All positions', type: '' },
      { id: 1, min: 1, inTeam: 0, name: 'Goalkeepers', type: 'gk' },
      { id: 2, min: 4, inTeam: 0, name: 'Defenders', type: 'def' },
      { id: 3, min: 4, inTeam: 0, name: 'Midfielders', type: 'mf' },
      { id: 4, min: 2, inTeam: 0, name: 'Forwards', type: 'fwd' },
    ];
    $scope.filterOptions = {
      clubs: self.draft.clubs,
      positions: $scope.positions
    };
    $scope.filterItem = {
      club: localStorageService.get('playersFilter') !== null ? _.findWhere(self.draft.clubs, { id: localStorageService.get('playersFilter').club }) : self.draft.clubs[0],
      position: localStorageService.get('playersFilter') !== null ? _.findWhere($scope.positions, { id: localStorageService.get('playersFilter').position }) : $scope.positions[0]
    };

    var saveFilter = function () {
      var club = $scope.filterItem.club ? $scope.filterItem.club.id : 0;
      var position = $scope.filterItem.position ? $scope.filterItem.position.id : 0;
      localStorageService.set('playersFilter', {
        club: club,
        position: position,
        open: $scope.showAddPlayers,
        page: $scope.pagination.current
      });
    };

    $scope.$watch('filterItem', function () {
      saveFilter();
    }, true);

    $scope.$watch('pagination', function () {
      saveFilter();
    }, true);

    $scope.$watch('showAddPlayers', function () {
      saveFilter();
    }, true);

    $scope.searchPlayer = { value: "" };
    $scope.customFilter = function (player) {
      if ($scope.filterItem.club.id !== 0 && player.Player.Club.id !== $scope.filterItem.club.id) {
        return false;
      } else if ($scope.filterItem.position.id !== 0 && player.Player.Position.id !== $scope.filterItem.position.id) {
        return false;
      } else if ($scope.searchPlayer.value !== "" && player.Player.name.toLowerCase().indexOf($scope.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    // Waivers
    self.recall = function (pick) {
      playerService.recall(pick.id, self.team.Team.Team.id).then(function (res) {
        pick.transfer = "0000-00-00 00:00:00";
        growl.addSuccessMessage(pick.Player.name + ' was recalled!');
      });
    };

    // Add/remove from shortlist
    $scope.shortlistAdd = function (player) {
      leagueService.toggleMock(player.Player.id, self.team.Team.Team.id);

      if ($scope.mocks.length === 0)
        $rootScope.$broadcast('mocks:notempty');

      player.Player.isMock = 1;
      $scope.mocks.push(player);
      updateProgressBar($scope.mocks.length);
      growl.addSuccessMessage(player.Player.short + ' added to shortlist');
    };

    $scope.shortlistRemove = function (player) {

      leagueService.toggleMock(player.Player.id, self.team.Team.Team.id);
      player.Player.isMock = 0;

      if ($scope.mocks.length === 1)
        $rootScope.$broadcast('mocks:empty');

      $scope.mocks = $scope.mocks.filter(function (item) {
        return item.Player.id != player.Player.id
      });

      updateProgressBar($scope.mocks.length);
      growl.addSuccessMessage(player.Player.short + ' removed from shortlist');

      // Find player in $scope.players
      _.each($scope.players, function (el) {
        if (el.Player.id === player.Player.id)
          el.Player.isMock = 0;
      });
    };

    // Previous picks / my picks
    $scope.previousPicks = [];
    _.each(self.draft.league.Team, function (team) {
      _.each(team.Pick, function (pick) {
        $scope.previousPicks.push(pick);

        // If my team
        if (team.id === self.team.Team.Team.id) {
          var type = _.findWhere($scope.positions, { id: pick.Player.Position.id })
          type.inTeam++;
        }
      });
    });

    self.hasPick = function (player) {
      if (player.Player.Pick.length > 0 && player.Player.Pick[0].id !== null)
        return true;
      else
        return false;
    };

    self.isMyPick = function (player) {
      if (player.Player.Pick.length > 0 && player.Player.Pick[0].Team.user_id === self.user.id)
        return true;
      else
        return false;
    };

    self.isOthersPick = function (player) {
      if (player.Player.Pick.length > 0 && player.Player.Pick[0].id !== null && player.Player.Pick[0].Team.user_id !== self.user.id && player.Player.Pick[0].team_id !== 0)
        return true;
      else
        return false;
    };

    // Current picking
    $scope.nowpick = {
      round: self.draft.league.League.round,
      pick: self.draft.league.League.pick,
      team_id: self.draft.nowpick && self.draft.nowpick.hasOwnProperty('Team') ? self.draft.nowpick.Team.id : null
    };


    // Pick button
    self.iPicked = 0;
    $scope.pick = function (player) {
      self.iPicked = 1;

      leagueService.pickPlayer(self.team.Team.Team.id, player.Player.id).then(null, function (err) {
        if (err.hasOwnProperty('message'))
          growl.addErrorMessage(err.message);
      });
    };

    // Pusher subscribe
    var channel = pusher.subscribe('league-' + self.draft.league.League.id);
    channel.bind('pick', function (data) {

      // Reset timer
      $rootScope.$broadcast('timer:reset', { value: data.timeleft });

      // Find player
      var player = null;
      _.each($scope.players, function (el) {
        if (el.Player.id === data.pick.Player.id) {
          player = el;
        }
      });
      player.team_id = data.pick.Team.id;
      player.round = data.pick.Pick.Pick.round;
      player.pick = data.pick.Pick.Pick.pick;

      // Add to previous picks
      $scope.previousPicks.push(player);

      // Add to team picks
      $scope.teams[data.pick.Team.id].Pick.push(player);

      // Update players list
      var pick = data.pick.Pick.Pick;
      pick.Team = data.pick.Team;

      _.each($scope.players, function (el) {
        if (el.Player.id === player.Player.id) {
          el.Player.Pick[0] = pick;
        }
      });

      // Update shortlist
      _.each($scope.mocks, function (el) {
        if (el.Player.id === player.Player.id) {
          el.Player.Pick[0] = pick;
        }
      });

      // Update nowpick
      $scope.nowpick.round = data.league.League.round;
      $scope.nowpick.pick = data.league.League.pick;
      $scope.nowpick.team_id = data.nowpick.Team.id;

      // Update positions if my pick
      if (data.pick.Team.user_id === self.user.id) {
        var position = _.findWhere($scope.positions, { id: data.pick.Position.id });
        if (position !== undefined)
          position.inTeam++;
      }


      if (!self.iPicked) {
        // Player picked animation
        $scope.$apply(function () {
          $scope.playerPicked = player;
        });

        setTimeout(function () {
          $scope.$apply(function () {
            $scope.playerPicked = false;
          });
        }, 2500);
      } else
        self.iPicked = 0;

      // check if draft has ended
      if (data.league.League.status > 2) {
        $scope.$apply(function () {
          $scope.nowpick.team_id = null;
          $rootScope.$broadcast('team:refresh');
          growl.addSuccessMessage('Draft has ended!');
        });
      }
    });

    // Player picked animation
    $scope.playerPicked = false;

    $rootScope.$on('playerPicked:hide', function (ev, args) {
      $scope.playerPicked = false;
    });

    $scope.$on('$destroy', function () {
      channel.unbind('pick');
    });
  }
};