import SoccerCalendar from './soccer-calendar/soccer-calendar.component';
import SoccerCaptain from './soccer-captain/soccer-captain.component';
import SoccerContactBudget from './soccer-contact-budget/soccer-contact-budget.component';
import SoccerDashboardBudget from './soccer-dashboard-budget/soccer-dashboard-budget.component';
import SoccerDeadlineCounterBudget from './soccer-deadline-counter-budget/soccer-deadline-counter-budget.component';
import SoccerDeadlinesBudget from './soccer-deadlines-budget/soccer-deadlines-budget.component';
import SoccerFaqBudget from './soccer-faq-budget/soccer-faq-budget.component';
import SoccerHomeBudget from './soccer-home-budget/soccer-home-budget.component';
import SoccerJoinLeague from './soccer-join-league/soccer-join-league.component';
import SoccerJoinLeagueBudget from './soccer-join-league-budget/soccer-join-league-budget.component';
import SoccerLeagueBudget from './soccer-league-budget/soccer-league-budget.component';
import SoccerLoginBudget from './soccer-login-budget/soccer-login-budget.component';
import SoccerMatch from './soccer-match/soccer-match.component';
import SoccerMostPickedWidgetBudget from './soccer-most-picked-widget-budget/soccer-most-picked-widget-budget.component';
import SoccerNewGame from './soccer-new-game/soccer-new-game.component';
import SoccerNewGameBudget from './soccer-new-game-budget/soccer-new-game-budget.component';
import SoccerNewGameCreateLeague from './soccer-new-game-create-league/soccer-new-game-create-league.component';
import SoccerNewGameCreateTeam from './soccer-new-game-create-team/soccer-new-game-create-team.component';
import SoccerNewGameCustomLeagueInfo from './soccer-new-game-custom-league-info/soccer-new-game-custom-league-info.component';
import SoccerNewGameExistingLeagueInfo from './soccer-new-game-existing-league-info/soccer-new-game-existing-league-info.component';
import SoccerNewGameInviteCode from './soccer-new-game-invite-code/soccer-new-game-invite-code.component';
import SoccerNewGameJoinLeague from './soccer-new-game-join-league/soccer-new-game-join-league.component';
import SoccerNewGameLeagueBudget from './soccer-new-game-league-budget/soccer-new-game-league-budget.component';
import SoccerNewGameProgress from './soccer-new-game-progress/soccer-new-game-progress.component';
import SoccerNewGameTopBar from './soccer-new-game-top-bar/soccer-new-game-top-bar.component';
import SoccerNewGameTypes from './soccer-new-game-types/soccer-new-game-types.component';
import SoccerPlayer from './soccer-player/soccer-player.component';
import SoccerPlayerBudget from './soccer-player-budget/soccer-player-budget.component';
import SoccerPlayersListBudget from './soccer-players-list-budget/soccer-players-list-budget.component';
import SoccerPostCodeBudget from './soccer-postcode-budget/soccer-postcode-budget.component';
import SoccerPrizesBudget from './soccer-prizes-budget/soccer-prizes-budget.component';
import SoccerPrizesWidgetBudget from './soccer-prizes-widget-budget/soccer-prizes-widget-budget.component';
import SoccerPublicPlayer from './soccer-public-player/soccer-public-player.component';
import SoccerRegisterBudget from './soccer-register-budget/soccer-register-budget.component';
import SoccerResetPasswordBudget from './soccer-reset-password-budget/soccer-reset-password-budget.component';
import SoccerRulesBudget from './soccer-rules-budget/soccer-rules-budget.component';
import SoccerSquadBudget from './soccer-squad-budget/soccer-squad-budget.component';
import SoccerSquadTransferBudget from './soccer-squad-transfer-budget/soccer-squad-transfer-budget.component';
import SoccerTeamDraft from './soccer-team-draft/soccer-team-draft.component';
import SoccerTeamEditBudget from './soccer-team-edit-budget/soccer-team-edit-budget.component';
import SoccerTeamLeague from './soccer-team-league/soccer-team-league.component';
import SoccerTeamLeagueFixture from './soccer-team-league-fixture/soccer-team-league-fixture.component';
import SoccerTeamPlayer from './soccer-team-player/soccer-team-player.component';
import SoccerTeamRoster from './soccer-team-roster/soccer-team-roster.component';
import SoccerTeamTeam from './soccer-team-team/soccer-team.component';
import SoccerUserProfileBudget from './soccer-user-profile-budget/soccer-user-profile-budget.component';
import SoccerWinnerWidgetBudget from './soccer-winner-widget-budget/soccer-winner-widget-budget.component';
import SoccerPrizesFr from './soccer-prizes-budget/soccer-prizes-budget-fr-component';
import SoccerPrizesMd from './soccer-prizes-budget/soccer-prizes-budget-md-component';
import SoccerBlanko from './soccer-blanko/soccer-blanko.component';
import SoccerEndWinnerWidget from './soccer-endwinner-widget/soccer-endwinner-widget.component';
import SoccerDeadlinesBudgetTable from './soccer-deadlines-budget-table/soccer-deadlines-budget-table.component';
import SoccerPlayersList from './soccer-players-list/soccer-players-list.component';
import SoccerMatchInfo from './soccer-match-info/soccer-match-info.component';
import SoccerTeamPlayerPointsDetails from './soccer-team-player-points-details/soccer-team-player-points-details';
import SoccerPlayersListScout from './soccer-players-list-scout/soccer-players-list-scout.component';

export {
  SoccerCalendar,
  SoccerCaptain,
  SoccerContactBudget,
  SoccerDashboardBudget,
  SoccerDeadlineCounterBudget,
  SoccerDeadlinesBudget,
  SoccerFaqBudget,
  SoccerHomeBudget,
  SoccerJoinLeague,
  SoccerJoinLeagueBudget,
  SoccerLeagueBudget,
  SoccerLoginBudget,
  SoccerMatch,
  SoccerMostPickedWidgetBudget,
  SoccerNewGame,
  SoccerNewGameBudget,
  SoccerNewGameCreateLeague,
  SoccerNewGameCreateTeam,
  SoccerNewGameCustomLeagueInfo,
  SoccerNewGameExistingLeagueInfo,
  SoccerNewGameInviteCode,
  SoccerNewGameJoinLeague,
  SoccerNewGameLeagueBudget,
  SoccerNewGameProgress,
  SoccerNewGameTopBar,
  SoccerNewGameTypes,
  SoccerPlayer,
  SoccerPlayerBudget,
  SoccerPlayersListBudget,
  SoccerPostCodeBudget,
  SoccerPrizesBudget,
  SoccerPrizesWidgetBudget,
  SoccerPublicPlayer,
  SoccerRegisterBudget,
  SoccerResetPasswordBudget,
  SoccerRulesBudget,
  SoccerSquadBudget,
  SoccerSquadTransferBudget,
  SoccerTeamDraft,
  SoccerTeamEditBudget,
  SoccerTeamLeague,
  SoccerTeamLeagueFixture,
  SoccerTeamPlayer,
  SoccerTeamRoster,
  SoccerTeamTeam,
  SoccerUserProfileBudget,
  SoccerWinnerWidgetBudget,
  SoccerPrizesFr,
  SoccerPrizesMd,
  SoccerBlanko,
  SoccerEndWinnerWidget,
  SoccerDeadlinesBudgetTable,
  SoccerPlayersList,
  SoccerMatchInfo,
  SoccerTeamPlayerPointsDetails,
  SoccerPlayersListScout
};