import template from './soccer-team-roster.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    displayTeam: '<',
    deadlineWeeks: '<',
    currentCompetition: '<',
    localAssets: '<',
    playersWithJerseys: '<',
    classicCaptainSelection: '<',
    showExtraInfo: '<',
    showBenchTransferButton: '<',
    saveSelectioOnEdit: '<',
    deadlineWeekInfo: '<',
    enableFirstWeekEdit: '<',
    statesPrefix: '<',
    benchAlwaysVisible: '<',
    gameName: '<',
    benchEnabled: '<',
    captainFactor: '<',
    captainFactorByTeamResult: '<',
    hidePositionBackground: '<',
    calendarAssetsURL: '<',
    pointsBreakDown: '<',
    matches: '<',
    displayWeek: '<',
    matchInfoEnabled: '<',
    editWeeks: '<',
    rosterFirstExtraWidget: '<',
    captainWithTeamPoints: '<',
    readOnly: '<',
    maxPlayersSameTeam: '<',
    hasLayoutClass: '<'
  },
  controller: function ($scope, $state, $stateParams, $rootScope, teamService, RESOURCES, gettext, growl,
                        SPORTS_ASSETS, PlayerService, FootballPicker, gameService, gettextCatalog,
                        $ngRedux, storeHelpers, storeActions) {
    var self = this;
    // $rootScope.title = this.team.Team.Team.name + " - " + "Team";
    $rootScope.title = gettext("Team");
    $rootScope.$broadcast('sidemenu:team:collapse', {team: self.team.Team.Team.id});

    self.resources = RESOURCES;
    self.popoverUrl = "/tpl/ui/teamPopover.html";
    self.popoverPlayer = null;
    self.myTeam = $rootScope.teams && $rootScope.teams.find(team => team.Team.id === self.team.Team.Team.id);
    self.localAssets = this.localAssets;
    self.playersWithJerseys = this.playersWithJerseys;
    self.noSelectionWeek = self.displayTeam.starting === 'no   selection week' && !self.displayTeam.bench;
    self.assetsURL = SPORTS_ASSETS.football.remote;
    self.isFirstDeadlineWeek = isFirstDeadlineWeek;
    self.deadlineWeekHasNoMatches = self.deadlineWeekInfo && !self.deadlineWeekInfo.hasMatches;
    self.matchDayLabel = gettextCatalog.getString('Matchday');
    self.editableTeamWeeks = self.editWeeks || [];
    self.resolved = false;
    self.maxSameClubPlayers = self.maxPlayersSameTeam || 2;
    self.teamChanges = 0;
    self.onCaptainChange = onCaptainChange;
    self.saveSelection = saveSelection;
    self.saveByButtonEnabled = false;
    self.dragByHandleOnly = false;

    self.playersPointsTableVisible = false

    if (self.deadlineWeekHasNoMatches) {
      self.deadlineWeeks = self.deadlineWeeks.filter(deadlineWeek =>
        deadlineWeek.week_id !== self.deadlineWeekInfo.deadlineWeek)
    }

    if (self.displayTeam.weeks.deadline_week) {
      if (self.readOnly) {
        self.deadlineWeeks = self.deadlineWeeks.filter(deadlineWeek =>
          deadlineWeek.week_id < self.displayTeam.weeks.deadline_week);
      } else {
        self.deadlineWeeks = self.deadlineWeeks.filter(deadlineWeek =>
          deadlineWeek.week_id >= self.displayTeam.weeks.deadline_week);
      }
    }

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    this.$onInit = function () {
      let disconnect = $ngRedux.connect(mapStateToParams, Object.assign({},
        storeActions.playersActions, storeActions.clubsActions))(this);
      $scope.$on('$destroy', disconnect);
      const clubs = this.clubs.find(clubInfo => clubInfo.competitionFeed === self.team.Team.League.Competition.competition_feed);
      const players = this.clubs.find(playerInfo => playerInfo.competitionFeed === self.team.Team.League.Competition.competition_feed);

      if (self.myTeam && (isFirstDeadlineWeek() || isInTeamEditsDeadlineWeek()) && self.enableFirstWeekEdit) {
        if (self.statesPrefix) {
          $state.go(self.statesPrefix + '-soccerTeamEditBudget', {id: this.team.Team.Team.id, '#': null});
        } else {
          $state.go('soccerTeamEditBudget', {id: this.team.Team.Team.id, '#': null});
        }
      } else {
        if (!players && !clubs) {
          storeHelpers.initPlayers()
            .then(() => {
              if (!clubs) {
                return storeHelpers.initClubs()
                  .then(() => activate());
              } else {
                activate();
              }
            });
        } else {
          activate();
        }
      }
    };


    if (!self.currentCompetition) {
      self.currentCompetition = gameService.getCompetitions()
        .find(competition => competition.competition_feed === self.team.Team.League.Competition.competition_feed);
    }

    if (self.localAssets) {
      self.assetsURL = SPORTS_ASSETS.football.local;
    }

    function activate() {
      const clubsInfo = self.clubs.find(clubInfo => clubInfo.competitionFeed === self.team.Team.League.Competition.competition_feed);

      self.clubs = clubsInfo.clubs;

      self.filterItem = {};
      $scope.starting = [];
      $scope.bench = [];

      if (self.readOnly) {
        self.spreadBench = self.displayTeam.bench;
        $scope.bench = teamService.aggregateValuesForDuplicatedPlayers({0: angular.copy(self.displayTeam.bench)});
      } else {
        angular.copy(self.team.bench, $scope.bench);
      }

      if (self.showOpponentInfo) {
        $scope.bench = $scope.bench
          .map(player => Object.assign({}, player, {opponentInfo: getOpponentInfo(player)}));
      }

      if (!self.displayTeam.starting && !self.team.starting) {
        $scope.starting = null;
      } else {
        var lineup = self.displayTeam.starting || self.team.starting;

        $scope.spreadStarting = getAllPlayersInList(angular.copy(lineup));
        preparePlayersStructure(angular.copy(lineup));
      }

      self.resolved = true;
    }


    self.changeCaptainHandler = function (captain) {
      var teamId = self.team.Team.Team.id;
      var captainId = self.classicCaptainSelection ? self.captain.id : captain.players.id;
      const competitionFeed = self.team.Team.League.competition_feed || self.team.Team.Team.competition_feed;
      teamService.changeFootbalCaptain(teamId, self.currentDisplayWeek.week_id, captainId, competitionFeed)
        .then(function (result) {
          growl.addSuccessMessage(gettextCatalog.getString('Football captain changed!'));
          teamService.get(teamId, self.currentDisplayWeek.week_id)
            .then(function (result) {
              self.displayTeam = result;
              preparePlayersStructure(self.displayTeam.starting);
            });
        })
        .catch(function () {
          growl.addErrorMessage(gettextCatalog.getString('Captain could not be saved'));
        });
    };

    self.popover = function (player) {
      self.popoverPlayer = player;

      // Close all other popovers
      for (prop in $scope.starting) {
        for (index in $scope.starting[prop])
          $scope.starting[prop][index].popoverOpen = false;
      }
    };

    self.moveToBench = function (player) {
      player.popoverOpen = false;
    };

    self.goToProfile = function (player) {
      player.popoverOpen = false;
    };

    // Keep track of original and current (changed match day) deadline week
    $scope.originalDeadlineWeek = self.team.deadline_week;

    // Check if start of league
    if (self.myTeam && ($scope.originalDeadlineWeek === self.team.Team.League.week_id && $scope.originalDeadlineWeek === 1)) {
      self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: self.team.Team.League.week_id});
    } else if (self.readOnly) {
      self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: self.displayWeek});
    } else {
      self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: self.displayTeam.weeks.deadline_week || self.displayTeam.weeks.display_week});
    }
    self.currentDisplayWeekId = self.currentDisplayWeek.week_id;

    const defaultCalendarDeadlineWeek = _.findWhere(self.deadlineWeeks, {week_id: self.displayTeam.weeks.display_week});

    if (defaultCalendarDeadlineWeek) {
      $scope.calendarDeadlineWeek = defaultCalendarDeadlineWeek.deadlineweek;
    }
    self.isLastWeek = self.currentDisplayWeek && (self.currentDisplayWeek.week_id === self.deadlineWeeks[self.deadlineWeeks.length - 1].week_id);

    self.displayWeekStats = self.displayTeam.Team && self.displayTeam.Team.Weekstat && self.displayTeam.Team.Weekstat.find(function (weekstat) {
      return weekstat.week_id === self.currentDisplayWeek.week_id;
    });

    self.onKeep = function () {
      var selectedWeekNotPlayed = self.currentDisplayWeek.week_id >= $scope.originalDeadlineWeek;

      teamService
        .get(self.displayTeam.Team.Team.id, self.currentDisplayWeek.week_id, selectedWeekNotPlayed)
        .then(function (res) {
          if (res.bench) {
            $scope.bench = res.bench;
          } else {
            $scope.bench = null;
          }
          if (res.starting) {
            preparePlayersStructure(res.starting);
          } else {
            $scope.starting = null;
          }
          self.displayTeam = res;
        });
    }

    self.changePlayDate = function (type, week) {
      var valid = true;

      if (type == 'prev' && self.currentDisplayWeek.week_id <= 1)
        valid = false;
      else if (type == 'next' && (self.currentDisplayWeek.week_id === self.deadlineWeeks[self.deadlineWeeks.length - 1].week_id))
        valid = false;

      if (valid) {
        if (type == 'prev') {
          self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: self.currentDisplayWeek.week_id - 1});
        }
        else if (type == 'next') {
          self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: self.currentDisplayWeek.week_id + 1});
        }

        self.currentDisplayWeekId = self.currentDisplayWeek.week_id;

        var selectedWeekNotPlayed = self.currentDisplayWeek.week_id >= $scope.originalDeadlineWeek;
        teamService.get(self.displayTeam.Team.Team.id, self.currentDisplayWeek.week_id, selectedWeekNotPlayed).then(function (res) {
          if (res.bench) {
            if (self.readOnly) {
              self.spreadBench = res.bench;
              $scope.bench = teamService.aggregateValuesForDuplicatedPlayers({0: angular.copy(res.bench)});
            } else {
              $scope.bench = res.bench;
            }
          } else {
            $scope.bench = null;
          }

          if (res.starting) {
            $scope.spreadStarting = getAllPlayersInList(angular.copy(res.starting));

            preparePlayersStructure(angular.copy(res.starting));
          } else {
            $scope.starting = null;
          }
          self.displayTeam = res;

          self.noSelectionWeek = self.displayTeam.starting === 'no selection week' && !self.displayTeam.bench;

          self.displayWeekStats = self.displayTeam.Team && self.displayTeam.Team.Weekstat && self.displayTeam.Team.Weekstat.find(function (weekstat) {
            return weekstat.week_id === self.currentDisplayWeek.week_id;
          });

          self.isLastWeek = self.currentDisplayWeek.week_id === self.deadlineWeeks[self.deadlineWeeks.length - 1].week_id;
        });

        $scope.calendarDeadlineWeek = self.currentDisplayWeek.deadlineweek;
      }
    };

    self.editSelection = function () {
      if (self.currentDisplayWeek.week_id < $scope.originalDeadlineWeek) {
        self.currentDisplayWeek = _.findWhere(self.deadlineWeeks, {week_id: $scope.originalDeadlineWeek});
        self.currentDisplayWeekId = self.currentDisplayWeek.week_id;

        teamService.get(self.displayTeam.Team.Team.id, self.currentDisplayWeek.week_id, true)
          .then(function (res) {
            $scope.bench = res.bench;
            preparePlayersStructure(res.starting);

            self.displayTeam = res;
            self.noSelectionWeek = self.displayTeam.starting === 'no selection week' && !self.displayTeam.bench;

            self.displayWeekStats = self.displayTeam.Team && self.displayTeam.Team.Weekstat && self.displayTeam.Team.Weekstat.find(function (weekstat) {
              return weekstat.week_id === self.currentDisplayWeek.week_id;
            });

          });
        $scope.calendarDeadlineWeek = self.currentDisplayWeek.deadlineweek;
      }
    };

    self.capitalize = function (string) {
      return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
    }

    self.getPickIds = function () {
      var arr = [];

      for (var prop in $scope.starting) {
        _.each($scope.starting[prop], function (el, index, list) {
          if (el.hasOwnProperty('picks'))
            arr.push(el.picks.id);
        });
      }

      return arr;
    };

    // Bench transfer out
    $scope.$on('bench:remove', function (ev, args) {
      $scope.bench = _.without($scope.bench, args.obj);
    });

    // DRAG AND DROP
    $scope.benchDrop = $scope.goalkeeperDrop = $scope.defendersDrop = $scope.midfieldersDrop = $scope.forwardersDrop = false;
    $scope.draggingPlayer = $scope.newPlayer = null;

    // New drag drop
    $scope.isDraggable2 = function (player) {
      if (self.currentDisplayWeek.week_id >= $scope.originalDeadlineWeek)
        return true;
      else
        return false;
    };

    $scope.isBenchDroppable = function (player) {
      if ($scope.dragElem !== null && $scope.fieldDrag)
        return true;

      return false;
    };

    $scope.isFieldDroppable = function (type, player) {
      if ($scope.dragElem !== null && !$scope.fieldDrag)
        return true;

      return false;
    };

    $scope.fieldDrag = false;
    $scope.dragElem = null;

    $scope.onDragStart2 = function ($event) {
      $event.stopPropagation();

      $scope.dragElem = $event.target;
      var player = angular.element($event.target).scope();

      if (!$event.target.classList.contains('player-bench'))
        $scope.fieldDrag = true;

      $scope.$apply();
    };

    $scope.onDragStop2 = function ($event) {
      $scope.$apply(function () {
        $scope.fieldDrag = false;

        setTimeout(function () {
          $scope.$apply(function () {
            $scope.dragElem = null;
          });
        }, 50);
      });
    };

    var getObjIndexInArray = function (arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (obj == arr[i])
          return i;
      }
    };

    $scope.onDropStop2 = function ($event, $index) {
      // Check if not dropped onto same type
      var dragged = angular.element($scope.dragElem).scope();
      var dropped = angular.element($event.target).scope();

      // reset any previously defined selections
      FootballPicker.set([], []);


      // Drop onto bench
      if ($event.target.classList.contains('player-bench')) {
        var fieldPlayer = null;
        var benchPlayer = null;
        var index = 0;

        var type = 'goalkeeper';
        if (dragged.hasOwnProperty('defender'))
          type = 'defender';
        if (dragged.hasOwnProperty('midfielder'))
          type = 'midfielder';
        if (dragged.hasOwnProperty('forward'))
          type = 'forward';


        fieldPlayer = dragged[type];
        benchPlayer = dropped.player;

        var startingWithoutDragged = getStartingList().filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(fieldPlayer, 'id')
        });

        var benchWithoutDropped = $scope.bench.filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(benchPlayer, 'id')
        });

        FootballPicker.set(startingWithoutDragged, benchWithoutDropped.concat([fieldPlayer]));
        var validFormation = FootballPicker.pick(benchPlayer).picked;

        if (!validFormation) {
          // Get indexes in arrays
          var benchIndex = getObjIndexInArray($scope.bench, benchPlayer);
          var fieldIndex = getObjIndexInArray($scope.starting[self.capitalize(type)], fieldPlayer);

          // Remove from arrays
          $scope.bench = _.without($scope.bench, _.findWhere($scope.bench, {$$hashKey: benchPlayer.$$hashKey}));
          $scope.starting[self.capitalize(type)] = _.without($scope.starting[self.capitalize(type)], _.findWhere($scope.starting[self.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          // Make copies
          benchPlayer = angular.copy(benchPlayer);
          fieldPlayer = angular.copy(fieldPlayer);

          // Add them again
          $scope.bench.splice(benchIndex, 0, benchPlayer);
          $scope.starting[self.capitalize(type)].splice(fieldIndex, 0, fieldPlayer);
        }
        else {

          if (fieldPlayer.selections && fieldPlayer.selections.captain) {
            fieldPlayer.selections.captain = false;
            if (!benchPlayer.selections) {
              benchPlayer.selections = {};
            }
            benchPlayer.selections.captain = true;

          }

          index = getObjIndexInArray($scope.bench, benchPlayer);
          $scope.starting[self.capitalize(type)] = _.without($scope.starting[self.capitalize(type)], _.findWhere($scope.starting[self.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          if (benchPlayer.players.position_id == 1)
            $scope.starting.Goalkeeper.unshift(benchPlayer);
          if (benchPlayer.players.position_id == 2)
            $scope.starting.Defender.unshift(benchPlayer);
          if (benchPlayer.players.position_id == 3)
            $scope.starting.Midfielder.unshift(benchPlayer);
          if (benchPlayer.players.position_id == 4)
            $scope.starting.Forward.unshift(benchPlayer);

          $scope.bench = _.without($scope.bench, _.findWhere($scope.bench, {$$hashKey: benchPlayer.$$hashKey}));
          $scope.bench.splice(index, 0, fieldPlayer);
        }
      }
      // Drop onto field
      else {
        var fieldPlayer = null;
        var benchPlayer = null;
        var index = 0;

        var type = 'goalkeeper';
        if (dropped.hasOwnProperty('defender'))
          type = 'defender';
        if (dropped.hasOwnProperty('midfielder'))
          type = 'midfielder';
        if (dropped.hasOwnProperty('forward'))
          type = 'forward';


        benchPlayer = _.findWhere($scope.bench, {$$hashKey: dragged.player.$$hashKey});
        fieldPlayer = _.findWhere($scope.starting[self.capitalize(type)], {$$hashKey: dropped[type].$$hashKey});

        var startingWithoutDropped = getStartingList().filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(fieldPlayer, 'id')
        });

        var benchWithoutDragged = $scope.bench.filter(function (player) {
          return PlayerService.getPlayerProp(player, 'id') !== PlayerService.getPlayerProp(benchPlayer, 'id')
        });

        FootballPicker.set(startingWithoutDropped, benchWithoutDragged.concat([fieldPlayer]));
        var validFormation = FootballPicker.pick(benchPlayer).picked;

        if (!validFormation) {
          // Get indexes in arrays
          var benchIndex = getObjIndexInArray($scope.bench, benchPlayer);
          var fieldIndex = getObjIndexInArray($scope.starting[self.capitalize(type)], fieldPlayer);

          // Remove from arrays
          $scope.bench = _.without($scope.bench, _.findWhere($scope.bench, {$$hashKey: benchPlayer.$$hashKey}));
          $scope.starting[self.capitalize(type)] = _.without($scope.starting[self.capitalize(type)], _.findWhere($scope.starting[self.capitalize(type)], {$$hashKey: fieldPlayer.$$hashKey}));

          // Make copies
          benchPlayer = angular.copy(benchPlayer);
          fieldPlayer = angular.copy(fieldPlayer);

          // Add them again
          $scope.bench.splice(benchIndex, 0, benchPlayer);
          $scope.starting[self.capitalize(type)].splice(fieldIndex, 0, fieldPlayer);

        }
        else {
          if (fieldPlayer.selections && fieldPlayer.selections.captain) {
            fieldPlayer.selections.captain = false;
            if (!benchPlayer.selections) {
              benchPlayer.selections = {};
            }
            benchPlayer.selections.captain = true;
          }

          // Remove from bench
          $scope.bench = _.without($scope.bench, benchPlayer);
          var replaceIndex = getObjIndexInArray($scope.starting[self.capitalize(type)], fieldPlayer);
          $scope.starting[self.capitalize(type)] = _.without($scope.starting[self.capitalize(type)], fieldPlayer);


          if (benchPlayer.players.position_id == 1) {
            $scope.starting.Goalkeeper.splice(replaceIndex, 0, benchPlayer);
          }
          if (benchPlayer.players.position_id == 2) {
            $scope.starting.Defender.splice(replaceIndex, 0, benchPlayer);
          }
          if (benchPlayer.players.position_id == 3) {
            $scope.starting.Midfielder.splice(replaceIndex, 0, benchPlayer);
          }
          if (benchPlayer.players.position_id == 4) {
            $scope.starting.Forward.splice(replaceIndex, 0, benchPlayer);
          }

          $scope.bench.unshift(fieldPlayer);
        }
      }

      $scope.dragElem = null;

      self.filterItem.teamPlayers = getStartingListForCaptain();

      if (self.classicCaptainSelection) {
        self.captain = getCaptainFromStartingList();
      } else {
        var listToCheck = getStartingList().concat($scope.bench);
        self.captain = getCaptain(listToCheck);
      }

      $scope.spreadStarting = [].concat(
        $scope.starting.Goalkeeper,
        $scope.starting.Defender,
        $scope.starting.Midfielder,
        $scope.starting.Forward
      );

      self.teamChanges += 1;


      if (!self.saveByButtonEnabled) {
        saveSelection();
      }

    };

    function onCaptainChange() {
      self.teamChanges += 1
    }

    function saveSelection() {
      var benchIds = $scope.bench.map(function (benchPlayer) {
        return benchPlayer.players.id;
      });

      var captainId = self.classicCaptainSelection ? self.captain.id : self.captain && self.captain.players && self.captain.players.id;

      var startingIds = $scope.starting.Goalkeeper.concat(
        $scope.starting.Defender,
        $scope.starting.Midfielder,
        $scope.starting.Forward
      ).map(function (player) {
        return player.players.id;
      });

      // Save selection to backend
      if (self.saveSelectioOnEdit) {
        teamService.saveFootballSelection(
          self.team.Team.Team.id,
          self.currentDisplayWeek.week_id,
          self.getPickIds(),
          captainId,
          self.team.Team.League.competition_feed
        )
          .then(function (res) {
            var teamId = self.team.Team.Team.id;
            self.teamChanges = 0;
            return teamService.changeFootbalCaptain(teamId, self.currentDisplayWeek.week_id, captainId);
          })
          .then(function (res) {
            $rootScope.$broadcast('team:refresh');
          });
      } else {
        teamService.saveFootballFormation(
          self.team.Team.Team.id,
          self.currentDisplayWeek.week_id,
          startingIds,
          benchIds,
          captainId,
          self.team.Team.League.competition_feed
        )
          .then(function (res) {
            self.teamChanges = 0;
            growl.addSuccessMessage(gettextCatalog.getString('Ploeg aangepast'));
            $rootScope.$broadcast('team:refresh');
          });
      }
    }

    function formatStarting(input) {
      const aggregatedPlayerValues = teamService.aggregateValuesForDuplicatedPlayers(input, self.captainFactor, self.captainWithTeamPoints, self.captainFactorByTeamResult);

      const getByPlayersByPositionId = function (positionId, starting) {
        const startingList = starting
          .filter(player => PlayerService.getPlayerProp(player, 'position_id') === positionId)

        if (!self.showOpponentInfo) {
          return startingList;
        } else {
          return startingList
            .map(player => Object.assign({}, player, {opponentInfo: getOpponentInfo(player)}));
        }
      }

      return {
        Goalkeeper: getByPlayersByPositionId(1, aggregatedPlayerValues),
        Defender: getByPlayersByPositionId(2, aggregatedPlayerValues),
        Midfielder: getByPlayersByPositionId(3, aggregatedPlayerValues),
        Forward: getByPlayersByPositionId(4, aggregatedPlayerValues)
      };
    }

    function getAllPlayersInList(starting) {
      return [].concat(starting[1], starting[2], starting[3], starting[4]);
    }

    function preparePlayersStructure(lineup) {
      $scope.starting = formatStarting(lineup);

      self.filterItem.teamPlayers = getStartingListForCaptain();

      if (self.classicCaptainSelection) {
        self.captain = getCaptainFromStartingList();
      } else {
        self.captain = getCaptain();
      }
    }

    function getCaptain(listToCheck) {
      var handler = function (player) {
        return player && player.selections && player.selections.captain;
      };

      if (listToCheck) {
        return listToCheck.find(handler);
      } else {
        return $scope.spreadStarting.find(handler);
      }
    }

    function isFirstDeadlineWeek() {
      return self.team && self.team.weeks.deadline_week === self.team.Team.Team.week_id;
    }

    function isInTeamEditsDeadlineWeek() {
      return self.team && (self.editableTeamWeeks.indexOf(self.team.weeks.deadline_week) !== -1);
    }

    function getStartingList() {
      var goalkeepers = $scope.starting['Goalkeeper'] || [];
      var defenders = $scope.starting['Defender'] || [];
      var midfielders = $scope.starting['Midfielder'] || [];
      var forwarders = $scope.starting['Forward'] || [];

      return goalkeepers.concat(defenders, midfielders, forwarders);
    }

    function getStartingListForCaptain() {
      var goalkeepers = $scope.starting['Goalkeeper'] || [];
      var defenders = $scope.starting['Defender'] || [];
      var midfielders = $scope.starting['Midfielder'] || [];
      var forwarders = $scope.starting['Forward'] || [];

      return goalkeepers.concat(defenders, midfielders, forwarders)
        .map(function (player) {
          return {
            id: PlayerService.getPlayerProp(player, 'id'),
            short: PlayerService.getPlayerProp(player, 'short'),
            captain: player.selections && player.selections.captain
          }
        });
    }

    function getCaptainFromStartingList() {
      return self.filterItem.teamPlayers
        .find(function (player) {
          return player.captain;
        });
    }

    function getOpponentInfo(player) {
      let playingHome = true;
      const playerClubInfostradaId = (player && player.Club && player.Club.infostrada_id) ||
        (player && player.clubs && player.clubs.infostrada_id);
      const playerMatch = self.matches.find(match => {
        const isHome = playerClubInfostradaId === match.n_HomeTeamID;
        const isAway = playerClubInfostradaId === match.n_AwayTeamID;

        if (isAway) {
          playingHome = false;
        }

        return isHome || isAway;
      });

      if (playerMatch) {
        const opponentClubInfostradaId = playingHome ? playerMatch.n_AwayTeamID : playerMatch.n_HomeTeamID;
        const opponentClub = self.clubs.find(club => club.Club.infostrada_id === opponentClubInfostradaId);
        return {
          club: opponentClub.Club.name,
          short: opponentClub.Club.short,
          homeAwayLabel: playingHome ? gettextCatalog.getString('home-label') : gettextCatalog.getString('away-label')
        }
      } else {
        return null;
      }
    }
  }
};