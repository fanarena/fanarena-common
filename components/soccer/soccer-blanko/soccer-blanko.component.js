import template from './soccer-blanko.component.tpl.html';

export default {
  template,
  bindings: {
    player: '=',
    team: '<',
    preventClick: '=',
    popoverClick: '=',
    enabled: '=',
    statesPrefix: '<',
    maxSameClubPlayers: '<',
    onKeep: '='
  },
  controller: function (RESOURCES, $rootScope, $state, $stateParams, playerService, growl, SPORTS_ASSETS,
                        PlayerService, teamService) {
    const vm = this;

    $rootScope.$on('popover:close', function (ev, args) {
      vm.player.blankoPopoverOpen = false;
    });

    vm.isPlayer = function () {
      return !!PlayerService.getPlayer(vm.player);
    };

    vm.getPlayerProp = function (prop) {
      return PlayerService.getPlayerProp(vm.player, prop);
    };

    vm.click = function () {
      $rootScope.$broadcast('popover:close');
      vm.player.blankoPopoverOpen = true;
    };

    $rootScope.$on('blanko:show', function (ev, args) {
      if(PlayerService.getPlayerProp(vm.player, 'id') === args.id) {
        vm.player.blankoPopoverOpen = true;
      }
    });

    vm.getBlankoClass = function () {
      const blanko = vm.player.selections.blanko;
      const isActive = vm.player.players.active;
      const transfersLeft = vm.team.Team.Team.transfers - vm.team.Team.Transfer.length;
      const allPlayers = vm.team.starting && vm.team.bench.concat(
        vm.team.starting[1],
        vm.team.starting[2],
        vm.team.starting[3],
        vm.team.starting[4]
      );
      const allPlayersWithoutTheCurrentPlayer = allPlayers.filter(player => PlayerService.getPlayerProp(player, 'id') !==
        PlayerService.getPlayerProp(vm.player, 'id'));

      var colleguesCount = allPlayersWithoutTheCurrentPlayer.filter(function (startingPlayer) {
        return PlayerService.getPlayerProp(startingPlayer, 'club_id') === PlayerService.getPlayerProp(vm.player, 'club_id');
      }).length;

      vm.canPlayInTheTeam = colleguesCount < vm.maxSameClubPlayers;

      let className = null;
      if (blanko === 1 && !isActive) {
        className = 'blanko-white';
      } else if (blanko === 1 && isActive && vm.canPlayInTheTeam) {
        className = 'blanko-green';
      } else if (blanko === 1 && isActive && !vm.canPlayInTheTeam) {
        className = 'blanko-orange';
      } else {
        className = 'blanko-white';
      }
      return className;
    };

    vm.keepIt = function () {
      teamService.blanko(vm.team.Team.Team.id, vm.player.players.id, vm.team.Team.Team.competition_feed)
        .then(function (result) {
          growl.addSuccessMessage(result.data.message);
          if(vm.onKeep) {
            vm.onKeep();
          }
          $rootScope.$broadcast('popover:close');
          $rootScope.$broadcast('team:refresh');
        })
    };

    vm.putItOnTransfersList = function () {
      if(vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-squadTransfers');
      } else {
        $state.go('squadTransfers');
      }
    };
  }
};