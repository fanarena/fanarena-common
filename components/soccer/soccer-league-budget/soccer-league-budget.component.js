import template from './soccer-league-budget.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    league: '<',
    globalLeagueIds: '<',
    enableLeagueSelection: '<',
    statesPrefix: '<',
    competitionFeed: '<',
    seasonId: '<',
    editWeeks: '<',
    deadlineWeeks: '<',
    showLeaveCompetitionButton: '<',
    hideMessage: '<'
  },
  controller: function ($location, $rootScope, gettextCatalog, Hashids, leagueService, teamService, $state,
                        localStorageService, analyticsService) {
    var vm = this;

    const currentUserTeamIds = $rootScope.teams.map(team => team.Team.id);

    vm.squad = this.squad;
    vm.league = this.league;
    vm.inviteURL = "";
    vm.processInviteURL = processInviteURL;
    vm.onKeyPress = onKeyPress;
    vm.joinLeague = joinLeague;
    vm.createLeague = createLeague;
    vm.leaveCompetition = leaveCompetition;
    vm.gameWeekChanged = gameWeekChanged;
    vm.pagination = {current: 1};
    vm.totalRecords = vm.league.total_records;
    vm.leagueStandings = vm.league.table.map(markOwnTeam);
    vm.onPageChange = onPageChange;
    vm.displayItems = 25;
    vm.editableTeamWeeks = vm.editWeeks || [];
    vm.goToTeamPoints = goToTeamPoints;
    vm.isGlobalLeague = vm.league && vm.league.League ?
      vm.globalLeagueIds.indexOf(vm.league.League.League.id) !== -1 :
      true;
    vm.user = localStorageService.get('user');

    vm.deadlineWeeksOptions = [{id: 0, label: gettextCatalog.getString('General')}]
      .concat(
        vm.deadlineWeeks.weeks.map(
          week => ({id: week.week_id, label: `${gettextCatalog.getString('Matchday')} ` + week.week_id}))
          .filter(item => item)
      )

    vm.currentDisplayWeek = vm.deadlineWeeksOptions[0];

    vm.firstWeek = (vm.squad.weeks.deadline_week === vm.squad.Team.Team.week_id) ||
      (vm.editableTeamWeeks.indexOf(vm.squad.weeks.deadline_week) !== -1);

    $rootScope.title = gettextCatalog.getString("League");

    if (vm.league && vm.league.League) {

      if (vm.statesPrefix) {
        vm.shareLink = "https://" + $location.host() + "/" + vm.statesPrefix + "/join/" + Hashids.encode(vm.league.League.League.id);
      } else {
        vm.shareLink = "https://" + $location.host() + "/join/" + Hashids.encode(vm.league.League.League.id);
      }
    }

    function markOwnTeam(team) {
      team.Team.belongsToCurrentUser = currentUserTeamIds.indexOf(team.Team.id) !== -1;
      return team;
    }

    function processInviteURL() {
      var inviteCode = getInviteCode(vm.inviteURL);
      if (inviteCode) {
        vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.inviteLeague = data.League.League;
            vm.squad.Team.Team.league_id = data.League.League.id;
          }, function (err) {
            vm.inviteLeague = null;
          });
      }
    }

    function joinLeague() {
      teamService.edit(vm.squad.Team.Team)
        .then(function () {
          return leagueService.get(vm.squad.Team.Team.league_id)
        })
        .then(function (result) {
          vm.league = result;
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');

          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {
              id: vm.squad.Team.Team.id,
              leagueId: vm.squad.Team.Team.league_id,
              '#': null
            });
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id, '#': null});
          }
        });
    }

    // TODO: @cosmin move bellow code into a service
    function getInviteCode(invite) {
      if (invite && invite.length && invite.indexOf('/') !== -1) {
        var inviteURLParts = invite.split('/');
        return inviteURLParts[inviteURLParts.length - 1];
      } else {
        return null;
      }
    }

    function onKeyPress(event, teamId) {
      if (event.keyCode === 13) {
        joinLeague();
      }
    }

    function leaveCompetition() {
      vm.squad.Team.Team.league_id = vm.globalLeagueIds.split(',').find(item => item);

      return teamService.edit(vm.squad.Team.Team)
        .then(result => {
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');
          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {
              id: vm.squad.Team.Team.id,
              leagueId: vm.squad.Team.Team.league_id,
              '#': null
            });
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id, '#': null});
          }
        })
    }

    function createLeague() {
      return leagueService.createLeagueWithExistingTeam(vm.squad.Team.Team.id, vm.newLeagueName, vm.competitionFeed)
        .then(newLeagueResult => {
          analyticsService.addToDataLayer({
            event: 'superclass_creation',
            user_id: vm.user.id,
            game: vm.statesPrefix
          })

          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');
          if (vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {
              id: vm.squad.Team.Team.id,
              leagueId: newLeagueResult.League.id,
              '#': null
            });
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: newLeagueResult.League.id, '#': null});
          }
        })
    }

    function getLeagueTeamsStats() {
      if (vm.currentDisplayWeek.id !== 0) {
        return leagueService.get(vm.squad.Team.Team.league_id, vm.currentDisplayWeek.id, vm.pagination.current);
      } else {
        return leagueService.get(vm.squad.Team.Team.league_id, null, vm.pagination.current);
      }
    }

    function gameWeekChanged() {
      vm.pagination.current = 1;
      return getLeagueTeamsStats()
        .then(result => {
          vm.leagueStandings = result.table.map(markOwnTeam);
          vm.totalRecords = result.total_records;
        })
    }

    function onPageChange(newPageNumber) {
      vm.pagination.current = newPageNumber;
      return getLeagueTeamsStats()
        .then(result => {
          vm.leagueStandings = result.table.map(markOwnTeam);
          vm.totalRecords = result.total_records;
        })
    }

    function goToTeamPoints(teamId) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-publicTeam', {id: teamId});
      } else {
        $state.go('publicTeam', {id: teamId});
      }
    }
  }
};