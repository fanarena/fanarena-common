import template from './soccer-new-game-join-league.component.tpl.html';

export default {
  template,
  bindings: {
    game: '<',
    leagueType: '<',
    increaseNewGameProgressStep: '=',
    filterType: '=',
    getLeagues: '=',
    joinExisting: '=',
    joinPlaySports: '=',
    competitions: '<',
    leagues: '<',

  },
  controller: function () {
    var vm = this;
  }
};