import template from './soccer-rules-budget.component.tpl.html';

export default {
  template,
  controller: function ($rootScope, gettextCatalog, appService) {
    var vm = this;

    vm.isFrench = appService.translateToFrance();
    $rootScope.title = gettextCatalog.getString("Rules");
  }
};