import template from './soccer-players-list-scout.component.tpl.html';

export default {
  template,
  bindings: {
    competitionInfo: '<',
    localAssets: '<',
    gameName: '<',
    hasContainerClass: '<',
    clubAssetsURL: '<',
    jerseyOnly: '<',
    showCoach: '<',
    hidePositionBackground: '<',
    simpleShirts: '<',
    deadlineInfo: '<'
  },
  controller: function (playerService, gameService, $rootScope, gettextCatalog, SPORTS_ASSETS, $scope,
                        storeActions, $stateParams, $location, $anchorScroll) {

    this.$onInit = function () {
      vm.loaded = false;
      vm.playersWithPoints = [];

      vm.totalRecords = 0;

      vm.filters = {
        competitionFeed: vm.competitionInfo.competitionFeed,
        seasonId: vm.competitionInfo.seasonId,
        pageNumber: 1,
        pageRecords: 10
      };
      vm.sorters = [];

      vm.seasons = [
        {id: 2016, label: `${gettextCatalog.getString('Season')} 2015/2016`},
        {id: 2017, label: `${gettextCatalog.getString('Season')} 2016/2017`},
        {id: 2018, label: `${gettextCatalog.getString('Season')} 2017/2018`},
        {id: 2019, label: `${gettextCatalog.getString('Season')} 2018/2019`},
      ];

      vm.selectedSeason = vm.seasons.find(season => season.id === vm.competitionInfo.seasonId);

      fetchClubs()
        .then(() => {
          if (vm.clubs.length) {
            vm.filterOptions.clubs = vm.clubs;
            vm.filterItem = {
              club: vm.clubs[0],
              budget: vm.filterOptions.budgets[0],
              selectedPosition: vm.positions[0]
            };
          }

          const positionsToShow = [1, 2, 3, 4];
          if (vm.showCoach) {
            positionsToShow.push(0);
          }
          return fetchPlayers()
        })
        .then(() => {
          initialize();
        })
    };

    function fetchPlayersData(weekId) {
      return fetchPlayers(weekId)
        .then(() => {
          initialize();
        })
    }

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    var vm = this;
    vm.selectedPlayer = null;
    vm.selectedPlayerInfo = null;
    vm.assetsURL = SPORTS_ASSETS.football.remote;
    vm.currentLanguage = gettextCatalog.getCurrentLanguage();

    if (vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    $rootScope.title = gettextCatalog.getString("Spelerslijst");

    vm.selectPlayer = selectPlayer;

    vm.isReverse = function (property) {
      return vm.sortFields.indexOf(`-${property}`) !== -1;
    };

    vm.filterOptions = {
      clubs: [],
      budgets: [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    vm.positions = [
      {id: -1, name: gettextCatalog.getString('All positions')},
      {id: 1, name: gettextCatalog.getString('Goalkeeper')},
      {id: 2, name: gettextCatalog.getString('Defender')},
      {id: 3, name: gettextCatalog.getString('Midfielder')},
      {id: 4, name: gettextCatalog.getString('Attacker')}
    ];

    if (vm.showCoach) {
      vm.positions.push({id: 0, name: gettextCatalog.getString('Coach')});
    }

    vm.filterItem = {
      budget: 999999999,
      text: 'Alle prijzen',
      selectedPosition: vm.positions[0]
    };

    vm.deadlineWeeksOptions = [{id: 0, label: gettextCatalog.getString('All Days')}]
      .concat(
        vm.deadlineInfo.weeks.map(
          week => ({id: week.week_id, label: `${gettextCatalog.getString('Matchday')} ` + week.week_id}))
          .filter(item => item)
      );

    vm.currentDisplayWeek = vm.deadlineWeeksOptions[0];

    vm.filterChanged = function () {
      vm.filters.pageNumber = 1;
      fetchPlayers();
    };
    vm.searchPlayer = {value: ""};
    vm.filterColumns = ['-total', '-playerValue'];


    function fetchPlayers() {
      const payload = Object.assign({}, vm.filters);

      payload.sorters = vm.filterColumns
        .map(sortColumn => {
          let column = sortColumn;
          let direction = 'ASC'
          if (sortColumn.indexOf('-') !== -1) {
            column = sortColumn.split('-')[1];
            direction = 'DESC';
          }
          return `${column} ${direction}`;
        })
        .join(',');

      if (vm.searchPlayer.value.length) {
        payload.name = vm.searchPlayer.value;
      }

      if (vm.currentDisplayWeek.id) {
        payload.weekId = vm.currentDisplayWeek.id;
      }

      if (vm.filterItem.club.Club.id) {
        payload.clubId = vm.filterItem.club.Club.id;
      }

      if (vm.filterItem.selectedPosition.id > 0) {
        payload.positionId = vm.filterItem.selectedPosition.id;
      }

      payload.playerValue = vm.filterItem.budget.budget;

      return playerService.stats(payload)
        .then(function (result) {
          vm.players = result.data;
          vm.totalRecords = result.totalRecords;
        })
    }

    function fetchClubs() {
      return gameService.getClubs(vm.competitionInfo.competitionFeed, vm.competitionInfo.seasonId)
        .then(function (clubs) {
          vm.clubs = [{
            Club: {
              id: 0,
              name: gettextCatalog.getString('All clubs') /*'Alle clubs'*/
            }
          }].concat(clubs);
        })
    }

    vm.removeFilterColumn = function (property) {
      vm.filterColumns = vm.filterColumns.filter(filterColumn => {
        const filterColumnWithoutPrefix = filterColumn.replace('-', '');
        if (filterColumnWithoutPrefix === property) {
          return false
        }
        return true;
      });
      vm.filters.pageNumber = 1;
      fetchPlayers();
    };

    vm.sortBy = function (propertyName) {
      let filterColumnIndex = null;

      const alreadyFiltered = vm.filterColumns
        .find((filterColumn, index) => {
          const filterColumnWithoutPrefix = filterColumn.replace('-', '');
          if (filterColumnWithoutPrefix === propertyName) {
            filterColumnIndex = index;
            return true;
          }
          return false;
        });

      if (alreadyFiltered) {
        if (alreadyFiltered.indexOf('-') !== -1) {
          vm.filterColumns[filterColumnIndex] = alreadyFiltered.replace('-', '');
        } else {
          vm.filterColumns[filterColumnIndex] = `-${alreadyFiltered}`;
        }
      } else {
        vm.filterColumns.push(propertyName);
      }
      fetchPlayers();
    };

    function initialize() {
      vm.loaded = true;
    }

    vm.getValueOject = function (value) {
      return angular.fromJson(value);
    };

    function selectPlayer(player) {
      vm.selectedPlayer = player;

      playerService.get(vm.selectedPlayer.Player.id, null, vm.competitionInfo.competitionFeed, vm.selectedSeason.id)
        .then(function (playerInfo) {
          vm.selectedPlayerInfo = playerInfo;

          if ($rootScope.isMobile) {
            $location.hash('playerStat');
            $anchorScroll();
          }
        });
    }

    vm.weeksChanged = function () {
      fetchPlayers();
    }

    vm.getPlayerStatus = function (info) {
      const playerObj = {Player: Object.assign({}, info, info.generalInfo)};
      return playerService.playerStatusEmojiName(playerObj);
    }

    vm.onPageChange = function (pageNumber) {
      vm.filters.pageNumber = pageNumber;
      fetchPlayers();
    }

    vm.onSearchPlayerChange = function () {
      fetchPlayers();
    }

    vm.getPositionNameById = function (positionId) {
      switch (positionId) {
        case 0:
          return gettextCatalog.getString('Coach');
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
        default:
          return gettextCatalog.getString('Goalkeeper');
      }
    };
  }
};