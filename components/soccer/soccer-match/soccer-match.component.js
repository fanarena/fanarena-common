import template from './soccer-match.component.tpl.html';

export default {
  template,
  bindings: {
    match: '<',
    showJerseys: '<',
    localAssets: '<',
    statesPrefix: '<',
    gameName: '<',
    statesPrefix: '<',
    hidePositionBackground: '<'
  },
  controller: function ($rootScope, SPORTS_ASSETS, gettextCatalog) {

    $rootScope.$broadcast('sidemenu:team:collapse');
    var self = this;

    self.home = { Goalkeeper: [], Defender: [], Midfielder: [], Forward: [], Bench: [] };
    self.away = { Goalkeeper: [], Defender: [], Midfielder: [], Forward: [], Bench: [] };

    var awayStatArray = (self.match.Away && self.match.Away.Stat) || [];
    var homeStatArray = (self.match.Home && self.match.Home.Stat) || [];

    self.homeTeamName = gettextCatalog.getString(self.match.Home.name);
    self.awayTeamName = gettextCatalog.getString(self.match.Away.name);

    var stat = awayStatArray.concat(homeStatArray);
    _.each(stat, function (el) {
      var addTo = self.home;
      if (el.Player.club_id === self.match.Away.id)
        addTo = self.away;

      // Transform the obj data to match player component
      el.positions = {
        id: el.Player.position_id
      };
      el.players = {
        id: el.Player.id,
        infostrada_id: el.Player.infostrada_id,
        short: el.Player.short
      };
      el.pts = {
        points: el.points,
        played: true,
        value: el.value
      };

      if (el.Player.club_id === self.match.Home.id)
        el.clubs = {
          infostrada_id: self.match.Home.infostrada_id,
          id: el.Player.club_id
        };
      else
        el.clubs = {
          infostrada_id: self.match.Away.infostrada_id,
          id: el.Player.club_id
        };

      // Did the player finish the game?
      if ((el && el.value) && el.value.out === el.value.time) {
        if (el.Player.position_id == 1)
          addTo.Goalkeeper.push(el);
        if (el.Player.position_id == 2)
          addTo.Defender.push(el);
        if (el.Player.position_id == 3)
          addTo.Midfielder.push(el);
        if (el.Player.position_id == 4)
          addTo.Forward.push(el);
      }
      else {
        addTo.Bench.push(el);
      }
    });
  }
};