import template from './soccer-new-game-league-budget.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    statesPrefix: '<'
  },
  controller: function ($stateParams, gameService, localStorageService, leagueService, $rootScope, growl, $state,
                        gettextCatalog, Hashids, $location) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString("League");

    vm.activeTab = 1;
    vm.inviteLeagueId = Hashids.decode(localStorageService.get('inviteLeagueId'))[0];
    vm.game = $stateParams.payload;

    vm.setActiveTab = setActiveTab;
    vm.saveTeam = saveTeam;
    vm.inviteCodeChanged = inviteCodeChanged;
    vm.joinInviteLeague = joinInviteLeague;
    vm.createLeague = createLeague;
    vm.joinLeague = joinLeague;

    activate();

    function setActiveTab(tab) {
      vm.activeTab = tab;
    }

    function goToDashboard() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-dashboard');
      } else {
        $state.go('dashboard');
      }
    }

    function goToNewGame() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-newGame');
      } else {
        $state.go('newGame');
      }
    }

    function saveTeam() {
      gameService.joinLeague(vm.game)
        .then(function (res) {
          if(res.error) {
            growl.addErrorMessage(res.error);
            goToNewGame();
          } else {
            growl.addSuccessMessage('Team created!');
            $rootScope.$broadcast('sidemenu:refresh');
            resetCache();
            goToDashboard();
          }
        });
    }

    function resetCache() {
      localStorageService.set('picksCacheCL', null);
    }

    // Actual join league
    function joinLeague() {
      gameService.joinLeague(vm.game).then(function (data) {
        // Refresh sidebar
        $rootScope.$broadcast('sidemenu:refresh');

        localStorageService.remove('inviteLeagueId');
        resetCache();
        goToDashboard();
      }, function (err) {
        if (err.message)
          growl.addErrorMessage(err.message);
      });
    };

    function inviteCodeChanged() {
      var inviteCode = getInviteCode(vm.invite);
      if (inviteCode) {
        vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.inviteLeague = data.League.League;
          }, function (err) {
            vm.inviteLeague = null;
            localStorageService.remove('inviteLeagueId');
          });
      }
    }

    function getInviteCode(invite) {
      if (invite && invite.length && invite.indexOf('/') !== -1) {
        var inviteURLParts = invite.split('/');
        return inviteURLParts[inviteURLParts.length - 1];
      } else {
        return null;
      }
    }

    if (vm.inviteLeagueId) {
      vm.activeTab = 2;
    }

    function joinInviteLeague() {
      //vm.frmTeamName.$setSubmitted();

      if (vm.game.teamName !== '') {
        vm.game.league = null;
        vm.game.league_id = vm.inviteLeague.id;
        vm.game.invite = vm.invite;
        vm.game.gameMode = {key: 'budget'};
        vm.joinLeague();
      }
      else {
        growl.addErrorMessage('Please choose a team name');
      }
    };


    // Create league
    vm.createLeagueName = '';
    vm.createLeagueDeadline = null;
    vm.dateTimeBeforeRender = leagueService.validateDeadlinePicker;
    function createLeague() {
      if (vm.frmTeamName) {
        vm.frmTeamName.$setSubmitted();
      }
      vm.frmCreateLeague.$setSubmitted();

      var valid = true;

      if (vm.createLeagueName === '') {
        growl.addErrorMessage('Please choose a league name');
        valid = false;
      }

      if (valid) {
        var deadline = moment(vm.createLeagueDeadline);
        deadline.utc();

        vm.game.league = {
          name: vm.createLeagueName,
          deadline: deadline.format('YYYY-MM-DD HH:mm:ss')
        };
        vm.game.gameMode = {
          key: 'budget'
        };

        gameService.createLeague(vm.game).then(function (data) {
          growl.addSuccessMessage('New game created');
          // Refresh sidebar
          $rootScope.$broadcast('sidemenu:refresh');
          resetCache();
          goToDashboard();
        });
      }
    }

    function activate() {
      if(!vm.game) {
        goToNewGame();
      }

      if (vm.inviteLeagueId) {
        vm.invite = 'https://' + $location.host() + '/join/' + localStorageService.get('inviteLeagueId');

        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.activeTab = 2;
            vm.inviteLeague = data.League.League;
          }, function (err) {
            vm.inviteLeague = null;
            localStorageService.remove('inviteLeagueId');
          });
      }
    }
  }
};