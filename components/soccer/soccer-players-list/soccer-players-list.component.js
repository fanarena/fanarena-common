import template from './soccer-players.list.component.tpl.html';

export default {
  template,
  bindings: {
    customFilter: '=',
    isPickable: '=',
    pick: '=',
    onPlayerlistItemClick: '=',
    players: '<',
    pagination: '<',
    hidePositionBackground: '<',
    localAssets: '<',
    assetsURL: '<',
    gameName: '<',
    showJerseys: '<',
    clubAssetsURL: '<',
    statesPrefix: '<'
  },
  controller: function (gettextCatalog, $state, playerService) {
    const vm = this;

    vm.getPositionNameById = function (positionId) {
      switch (positionId) {
        case 0:
          return gettextCatalog.getString('Coach');
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
        default:
          return gettextCatalog.getString('Goalkeeper');
      }
    };

    vm.onPlayerClick = function (player) {
      if (vm.onPlayerlistItemClick) {
        vm.onPlayerlistItemClick(player);
      }
    }

    vm.goToPlayer = function (player) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-publicPlayers', {subjectPlayerId: player.Player.id});
      } else {
        $state.go('publicPlayers', {subjectPlayerId: player.Player.id});
      }
    }
    vm.getPlayerStatus = function(player) {
      return playerService.playerStatusEmojiName(player);
    }
  }
};