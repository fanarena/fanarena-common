import template from './soccer-endwinner-widget.component.tpl.html';

export default {
  template,
  bindings: {
    previousYear: '<',
    data: '<',
    gameName: '<',
    localAssets: '<',
    jerseyOnly: '<',
    teamId: '<',
    seasonId: '<',
    competitionFeed: '<',
    week: '<',
    captainFactor: '<',
    captainWithTeamPoints: '<'
  },
  controller: function (playerService, teamService, $scope, gettextCatalog, SPORTS_ASSETS) {
    var vm = this;
    vm.title = gettextCatalog.getString("Eindwinnaar");

    vm.assetsURL = SPORTS_ASSETS.football.local;

    function chunk(arr, size) {
      var newArr = [];
      for (var i = 0; i < arr.length; i += size) {
        newArr.push(arr.slice(i, i + size));
      }
      return newArr;
    };

    function activate() {
      var rankingsPayload = {
        competition_feed: vm.competitionFeed,
        season_id: vm.seasonId,
        type: 'general',
        search_query: '',
        limit: 10,
        page: 1
      };

      playerService.getRankingsWithFilters(rankingsPayload)
        .then(function (result) {
          const winner = result.Teams.find(team => team.Team.rank === 1);
          if(winner) {
            return teamService.get(winner.Team.id, vm.week)
          } else {
            return null;
          }
        })
        .then(function (team) {
          if(team) {
            vm.leaderTeam = team;
            const factor = vm.captainFactor || 1;
            vm.chunkedData = teamService.aggregateValuesForDuplicatedPlayers(vm.leaderTeam.starting, factor, vm.captainWithTeamPoints);
            vm.teamPoints = vm.leaderTeam.Team.Weekstat.find(weekStat => weekStat.week_id === vm.week);
          }
        })
        .catch(function () {
          vm.teams = [];
        });
    };
    activate();
  }
};