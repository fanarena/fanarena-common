import template from './soccer-deadlines-budget-table.component.tpl.html';
import moment from 'moment';

export default {
  template,
  bindings: {
    weeks: '<',
    matchesPhases: '<',
    calendarWeekId: '<',
    onMatchClick: '=',
    config: '<'
  },
  controller: function (teamService, $rootScope, gettextCatalog) {
    var vm = this;
    
    vm.getLocalDate = getLocalDate;
    
    vm.defaultMatches = [null,
      `${gettextCatalog.getString("Group Phase")}  1`,
      `${gettextCatalog.getString("Group Phase")}  2`,
      `${gettextCatalog.getString("Group Phase")}  3`,
      gettextCatalog.getString("Round of 16"),
      gettextCatalog.getString("Quarter-finals"),
      gettextCatalog.getString("Semi-finals"),
      gettextCatalog.getString("Finals & 3'rd place"),
    ];

    vm.matches = vm.matchesPhases || vm.defaultMatches;

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('ddd DD MMMM HH:mm:ss');
    }
  }
};