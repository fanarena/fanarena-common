import template from './soccer-player.component.tpl.html';

export default {
  template,
  bindings: {
    player: '=',
    teamId: '<',
    captain: '<',
    preventClick: '=',
    popoverClick: '=',
    showTransfer: '<',
    showJerseys: '<',
    localAssets: '<',
    onRemove: '=',
    hidePositionBackground: '<'
  },
  controller: function (RESOURCES, $rootScope, $state, $stateParams, playerService, growl, SPORTS_ASSETS,
                        PlayerService) {
    var vm = this;
    vm.resources = RESOURCES;

    vm.assetsURL = SPORTS_ASSETS.football.remote;

    if (vm.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    $rootScope.$on('popover:close', function (ev, args) {
      vm.player.popoverOpen = false;
    });

    vm.isPlayer = function () {
      return !!PlayerService.getPlayer(vm.player);
    };

    vm.getPlayerProp = function (prop) {
      return PlayerService.getPlayerProp(vm.player, prop);
    };

    vm.isCaptain = function () {
      if (vm.captain !== undefined) {
        return vm.captain;
      } else {
        return vm.player && vm.player.selections && vm.player.selections.captain;
      }
    };

    vm.click = function () {
      // Show popover?
      if (!vm.popoverClick) {
        vm.goToProfile();
      }
      else {
        // Close all others
        $rootScope.$broadcast('popover:close');
        vm.player.popoverOpen = true;
      }
    };

    vm.hasRemove = function () {
      return !!vm.onRemove;
    };

    vm.remove = this.onRemove;

    vm.goToProfile = function () {
      if (vm.teamId !== undefined && !vm.preventClick) {
        $state.go('teamPlayer', {teamId: vm.teamId, playerId: vm.player.players.id || vm.player.Player.id});
      } else if (!vm.preventClick) {
        if (vm.player.hasOwnProperty('player_id'))
          $state.go('publicPlayer', {playerId: vm.player.player_id});
        else
          $state.go('publicPlayer', {playerId: vm.player.players.id});
      }
    };

    vm.transfer = function () {
      playerService.transfer(vm.player.picks.id, vm.player.picks.team_id).then(function (res) {
        growl.addSuccessMessage(vm.player.players.name + ' was transferred out!');
        $rootScope.$broadcast('bench:remove', {obj: vm.player});
      });
    };
  }
};