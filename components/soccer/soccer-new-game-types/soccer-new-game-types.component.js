import template from './soccer-new-game-types.component.tpl.html';

export default {
  template,
  bindings: {
    playNewGameWithFans: '=',
    createNewGameWithFriends: '=',
    activeTab: '<'
  },
  controller: function ($location, $anchorScroll) {
    var vm = this;

    vm.onPlaySportsClick = function () {
      vm.playNewGameWithFans();
      vm.goToBottom('playsports-info');
    };

    vm.onCreateNewGameWithFriends = function() {
      vm.createNewGameWithFriends();
      vm.goToBottom('custom-league-info');
    };

    vm.goToBottom = function (to) {
      $location.hash(to);
      $anchorScroll();
    }
  }
};