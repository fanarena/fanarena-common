import template from './soccer-new-game-budget.component.tpl.html';

export default {
  template,
  bindings: {
    localAssets: '<',
    hasContainerClass: '<',
    budgetValue: '<',
    cachePrefix: '<',
    gameName: '<',
    hidePositionBackground: '<',
    maxPlayersSameClub: '<',
    statesPrefix: '<',
    competitionInfo: '<',
    showJerseys: '<',
    firstScreenOnly: '<',
    labels: '<'
  },
  controller: function ($scope, $state, RESOURCES, playerService, growl, gameService,
                        localStorageService, $rootScope, gettextCatalog, SPORTS_ASSETS,
                        POSITIONSIDS, FootballPicker, storeActions, storeHelpers, $ngRedux,
                        analyticsService) {

    const vm = this;

    this.$onInit = function () {
      let disconnect = $ngRedux.connect(mapStateToParams, Object.assign({},
        storeActions.playersActions, storeActions.clubsActions))(this);
      $scope.$on('$destroy', disconnect);
      const clubs = this.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const players = this.clubs.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);

      if (!clubs) {
        storeHelpers.initClubs()
          .then(() => {
            if (!players) {
              return storeHelpers.initPlayers();
            } else {
              return this.players;
            }
          })
          .then(() => initialize());
      } else {
        initialize();
      }
    };

    vm.positions = [
      {id: -1, name: gettextCatalog.getString('All positions')},
      {id: 1, name: gettextCatalog.getString('Goalkeeper')},
      {id: 2, name: gettextCatalog.getString('Defender')},
      {id: 3, name: gettextCatalog.getString('Midfielder')},
      {id: 4, name: gettextCatalog.getString('Attacker')}
    ];

    vm.cacheKey = `${vm.cachePrefix}-picks-football`;

    const competitionsBudges = {
      'GWC': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'G11': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ],
      'CL': [
        {budget: 100, text: gettextCatalog.getString('All budgets')},
        {budget: 25, text: `${gettextCatalog.getString('Onder')} 25 ${gettextCatalog.getString('miljoen')}`},
        {budget: 20, text: `${gettextCatalog.getString('Onder')} 20 ${gettextCatalog.getString('miljoen')}`},
        {budget: 15, text: `${gettextCatalog.getString('Onder')} 15 ${gettextCatalog.getString('miljoen')}`},
        {budget: 10, text: `${gettextCatalog.getString('Onder')} 10 ${gettextCatalog.getString('miljoen')}`},
        {budget: 5, text: `${gettextCatalog.getString('Onder')} 5 ${gettextCatalog.getString('miljoen')}`}
      ]
    };

    vm.filterOptions = {
      budgets: competitionsBudges[vm.competitionInfo.competitionFeed]
    };
    vm.filterItem = {
      budget: vm.filterOptions.budgets[0],
      selectedPosition: vm.positions[0]
    };

    function mapStateToParams(state) {
      return {
        players: state.footballPlayers,
        clubs: state.footballClubs
      };
    }

    function initialize() {
      const clubsInfo = vm.clubs.find(clubInfo => clubInfo.competitionFeed === vm.competitionInfo.competitionFeed);
      const playersInfo = vm.players.find(playerInfo => playerInfo.competitionFeed === vm.competitionInfo.competitionFeed);

      vm.clubs = clubsInfo.clubs;
      vm.players = playersInfo.players;

      vm.filterOptions.clubs = vm.clubs;
      vm.filterItem.club = vm.clubs[0];

      vm.players = vm.players
        .map(player => {
          const playerClub = vm.clubs.find(club => player.Club.id === club.Club.id);
          return Object.assign({}, player, {Club: playerClub && playerClub.Club});
        })
        .filter(player => {
          return [1, 2, 3, 4].indexOf(player.Player.position_id) !== -1;
        });
    }

    vm.resources = RESOURCES;
    $rootScope.title = gettextCatalog.getString("Soccer new game");
    vm.starting = {
      Goalkeeper: [],
      Defender: [],
      Midfielder: [],
      Forward: []
    };

    vm.user = localStorageService.get('user');

    vm.budget = parseInt(vm.budgetValue);
    vm.bench = [{}, {}, {}, {}];
    vm.maxPositionPicks = {
      Goalkeeper: {
        min: 1,
        max: 1
      },
      Defender: {
        min: 3,
        max: 5
      },
      Midfielder: {
        min: 3,
        max: 5
      },
      Forward: {
        min: 1,
        max: 3
      }
    };

    vm.localAssets = this.localAssets;
    vm.assetsURL = SPORTS_ASSETS.football.remote;

    if (this.localAssets) {
      vm.assetsURL = SPORTS_ASSETS.football.local;
    }

    vm.captain = null;
    vm.translate = translate;

    var cachedPicks = localStorageService.get(vm.cacheKey);
    vm.possibleFormations = FootballPicker.getPossibleFormations();

    // reset any previously defined selections
    FootballPicker.set([], []);

    if (cachedPicks) {
      vm.starting = cachedPicks.starting;
      vm.bench = cachedPicks.bench;

      var pickerStarting = getStartingList().filter(function (startingPlayer) {
        return startingPlayer && startingPlayer.Player;
      });

      var pickerBench = cachedPicks.bench.filter(function (benchPlayer) {
        return benchPlayer && benchPlayer.Player;
      });

      FootballPicker.set(pickerStarting, pickerBench);
      vm.possibleFormations = FootballPicker.getPossibleFormations();


      if (vm.starting) {
        getPicksList().forEach(function (player) {
          if (player && player.Player && player.Player.value) {
            vm.budget -= player.Player.value;
          }
        });
      }
    }

    function getPicksList() {
      return vm.bench.concat(
        vm.starting.Goalkeeper,
        vm.starting.Defender,
        vm.starting.Midfielder,
        vm.starting.Forward
      );
    }

    vm.teamName = {
      value: ''
    };

    vm.filterChanged = function () {
      vm.pagination.current = 1;
    };

    vm.isCaptain = function (player) {
      return vm.captain && (vm.captain.Player.id === player.Player.id);
    };

    vm.getStartingList = getStartingList;

    function getStartingList() {
      return vm.starting['Goalkeeper'].concat(
        vm.starting['Defender'],
        vm.starting['Midfielder'],
        vm.starting['Forward']
      );
    };

    vm.searchPlayer = {value: ""};
    vm.customFilter = function (player) {
      // Check budget
      if (player && player.Player && player.Player.value > vm.filterItem.budget.budget) {
        return false;
      }
      // Check club
      else if (player && player.Club && vm.filterItem.club && vm.filterItem.club.Club && vm.filterItem.club.Club.id !== 0 && player.Club.id !== vm.filterItem.club.Club.id) {
        return false;
      }
      // Check position
      else if (player && player.Player && vm.filterItem.selectedPosition.id !== -1 && player.Player.position_id !== vm.filterItem.selectedPosition.id) {
        return false;
      }
      // Check name
      else if (vm.searchPlayer.value !== "" && player && player.Player && player.Player.name.toLowerCase().indexOf(vm.searchPlayer.value.toLowerCase()) == -1) {
        return false;
      } else {
        return true;
      }
    };

    // Players pagination
    vm.pagination = {
      current: 1
    };

    vm.pick = function (player) {
      if (playerService.breaksTheFootballClubLimit(player, getStartingList().concat(vm.bench), vm.maxPlayersSameClub)) {
        growl.addErrorMessage(`${gettextCatalog.getString('You are allowed to pick maximum')} ${vm.maxPlayersSameClub} ${gettextCatalog.getString('football players from the same team!')}`);
        return;
      }

      const pickResult = FootballPicker.pick(player);
      if (pickResult.picked) {
        vm.possibleFormations = pickResult.possibleFormations;
        vm.starting = {
          Goalkeeper: getPlayersByIds(pickResult.result.Goalkeeper),
          Defender: getPlayersByIds(pickResult.result.Defender),
          Midfielder: getPlayersByIds(pickResult.result.Midfielder),
          Forward: getPlayersByIds(pickResult.result.Forward)
        };

        vm.bench = getPlayersByIds(pickResult.result.Bench);
        vm.budget -= player.Player.value;
        updatePicksCache();
      }
    };

    function getPlayersByIds(playersIds) {
      return playersIds.map(function (playerId) {
        return vm.players.find(function (player) {
          return player.Player.id === playerId
        })
      });
    }

    vm.isPickable = function (player) {
      var pickable = true;

      if (playerService.breaksTheFootballClubLimit(player, getStartingList().concat(vm.bench), vm.maxPlayersSameClub)) {
        return false;
      }

      if (player.Player.value > vm.budget) {
        return false;
      }

      pickable = FootballPicker.canPick(player);

      return pickable;
    };

    vm.startingSelected = function () {
      var count = 0;
      var positions = Object.keys(vm.starting);

      positions.forEach(function (position) {
        vm.starting[position].forEach(function (player) {
          if (player && player.hasOwnProperty('Player')) {
            count++;
          }
        });
      });

      return count;
    };

    vm.benchSelected = function () {
      var count = 0;

      vm.bench.forEach(function (player) {
        if (player && player.hasOwnProperty('Player')) {
          count++;
        }
      });

      return count;
    };

    vm.playersSelected = function () {
      var count = 0;
      var positions = Object.keys(vm.starting);

      positions.forEach(function (position) {
        vm.starting[position].forEach(function (player) {
          if (player && player.hasOwnProperty('Player')) {
            count++;
          }
        });
      });

      vm.bench.forEach(function (benchPlayer) {
        if (benchPlayer && benchPlayer.hasOwnProperty('Player')) {
          count++;
        }
      });

      return count;
    };

    function removeByPosition(positionName, player) {
      var removeIndex = null;
      for (var index = 0; index <= vm.starting[positionName].length; index++) {
        if (
          vm.starting[positionName][index] &&
          vm.starting[positionName][index].Player &&
          vm.starting[positionName][index].Player.id === player.Player.id
        ) {
          removeIndex = index;
          break;
        }
      }


      setTimeout(function () {
        if (removeIndex !== -1) {
          vm.budget += player.Player.value;
        }

        vm.starting[positionName].splice(removeIndex, 1);
        var removeResult = FootballPicker.remove(player);
        if (removeResult) {
          vm.possibleFormations = removeResult.possibleFormations;
        }

        $scope.$digest();
      }, 0);
    }

    vm.playerRemove = function (player) {
      switch (player.Player.position_id) {
        case 1:
          removeByPosition('Goalkeeper', player);
          break;

        case 2:
          removeByPosition('Defender', player);
          break;

        case 3:
          removeByPosition('Midfielder', player);
          break;

        case 4:
          removeByPosition('Forward', player);
          break;
      }
      updatePicksCache();
    };

    vm.benchRemove = function (player, $event) {
      $event.preventDefault();
      vm.budget += player.Player.value;
      vm.bench.forEach(function (benchPlayer, index) {
        if (benchPlayer.Player && benchPlayer.Player.id === player.Player.id) {
          vm.bench[index] = {};

          var removeResult = FootballPicker.remove(player);
          if (removeResult) {
            vm.possibleFormations = removeResult.possibleFormations;
          }
        }
      });
      updatePicksCache();
    };

    vm.notEmpty = function (player) {
      if (player && player.hasOwnProperty('Player'))
        return true;
      else {
        return false;
      }
    };

    vm.getPositionNameById = function (positionId) {
      switch (positionId) {
        case 0:
          return gettextCatalog.getString('Coach');
        case 1:
          return gettextCatalog.getString('Goalkeeper');
        case 2:
          return gettextCatalog.getString('Defender');
        case 3:
          return gettextCatalog.getString('Midfielder');
        case 4:
          return gettextCatalog.getString('Attacker');
        default:
          return gettextCatalog.getString('Goalkeeper');
      }
    };

    vm.create = function () {
      var valid = true;

      if (!vm.teamName.value || (vm.teamName.value && !vm.teamName.value.length)) {
        growl.addErrorMessage(gettextCatalog.getString('You need to choose a team name!'));
        valid = false;
      }

      if (!vm.captain) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('You need to select the captain!'));
      }

      if (vm.playersSelected() < 15) {
        valid = false;
        growl.addErrorMessage(gettextCatalog.getString('You need 15 players in your team!'));
      }

      if (valid) {
        var picks = [];
        _.each(vm.getStartingList(), function (player) {
          picks.push(player.Player.id);
        });

        var benchIds = [];
        vm.bench.forEach(function (benchPlayer) {
          benchIds.push(benchPlayer.Player.id);
        });

        // Post new team
        var obj = {
          teamName: vm.teamName.value.replace(new RegExp(',', 'g'), ''),
          competition: {
            competition_feed: vm.competitionInfo.competitionFeed
          },
          gameMode: {
            key: 'budget'
          },
          captain: vm.captain.Player.id,
          bench: benchIds,
          picks: picks
        };

        if (vm.firstScreenOnly) {
          gameService.joinLeague(obj)
            .then(function (res) {
              if (res.error) {
                growl.addErrorMessage(res.error);
                if (vm.statesPrefix) {
                  $state.go(vm.statesPrefix + '-newGame');
                } else {
                  $state.go('newGame');
                }
              } else {
                growl.addSuccessMessage('Team created!');

                analyticsService.addToDataLayer({
                  event: 'team_creation',
                  game: vm.statesPrefix,
                  total_team_numbers: ($rootScope.teams && $rootScope.teams.length && ($rootScope.teams.length + 1)) || 1,
                  user_id: vm.user.id
                });

                $rootScope.$broadcast('sidemenu:refresh');
                localStorageService.set(vm.cacheKey, null);

                if (vm.statesPrefix) {
                  $state.go(vm.statesPrefix + '-dashboard', {subjectTeamInfo: res.team.Team});
                } else {
                  $state.go('dashboard');
                }
              }
            });
        } else {
          if (vm.statesPrefix) {
            $state.go(`${vm.statesPrefix}-soccerNewGameLeagueBudget`, {payload: obj});
          } else {
            $state.go('soccerNewGameLeagueBudget', {payload: obj});
          }
        }
      }
    };

    function translate(string) {
      return gettextCatalog.getString(string);
    }

    function updatePicksCache() {
      localStorageService.set(vm.cacheKey, {starting: vm.starting, bench: vm.bench});
    }
  }
};