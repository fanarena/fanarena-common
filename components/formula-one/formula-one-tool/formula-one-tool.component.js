import template from './formula-one-tool.component.tpl.html';

export default {
  template,
  bindings: {
    tool: '<',
    showShort: '<',
    onRemove: '=',
    showPoints: '<',
    toolLabel: '<',
    toolName: '<'
  },
  controller: function () {
    var vm = this;

    vm.tool = this.tool;
    vm.hasRemove = function() {
      return !!vm.onRemove;
    };

    vm.remove = this.onRemove;
  }
};