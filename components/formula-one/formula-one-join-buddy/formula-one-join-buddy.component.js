import template from './formula-one-join-buddy.component.tpl.html';

export default {
  template,
  controller: function (localStorageService, $state, $stateParams, NewGameCacheService, $rootScope, growl,
                        SportTypesValues) {
    if ($stateParams.inviteTeamId) {
      localStorageService.set('f1InviteBuddyTeamId', $stateParams.inviteTeamId);
    }

    if (localStorageService.get('user') === null) {
      $state.go('publicRegister');
    } else {
      console.log('$rootScope.hasAF1Team', $rootScope.hasAF1Team);

      if ($stateParams.inviteTeamId && !$rootScope.hasAF1Team) {
        NewGameCacheService.setGameProgress(2);
        NewGameCacheService.setSportType(SportTypesValues.FORMULA_ONE);
        $state.go('f1NewGame');
      } else {
        if ($rootScope.hasAF1Team) {
          $state.go('f1TeamDetails', {id: $rootScope.hasAF1Team.Team.id});
        } else {
          $state.go('dashboard');
        }
      }
    }
  }
};