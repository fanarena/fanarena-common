import template from './formula-one-transfers.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    assets: '<',
    clubs: '<',
    config: '<',
    deadlineWeeks: '<'
  },
  controller: function () {
    const vm = this;
    vm.team = this.team;
    vm.cyclists = this.cyclists;
    vm.clubs = this.clubs;
    vm.deadlineWeeks = this.deadlineWeeks;
    vm.transferWeek = {
      week_id: vm.team.Team.Team.week_id,
      isFirstWeek: vm.team.weeks.deadline_week === vm.team.Team.Team.week_id
    };
    vm.isFirstDeadlineWeek = function() {
      return vm.deadlineWeeks && vm.deadlineWeeks.week && vm.deadlineWeeks.week.deadline_week < 2;
    }
  }
};