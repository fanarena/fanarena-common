import template from './formula-one-match-info.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    match: '<',
    inviteBuddyLink: '<',
    buddyPoints: '<',
    buddyTeam: '<'
  },
  controller: function (UtilsService, $state, stageUtils, CHANNELS) {
    const vm = this;
    vm.matchWithInfo = null;
    vm.previousWinners = null;

    vm.stageIcon = stageIcon;
    vm.getLocalDate = UtilsService.getLocalDate;
    vm.getChannelIdByName = getChannelIdByName;
    vm.goToMatch = goToMatch;

    activate(vm.match);

    this.$onChanges = function (bindings) {
      if (bindings.match && bindings.match.currentValue && bindings.match.currentValue.channels) {
        activate(vm.match);
      }
    };

    function activate(match) {
      const matchesWithParsedJSONString = stageUtils.prepareStages([{matches: match}]);
      vm.matchWithInfo = matchesWithParsedJSONString[matchesWithParsedJSONString.length - 1];
      vm.previousWinners = getLastThreeWinners();
    }

    function getLastThreeWinners() {
      const ordered = vm.matchWithInfo.matches.historyObject.sort((first, second) => {
        if (first.year < second.year) {
          return 1
        } else if (first.year > second.year) {
          return -1;
        }
        return 0;
      });
      return ordered.slice(0, 3);
    }

    function goToMatch(match) {
      $state.go('f1Race', {race: match.matches.id});
    }

    function stageIcon(stage) {
      return `icon icon-${stage.matches.id}`;
    }

    function getChannelIdByName(name) {
      return CHANNELS[name];
    }
  }
};