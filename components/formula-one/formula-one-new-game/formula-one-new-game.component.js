import template from './formula-one-new-game.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    assets: '<',
    clubs: '<',
    config: '<'
  },
  controller: function ($state, RESOURCES, playerService, growl, gameService, localStorageService, $rootScope,
                        gettextCatalog, NewGameCacheService, $anchorScroll, $location, inviteService, Hashids,
                        teamService) {

    let vm = this;
    vm.editMode = !!vm.team;
    let cachedPicks = null;

    if (!vm.editMode) {
      cachedPicks = localStorageService.get('f1Picks');
    }

    vm.resources = RESOURCES;
    $rootScope.title = gettextCatalog.getString("New Game");
    vm.budget = vm.config.budgetValue;
    vm.searchPlayer = {value: ''};
    vm.starting = [{}, {}, {}, {}];
    vm.teamName = {value: ''};
    vm.pagination = {current: 1};
    vm.drivers = [];
    vm.engines = [];
    vm.chassis = [];
    vm.selectedItems = {engine: {}, chassis: {}};
    vm.itemsListActiveTab = 1;
    vm.inviteTeamId = Hashids.decode(localStorageService.get('f1InviteBuddyTeamId'))[0];
    vm.withBuddy = true;

    vm.assetsFilter = assetsFilter;
    vm.chassisFilter = chassisFilter;
    vm.engineFilter = engineFilter;
    vm.decreaseNewGameProgressStep = decreaseNewGameProgressStep;
    vm.isPickable = isPickable;
    vm.pickDriver = pickDriver;
    vm.driversSelected = driversSelected;
    vm.driverRemove = driverRemove;
    vm.notEmpty = notEmpty;
    vm.setItemsActiveTab = setItemsActiveTab;
    vm.onAssetClick = onAssetClick;
    vm.pickChassis = pickChassis;
    vm.chassisRemove = chassisRemove;
    vm.pickEngine = pickEngine;
    vm.engineRemove = engineRemove;
    vm.inviteCodeChanged = inviteCodeChanged;
    vm.edit = edit;
    vm.create = create;

    activate();

    function setItemsActiveTab(tabNumber) {
      vm.itemsListActiveTab = tabNumber;
      vm.pagination.current = 1;
      vm.searchPlayer.value = '';
    }

    function activate() {

      if (!vm.editMode) {

        if (NewGameCacheService.getProgress() === 1) {
          NewGameCacheService.resetSportType();
          NewGameCacheService.resetGameMode();
          NewGameCacheService.resetProgress();
          $state.go('faNewGame');
        }

        if (cachedPicks && cachedPicks.drivers) {
          vm.starting = cachedPicks.drivers.map(function (driver) {
            return driver ? driver : {};
          });
        }

        if (cachedPicks && cachedPicks.engine && cachedPicks.engine.Player) {
          vm.selectedItems.engine = cachedPicks.engine;
          vm.budget -= vm.selectedItems.engine.Player.value;
        }

        if (cachedPicks && cachedPicks.chassis && cachedPicks.chassis.Player) {
          vm.selectedItems.chassis = cachedPicks.chassis;
          vm.budget -= vm.selectedItems.chassis.Player.value;
        }

        if (vm.starting.length) {
          vm.starting.forEach(function (driver) {
            if (driver && driver.Player && driver.Player.value) {
              vm.budget -= driver.Player.value;
            }
          });
        }

        if (vm.inviteTeamId) {
          vm.invite = inviteService.getF1BuddyInviteLink(vm.inviteTeamId);

          teamService.getById(vm.inviteTeamId)
            .then(function (data) {
              vm.inviteTeam = data.data.Team;
            }, function (err) {
              vm.inviteTeam = null;
              localStorageService.remove('f1InviteBuddyTeamId');
            });
        }
      } else {
        vm.team.starting.forEach(function (driver) {
          if (driver && driver.Player && driver.Player.value) {
            vm.budget -= driver.Player.value;
          }
        });

        vm.teamName.value = vm.team.Team.Team.name;

        vm.starting = vm.team.starting.filter(asset => asset.Player.position_id === vm.config.driverPositionId);
        vm.selectedItems.engine = vm.team.starting.find(asset => asset.Player.position_id === vm.config.enginePositionId);
        vm.selectedItems.chassis = vm.team.starting.find(asset => asset.Player.position_id === vm.config.chassisPositionId);
      }

      vm.filterOptions = {
        clubs: vm.clubs,
        budgets: [
          {budget: 100, text: 'Budgetten'},
          {budget: 25, text: 'Onder 25 miljoen'},
          {budget: 20, text: 'Onder 20 miljoen'},
          {budget: 15, text: 'Onder 15 miljoen'},
          {budget: 10, text: 'Onder 10 miljoen'},
          {budget: 5, text: 'Onder 5 miljoen'}
        ]
      };
      vm.filterItem = {
        club: vm.clubs[0],
        budget: vm.filterOptions.budgets[0]
      };

      vm.drivers = vm.assets.filter(asset => asset.Player.position_id === vm.config.driverPositionId);
      vm.engines = vm.assets.filter(asset => asset.Player.position_id === vm.config.enginePositionId);
      vm.chassis = vm.assets.filter(asset => asset.Player.position_id === vm.config.chassisPositionId);
    }

    function chassisFilter(driver) {
      return driver.Player.value <= vm.filterItem.budget.budget;
    }

    function engineFilter(driver) {
      return driver.Player.value <= vm.filterItem.budget.budget;
    }

    function assetsFilter(driver) {
      // Check budget
      if (driver.Player.value > vm.filterItem.budget.budget) {
        return false;
      }

      // Check name
      else if (vm.searchPlayer.value !== '' && driver.Player.name.toLowerCase().indexOf(
          vm.searchPlayer.value.toLowerCase()) === -1) {
        return false;
      }

      return true;
    }

    function pickDriver(driver) {
      addToFirstAvailableSpot(driver, vm.starting);
      updateF1Cache();
      vm.budget -= driver.Player.value;
      goToLineup();
    }

    function goToLineup() {
      const hashCode = 'f1-lineup';
      /*if ($location.hash() !== hashCode) {
        $location.hash(hashCode);
      } else {
        $anchorScroll();
      }*/
    }

    function hasEmptySpots(arr) {
      let hasEmpty = false;
      arr.forEach(function (player) {
        if (player && !player.hasOwnProperty('Player')) {
          hasEmpty = true;
        }
      });
      return hasEmpty;
    }

    function addToFirstAvailableSpot(driver, arr) {
      let index = null;
      arr.forEach(function (player, i) {
        if (player && !player.hasOwnProperty('Player') && index === null) {
          index = i;
        }
      });
      arr[index] = driver;
    }


    function isPickable(asset) {
      let pickable = true;

      //checks specific for assets
      if (asset.Player.position_id === vm.config.driverPositionId) {
        const totalPickedSoFar = vm.starting.filter(item => item.Player).length;
        if (totalPickedSoFar >= 4) {
          return false;
        }
      }
      //engine checks
      if ((asset.Player.position_id === vm.config.enginePositionId) && vm.selectedItems.engine.Player) {
        return false
      }

      //chassis checks
      if ((asset.Player.position_id === vm.config.chassisPositionId) && vm.selectedItems.chassis.Player) {
        return false
      }

      //general checks
      if (asset.Player.value > vm.budget) {
        return false;
      }

      vm.starting.forEach(function (c) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === asset.Player.id) {
          pickable = false;
        }
      });

      return pickable;
    }

    function driversSelected() {
      let count = 0;

      vm.starting.forEach(function (driver) {
        if (driver && driver.hasOwnProperty('Player'))
          count++;
      });

      return count;
    }

    function driverRemove(driver, $event) {
      $event.stopPropagation();
      vm.budget += driver.Player.value;

      vm.starting.forEach(function (c, i) {
        if (c && c.hasOwnProperty('Player') && c.Player.id === driver.Player.id) {
          vm.starting[i] = {};

          if (i === 0) {
            let firstNext = null;

            vm.starting.forEach(function (c2, i2) {
              if (c2 && c2.hasOwnProperty('Player') && c2.Player.id !== driver.Player.id && firstNext === null) {
                firstNext = i2;
              }
            });

            if (firstNext !== null) {
              vm.starting[0] = vm.starting[firstNext];
              vm.starting[firstNext] = {};
              vm.frontman = vm.starting[0];
            }
          }
        }
      });

      updateF1Cache();
    }

    function notEmpty(driver) {
      return driver && driver.hasOwnProperty('Player');
    }

    function isValid() {
      let valid = true;

      const driversLength = vm.driversSelected();

      if (vm.teamName.value === '') {
        growl.addErrorMessage('You need to choose a team name!');
        vm.frmTeamName.$setSubmitted();
        vm.frmTeamName.$setValidity('required', false);
        valid = false;
      }

      if (!vm.selectedItems.engine.Player) {
        growl.addErrorMessage('You need to choose the engine!');
        vm.frmTeamName.$setSubmitted();
        vm.frmTeamName.$setValidity('required', false);
        valid = false;
      }

      if (!vm.selectedItems.chassis.Player) {
        growl.addErrorMessage('You need to choose the chassis!');
        vm.frmTeamName.$setSubmitted();
        vm.frmTeamName.$setValidity('required', false);
        valid = false;
      }

      if (driversLength < 4) {
        valid = false;
        growl.addErrorMessage('You need 4 drivers in your team!');
      }

      return valid;
    }

    function create() {
      let valid = isValid();

      if (valid) {
        const picks = vm.starting.map(driver => driver.Player.id);
        const payload = {
          teamName: vm.teamName.value,
          competition: {competition_feed: vm.config.competitionFeed},
          gameMode: {key: 'budget'},
          picks: picks.concat([vm.selectedItems.chassis.Player.id, vm.selectedItems.engine.Player.id])
        };

        gameService.joinLeague(payload)
          .then(newTeam => {
            if (vm.inviteTeam) {
              const buddiesIds = [newTeam.team.Team.id, vm.inviteTeam.Team.id];
              return inviteService.createBuddies({teamsIds: buddiesIds})
                .then(data => {
                  localStorageService.remove('f1InviteBuddyTeamId');
                  return newTeam;
                });
            } else {
              return newTeam;
            }
          })
          .then(function (res) {
            if (res.error) {
              growl.addErrorMessage(res.error);
              $state.go('dashboard');
            } else {
              growl.addSuccessMessage('Team created!');
              $rootScope.$broadcast('sidemenu:refresh');
              $state.go('f1League', {id: res.team.Team.id, leagueId: res.team.Team.league_id});
            }
            resetF1Cache();
          });
      }
    }

    function edit() {
      let valid = isValid();

      if (valid) {
        const picks = vm.starting.map(driver => driver.Player.id).concat([vm.selectedItems.chassis.Player.id, vm.selectedItems.engine.Player.id]);
        const payload = {
          teamId: vm.team.Team.Team.id,
          teamName: vm.teamName.value,
          week_id: vm.team.weeks.deadline_week,
          picks: picks,
          competition_feed: vm.config.competitionFeed
        };

        teamService.editF1Selection(payload)
          .then(newTeam => {
            if (vm.inviteTeam) {
              const buddiesIds = [vm.team.Team.Team.id, vm.inviteTeam.Team.id];
              return inviteService.createBuddies({teamsIds: buddiesIds})
                .then(data => {
                  return newTeam;
                });
            } else {
              return newTeam;
            }
          })
          .then(function (res) {
            if (res.error) {
              growl.addErrorMessage(res.error);
              $state.go('dashboard');
            } else {
              growl.addSuccessMessage('Team updated!');
              $rootScope.$broadcast('sidemenu:refresh');
              $state.go('f1TeamDetails', {id: res.team.Team.id});
            }
            resetF1Cache();
          });
      }
    }

    function resetF1Cache() {
      localStorageService.set('f1Picks', null);
      NewGameCacheService.resetSportType();
      NewGameCacheService.resetGameMode();
      NewGameCacheService.resetProgress();
    }

    function updateF1Cache() {
      localStorageService.set('f1Picks', {
        drivers: vm.starting,
        chassis: vm.selectedItems.chassis,
        engine: vm.selectedItems.engine
      });
    }

    function inviteCodeChanged() {
      const inviteCode = inviteService.getInviteCode(vm.invite);
      if (inviteCode) {
        vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
        teamService.getById(vm.inviteLeagueId)
          .then(function (data) {
            vm.inviteTeam = data.data.Team;
          }, function (err) {
            vm.inviteTeam = null;
            localStorageService.remove('f1InviteBuddyTeamId');
          });
      } else {
        vm.inviteTeam = null;
        localStorageService.remove('f1InviteBuddyTeamId');
      }
    }

    function onAssetClick(asset, hashCode) {
      if (asset && !asset.Player) {
        if (hashCode === 'chassis-list') {
          vm.itemsListActiveTab = 2;
        } else if (hashCode === 'engine-list') {
          vm.itemsListActiveTab = 3;
        } else {
          vm.itemsListActiveTab = 1;
        }

        /*if ($location.hash() !== hashCode) {
          $location.hash(hashCode);
        } else {
          $anchorScroll();
        }*/
      }
    }

    function decreaseNewGameProgressStep() {
      NewGameCacheService.resetSportType();
      NewGameCacheService.resetGameMode();
      NewGameCacheService.resetProgress();
      $state.go('faNewGame');
    }

    function pickChassis(chassis) {
      vm.budget -= chassis.Player.value;
      vm.selectedItems.chassis = chassis;
      updateF1Cache();
      goToLineup();
    }

    function chassisRemove(chassis, $event) {
      $event.stopPropagation();
      vm.budget += chassis.Player.value;
      vm.selectedItems.chassis = {};
      updateF1Cache();
    }

    function pickEngine(engine) {
      vm.budget -= engine.Player.value;
      vm.selectedItems.engine = engine;
      updateF1Cache();
      goToLineup();
    }

    function engineRemove(engine, $event) {
      $event.stopPropagation();
      vm.budget += engine.Player.value;
      vm.selectedItems.engine = {};
      updateF1Cache();
    }

  }
};