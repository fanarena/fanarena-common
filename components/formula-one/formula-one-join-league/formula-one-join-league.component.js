import template from './formula-one-join-league.component.tpl.html';

export default {
  template,
  controller: function (localStorageService, $state, $stateParams, NewGameCacheService, $rootScope, growl,
                        SportTypesValues) {
    if ($stateParams.inviteLeagueId)
      localStorageService.set('f1InviteLeagueId', $stateParams.inviteLeagueId);

    if (localStorageService.get('user') === null) {
      $state.go('publicRegister');
    }

    else {
      if ($stateParams.inviteLeagueId && !$rootScope.hasAF1Team) {
        NewGameCacheService.setGameProgress(2);
        NewGameCacheService.setSportType(SportTypesValues.FORMULA_ONE);
        $state.go('f1NewGame');
      } else {
        growl.addErrorMessage('U heeft reeds een veldritploeg!');
        $state.go('dashboard');
      }
    }
  }
};