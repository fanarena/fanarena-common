import template from './formula-one-race.component.tpl.html';

export default {
  template,
  bindings: {
    deadlineWeeks: '<'
  },
  controller: function ($rootScope, gettextCatalog, $stateParams, teamService) {
    const vm = this;
    vm.race = parseInt($stateParams.race);
    vm.stageIcon = stageIcon;

    function activate() {
      const found = vm.deadlineWeeks.stages.find(function (stage) {
        return stage.matches.id === vm.race;
      });

      if (found) {
        vm.history = JSON.parse(found.matches.history);
        vm.info = JSON.parse(found.matches.info)[0];
        vm.infoProps = Object.keys(vm.info);
        vm.stage = found;
      } else {
        vm.history = [];
        vm.info = {};
      }
    }

    activate();

    vm.isMobile = $rootScope.isMobile;

    function stageIcon(stage) {
      return `icon icon-${stage.matches.id}`;
    }

    $rootScope.title = gettextCatalog.getString("Race");
  }
};