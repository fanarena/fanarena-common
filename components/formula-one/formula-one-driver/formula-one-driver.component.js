import template from './formula-one-driver.component.tpl.html';

export default {
  template,
  bindings: {
    player: '<',
    showShort: '<',
    onRemove: '=',
    showPoints: '<'
  },
  controller: function () {
    var vm = this;

    vm.cyclist = this.player;
    vm.hasRemove = function() {
      return !!vm.onRemove;
    };

    vm.remove = this.onRemove;

    vm.getValueOject = function (value) {
      return angular.fromJson(value);
    };
  }
};