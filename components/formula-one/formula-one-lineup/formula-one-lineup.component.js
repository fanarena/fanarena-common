import template from './formula-one-lineup.component.tpl.html';

export default {
  template,
  bindings: {
    starting: '<',
    selectedItems: '<',
    blockTitle: '@'
  },
  controller: function ($window) {
    const vm = this;

    vm.goTo = goTo;

    function goTo(page) {
      $window.location.href = page;
    }

  }
};