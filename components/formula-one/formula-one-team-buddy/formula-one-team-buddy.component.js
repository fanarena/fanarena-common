import template from './formula-one-team-buddy.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<',
    deadlineWeeks: '<',
    config: '<'
  },
  controller: function ($state, RESOURCES, playerService, growl, gameService, $rootScope, gettextCatalog, teamService,
                        inviteService) {

    let vm = this;
    vm.resources = RESOURCES;
    $rootScope.title = gettextCatalog.getString("New Game");
    vm.starting = [{}, {}, {}, {}];
    vm.teamName = {value: ''};
    vm.pagination = {current: 1};
    vm.selectedItems = {engine: {}, chassis: {}};
    vm.matches = vm.deadlineWeeks;
    vm.itemsListActiveTab = 1;
    vm.inviteBuddyURL = inviteService.getF1BuddyInviteLink(vm.team.Team.Team.id);


    vm.hasBuddy = vm.team && vm.team.Team.Buddy && vm.team.Team.Buddy.id;
    vm.buddySelectedItems = {engine: {}, chassis: {}};
    vm.buddyTeam = null;
    vm.buddyAllPlayers = [];
    vm.buddyStarting = [];
    vm.buddyPoints = 0;

    activate();

    vm.setItemsActiveTab = setItemsActiveTab;

    function setItemsActiveTab(tabNumber) {
      vm.itemsListActiveTab = tabNumber;
    }

    function setInitialBuddyData(team) {
      vm.buddyTeam = team.Team.Buddy;
      vm.buddyAllPlayers = (team.Team.Team.Buddy && team.Team.Team.Buddy.starting) || [];

      if (vm.buddyAllPlayers.length) {
        let points = 0;
        vm.buddyAllPlayers.forEach(function (player) {
          if (player && player.Selection && player.Selection.points) {
            points += player.Selection.points;
          }
        });

        vm.buddyPoints = points;
      }
    }

    function activate() {
      setInitialBuddyData(vm.team);

      setCurrentFormationItems(vm.team.starting);

      if (vm.buddyTeam && vm.buddyTeam.id) {
        setCurrentBuddyFormationItems(vm.buddyAllPlayers);
      }

      vm.lastMatchInTheLeague = vm.matches.stages[vm.matches.stages.length - 1];
      vm.deadline_week_id = vm.matches.week.deadline_week;
      vm.week_id = vm.matches.week.display_week;
      vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
        return stage.matches.week_id === vm.week_id;
      }).matches;
    }

    function setCurrentFormationItems(starting) {
      vm.starting = starting.filter(player => player.Player.position_id === vm.config.driverPositionId);
      vm.selectedItems.engine = starting.find(player => player.Player.position_id === vm.config.enginePositionId);
      vm.selectedItems.chassis = starting.find(player => player.Player.position_id === vm.config.chassisPositionId);
    }

    function setCurrentBuddyFormationItems(starting) {
      vm.buddyStarting = starting.filter(player => player.Player.position_id === vm.config.driverPositionId);
      vm.buddySelectedItems.engine = starting.find(player => player.Player.position_id === vm.config.enginePositionId);
      vm.buddySelectedItems.chassis = starting.find(player => player.Player.position_id === vm.config.chassisPositionId);
    }

    vm.changePlayDate = function (type) {
      let valid = true;


      if (type === 'prev' && vm.currentDisplayWeek.week_id <= 1) {
        valid = false;
      }
      else if (type === 'next' && vm.currentDisplayWeek.week_id === vm.matches.weeks[vm.matches.weeks.length - 1]) {
        valid = false;
      }

      if (valid) {
        if (type === 'prev') {
          vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
            return stage.matches.week_id === vm.currentDisplayWeek.week_id - 1;
          }).matches;
        }
        else if (type === 'next') {
          vm.currentDisplayWeek = vm.matches.stages && vm.matches.stages.find(function (stage) {
            return stage.matches.week_id === vm.currentDisplayWeek.week_id + 1;
          }).matches;
        }

        updateFormationAndItems();
      }
    };

    function filterByCurrentWeek(match) {
      if (vm.currentDisplayWeek && vm.currentDisplayWeek.week_id) {
        return match.matches.week_id === vm.currentDisplayWeek.week_id;
      }
      return false;
    }

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('YYYY-MM-DD HH:mm:ss');
    }

    function getRaceProps(race) {
      return {
        date: getLocalDate(race.matches.date),
        feed_url: race.matches.feed_url,
        type: race.matches.type
      }
    }

    vm.getWeekRaces = function () {
      return vm.matches && vm.matches.stages
        .filter(filterByCurrentWeek)
        .map(getRaceProps);
    };

    function updateFormationAndItems() {
      vm.isFetching = true;
      teamService.get(vm.team.Team.Team.id, vm.currentDisplayWeek.week_id)
        .then(function (res) {
          setInitialBuddyData(res);

          setCurrentFormationItems(res.starting);

          if (vm.buddyTeam) {
            setCurrentBuddyFormationItems(vm.buddyAllPlayers);
          }

          vm.starting = res.starting;

          vm.currentWeekRaces = vm.getWeekRaces();
          vm.week_id = res.week;
          vm.isFetching = false;

        })
        .catch(function (error) {
          console.log('error', error);
        });
    }
  }
};