import FormulaOneNewGame from './formula-one-new-game/formula-one-new-game.component';
import FormulaOneDriver from './formula-one-driver/formula-one-driver.component';
import FormulaOneTool from './formula-one-tool/formula-one-tool.component';
import FormulaOneLeague from './formula-one-league/formula-one-league.component';
import FormulaOneJoinLeague from './formula-one-join-league/formula-one-join-league.component';
import FormulaOneTeam from './formula-one-team/formula-one-team.component';
import FormulaOneTeamDetails from './formula-one-team-details/formula-one-team-details.component';
import FormulaOneMatchInfo from './formula-one-match-info/formula-one-match-info.component';
import FormulaOneCalendar from './formula-one-calendar/formula-one-calendar.component';
import FormulaOneRace from './formula-one-race/formula-one-race.component';
import FormulaOneJoinBuddy from './formula-one-join-buddy/formula-one-join-buddy.component';
import FormulaOneTransfers from './formula-one-transfers/formula-one-transfers.component';
import FormulaOneTeamBuddy from './formula-one-team-buddy/formula-one-team-buddy.component';
import FormulaOneLineup from './formula-one-lineup/formula-one-lineup.component';

export {
  FormulaOneNewGame,
  FormulaOneDriver,
  FormulaOneTool,
  FormulaOneLeague,
  FormulaOneJoinLeague,
  FormulaOneTeam,
  FormulaOneTeamDetails,
  FormulaOneMatchInfo,
  FormulaOneCalendar,
  FormulaOneRace,
  FormulaOneJoinBuddy,
  FormulaOneTransfers,
  FormulaOneTeamBuddy,
  FormulaOneLineup
};