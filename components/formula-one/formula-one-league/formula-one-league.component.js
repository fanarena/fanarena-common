import template from './formula-one-league.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    league: '<'
  },
  controller: function ($location, $rootScope, gettextCatalog, Hashids, GLOBAL_LEAGUE_ID, leagueService,
                        teamService, growl, $state, CYCLING_PUBLIC_LEAGUES, APP_CONFIG) {
    var vm = this;
    vm.isGlobalLeague = isGlobalLeague;

    vm.squad = this.squad;
    vm.league = this.league;
    vm.inviteURL = "";
    vm.processInviteURL = processInviteURL;
    vm.onKeyPress = onKeyPress;
    vm.joinLeague = joinLeague;
    vm.deleteTeam = deleteTeam;

    $rootScope.title = gettextCatalog.getString("League");
    vm.shareLink = "https://" + $location.host() + "/join-f1/" + Hashids.encode(vm.league.League.League.id);

    function isGlobalLeague() {
      const globalLeagueIds = CYCLING_PUBLIC_LEAGUES
        .split(',')
        .map(globalLeagueId => parseInt(globalLeagueId));
      return globalLeagueIds.indexOf(vm.league.League.League.id) !== -1;
    }

    // Delete
    function deleteTeam() {
      teamService
        .delete(vm.squad.Team.Team.id).then(function (res) {
        growl.addSuccessMessage('Successfully deleted!');

        // Refresh sidebar
        $rootScope.$broadcast('sidemenu:refresh');

        $state.go('home');
      });
    };

    function processInviteURL() {
      var inviteCode = getInviteCode(vm.inviteURL);
      if (inviteCode) {
        vm.inviteLeagueId = Hashids.decode(inviteCode)[0];
        leagueService.get(vm.inviteLeagueId)
          .then(function (data) {
            vm.inviteLeague = data.League.League;
            vm.squad.Team.Team.league_id = data.League.League.id;
          }, function (err) {
            vm.inviteLeague = null;
          });
      }
    }

    function joinLeague() {
      teamService.edit(vm.squad.Team.Team)
        .then(function () {
          return leagueService.get(vm.squad.Team.Team.league_id)
        })
        .then(function(result){
          vm.league = result;
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');
        });
    }

    function getInviteCode(invite) {
      if (invite && invite.length && invite.indexOf('/') !== -1) {
        var inviteURLParts = invite.split('/');
        return inviteURLParts[inviteURLParts.length - 1];
      } else {
        return null;
      }
    }

    function onKeyPress(event, teamId) {
      if (event.keyCode === 13) {
        updateTeam(vm.editTeams[teamId]);
      }
    }
  }
};