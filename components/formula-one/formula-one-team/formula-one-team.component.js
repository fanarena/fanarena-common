import template from './formula-one-team.component.tpl.html';

export default {
  template,
  bindings: {
    team: '<'
  },
  controller: function ($scope, $rootScope, gettextCatalog) {
    $scope.starting = this.team.starting;
    $rootScope.title = gettextCatalog.getString("F1 Formation");
  }
};