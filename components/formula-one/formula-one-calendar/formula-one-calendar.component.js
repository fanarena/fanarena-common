import template from './formula-one-calendar.component.tpl.html';

export default {
  template,
  bindings: {
    displayWeek: '<',
    team: '<',
    deadlineWeeks: '<',
    showSelect: '<',
    hideTitle: '<',
    competitionFeed: '<'
  },
  controller: function ($scope, RESOURCES, teamService, gameService, $state, UtilsService, CHANNELS, stageUtils) {
    var vm = this;
    vm.resources = RESOURCES;
    vm.selectedCompetition = {competition_feed: getCompetitionFeed()};
    vm.stages = [];

    vm.months = [{id: 0, value: 0, name: 'Alle maanden'}];
    vm.types = [{id: 0, name: 'all-types', label: 'Alle types'}];

    vm.stageIcon = stageIcon;
    vm.isVisible = isVisible;
    vm.getDeadlineWeeks = getDeadlineWeeks;
    vm.getLocalDate = UtilsService.getLocalDate;
    $scope.selectedWeek = _.findWhere(vm.deadlineWeeks, {week_id: vm.displayWeek});
    vm.competitionChanged = competitionChanged;
    this.$doCheck = $doCheck;
    vm.goToMatch = goToMatch;
    vm.matchHasStarted = matchHasStarted;
    vm.getChannelIdByName = getChannelIdByName;
    vm.stageIcon = stageIcon;
    let previousDisplayWeekValue = vm.displayWeek;

    if (!vm.deadlineWeeks) {
      vm.getDeadlineWeeks();
    } else {
      initializeData(vm.deadlineWeeks);
    }

    function getChannelIdByName(name) {
      return CHANNELS[name];
    }

    function stageIcon(stage) {
      return `icon icon-${stage.matches.id}`;
    }

    function isVisible(stage) {
      if (vm.selectedMonth && vm.selectedMonth.value !== 0 && parseInt(moment(stage.matches.date).format("M")) !== vm.selectedMonth.value) {
        return false;
      } else if (vm.selectedType.id !== 0 && stage.matches.type !== vm.selectedType.name) {
        return false;
      } else {
        return true;
      }
    }

    function stageIcon(stage) {
      return `icon icon-${stage.matches.id}`;
    }

    function getCompetitionFeed() {
      return (vm.team && vm.team.Team.League.competition_feed) || vm.competitionFeed;
    }

    function initializeData(data) {
      vm.deadlineWeeks = data.weeks;
      vm.displayWeek = _.findWhere(vm.deadlineWeeks, {week_id: data.week.display_week});
      vm.stages = stageUtils.prepareStages(data.stages);
      buildMonthFilter(vm.stages);
      buildTypesFilter(vm.stages);

      if (vm.displayWeek) {
        vm.selectedMonth = vm.months.find(function (month) {
          return month.value === parseInt(moment(vm.displayWeek.date).format("M"));
        });
      } else {
        vm.selectedMonth = vm.months.find(function (month) {
          return month.value === parseInt(moment().format("M"));
        });
      }

      vm.selectedType = vm.types[0];
    }

    function getDeadlineWeeks() {
      const seasonId = (vm.team && vm.team.Team.League.season_id) || '2018';

      return teamService.getDeadlineWeeks(seasonId, vm.selectedCompetition.competition_feed)
        .then(function (data) {
          initializeData(data);
        });
    }

    function buildTypesFilter(stages) {
      stages.forEach(stage => {
        const alreadyAdded = vm.types.find(type => type.name === stage.matches.type);

        if (!alreadyAdded && stage.matches.type) {
          vm.types.push({
            id: vm.types.length,
            name: stage.matches.type,
            label: stage.matches.type
          })
        }
      })
    }

    function buildMonthFilter(stages) {
      stages.forEach(stage => {
        const monthNumber = parseInt(moment(stage.matches.date).format('M'));
        const monthLabel = moment(stage.matches.date).format('MMMM');
        const alreadyAdded = vm.months.find(month => month.value === monthNumber);

        if (!alreadyAdded) {
          vm.months.push({
            id: vm.months.length,
            value: monthNumber,
            name: monthLabel
          })
        }
      })
    }

    function competitionChanged() {
      vm.getDeadlineWeeks();
    }

    function $doCheck() {
      if (!vm.showSelect && previousDisplayWeekValue !== vm.displayWeek) {
        $scope.selectedWeek = _.findWhere(vm.deadlineWeeks, {week_id: vm.displayWeek});

        // Get new matches
        vm.getMatches(vm.displayWeek);
        previousDisplayWeekValue = vm.displayWeek;
      }
    }

    function goToMatch(match) {
      $state.go('f1Race', {race: match.matches.id});
    }

    function matchHasStarted(match) {
      const start = moment.utc(match.d_MatchStartTime);
      start.local();

      const now = moment();

      return now > start;
    }
  }
};