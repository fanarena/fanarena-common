import template from './budget-league-table.component.tpl.html';

export default {
  template,
  bindings: {
    squad: '<',
    league: '<',
    competitionFeed: '<',
    seasonId: '<',
    enableLeagueSelection: '<',
    isGlobalLeague: '<',
    globalLeagueIds: '<',
    statesPrefix: '<',
    firstWeek: '<'
  },
  controller: function ($location, $scope, $rootScope, gettextCatalog, Hashids, leagueService,
                        teamService, growl, $state, CYCLING_PUBLIC_LEAGUES, APP_CONFIG) {
    var vm = this;

    vm.squad = this.squad;
    vm.league = this.league;
    vm.competitionFeed = this.competitionFeed;
    vm.seasonId = this.seasonId;
    vm.shownLeague = null;
    vm.changeLeague = changeLeague;
    vm.joinLeague = joinLeague;
    vm.searchLeagueLabel = gettextCatalog.getString('Search football league to join');
    vm.globalLeaguesIdsList = vm.globalLeagueIds;
    vm.currentSelectedLeagueIsGlobal = true;
    vm.currentLeagueTeams = vm.isGlobalLeague ?  [] : vm.league.League.Team;

    if (!vm.showLeague && vm.league) {
      vm.showLeague = vm.league;
    }

    if (vm.enableLeagueSelection) {
      leagueService.getLeagues(vm.competitionFeed, vm.seasonId)
        .then(function (result) {
          vm.listOfLeagues = result;
          vm.listOfLeagues.push({id: vm.league.League.League.id, name: vm.league.League.League.name});

          vm.selected = {value: vm.listOfLeagues[vm.listOfLeagues.length - 1]};
          vm.shownLeague = vm.league;
        });
    }

    function joinLeague() {
      vm.squad.Team.Team.league_id = vm.selected.value.id;
      teamService.edit(vm.squad.Team.Team)
        .then(function () {
          return leagueService.get(vm.selected.value.id)
        })
        .then(function (result) {
          vm.league = result;
          vm.shownLeague = vm.league;
          $rootScope.$broadcast('sidemenu:refresh');
          $rootScope.$broadcast('team:refresh');

          if(vm.statesPrefix) {
            $state.go(vm.statesPrefix + '-league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id, '#': null});
          } else {
            $state.go('league', {id: vm.squad.Team.Team.id, leagueId: vm.squad.Team.Team.league_id, '#': null});
          }
        });
    }

    function changeLeague() {
      leagueService.get(vm.selected.value.id)
        .then(function (result) {
          vm.currentLeagueTeams = result.League.Team;
          vm.currentSelectedLeagueIsGlobal = vm.globalLeaguesIdsList.indexOf(vm.selected.value.id) !== -1
        })
    }
  }
};