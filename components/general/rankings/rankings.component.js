import template from './rankings.component.tpl.html';

export default {
  template,
  bindings: {
    hasContainerClass: '<',
    competitionInfo: '<',
    statesPrefix: '<',
    lastGameWeek: '<',
    competitionPeriodes: '<',
    selectedPeriod: '<'
  },
  controller: function (playerService, teamService, matchService, leagueService, $rootScope, gettextCatalog,
    RankingsService, LeaguesIds, gameService, $state, appGamesService, PRACTICE_WEEKS_PRIZES, VIP_INFO, PROMO_BANNER_URL) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString('Klassement');

    vm.setActiveTab = setActiveTab;
    vm.gameDaysChanged = gameDaysChanged;
    vm.onSearchButtonClick = onSearchButtonClick;
    vm.onPageChange = onPageChange;
    vm.getPrize = getPrize;
    vm.hasCorrectLeague = hasCorrectLeague;
    vm.isDataLoaded = isDataLoaded;
    vm.goToPostcode = goToPostcode;
    vm.periodeChanged = periodeChanged;
    vm.goToTeam = goToTeam;
    vm.getLeaguePoints = getLeaguePoints;
    vm.isLoggedIn = $rootScope.user && $rootScope.user.id;
    vm.loading = true;

    vm.promoBannerURL = PROMO_BANNER_URL[vm.competitionInfo.competitionFeed];
    vm.promoBannerImage = `/images/promo-wide-banner-${vm.competitionInfo.competitionFeed}.jpg`;

    vm.searchPlayer = { value: '' };
    vm.deadlineWeeks = null;
    vm.deadlineWeeksIds = [];
    vm.pagination = { current: 1 };
    vm.prizesLists = [];
    vm.totalRecords = 0;
    vm.activeTab = 1;
    vm.displayTypes = {
      GIRO: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'sprint',
        6: 'mountain',
        7: 'peloton-week',
        9: 'vip',
      },
      GWC: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        8: 'periode'
      },
      TOUR: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'sprint',
        6: 'mountain',
        7: 'peloton-week',
      },
      VUEL: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'sprint',
        6: 'mountain',
        7: 'peloton-week',
      },
      GKLAS: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'de-dubbel',
      },
      KLAS: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'sprint',
        6: 'mountain',
        7: 'peloton-week',
      },
      GCX: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        5: 'sprint',
        6: 'mountain',
        7: 'peloton-week',
      },
      G11: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        8: 'periode'
      },
      CL: {
        1: 'general',
        2: 'week',
        3: 'peloton',
        4: 'post_code',
        8: 'periode'
      }
    };

    vm.footballFeeds = ['GWC', 'G11', 'CL'];
    vm.customLeagueLabel = gettextCatalog.getString('Cycling league');
    vm.sprintLabel = 'Sprint';
    vm.bergLabel = 'Berg';
    vm.customPlayerLabel = gettextCatalog.getString('Player');
    if (vm.footballFeeds.indexOf(vm.competitionInfo.competitionFeed) !== -1) {
      vm.customLeagueLabel = gettextCatalog.getString('Football league');
      vm.customPlayerLabel = gettextCatalog.getString('Football player');
    }

    if (vm.competitionInfo.competitionFeed === 'GCX') {
      vm.bergLabel = gettextCatalog.getString('Superprestige');
      vm.sprintLabel = gettextCatalog.getString('Wereldbeker');
    }

    if (vm.competitionInfo.competitionFeed === 'GKLAS') {
      vm.sprintLabel = gettextCatalog.getString('De Dubbel');
    }

    vm.displayItems = 10;
    vm.defaultWeeksFilter = { id: null, label: gettextCatalog.getString('All Days') };

    vm.periodes = vm.competitionPeriodes || [
      { id: 1, label: gettextCatalog.getString('Group Stage') },
      { id: 2, label: gettextCatalog.getString('Knockout stage') }
    ];

    if (vm.selectedPeriod) {
      vm.defaultPeriode = vm.periodes.find(periode => periode.id === vm.selectedPeriod);
    } else {
      vm.defaultPeriode = vm.periodes.find(periode => periode.id === 1);
    }

    vm.filterItem = { gameDays: null, periode: vm.defaultPeriode };
    vm.subscriptions = 0;

    RankingsService.getPrizesLists(vm.competitionInfo.competitionFeed)
      .then(prizesData => {
        vm.prizesLists = prizesData;
        activate();
      });


    function activate() {
      vm.loading = true;

      if (vm.activeTab < 9) {
        var payload = {
          competition_feed: vm.competitionInfo.competitionFeed,
          season_id: vm.competitionInfo.seasonId,
          type: vm.displayTypes[vm.competitionInfo.competitionFeed][vm.activeTab],
          search_query: vm.searchPlayer.value,
          limit: vm.displayItems,
          page: vm.pagination.current
        };

        if (vm.reset === true) {
          payload.page = 1;
          vm.reset = false;
        }

        if (vm.activeTab === 2 || vm.activeTab === 3) {
          if (vm.filterItem && vm.filterItem.gameDays && vm.filterItem.gameDays.id) {
            payload.week = vm.filterItem.gameDays.id;
          }
        }

        if (vm.activeTab === 8) {
          payload.periode = vm.filterItem.periode.id;
        }

        playerService.getRankingsWithFilters(payload)
          .then(function (result) {
            var rank = vm.pagination.current === 1 ? vm.pagination.current : ((vm.pagination.current - 1) * vm.displayItems) + 1;
            vm.totalRecords = result.total_records;
            vm.teams = result.Teams;
            vm.loading = false;
            angular.forEach(vm.teams, function (obj, i) {
              obj.Local = { rank: rank };
              if (vm.activeTab === 1 || vm.activeTab === 2 || vm.activeTab === 5 || vm.activeTab === 6 || vm.activeTab === 8) {
                if (obj && obj.User) {
                  obj.User.full_name = obj.User.first_name + ' ' + obj.User.last_name;
                }
              }
              rank++;
            });
          })
          .catch(function () {
            vm.teams = [];
            vm.loading = false;
          });

        if (!vm.deadlineWeeks) {
          teamService.getDeadlineWeeks(vm.competitionInfo.seasonId, vm.competitionInfo.competitionFeed)
            .then(function (deadlineWeeks) {
              vm.deadlineWeeks = deadlineWeeks;
              vm.deadlineWeeksIds = deadlineWeeks.weeks
                .map(function (week) {
                  if (week.week_id < vm.lastGameWeek) {
                    return { id: week.week_id, label: gettextCatalog.getString('Matchday') + ' ' + week.week_id };
                  } else {
                    return null;
                  }
                })
                .filter(item => item);

              vm.filterItem.gameDays = vm.deadlineWeeksIds.find(function (week) {
                if ((vm.deadlineWeeks.week.deadline_week > vm.lastGameWeek) && vm.lastGameWeek) {
                  return week.id === (vm.lastGameWeek - 1);
                } else {
                  return week.id === vm.deadlineWeeks.week.display_week;
                }
              });

            });
        }
      } else {
        const userId = $rootScope.user && $rootScope.user.id;
        let vipPlayerIds = (VIP_INFO[vm.competitionInfo.competitionFeed] && VIP_INFO[vm.competitionInfo.competitionFeed].userIds) || [];
        const vipLeagueId = VIP_INFO[vm.competitionInfo.competitionFeed] && VIP_INFO[vm.competitionInfo.competitionFeed].leagueId;
        const weekId = vm.filterItem && vm.filterItem.gameDays && vm.filterItem.gameDays.id;

        if (userId) {
          const userTeams = (
            $rootScope.teams && $rootScope.teams.filter(team =>
              team &&
              team.League &&
              team.League.Competition &&
              (
                team.League.Competition.competition_feed === vm.competitionInfo.competitionFeed &&
                team.League.Competition.season_id === vm.competitionInfo.seasonId
              ) &&
              team.Team.paid
            )
          ) || [];
          if (userTeams.length) {
            vipPlayerIds = vipPlayerIds.concat(userTeams.map(item => item.Team.id));
          }
        }


        leagueService.getByIdWithRankings(vipLeagueId, vipPlayerIds, weekId)
          .then(result => {
            vm.teams = result.data.teams;
            vm.loading = false;
          })
          .catch(() => {
            vm.teams = [];
            vm.loading = false;
          })
      }
    }

    function setActiveTab(tabNumber) {
      vm.activeTab = tabNumber;
      vm.pagination.current = 1;
      vm.searchPlayer.value = '';

      if (tabNumber === 3 || tabNumber === 9) {

        var hasDefault = vm.deadlineWeeksIds.find(function (deadlineWeek) {
          return deadlineWeek.id === vm.defaultWeeksFilter.id;
        });

        if (!hasDefault) {
          vm.deadlineWeeksIds.unshift(vm.defaultWeeksFilter);
        }

        vm.filterItem.gameDays = vm.defaultWeeksFilter;
      } else {
        vm.deadlineWeeksIds = vm.deadlineWeeksIds.filter(function (deadlineWeek) {
          return deadlineWeek.id !== vm.defaultWeeksFilter.id;
        });

        vm.filterItem.gameDays = vm.filterItem.gameDays = vm.deadlineWeeksIds.find(function (week) {
          if ((vm.deadlineWeeks.week.deadline_week > vm.lastGameWeek) && vm.lastGameWeek) {
            return week.id === (vm.lastGameWeek - 1);
          } else {
            return week.id === vm.deadlineWeeks.week.display_week;
          }
        });
      }

      activate();
    }

    function onSearchButtonClick() {
      vm.reset = true;
      activate(vm.reset);
    }

    function onPageChange(newPageNumber) {
      vm.pagination.current = newPageNumber;
      activate();
    }

    function gameDaysChanged() {
      activate();
    }

    function getPrize(type, rank) {
      const foundPrize = vm.prizesLists && vm.prizesLists[type] && vm.prizesLists[type].find(function (rankObject) {
        return rankObject.ranks.indexOf(rank) !== -1;
      });

      const hasTempPrize = vm.deadlineWeeks && PRACTICE_WEEKS_PRIZES[vm.competitionInfo.competitionFeed]
        .weeks.find(week => week === vm.deadlineWeeks.week.deadline_week);

      if (type === 'general' &&
        rank === 1 &&
        vm.deadlineWeeks &&
        hasTempPrize) {
        return { text: PRACTICE_WEEKS_PRIZES[vm.competitionInfo.competitionFeed].prize || '' }

      } else {
        return foundPrize || { text: '' };
      }
    }

    function hasCorrectLeague(stat) {
      return true;
    }

    function isDataLoaded() {
      return !(vm.filterItem.gameDays === null || vm.filterItem.gameDays === undefined);
    }

    function goToPostcode(stat) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-rankingsPostcode', { postcode: stat.User.post_code });
      } else {
        $state.go('rankingsPostcode', { postcode: stat.User.post_code });
      }
    }

    function periodeChanged() {
      activate();
    }

    function goToTeam(teamId) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-publicTeam', { id: teamId });
      } else {
        $state.go('publicTeam', { id: teamId });
      }
    }

    function getLeaguePoints(stat) {
      if (vm.competitionInfo.competitionFeed === 'G11' && vm.filterItem && vm.filterItem.gameDays && !vm.filterItem.gameDays.id) {
        return stat && stat.League && stat.League.total_points;
      } else {
        return stat && stat['0'] && stat['0'].total_sum;
      }
    }
  }
};
