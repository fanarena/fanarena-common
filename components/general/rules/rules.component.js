import template from './rules.component.tpl.html';

export default {
  bindings: {
    hasContainerClass: '<',
    competitionFeed: '<',
    hideStructureClasses: '<'
  },
  template,
  controller: function ($rootScope, gettextCatalog, $http, API) {
    var vm = this;
    vm.rules = [];
    vm.loaded = false;
    $rootScope.title = gettextCatalog.getString("Reglement");
    
    activate();

    function activate() {
      return $http({ method: 'GET', url: `${API}rules?competitionFeed=${vm.competitionFeed}` })
        .then(result => {
          vm.loaded = true;
          vm.rules = [result.data]
            .map(ruleData => {
              const translatedRule = ruleData.translations.find(
                rule => rule.languageCode === gettextCatalog.getCurrentLanguage()
              );
              return translatedRule && translatedRule.body;
            })
        });
    }
  }
};