import template from './dashboard.component.tpl.html';

export default {
  bindings: {
    competitionFeed: '<',
    seasonId: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    deadlineWeeks: '<',
    promoWeeks: '<',
    endWeek: '<'
  },
  template,
  controller: function (teamService, userService, $window, $rootScope, $state, growl, gettextCatalog, Hashids,
                        leagueService, $location, $sce, $stateParams) {
    var vm = this;
    vm.getUnpaidTeams = getUnpaidTeams;
    vm.removeTeam = removeTeam;
    vm.getTeamPoints = getTeamPoints;
    vm.getTeamRank = getTeamRank;
    vm.editTeam = editTeam;
    vm.goToLeague = goToLeague;
    vm.updateTeam = updateTeam;
    vm.cancelEditTeam = cancelEditTeam;
    vm.cloneTeam = cloneTeam;
    vm.onKeyPress = onKeyPress;
    $rootScope.title = gettextCatalog.getString("Dashboard");
    vm.editTeams = {};
    vm.centsForATeam = 900;
    vm.getPayingTeamNames = getPayingTeamNames;
    vm.selectedTeamsForPaying = selectedTeamsForPaying;
    vm.goToTeam = goToTeam;
    vm.checkTeamName = checkTeamName;
    vm.goToNewTeam = goToNewTeam;
    vm.totalUnpaidTeams = 0;
    vm.goToPayments = goToPayments;
    
    vm.removeTeamConfirmationMessage = gettextCatalog.getString('Are you sure that you want to remove');
    vm.cloneTeamConfirmationMessage = gettextCatalog.getString('Are you sure that you want to clone');
    vm.subjectTeamInfo = $stateParams.subjectTeamInfo;
    vm.customLeagueLabel = gettextCatalog.getString('Cycling league');
    vm.footballFeeds = ['GWC', 'G11', 'CL'];
    vm.showNewGameButton = vm.deadlineWeeks.deadline_week && vm.deadlineWeeks.deadline_week < vm.endWeek;
    
    if (vm.footballFeeds.indexOf(vm.competitionFeed) !== -1) {
      vm.customLeagueLabel = gettextCatalog.getString('Football league');
      vm.centsForATeam = 700;
    }

    const promoWeeksIds = vm.promoWeeks || [];

    vm.showPromo = promoWeeksIds.indexOf(vm.deadlineWeeks.deadline_week) !== -1;
    vm.showPayment = vm.deadlineWeeks.deadline_week < vm.endWeek && vm.deadlineWeeks.display_week <= vm.deadlineWeeks.deadline_week;

    activate();

    function getTeams() {
      return teamService.getAll()
        .then(function (teams) {
          vm.teams = teams
            .filter(filterCycling)
            .map(team => Object.assign({}, team, {pay: true}));

          vm.payingTeams = vm.teams
            .filter(team => !parseInt(team.Team.paid))
            .map(team => ({id: team.Team.id, name: team.Team.name, pay: true}));

          $location.search({});
        });
    }

    function goToTeam(team) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-squadTeam', {id: team.Team.id});
      } else {
        $state.go('squadTeam', {id: team.Team.id});
      }
    }

    function goToNewTeam() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-newGame');
      } else {
        $state.go('newGame');
      }
    }

    function selectedTeamsForPaying() {
      return vm.teams.filter(payingTeam => payingTeam.pay).length;
    }

    function getPayingTeamNames() {
      return vm.getUnpaidTeams().map(team => team.Team.name).join(', ');
    }

    function activate() {
      const queryParams = $location.search();
      getTeams();
    }

    function filterCycling(team) {
      return team['League'].competition_feed === vm.competitionFeed ||
        team['Team'].competition_feed === vm.competitionFeed;
    }

    function getUnpaidTeams() {
      return vm.teams.filter(function (team) {
        return !parseInt(team.Team.paid) && team.pay;
      });
    }

    function removeTeam(team) {
      teamService.delete(team.Team.id).then(function (res) {
        growl.addSuccessMessage(gettextCatalog.getString('Successfully deleted!'));
        $rootScope.$broadcast('sidemenu:refresh');
        activate();
      });
    }

    function onKeyPress(event, teamId) {
      if (event.keyCode === 13) {
        updateTeam(vm.editTeams[teamId]);
      }
    }

    function getTeamPoints(team) {
      var points = team.Team.points; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].points;
      if (!points) {
        return 0;
      }
      return points;
    }

    function getTeamRank(team) {
      var rank = team.Team.rank; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].rank;
      if (!rank) {
        return '-';
      }
      return rank;
    }

    function editTeam(team) {
      vm.editTeams[team.Team.id] = Object.assign({}, team.Team, {inviteURL: ''});
    }

    function checkTeamName(teamName) {
      if (teamName[teamName.length - 1] === ',') return teamName.substr(0, teamName.length - 1);
      return teamName;
    }

    function cancelEditTeam(team) {
      vm.editTeams[team.id] = null;
    }

    function updateTeam(team) {
      teamService.edit(team)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Team successfully modified!'));
          $rootScope.$broadcast('sidemenu:refresh');
          cancelEditTeam(team);
          activate();
        });
    }

    function cloneTeam(team) {
      teamService.clone(team.Team.id, vm.competitionFeed)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Successfully copied!'));
          $rootScope.$broadcast('sidemenu:refresh');
          activate();
        });
    }

    function getTotal() {
      return (getUnpaidTeams().length - getTeamsPaidWithCoupons()) * 9;
    }
    
    function goToPayments() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-payments');
      } else {
        $state.go('payments');
      }
    }

    function goToLeague() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-league', {id: vm.subjectTeamInfo.id, leagueId: vm.subjectTeamInfo.league_id});
      } else {
        $state.go('league', {id: vm.subjectTeamInfo.id, leagueId: vm.subjectTeamInfo.league_id});
      }
    }

    vm.onNewGameClick = function () {
      $state.go(vm.statesPrefix + '-newGame');
    }
  }
};