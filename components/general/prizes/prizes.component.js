import template from './prizes.component.tpl.html';

export default {
  bindings: {
    enabledPrizesCategories: '<',
    hasContainerClass: '<',
    competitionInfo: '<'
  },
  template,
  controller: function ($rootScope, gettextCatalog, FACms) {
    var vm = this;

    $rootScope.title = gettextCatalog.getString('Prizes');

    vm.activeTab = 1;
    vm.setActiveTab = setActiveTab;
    vm.prizesEnabled = vm.enabledPrizesCategories || ['general', 'week', 'peloton', 'peloton-week', 'sprint', 'mountain'];
    vm.prizesMap = {};
    vm.loaded = false;

    vm.footballFeeds = ['GWC', 'G11', 'CL'];
    vm.customLeagueLabel = gettextCatalog.getString('Cycling league');
    vm.customWeekLeagueLabel = gettextCatalog.getString('Peloton Week');

    vm.sprintLabel = 'Sprint';
    vm.bergLabel = 'Berg';
    vm.dedubbelLabel = 'De Dubbel';

    if(vm.competitionInfo.competitionFeed === 'GCX') {
      vm.bergLabel = gettextCatalog.getString('Superprestige');
      vm.sprintLabel = gettextCatalog.getString('Wereldbeker');
    }

    if (vm.footballFeeds.indexOf(vm.competitionInfo.competitionFeed) !== -1) {
      vm.customLeagueLabel = gettextCatalog.getString('Football league');
      vm.customWeekLeagueLabel = gettextCatalog.getString('Superklasse Week');
    }

    activate();

    function activate() {
      return FACms.getPrizes(vm.prizesEnabled, vm.competitionInfo.competitionFeed)
        .then(result => {
          vm.prizesMap = result;
          vm.loaded = true;
        })
    }

    function setActiveTab(tabNumber) {
      vm.activeTab = tabNumber;
    }
  }
};