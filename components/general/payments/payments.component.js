import template from './payments.component.tpl.html';

export default {
  bindings: {
    competitionFeed: '<',
    seasonId: '<',
    paymentProvider: '<',
    hasContainerClass: '<',
    statesPrefix: '<',
    ogonePaymentURL: '<',
    deadlineWeeks: '<',
    promoWeeks: '<',
    endWeek: '<'
  },
  template,
  controller: function (teamService, userService, $window, $rootScope, $state, growl, gettextCatalog, Hashids,
    leagueService, $location, $sce, $stateParams, analyticsService) {
    var vm = this;
    vm.getUnpaidTeams = getUnpaidTeams;
    vm.payTeams = payTeams;
    vm.removeTeam = removeTeam;
    vm.addCoupon = addCoupon;
    vm.removeCoupon = removeCoupon;
    vm.getTeamPoints = getTeamPoints;
    vm.getTeamRank = getTeamRank;
    vm.editTeam = editTeam;
    vm.goToLeague = goToLeague;
    vm.updateTeam = updateTeam;
    vm.cancelEditTeam = cancelEditTeam;
    vm.validateCoupon = validateCoupon;
    vm.couponValid = couponValid;
    vm.cloneTeam = cloneTeam;
    vm.onKeyPress = onKeyPress;
    vm.coupons = [];
    vm.getCouponsValue = getCouponsValue;
    vm.couponValidationClass = couponValidationClass;
    $rootScope.title = gettextCatalog.getString("Dashboard");
    vm.editTeams = {};
    vm.validCoupons = [];
    vm.centsForATeam = 900;
    vm.paymentIntegration = vm.paymentProvider || 'mollie';
    vm.ogoneTransaction = null;
    vm.mollieTransaction = null;
    vm.getPayingTeamNames = getPayingTeamNames;
    vm.disableOgoneTransaction = disableOgoneTransaction;
    vm.selectedTeamsForPaying = selectedTeamsForPaying;
    vm.goToTeam = goToTeam;
    vm.cancelPayment = cancelPayment;
    vm.checkTeamName = checkTeamName;
    vm.goToNewTeam = goToNewTeam;
    vm.paymentStates = {
      accept: $location.absUrl() + '?paymentResultCode=ACCEPT',
      decline: $location.absUrl() + '?paymentResultCode=DECLINE',
      exception: $location.absUrl() + '?paymentResultCode=EXCEPTION',
      cancel: $location.absUrl() + '?paymentResultCode=CANCEL',
    };
    vm.ogoneFormURL = $sce.trustAsResourceUrl(vm.ogonePaymentURL);
    vm.totalForPayment = totalForPayment;
    vm.togglePay = togglePay;
    vm.getTotalWithPromo = getTotalWithPromo;
    vm.removeTeamConfirmationMessage = gettextCatalog.getString('Are you sure that you want to remove');
    vm.cloneTeamConfirmationMessage = gettextCatalog.getString('Are you sure that you want to clone');
    vm.cancelPaymentMessage = gettextCatalog.getString('Are you sure that you want cancel payment for teams: ');

    vm.multipleRestrictionLimitMessage = gettextCatalog.getString('You can only use one coupon of this type and you cannot combine this with other coupons.');
    vm.multipleRestrictionTeamLimiMessage = gettextCatalog.getString('Note: In order to use this coupon, you must select at least 2 teams in the payment.');

    vm.couponLabel = gettextCatalog.getString('Couponcode');
    vm.subjectTeamInfo = $stateParams.subjectTeamInfo;
    vm.customLeagueLabel = gettextCatalog.getString('Cycling league');
    vm.footballFeeds = ['GWC', 'G11', 'CL'];
    vm.getTeamName = getTeamName;
    vm.myPayments = [];
    vm.activePaymentsList = [];
    vm.cancelPaymentTriggered = false;
    vm.activePaymentsInfo = {};
    vm.getPaymentStatus = getPaymentStatus;
    vm.getLocalDate = getLocalDate;

    if (vm.footballFeeds.indexOf(vm.competitionFeed) !== -1) {
      vm.customLeagueLabel = gettextCatalog.getString('Football league');
      vm.centsForATeam = 700;
    }

    const promoWeeksIds = vm.promoWeeks || [];

    vm.showPromo = promoWeeksIds.indexOf(vm.deadlineWeeks.deadline_week) !== -1;
    vm.showPayment = vm.deadlineWeeks.deadline_week < vm.endWeek && vm.deadlineWeeks.display_week <= vm.deadlineWeeks.deadline_week;

    activate();

    function getTeams() {
      return teamService.getAll()
        .then(function (teams) {
          vm.teams = teams
            .filter(filterCycling)
            .filter(team => !parseInt(team.Team.paid) && !team.Team.payment_id)
            .map(team => Object.assign({}, team, { pay: true }));

          vm.pendingPaymentTeams = teams
            .filter(filterCycling)
            .filter(team => !parseInt(team.Team.paid) && team.Team.payment_id);

          const activePaymentsMap = {};

          vm.pendingPaymentTeams.forEach(team => {
            const hasActivePayment = activePaymentsMap[team.Team.payment_id];

            if (hasActivePayment) {
              activePaymentsMap[team.Team.payment_id].push(team.Team);
            } else {
              activePaymentsMap[team.Team.payment_id] = [team.Team];
            }
          });

          vm.activePaymentsList = Object.keys(activePaymentsMap)
            .map(paymentId => Object.assign({}, { paymentId }, { teams: activePaymentsMap[paymentId] }));

          vm.totalToPayWithCoupons = totalForPayment(true);

          $location.search({});
          vm.cancelPaymentTriggered = false;
        });
    }

    function togglePay(teamId) {
      const toggledTeam = vm.teams.find(team => team.Team.id === teamId);
      vm.totalToPayWithCoupons = totalForPayment(true);

      if (toggledTeam && !toggledTeam.pay) {
        vm.coupons.forEach((item, index) => vm.removeCoupon(index));
      }
    }

    function goToTeam(team) {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-squadTeam', { id: team.Team.id });
      } else {
        $state.go('squadTeam', { id: team.Team.id });
      }
    }

    function goToNewTeam() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-newGame');
      } else {
        $state.go('newGame');
      }
    }

    function disableOgoneTransaction() {
      vm.ogoneTransaction = null;
    }

    function selectedTeamsForPaying() {
      return vm.teams.filter(payingTeam => payingTeam.pay).length;
    }

    function getPayingTeamNames() {
      return vm.getUnpaidTeams().map(team => team.Team.name).join(', ');
    }

    function activate() {
      const queryParams = $location.search();

      if (queryParams && queryParams.paymentResultCode) {
        const cleanKeys = Object.keys(queryParams).filter(key => key !== 'paymentResultCode');
        const ogoneCallbackPayload = {};

        cleanKeys.forEach(cleanKey => {
          ogoneCallbackPayload[cleanKey] = queryParams[cleanKey];
        })

        userService.ogone(vm.competitionFeed, ogoneCallbackPayload)
          .then(getTeams);
      } else {
        getTeams();
      }
    }

    function filterCycling(team) {
      return team['League'].competition_feed === vm.competitionFeed ||
        team['Team'].competition_feed === vm.competitionFeed;
    }

    function getUnpaidTeams() {
      return vm.teams.filter(function (team) {
        return !parseInt(team.Team.paid) && team.pay;
      });
    }

    function getCouponsValue() {
      var totalCents = 0;
      vm.validCoupons.forEach(function (coupon) {
        totalCents += parseInt(coupon.Coupon.value);
      });
      return totalCents / 100;
    }

    function payTeams(customPaymentProvider) {
      var amount = totalForPayment() * 100;
      var coupons = [];
      const unpaidTeams = getUnpaidTeams();
      var teamIds = unpaidTeams.map(team => team.Team.id);
      var requestedProvider = customPaymentProvider || vm.paymentIntegration;
      var locale = 'nl_BE';

      if (gettextCatalog.currentLanguage === 'fr') {
        locale = 'fr_BE';
      }

      const hasAddedMultipleRestrictedCoupons = vm.validCoupons.find(item => item.Coupon.restriction === 'multiple_restricted');
      if (hasAddedMultipleRestrictedCoupons && teamIds.length < 2) {
        growl.addErrorMessage(vm.multipleRestrictionTeamLimiMessage);
        return;
      }

      vm.validCoupons.forEach(function (coupon) {
        coupons.push(coupon.Coupon.code);
      });

      return userService
        .pay(coupons, vm.competitionFeed, vm.seasonId, teamIds, $location.absUrl(), locale)
        .then(function (result) {
          if (result.message) {
            growl.addSuccessMessage(gettextCatalog.getString());
          }

          const ecommerceInfo = {
            'ecommerce': {
              'purchase': {
                'actionField': {
                  'id': result.paymentId,
                  'affiliation': '',
                  'revenue': result.order.amount,
                  'tax': '0',
                  'shipping': '0',
                  'coupon': coupons.join('')
                },
                'products': unpaidTeams.map(team => {
                  return {
                    id: team.Team.id,
                    name: team.Team.name,
                    price: team.Team.price,
                    brand: vm.statesPrefix || '',
                    category: 'team',
                    variant: '',
                    quantity: 1,
                  }
                })
              }
            }
          };

          analyticsService.addToDataLayer(ecommerceInfo);

          if (result.provider && result.provider.mollie && result.provider.mollie._links && result.provider.mollie._links.checkout) {
            if ($window.fbq && (vm.totalToPayWithCoupons > 0)) {
              $window.fbq('track', 'InitiateCheckout');
              $window.fbq('track', 'Purchase', { 'value': vm.totalToPayWithCoupons, 'currency': 'EUR' });
            }
            $window.location.href = result.provider.mollie._links.checkout.href;
          } else {
            $rootScope.$broadcast('sidemenu:refresh');
            vm.validCoupons = [];
            vm.coupons = [];
            activate();
          }
        })
        .catch(function (error) {
          growl.addErrorMessage(error.message);
        });
    }

    function removeTeam(team) {
      teamService.delete(team.Team.id).then(function (res) {
        growl.addSuccessMessage(gettextCatalog.getString('Successfully deleted!'));
        $rootScope.$broadcast('sidemenu:refresh');
        activate();
      });
    }

    function onKeyPress(event, teamId) {
      if (event.keyCode === 13) {
        updateTeam(vm.editTeams[teamId]);
      }
    }

    function addCoupon() {
      var couponsLength = vm.coupons.length;

      const hasAddedMultipleRestrictedCoupons = vm.validCoupons.find(item => item.Coupon.restriction === 'multiple_restricted');

      if (hasAddedMultipleRestrictedCoupons) {
        growl.addErrorMessage(vm.multipleRestrictionLimitMessage);
        return;
      }

      const teamsToPay = vm.getUnpaidTeams();
      const teamsTotalPrice = teamsToPay.reduce((price, item) => item.Team.price + price, 0);

      var toBePaid = teamsTotalPrice - vm.getCouponsValue();
      var previousProvidedCoupon = vm.coupons[couponsLength - 1];
      var isPreviousValid = vm.validCoupons.find(function (coupon) {
        return previousProvidedCoupon === coupon.Coupon.code;
      });

      const generalCoupons = vm.validCoupons.filter(coupon => coupon && coupon.Coupon && coupon.Coupon.type === 'general');

      if (generalCoupons.length === teamsToPay.length) {
        growl.addErrorMessage(gettextCatalog.getString('You cannot combine general coupons with other coupon types for paying one team'));
        return;
      }

      if (couponsLength === 5) {
        return;
      }

      if (couponsLength > 0 && !isPreviousValid) {
        growl.addErrorMessage(gettextCatalog.getString('You must provide a valid coupon at a time!'));
        return;
      }

      if (toBePaid <= 0) {
        growl.addErrorMessage(gettextCatalog.getString('No need to provide a coupon!'));
        return;
      }

      vm.coupons.push('');
    }

    function removeCoupon(couponIndex) {
      vm.validCoupons = vm.validCoupons.filter(function (coupon) {
        return coupon.Coupon.code !== vm.coupons[couponIndex];
      });
      vm.coupons.splice(couponIndex, 1);
      vm.totalToPayWithCoupons = totalForPayment(true);
    }

    function getTeamPoints(team) {
      var points = team.Team.points; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].points;
      if (!points) {
        return 0;
      }
      return points;
    }

    function getTeamRank(team) {
      var rank = team.Team.rank; //team.Weekstat && team.Weekstat[0] && team.Weekstat[0].rank;
      if (!rank) {
        return '-';
      }
      return rank;
    }

    function editTeam(team) {
      vm.editTeams[team.Team.id] = Object.assign({}, team.Team, { inviteURL: '' });
    }

    function checkTeamName(teamName) {
      if (teamName[teamName.length - 1] === ',') return teamName.substr(0, teamName.length - 1);
      return teamName;
    }

    function cancelEditTeam(team) {
      vm.editTeams[team.id] = null;
    }

    function updateTeam(team) {
      teamService.edit(team)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Team successfully modified!'));
          $rootScope.$broadcast('sidemenu:refresh');
          cancelEditTeam(team);
          activate();
        });
    }

    function onCorrectCouponModified(correctCouponCode) {
      vm.validCoupons = vm.validCoupons.filter(function (coupon) {
        return coupon.Coupon.code !== correctCouponCode;
      });
      vm.totalToPayWithCoupons = totalForPayment(true);
    }

    function validateCoupon(couponIndex, oldValue) {
      if (vm.coupons[couponIndex] && (vm.coupons[couponIndex].length > 5) && (vm.coupons[couponIndex].length <= 15)) {
        userService.validateCoupon(vm.coupons[couponIndex], vm.competitionFeed)
          .then(function (response) {
            const result = {
              coupon: {
                Coupon: response.coupon
              }
            }
            var alreadyUsed = vm.validCoupons.find(function (coupon) {
              return coupon.Coupon.code === result.coupon.Coupon.code;
            });

            const competitionTypeCouponsValidatedSoFar = vm.validCoupons.filter(item => item.Coupon.restriction === 'competition');
            if (result.coupon.Coupon.restriction === 'competition' && competitionTypeCouponsValidatedSoFar.length) {
              return;
            }

            const allreadyUsedCoupons = vm.validCoupons.length;
            const teamsToPay = vm.getUnpaidTeams();

            if (result.coupon.Coupon.type === 'general' && (teamsToPay.length === allreadyUsedCoupons)) {
              growl.addErrorMessage(gettextCatalog.getString('You cannot combine general coupons with other coupon types for paying one team'));
              return;
            }

            const hasAddedMultipleRestrictedCoupons = vm.validCoupons.find(item => item.Coupon.restriction === 'multiple_restricted');
            const couponsIsMultipleRestrictedAndHasCouponsAdded = (result.coupon.Coupon && result.coupon.Coupon.restriction === 'multiple_restricted') && !!vm.validCoupons.length;

            if (hasAddedMultipleRestrictedCoupons || couponsIsMultipleRestrictedAndHasCouponsAdded) {
              growl.addErrorMessage(vm.multipleRestrictionLimitMessage);
              return;
            }

            if (
              (
                (result.coupon.Coupon.competitionFeed && (result.coupon.Coupon.competitionFeed === vm.competitionFeed))
                || !result.coupon.Coupon.competitionFeed
              ) && result.coupon && !alreadyUsed) {
              vm.validCoupons.push(result.coupon);
              vm.totalToPayWithCoupons = totalForPayment(true);
            }
          })
          .catch(error => {
            onCorrectCouponModified(oldValue);
          })
      } else if (oldValue && oldValue.length > 5 && (!vm.coupons[couponIndex] || (vm.coupons[couponIndex] && vm.coupons[couponIndex].length < oldValue.length))) {
        onCorrectCouponModified(oldValue);
      }
    }

    function couponValid(couponIndex) {
      var couponValue = vm.coupons[couponIndex];
      if (vm.coupons[couponIndex] && vm.coupons[couponIndex].length < 7) {
        return true;
      }
      return !!vm.validCoupons.find(function (coupon) {
        return coupon.Coupon.code === couponValue;
      });
    }

    function couponValidationClass(couponIndex) {
      var couponValue = vm.coupons[couponIndex];
      var couponValid = !!vm.validCoupons.find(function (coupon) {
        return coupon.Coupon.code === couponValue;
      });

      if (couponValue.length < 7) {
        return '';
      } else {
        return couponValid ? 'fa fa-check-circle-o text-success' : 'fa fa-exclamation-triangle text-danger';
      }
    }

    function cloneTeam(team) {
      teamService.clone(team.Team.id, vm.competitionFeed)
        .then(function () {
          growl.addSuccessMessage(gettextCatalog.getString('Successfully copied!'));
          $rootScope.$broadcast('sidemenu:refresh');
          activate();
        });
    }

    function totalForPayment(withCoupons) {
      const teamsToPay = vm.getUnpaidTeams();
      const teamsTotalPrice = teamsToPay.reduce((price, item) => item.Team.price + price, 0);

      if (withCoupons) {
        return teamsTotalPrice - vm.getCouponsValue();
      } else {
        return teamsTotalPrice;
      }
    }

    function getTeamsPaidWithCoupons() {
      var totalCents = 0;
      vm.validCoupons.forEach(function (coupon) {
        totalCents += parseInt(coupon.Coupon.value);
      });
      return totalCents ? (totalCents / vm.centsForATeam) : 0;
    }

    function getTotal() {
      return (getUnpaidTeams().length - getTeamsPaidWithCoupons()) * 9;
    }

    function getDiscount() {
      return Math.floor((getUnpaidTeams().length - getTeamsPaidWithCoupons()) / 2) * 9;
    }

    function getTotalWithPromo() {
      var total = getTotal();
      var discount = getDiscount();
      return total - discount;
    }

    function goToLeague() {
      if (vm.statesPrefix) {
        $state.go(vm.statesPrefix + '-league', { id: vm.subjectTeamInfo.id, leagueId: vm.subjectTeamInfo.league_id });
      } else {
        $state.go('league', { id: vm.subjectTeamInfo.id, leagueId: vm.subjectTeamInfo.league_id });
      }
    }

    function getTeamName(team) {
      return team.name;
    }

    function cancelPayment(paymentId) {
      vm.cancelPaymentTriggered = true;
      return userService.cancelPayment(paymentId)
        .then(() => {
          $rootScope.$broadcast('sidemenu:refresh');
          vm.validCoupons = [];
          vm.coupons = [];
          activate();
        })
        .catch(() => {
          vm.cancelPaymentTriggered = false;
        });
    }

    function getPaymentStatus(paymentId) {
      return userService.paymentStatus(paymentId)
        .then((data) => {
          vm.activePaymentsInfo[paymentId] = data;
        })
    }

    function getLocalDate(utcDate) {
      return moment.utc(utcDate).local().format('ddd DD MMMM HH:mm:ss');
    }
  }
};