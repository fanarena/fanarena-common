import Prizes from './prizes/prizes.component';
import Rankings from './rankings/rankings.component';
import Rules from './rules/rules.component';
import BudgetLeagueTable from './budget-league-table/budget-league-table.component';
import Dashboard from './dashboard/dashboard.component';
import Payments from './payments/payments.component';

export {
  Prizes,
  Dashboard,
  Rankings,
  Rules,
  BudgetLeagueTable,
  Payments
};