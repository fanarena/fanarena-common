export default angular.module('app.common.controllers', [])
  .controller('faPopoverController', function ($scope, faHelpService) {
    var isOpen = false;

    $scope.openPopup = function () {
      faHelpService.activateHelp();
      isOpen = true;
    };

    $scope.closePopup = function (event) {
      faHelpService.deactivateHelp();
      event.stopPropagation();
      isOpen = false;
    };

    $scope.isOpen = function () {
      return isOpen;
    };

    $scope.content = function () {
      return $scope.help.selected.html(faHelpService.getPlaceholderData());
    }
  })
  .controller('FaConfirmationDialogCtrl', function ($uibModalInstance, body, title) {
    var vm = this;
    vm.title = title;
    vm.body = body;

    vm.ok = function () {
      $uibModalInstance.close();
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  })

  .controller('faPopupController', function ($scope, $uibModalInstance, module, faHelpService) {
    $scope.helpSettings = faHelpService.getGeneralHelpContent();
    $scope.module = module;

    faHelpService.activateHelp();
    var pageIndex = 0;

    $scope.closePopup = function () {
      faHelpService.deactivateHelp();
      $uibModalInstance.dismiss('cancel');
    };

    $scope.onDisableHelp = function () {
      faHelpService.updateGeneralHelpContentSettings($scope.helpSettings);
    };

    $scope.content = function () {
      return $scope.module.html(faHelpService.getPlaceholderData());
    };

    $scope.hasPagination = function () {
      return angular.isArray($scope.module.html);
    };

    $scope.previousPage = function () {
      if (pageIndex > 0) {
        pageIndex--;
      }
    };

    $scope.nextPage = function () {
      if (pageIndex < ($scope.module.html.length - 1)) {
        pageIndex++;
      }
    };

    $scope.currentPageNumber = function () {
      return (pageIndex + 1) + "/" + $scope.module.html.length;
    };

    $scope.currentPageContent = function () {
      return $scope.module.html[pageIndex](faHelpService.getPlaceholderData());
    }
  });
